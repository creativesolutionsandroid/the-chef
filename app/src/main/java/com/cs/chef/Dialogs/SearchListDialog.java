package com.cs.chef.Dialogs;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cs.chef.Constants;
import com.cs.chef.Models.StoresList;
import com.cs.chef.R;

import java.util.ArrayList;

//import com.cs.chef.Adapters.SearchDialogAdapter;

//import static com.cs.chef.Adapters.SearchmenuListAdapter.TAG;

public class SearchListDialog extends BottomSheetDialogFragment {
    TextView storename,summarytext ;
    ImageView storelogo;
    View rootView;;
    RecyclerView listView;
//    private SearchDialogAdapter mstorediolgadapter;
    private ArrayList<StoresList.StoresDetails> mStorelist = new ArrayList<>();
    int pos = 0;

 public static SearchListDialog newInstance() {
        return new SearchListDialog();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.storesview_dialog1, container, false);
        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        mStorelist = (ArrayList<StoresList.StoresDetails>) getArguments().getSerializable("array1");
        pos = getArguments().getInt("pos1", 0);
        listView = (RecyclerView)rootView.findViewById(R.id.stores_list_dialog) ;
        storename = (TextView) rootView.findViewById(R.id.storename);
        summarytext = (TextView) rootView.findViewById(R.id.subtext);
        storelogo =(ImageView)rootView.findViewById(R.id.storelogo);
        storename.setText(mStorelist.get(pos).getBrandName_En());
        summarytext.setText(mStorelist.size()+" outlets near by you");
        Log.d("TAG", "onCreateView"+mStorelist);

        Glide.with(getContext())
                .load(Constants.IMAGE_URL+mStorelist.get(pos).getStoreImage_En())
                .into(storelogo);
//        Collections.sort(mStorelist.get(pos).getBrands(), Brands.distanceSort);
//        mstorediolgadapter = new SearchDialogAdapter(getContext(), mStorelist, pos, getActivity());
//        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
//        listView.setLayoutManager(layoutManager);
//        listView.addItemDecoration(new DividerItemDecoration(listView.getContext(), DividerItemDecoration.HORIZONTAL));
//        listView.setAdapter(mstorediolgadapter);
        return rootView;
    }
    @Override
    public void onActivityCreated(Bundle arg0) {
        super.onActivityCreated(arg0);
//        getDialog().getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
    }
    private void setTypeface(){
        Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(),
                "helvetica.ttf");
        storename.setTypeface(typeface);
        summarytext.setTypeface(typeface);
    }
    }

