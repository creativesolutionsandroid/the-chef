package com.cs.chef.Dialogs;

import android.app.AlertDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.chef.Activities.ForgotPasswordActivity;
import com.cs.chef.Activities.SignUpActivity;
import com.cs.chef.Activities.SplashScreenActivity;
import com.cs.chef.Constants;
import com.cs.chef.Models.UserRegistrationResponse;
import com.cs.chef.Models.VerifyMobileResponse;
import com.cs.chef.R;
import com.cs.chef.Rest.APIInterface;
import com.cs.chef.Rest.ApiClient;
import com.cs.chef.Utils.NetworkUtil;
import com.mukesh.OtpListener;
import com.mukesh.OtpView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

//import com.cs.chef.Activities.ForgotPasswordActivity;
//import com.cs.chef.Activities.SignUpActivity;

public class VerifyOtpDialog extends BottomSheetDialogFragment implements View.OnClickListener {

    TextView textMobileNumber, textOtpTitle, textBody1, textBody2, textBody3 ;
    String otpEntered="", strName, strEmail, strMobile, strPassword, serverOtp;
    Button buttonResend, buttonVerify;
    ImageView imgEditMobile;
    String language;
    OtpView otpView;
    View rootView;
    private static String TAG = "TAG";
    CountDownTimer countDownTimer;
    SharedPreferences userPrefs;
    SharedPreferences LanguagePrefs;
    SharedPreferences.Editor userPrefsEditor;

    SharedPreferences languagePrefs;

    public static VerifyOtpDialog newInstance() {
        return new VerifyOtpDialog();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        languagePrefs = getActivity().getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            rootView = inflater.inflate(R.layout.dialog_verify_otp, container, false);
        } else {
            rootView = inflater.inflate(R.layout.dialog_verify_otp_arabic, container, false);
        }
        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        userPrefs = getContext().getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefsEditor = userPrefs.edit();

        textMobileNumber = (TextView) rootView.findViewById(R.id.otp_mobile_number);
        imgEditMobile = (ImageView) rootView.findViewById(R.id.edit_mobile_number);
        textOtpTitle = (TextView) rootView.findViewById(R.id.otp_title);
        textBody1 = (TextView) rootView.findViewById(R.id.otp_body1);
        textBody2 = (TextView) rootView.findViewById(R.id.otp_body2);
        textBody3 = (TextView) rootView.findViewById(R.id.otp_body3);
        otpView = (OtpView) rootView.findViewById(R.id.otp_view);

        buttonVerify = (Button) rootView.findViewById(R.id.button_verify_otp);
        buttonResend = (Button) rootView.findViewById(R.id.button_resend_otp);

        strName = getArguments().getString("name");
        strEmail = getArguments().getString("email");
        strMobile = getArguments().getString("mobile");
        strPassword = getArguments().getString("password");
        serverOtp = getArguments().getString("otp");
        textMobileNumber.setText(Constants.Country_Code+strMobile);

        buttonResend.setEnabled(false);
        buttonResend.setAlpha(0.5f);
        setTypeface();
        setTimerForResend();

        otpView.setListener(new OtpListener() {
            @Override
            public void onOtpEntered(String otp) {
                otpEntered = otp;
            }
        });

        buttonVerify.setOnClickListener(this);
        buttonResend.setOnClickListener(this);
        imgEditMobile.setOnClickListener(this);

        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle arg0) {
        super.onActivityCreated(arg0);
//        getDialog().getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
    }

    private void setTypeface(){
        Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(),
                "helvetica.ttf");

        textBody1.setTypeface(typeface);
        textBody2.setTypeface(typeface);
        textBody3.setTypeface(typeface);
        textOtpTitle.setTypeface(typeface);
        buttonResend.setTypeface(typeface);
        buttonVerify.setTypeface(typeface);
        textMobileNumber.setTypeface(typeface);
    }

    private void setTimerForResend(){
        countDownTimer = new CountDownTimer(120000, 1000) {

            public void onTick(long millisUntilFinished) {
                String timeRemaining = String.format("%02d:%02d",
                        TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) % TimeUnit.HOURS.toMinutes(1),
                        TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) % TimeUnit.MINUTES.toSeconds(1));
//                Log.i(TAG, "onTick: "+timeRemaining);
                if(getDialog()!=null) {
                    if (language.equalsIgnoreCase("En")) {
                        buttonResend.setText(getResources().getString(R.string.otp_msg_resend) + " in " + timeRemaining);
                    } else {
                        buttonResend.setText( timeRemaining + " في " +  getResources().getString(R.string.otp_msg_resend_ar));
                    }
                }
            }

            public void onFinish() {
                if(getDialog()!=null) {
                    if (language.equalsIgnoreCase("En")) {
                        buttonResend.setText(getResources().getString(R.string.otp_msg_resend));
                    } else {
                        buttonResend.setText(getResources().getString(R.string.otp_msg_resend_ar));
                    }
                    buttonResend.setEnabled(true);
                    buttonResend.setAlpha(1.0f);
                }
            }

        }.start();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.button_verify_otp:
                if(otpEntered.length() != 4){
                    if (language.equalsIgnoreCase("En")) {
                        Constants.showOneButtonAlertDialog(getResources().getString(R.string.otp_alert1),
                                getResources().getString(R.string.alert_invalid_otp), getResources().getString(R.string.ok), getActivity());
                    } else {
                        Constants.showOneButtonAlertDialog(getResources().getString(R.string.otp_alert1_ar),
                                getResources().getString(R.string.alert_invalid_otp_ar), getResources().getString(R.string.ok_ar), getActivity());
                    }
                }
                else if(!serverOtp.equals(otpEntered)){
                    if (language.equalsIgnoreCase("En")) {
                        Constants.showOneButtonAlertDialog(getResources().getString(R.string.otp_alert1),
                                getResources().getString(R.string.alert_invalid_otp), getResources().getString(R.string.ok), getActivity());
                    } else {
                        Constants.showOneButtonAlertDialog(getResources().getString(R.string.otp_alert1_ar),
                                getResources().getString(R.string.alert_invalid_otp_ar), getResources().getString(R.string.ok_ar), getActivity());
                    }
                }
                else{
                    Fragment forgot=getFragmentManager().findFragmentByTag("forgot");
                    if(forgot!=null && forgot.isVisible())
                    {
                        ForgotPasswordActivity.isOTPSuccessful = true;
                        getDialog().cancel();
                    }

                    Fragment register=getFragmentManager().findFragmentByTag("register");
                    if(register!=null && register.isVisible())
                    {
                        String networkStatus = NetworkUtil.getConnectivityStatusString(getContext());
                        if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                            new userRegistrationApi().execute();
                        }
                        else{
                            if (language.equalsIgnoreCase("En")) {
                                Toast.makeText(getContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(getContext(), R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                            }
                        }
                    }

                }
                break;

            case R.id.edit_mobile_number:
                SignUpActivity.isOTPVerified = false;
                getDialog().cancel();
                break;

            case R.id.button_resend_otp:
                String networkStatus = NetworkUtil.getConnectivityStatusString(getContext());
                if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                    new verifyMobileApi().execute();
                }
                else{
                    if (language.equalsIgnoreCase("En")) {
                        Toast.makeText(getContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getContext(), R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                    }
                }
                break;
        }
    }

    private String prepareVerifyMobileJson(){
        JSONObject parentObj = new JSONObject();
        JSONObject mobileObj = new JSONObject();
        try {
            mobileObj.put("Mobile","966"+strMobile);
            parentObj.put("VerifyMobile",mobileObj);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return parentObj.toString();
    }

    private String prepareSignUpJson(){
        JSONObject parentObj = new JSONObject();
        JSONObject userDetailsObj = new JSONObject();
        JSONObject userAuthObj = new JSONObject();

        try {
            userDetailsObj.put("UserId", "0");
            userDetailsObj.put("FullName", strName);
            userDetailsObj.put("FamilyName","");
            userDetailsObj.put("NickName","");
            userDetailsObj.put("Gender","");
            userDetailsObj.put("Mobile","966"+strMobile);
            userDetailsObj.put("Email", strEmail);
            userDetailsObj.put("Password", strPassword);
            userDetailsObj.put("Language", language);
            userDetailsObj.put("DeviceType", Constants.getDeviceType(getContext()));
            userDetailsObj.put("UserType", Constants.UserType);
            userAuthObj.put("DeviceToken", SplashScreenActivity.regId);

            parentObj.put("UserDetails", userDetailsObj);
            parentObj.put("UserAuthActivity",userAuthObj);
            Log.d(TAG, "prepareSignUpJson: "+parentObj.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return parentObj.toString();
    }

    private class userRegistrationApi extends AsyncTask<String, String, String> {

//        ACProgressFlower dialog;
        AlertDialog loaderDialog = null;
        String inputStr;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareSignUpJson();
//            dialog = new ACProgressFlower.Builder(getContext())
//                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
//                    .themeColor(Color.WHITE)
//                    .fadeColor(Color.DKGRAY).build();
//            dialog.show();
            Constants.showLoadingDialog(getActivity());
//            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
//            // ...Irrelevant code for customizing the buttons and title
//            LayoutInflater inflater = getLayoutInflater();
//            int layout = R.layout.loder_dialog;
//            View dialogView = inflater.inflate(layout, null);
//            dialogBuilder.setView(dialogView);
//            dialogBuilder.setCancelable(true);
//
//            loaderDialog = dialogBuilder.create();
//            loaderDialog.show();
//            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
//            Window window = loaderDialog.getWindow();
//            window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//            lp.copyFrom(window.getAttributes());
//            //This makes the dialog take up the full width
//            Display display = getActivity().getWindowManager().getDefaultDisplay();
//            Point size = new Point();
//            display.getSize(size);
//            int screenWidth = size.x;
//
//            double d = screenWidth*0.85;
//            lp.width = (int) d;
//            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
//            window.setAttributes(lp);
        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<UserRegistrationResponse> call = apiService.userRegistration(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<UserRegistrationResponse>() {
                @Override
                public void onResponse(Call<UserRegistrationResponse> call, Response<UserRegistrationResponse> response) {
                    if(response.isSuccessful()){
                        UserRegistrationResponse registrationResponse = response.body();
                        if(registrationResponse.getStatus()){
//                          status true case
                            String userId = registrationResponse.getData().getUserid();
                            userPrefsEditor.putString("userId", userId);
                            userPrefsEditor.putString("name", registrationResponse.getData().getName());
                            userPrefsEditor.putString("email", registrationResponse.getData().getEmail());
                            userPrefsEditor.putString("mobile", registrationResponse.getData().getMobile());
                            userPrefsEditor.commit();
                            SignUpActivity.isOTPVerified = true;
                            getDialog().dismiss();
                        }
                        else {
//                          status false case
                            if (language.equalsIgnoreCase("En")) {
                                String failureResponse = registrationResponse.getMessage();
                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name),
                                        getResources().getString(R.string.ok), getActivity());
                            } else {
                                String failureResponse = registrationResponse.getMessageAr();
                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name_ar),
                                        getResources().getString(R.string.ok_ar), getActivity());
                            }
                        }
                    }
                    else{
                        if(networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                            if (language.equalsIgnoreCase("En")) {
                                Toast.makeText(getActivity(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(getActivity(), R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                            }
                        }
                        else {
                            if (language.equalsIgnoreCase("En")) {
                                Toast.makeText(getActivity(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(getActivity(), R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                            }
                        }
                    }

                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<UserRegistrationResponse> call, Throwable t) {
                    if(networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(getActivity(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getActivity(), R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }
                    }
                    else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(getActivity(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getActivity(), R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }

                    Constants.closeLoadingDialog();
                }
            });
            return null;
        }
    }

    private class verifyMobileApi extends AsyncTask<String, String, String> {
//        ACProgressFlower dialog;
        String inputStr;
//        AlertDialog loaderDialog = null;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareVerifyMobileJson();
//            dialog = new ACProgressFlower.Builder(getContext())
//                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
//                    .themeColor(Color.WHITE)
//                    .fadeColor(Color.DKGRAY).build();
//            dialog.show();
            Constants.showLoadingDialog(getActivity());
//            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
//             ...Irrelevant code for customizing the buttons and title
//            LayoutInflater inflater = getLayoutInflater();
//            int layout = R.layout.loder_dialog;
//            View dialogView = inflater.inflate(layout, null);
//            dialogBuilder.setView(dialogView);
//            dialogBuilder.setCancelable(true);
//
//            loaderDialog = dialogBuilder.create();
//            loaderDialog.show();
//            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
//            Window window = loaderDialog.getWindow();
//            window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//            lp.copyFrom(window.getAttributes());
            //This makes the dialog take up the full width
//            Display display = getActivity().getWindowManager().getDefaultDisplay();
//            Point size = new Point();
//            display.getSize(size);
//            int screenWidth = size.x;

//            double d = screenWidth*0.85;
//            lp.width = (int) d;
//            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
//            window.setAttributes(lp);
        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<VerifyMobileResponse> call = apiService.verfiyMobileNumber(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<VerifyMobileResponse>() {
                @Override
                public void onResponse(Call<VerifyMobileResponse> call, Response<VerifyMobileResponse> response) {
                    if(response.isSuccessful()){
                        VerifyMobileResponse verifyMobileResponse = response.body();
                        try {
                            if(verifyMobileResponse.getStatus()){
                                serverOtp = verifyMobileResponse.getData().getOtp();
                                Log.i(TAG, "onResponse: "+serverOtp);
                            }
                            else {
                                if (language.equalsIgnoreCase("En")) {
                                    String failureResponse = verifyMobileResponse.getMessage();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name),
                                            getResources().getString(R.string.ok), getActivity());
                                } else {
                                    String failureResponse = verifyMobileResponse.getMessageAr();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name_ar),
                                            getResources().getString(R.string.ok_ar), getActivity());
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            if (language.equalsIgnoreCase("En")) {
                                Toast.makeText(getActivity(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(getActivity(), R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                    else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(getActivity(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getActivity(), R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }

                    Constants.closeLoadingDialog();
                    buttonResend.setEnabled(false);
                    buttonResend.setAlpha(0.5f);
                    setTimerForResend();
                }

                @Override
                public void onFailure(Call<VerifyMobileResponse> call, Throwable t) {
                    if(networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(getActivity(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getActivity(), R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }
                    }
                    else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(getActivity(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getActivity(), R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }

                    Constants.closeLoadingDialog();
                }
            });
            return null;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(countDownTimer!=null){
            countDownTimer.cancel();
        }
    }
}
