package com.cs.chef;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.cs.chef.Models.Order;

import java.util.ArrayList;
import java.util.HashMap;

public class DataBaseHelper extends SQLiteOpenHelper {
    private static final String LOGCAT = null;

    public DataBaseHelper(Context applicationcontext) {
        super(applicationcontext, "cheff.db", null, 7);
        Log.d(LOGCAT, "Created");
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        String query, query1;
        query = "CREATE TABLE \"OrderTable\" (\"orderId\" INTEGER PRIMARY KEY  AUTOINCREMENT ,\"itemId\" INTEGER,\"itemTypeId\" INTEGER, \"subCategoryId\" INTEGER,\"additionals_name_En\" VARCHAR,\"additionals_name_Ar\" VARCHAR,\"qty\"VARCHAR,\"price\" TEXT DEFAULT 0,\"additionalsPrice\" INTEGER,\"additionalsTypeId\" TEXT DEFAULT 0,\"totalAmount\" TEXT DEFAULT 0,\"comment\" TEXT DEFAULT 0,\"status\" TEXT , \"creationDate\" TEXT DEFAULT 0, \"modifiedDate\" INTEGER DEFAULT 0,\"categoryId\" TEXT,\"itemName\" TEXT DEFAULT 0,\"itemNameAr\" TEXT,\"image\"TEXT,\"item_desc\" TEXT,\"item_desc_Ar\" TEXT,\"sub_itemName\" TEXT,\"sub_itemName_Ar\" TEXT,\"sub_itemImage\" TEXT,\"sub_item_id\" TEXT,\"sub_item_count\" TEXT,\"ItemType\" TEXT ,\"sizename_en\" TEXT ,\"sizename_ar\" TEXT ,\"discount\" TEXT ,\"additionalitemQTY\" TEXT ,\"branchName\" TEXT , \"address\" TEXT ,\"brandName\" TEXT ,\"brandName_Ar\" TEXT ,\"orderType\" TEXT ,\"BrandId\" TEXT ,\"latitute\" TEXT ,\"longitute\" TEXT ,\"branchName_Ar\" TEXT ,\"item_image\" TEXT ,\"mainTotalAmount\" TEXT ,\"dis_item_add_price\" TEXT, \"main_item_add_price\" TEXT, \"min_amount\" TEXT, \"max_days\" TEXT, \"price_with_disc\" TEXT, \"delivery_charges\" TEXT )";

        database.execSQL(query);
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, int version_old, int current_version) {
        String query;
        query = "DROP TABLE IF EXISTS OrderTable";
        database.execSQL(query);
        onCreate(database);
    }


    public void insertOrder(HashMap<String, String> queryValues) {
        Log.e("TAG", "insertorder");
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("itemId", queryValues.get("itemId"));
        values.put("itemTypeId", queryValues.get("itemTypeId"));
        values.put("subCategoryId", queryValues.get("subCategoryId"));
        values.put("additionals_name_En", queryValues.get("additionals_name_En"));
        values.put("additionals_name_Ar", queryValues.get("additionals_name_Ar"));
        values.put("qty", queryValues.get("qty"));
        values.put("price", queryValues.get("price"));
        values.put("additionalsPrice", queryValues.get("additionalsPrice"));
        values.put("additionalsTypeId", queryValues.get("additionalsTypeId"));
        values.put("totalAmount", queryValues.get("totalAmount"));
        values.put("comment", queryValues.get("comment"));
        values.put("status", queryValues.get("status"));
        values.put("creationDate", queryValues.get("creationDate"));
        values.put("modifiedDate", queryValues.get("modifiedDate"));
        values.put("categoryId", queryValues.get("categoryId"));
        values.put("itemName", queryValues.get("itemName"));
        values.put("itemNameAr", queryValues.get("itemNameAr"));
        values.put("image", queryValues.get("image"));
        values.put("item_desc", queryValues.get("item_desc"));
        values.put("item_desc_Ar", queryValues.get("item_desc_Ar"));
        values.put("sub_itemName", queryValues.get("sub_itemName"));
        values.put("sub_itemName_Ar", queryValues.get("sub_itemName_Ar"));
        values.put("sub_itemImage", queryValues.get("sub_itemImage"));
        values.put("sub_item_id", queryValues.get("sub_item_id"));
        values.put("sub_item_count", queryValues.get("sub_item_count"));
        values.put("ItemType", queryValues.get("ItemType"));
        values.put("sizename_en", queryValues.get("sizename_en"));
        values.put("sizename_ar", queryValues.get("sizename_ar"));
        values.put("discount", queryValues.get("discount"));
        values.put("additionalitemQTY", queryValues.get("additionalitemQTY"));
        values.put("branchName", queryValues.get("branchName"));
        values.put("address", queryValues.get("address"));
        values.put("brandName", queryValues.get("brandName"));
        values.put("brandName_Ar", queryValues.get("brandName_Ar"));
        values.put("orderType", queryValues.get("orderType"));
        values.put("BrandId", queryValues.get("BrandId"));
        values.put("latitute", queryValues.get("latitute"));
        values.put("longitute", queryValues.get("longitute"));
        values.put("branchName_Ar", queryValues.get("branchName_Ar"));
        values.put("item_image", queryValues.get("item_image"));
        values.put("mainTotalAmount", queryValues.get("mainTotalAmount"));
        values.put("dis_item_add_price", queryValues.get("dis_item_add_price"));
        values.put("main_item_add_price", queryValues.get("main_item_add_price"));
        values.put("min_amount", queryValues.get("min_amount"));
        values.put("max_days", queryValues.get("max_days"));
        values.put("price_with_disc", queryValues.get("price_with_disc"));
        values.put("delivery_charges", queryValues.get("delivery_charges"));



        database.insert("OrderTable", null, values);
        database.close();
        Log.e("TAG", "insertorder1");
    }

    public void updateOrder(String qty, String price, String orderId) {
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("qty", qty);
        values.put("price", price);
//		return database.update("news_count", values, "newsId" + " = ?", new String[] { queryValues.get("newsId") });
        String updateQuery = "UPDATE OrderTable SET qty =" + qty + ",totalAmount =" + price + " where orderId =" + orderId;
        database.execSQL(updateQuery);
    }

    public void updateMainOrder(String qty, String price, String orderId) {
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("qty", qty);
        values.put("price", price);
//		return database.update("news_count", values, "newsId" + " = ?", new String[] { queryValues.get("newsId") });
        String updateQuery = "UPDATE OrderTable SET qty =" + qty + ",mainTotalAmount =" + price + " where orderId =" + orderId;
        database.execSQL(updateQuery);
    }

    public void updateOrderwithOrderId(String qty, String price, String itemId) {
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("qty", qty);
        values.put("price", price);
//		return database.update("news_count", values, "newsId" + " = ?", new String[] { queryValues.get("newsId") });
        String updateQuery = "UPDATE OrderTable SET qty =" + qty + ",totalAmount =" + price + " where orderId =" + itemId;
        database.execSQL(updateQuery);
    }


    public ArrayList<Order> getOrderInfo() {
        ArrayList<Order> orderList = new ArrayList<>();
        String selectQuery = "SELECT * FROM  OrderTable";
        SQLiteDatabase database = this.getWritableDatabase();
        Cursor cursor = database.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                Log.e("TAG", "orderinfo");
                Order sc = new Order();
                sc.setOrderId(cursor.getString(0));
                sc.setItemId(cursor.getString(1));
                sc.setItemTypeId(cursor.getString(2));
                sc.setSubCatId(cursor.getString(3));
                sc.setAdditionals_nameEn(cursor.getString(4));
                sc.setAdditionals_name_Ar(cursor.getString(5));
                sc.setQty(cursor.getString(6));
                sc.setPrice(cursor.getString(7));
                sc.setAdditionalsPrice(cursor.getString(8));
                sc.setAdditionalsTypeId(cursor.getString(9));
                sc.setTotalAmount(cursor.getString(10));
                sc.setComment(cursor.getString(11));
                sc.setStatus(cursor.getString(12));
                sc.setCreationDate(cursor.getString(13));
                sc.setModifiedDate(cursor.getString(14));
                sc.setCategoryId(cursor.getString(15));
                sc.setItemName(cursor.getString(16));
                sc.setItemNameAr(cursor.getString(17));
                sc.setImage(cursor.getString(18));
                sc.setItem_desc(cursor.getString(19));
                sc.setItem_desc_Ar(cursor.getString(20));
                sc.setSub_itemName(cursor.getString(21));
                sc.setSub_itemName_Ar(cursor.getString(22));
                sc.setSub_itemImage(cursor.getString(23));
                sc.setSub_itemId(cursor.getString(24));
                sc.setSub_itemcount(cursor.getString(25));
                sc.setItemType(cursor.getString(26));
                sc.setSizename_en(cursor.getString(27));
                sc.setSizename_ar(cursor.getString(28));
                sc.setDiscount(cursor.getString(29));
                sc.setAdditionalitemQTY(cursor.getString(30));
                sc.setBranchName(cursor.getString(31));
                sc.setAddress(cursor.getString(32));
                sc.setBrandName(cursor.getString(33));
                sc.setBrandName_Ar(cursor.getString(34));
                sc.setOrderType(cursor.getString(35));
                sc.setBrandId(cursor.getString(36));
                sc.setLatitute(cursor.getString(37));
                sc.setLongitute(cursor.getString(38));
                sc.setBranchName_Ar(cursor.getString(39));
                sc.setItem_image(cursor.getString(40));
                sc.setMainTotalAmount(cursor.getString(41));
                sc.setDis_item_add_price(cursor.getString(42));
                sc.setMain_item_add_price(cursor.getString(43));
                sc.setMin_amount(cursor.getString(44));
                sc.setMax_days(cursor.getString(45));
                sc.setPrice_with_disc(cursor.getString(46));
                sc.setDelivery_charges(cursor.getString(47));


                orderList.add(sc);
            } while (cursor.moveToNext());
        }
        return orderList;
    }

    public void getCakeItemList(String Itemtype, String itemtypeid, String itemId) {
        ArrayList<Order> orderList = new ArrayList<>();
        String selectQuery = "SELECT * FROM  OrderTable where itemId" + itemId;
        SQLiteDatabase database = this.getWritableDatabase();
    }

    public int getItemOrderCount(String orderId) {
        int cnt = 0;
        SQLiteDatabase database = this.getWritableDatabase();
        String deleteQuery = "SELECT SUM(qty) FROM  OrderTable where orderId=" + orderId;
        Cursor c = database.rawQuery(deleteQuery, null);
        if (c.moveToFirst()) {
            cnt = c.getInt(0);
        }
        return cnt;
    }

    public int getItemOrderCount1(String itemId) {
        int cnt = 0;
        SQLiteDatabase database = this.getWritableDatabase();
        String deleteQuery = "SELECT SUM(qty) FROM  OrderTable where itemId=" + itemId;
        Cursor c = database.rawQuery(deleteQuery, null);
        if (c.moveToFirst()) {
            cnt = c.getInt(0);
        }
        return cnt;
    }

    public int getItemOrderCountWithSize(String itemId, String size) {
        int cnt = 0;
        SQLiteDatabase database = this.getWritableDatabase();
        String deleteQuery = "SELECT SUM(qty) FROM  OrderTable where itemId=" + itemId + " AND itemTypeId =" + size;
        Cursor c = database.rawQuery(deleteQuery, null);
        if (c.moveToFirst()) {
            cnt = c.getInt(0);
        }
        return cnt;
    }

    public int getItemOrderPrice(String itemId) {
        int cnt = 0;
        SQLiteDatabase database = this.getWritableDatabase();
        String deleteQuery = "SELECT SUM(price) FROM  OrderTable where orderId=" + itemId;
        Cursor c = database.rawQuery(deleteQuery, null);
        if (c.moveToFirst()) {
            cnt = c.getInt(0);
        }
        return cnt;
    }

    public float getItemOrderPrice1(String itemId) {
        float cnt = 0;
        SQLiteDatabase database = this.getWritableDatabase();
        String deleteQuery = "SELECT SUM(totalAmount) FROM  OrderTable where itemId=" + itemId;
        Cursor c = database.rawQuery(deleteQuery, null);
        if (c.moveToFirst()) {
            cnt = c.getFloat(0);
        }
        return cnt;
    }

    public int getSubcatOrderCount(String subCategoryId) {
        int cnt = 0;
        SQLiteDatabase database = this.getWritableDatabase();
        String deleteQuery = "SELECT SUM(qty) FROM  OrderTable where subCategoryId=" + subCategoryId;
        Cursor c = database.rawQuery(deleteQuery, null);
        if (c.moveToFirst()) {
            cnt = c.getInt(0);
        }
        return cnt;
    }

    public int getTotalOrderQty() {
        int qty = 0;
        String selectQuery = "SELECT itemId FROM  OrderTable";
        SQLiteDatabase database = this.getWritableDatabase();
        try {
            Cursor cur = database.rawQuery("SELECT SUM(qty) FROM OrderTable", null);

            if (cur.moveToFirst()) {
                qty = cur.getInt(0);
            }
        } catch (SQLiteException sqle) {
            sqle.printStackTrace();
        }
        return qty;
    }


    public int getCategoryQty(String catId) {
        int qty = 0;
        String selectQuery = "SELECT itemId FROM  OrderTable";
        SQLiteDatabase database = this.getWritableDatabase();
        try {
            Cursor cur = database.rawQuery("SELECT SUM(qty) FROM OrderTable where categoryId = " + catId, null);

            if (cur.moveToFirst()) {
                qty = cur.getInt(0);
            }
        } catch (SQLiteException sqle) {
            sqle.printStackTrace();
        }
        return qty;
    }

    public int getItemQty(String itemId) {
        int qty = 0;
        String selectQuery = "SELECT itemId FROM  OrderTable";
        SQLiteDatabase database = this.getWritableDatabase();
        try {
            Cursor cur = database.rawQuery("SELECT SUM(qty) FROM OrderTable where itemId = " + itemId, null);

            if (cur.moveToFirst()) {
                qty = cur.getInt(0);
            }
        } catch (SQLiteException sqle) {
            sqle.printStackTrace();
        }
        return qty;
    }

    public float getTotalOrderPrice() {
        float qty = 0;
        String selectQuery = "SELECT itemId FROM  OrderTable";
        SQLiteDatabase database = this.getWritableDatabase();
        Cursor cur = database.rawQuery("SELECT SUM(totalAmount) FROM OrderTable", null);
        if (cur.moveToFirst()) {
            qty = cur.getFloat(0);
        }

        return qty;
    }

    public float getMainTotalOrderPrice() {
        float qty = 0;
        String selectQuery = "SELECT itemId FROM  OrderTable";
        SQLiteDatabase database = this.getWritableDatabase();
        Cursor cur = database.rawQuery("SELECT SUM(mainTotalAmount) FROM OrderTable", null);
        if (cur.moveToFirst()) {
            qty = cur.getFloat(0);
        }

        return qty;
    }

    public void deleteSubItemFromOrder(String orderId) {
        SQLiteDatabase database = this.getWritableDatabase();

//		return database.update("news_count", values, "newsId" + " = ?", new String[] { queryValues.get("newsId") });
        String updateQuery = "DELETE FROM OrderTable where subCategoryId = " + orderId;
        database.execSQL(updateQuery);

    }

    public void deleteItemFromOrderlist(String orderId) {
        SQLiteDatabase database = this.getWritableDatabase();

//		return database.update("news_count", values, "newsId" + " = ?", new String[] { queryValues.get("newsId") });
        String updateQuery = "DELETE FROM OrderTable where orderId = " + orderId;
        database.execSQL(updateQuery);

    }

    public void deleteItemFromOrder(String orderId) {
        SQLiteDatabase database = this.getWritableDatabase();

//		return database.update("news_count", values, "newsId" + " = ?", new String[] { queryValues.get("newsId") });
        String updateQuery = "DELETE FROM OrderTable where orderId = " + orderId;
        database.execSQL(updateQuery);

    }

    public void deleteOrderTable() {
        SQLiteDatabase database = this.getWritableDatabase();

//		return database.update("news_count", values, "newsId" + " = ?", new String[] { queryValues.get("newsId") });
        String updateQuery = "DELETE FROM OrderTable";
        database.execSQL(updateQuery);
    }

    public void updateComment(String orderId, String comment, String size) {
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues values = new ContentValues();
//		return database.update("news_count", values, "newsId" + " = ?", new String[] { queryValues.get("newsId") });
        String updateQuery = "UPDATE OrderTable SET comment = '" + comment + "' where orderId =" + orderId + " AND sizename =" + size;
        database.execSQL(updateQuery);
    }


    public ArrayList<Order> getItemOrderInfo(String itemId) {
        ArrayList<Order> orderList = new ArrayList<>();
        String selectQuery = "SELECT * FROM  OrderTable where itemId = " + itemId;
        SQLiteDatabase database = this.getWritableDatabase();
        Cursor cursor = database.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                Order sc = new Order();
                sc.setOrderId(cursor.getString(0));
                sc.setItemId(cursor.getString(1));
                sc.setItemTypeId(cursor.getString(2));
                sc.setSubCatId(cursor.getString(3));
                sc.setAdditionals_nameEn(cursor.getString(4));
                sc.setAdditionals_name_Ar(cursor.getString(5));
                sc.setQty(cursor.getString(6));
                sc.setPrice(cursor.getString(7));
                sc.setAdditionalsPrice(cursor.getString(8));
                sc.setAdditionalsTypeId(cursor.getString(9));
                sc.setTotalAmount(cursor.getString(10));
                sc.setComment(cursor.getString(11));
                sc.setStatus(cursor.getString(12));
                sc.setCreationDate(cursor.getString(13));
                sc.setModifiedDate(cursor.getString(14));
                sc.setCategoryId(cursor.getString(15));
                sc.setItemName(cursor.getString(16));
                sc.setItemNameAr(cursor.getString(17));
                sc.setImage(cursor.getString(18));
                sc.setItem_desc(cursor.getString(19));
                sc.setItem_desc_Ar(cursor.getString(20));
                sc.setSub_itemName(cursor.getString(21));
                sc.setSub_itemName_Ar(cursor.getString(22));
                sc.setSub_itemImage(cursor.getString(23));
                sc.setSub_itemId(cursor.getString(24));
                sc.setSub_itemcount(cursor.getString(25));
                sc.setItemType(cursor.getString(26));
                sc.setSizename_en(cursor.getString(27));
                sc.setSizename_ar(cursor.getString(28));
                sc.setDiscount(cursor.getString(29));
                sc.setAdditionalitemQTY(cursor.getString(30));
                sc.setBranchName(cursor.getString(31));
                sc.setAddress(cursor.getString(32));
                sc.setBrandName(cursor.getString(33));
                sc.setBrandName_Ar(cursor.getString(34));
                sc.setOrderType(cursor.getString(35));
                sc.setBrandId(cursor.getString(36));
                sc.setLatitute(cursor.getString(37));
                sc.setLongitute(cursor.getString(38));
                sc.setBranchName_Ar(cursor.getString(39));
                sc.setItem_image(cursor.getString(40));
                sc.setMainTotalAmount(cursor.getString(41));
                sc.setDis_item_add_price(cursor.getString(42));
                sc.setMain_item_add_price(cursor.getString(43));
                sc.setMin_amount(cursor.getString(44));
                sc.setMax_days(cursor.getString(45));
                sc.setPrice_with_disc(cursor.getString(46));
                sc.setDelivery_charges(cursor.getString(47));

                orderList.add(sc);
            } while (cursor.moveToNext());
        }
        return orderList;
    }

}

