package com.cs.chef.Rest;

import com.cs.chef.Models.ActiveOrders;
import com.cs.chef.Models.AddAddress;
import com.cs.chef.Models.CancelOrder;
import com.cs.chef.Models.CardRegistration;
import com.cs.chef.Models.ChangeLanguageList;
import com.cs.chef.Models.CheckOutId;
import com.cs.chef.Models.DeleteCardList;
import com.cs.chef.Models.DisplayProfile;
import com.cs.chef.Models.Favourite;
import com.cs.chef.Models.InsertOrder;
import com.cs.chef.Models.MenuItems;
import com.cs.chef.Models.MyAddress;
import com.cs.chef.Models.MyCards;
import com.cs.chef.Models.MyOrderItems;
import com.cs.chef.Models.Promotions;
import com.cs.chef.Models.Rating;
import com.cs.chef.Models.Response;
import com.cs.chef.Models.SaveCardToDB;
import com.cs.chef.Models.SavedCards;
import com.cs.chef.Models.StoreMenuItems;
import com.cs.chef.Models.StoresList;
import com.cs.chef.Models.SubAdditionals;
import com.cs.chef.Models.TrackOrder;
import com.cs.chef.Models.UserRegistrationResponse;
import com.cs.chef.Models.VerifyMobileResponse;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface APIInterface {

    @POST("UserAPI/GetMobileEmailVerify")
    Call<VerifyMobileResponse> verfiyMobileNumber(@Body RequestBody body);

    @POST("UserAPI/SaveUserDetails")
    Call<UserRegistrationResponse> userRegistration(@Body RequestBody body);

    @POST("UserAPI/GetUserValidation")
    Call<UserRegistrationResponse> userLogin(@Body RequestBody body);

    @POST("UserAPI/GetMobileVerify")
    Call<VerifyMobileResponse> forgotPassword(@Body RequestBody body);

    @POST("UserAPI/SetNewPassword")
    Call<UserRegistrationResponse> resetPassword(@Body RequestBody body);

    @POST("UserAuth/DisplayProfile")
    Call<DisplayProfile> displayProfile(@Body RequestBody body);

    @POST("UserAuth/UpdateProfile")
    Call<DisplayProfile> editProfile(@Body RequestBody body);

    @POST("UserAPI/ChangePassword")
    Call<UserRegistrationResponse> changePassword(@Body RequestBody body);

    @POST("UserAPI/GetUserAddress")
    Call<MyAddress> myAddress(@Body RequestBody body);

    @POST("UserAPI/SaveUserAddress")
    Call<AddAddress> addAddress(@Body RequestBody body);

    @POST("StoreInformationAPI/GetStoreCatInformation")
    Call<StoresList> getStoresList(@Body RequestBody body);

    @POST("MenuItemsAPI/GetCatgoriesWithItems")
    Call<MenuItems> getMenuItems(@Body RequestBody body);

    @POST("ManageItems/GetMenuItems")
    Call<StoreMenuItems> getStoreMenu(@Body RequestBody body);

    @POST("OrderInformation/GetUserOrderHistory")
    Call<MyOrderItems> getMyorder(@Body RequestBody body);

    @POST("ManageItems/GetAdditionalItemsPrices")
    Call<SubAdditionals> getSubAdditionals(@Body RequestBody body);

    @POST("OrderInformation/GetOrderTracking")
    Call<TrackOrder> getTrackOrder(@Body RequestBody body);

    @POST("OrderInformation/InsertFavRatingDeleteOrder")
    Call<Favourite> getFavOrder(@Body RequestBody body);

    @POST("OrderInformation/InsertFavRatingDeleteOrder")
    Call<Rating> getRating(@Body RequestBody body);

    @POST("OrderInformation/CancelOrder")
    Call<CancelOrder> getCancelOrder(@Body RequestBody body);

    @POST("OrderInformation/InsertOrder")
    Call<InsertOrder> getInsertOrder(@Body RequestBody body);

    @POST("OrderInformation/UserActiveOrder")
    Call<ActiveOrders> getActiveOrders(@Body RequestBody body);

    @POST("PromotionAPI/NewgetAppPromotions")
    Call<Promotions> getPromotions(@Body RequestBody body);

//    Hyperpay
    @POST("PaymentAPI/NewCheckoutRequestFromSDK")
    Call<CheckOutId> generateCheckOutId(@Body RequestBody body);

    @POST("PaymentAPI/paymentStatus")
    Call<MyCards> getPaymentStatus(@Body RequestBody body);

    @POST("PaymentAPI/AddCardStatus")
    Call<CardRegistration> addCardStatus(@Body RequestBody body);

    @POST("PaymentAPI/SaveCard")
    Call<SaveCardToDB> insertCreditCard(@Body RequestBody body);

    @POST("PaymentAPI/GetAllSavedCards")
    Call<SavedCards> GetAllSavedCards(@Body RequestBody body);

    @POST("PaymentAPI/deleteCard")
    Call<DeleteCardList> deleteCard(@Body RequestBody body);

    @POST("UserAPI/languageChange")
    Call<ChangeLanguageList> getchangelang(@Body RequestBody body);
 }
