package com.cs.chef.hyperpay;

import android.os.AsyncTask;
import android.util.JsonReader;
import android.util.Log;


import com.cs.chef.Constants;
import com.cs.chef.Models.CardRegistration;
import com.cs.chef.Models.MyCards;
import com.cs.chef.Rest.APIInterface;
import com.cs.chef.Rest.ApiClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Represents an async task to request a payment status from the server.
 */
public class CardRegistrationStatusRequestAsyncTask extends AsyncTask<String, Void, CardRegistration> {

    private CardRegistrationStatusRequestListener listener;
    private CardRegistration cardsResponse = null;

    public CardRegistrationStatusRequestAsyncTask(CardRegistrationStatusRequestListener listener) {
        this.listener = listener;
    }

    @Override
    protected CardRegistration doInBackground(String... params) {
        if (params.length != 1) {
            return null;
        }

        String resourcePath = params[0];

        if (resourcePath != null) {
            // prepare input json
            JSONObject parentObj = new JSONObject();
            try {
                parentObj.put("resourcePath", params[0]);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            final APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<CardRegistration> call = apiService.addCardStatus(
                    RequestBody.create(MediaType.parse("application/json"), parentObj.toString()));
            call.enqueue(new Callback<CardRegistration>() {
                @Override
                public void onResponse(Call<CardRegistration> call, Response<CardRegistration> response) {
                    Log.d("TAG", "onResponse: " + response);
                    if (response.isSuccessful()) {
                        CardRegistration checkOutIdResponse = response.body();
                        try {
                            if (checkOutIdResponse.getStatus()) {
                                cardsResponse = checkOutIdResponse;
                                Log.d("TAG", "sucess: "+cardsResponse);

                                if (listener != null) {
                                    listener.onCardRegistrationStatusReceived(cardsResponse);
                                }
                            } else {
                                Log.d("TAG", "failure: "+cardsResponse);
                                // status false case
//                                if (listener != null) {
//                                    listener.onErrorOccurred();
//                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
//                            if (listener != null) {
//                                listener.onErrorOccurred();
//                            }
                        }
                    } else {
                        Log.d("TAG", "onResponse failure: ");
//                        if (listener != null) {
//                            listener.onErrorOccurred();
//                        }
                    }
                }

                @Override
                public void onFailure(Call<CardRegistration> call, Throwable t) {
                    Log.d("TAG", "onFailure: " + t.toString());
                    if (listener != null) {
                        listener.onErrorOccurred();
                    }
                }
            });
        }

        return null;
    }

    @Override
    protected void onPostExecute(CardRegistration paymentStatus) {

    }

//    private MyCards requestPaymentStatus(String resourcePath) {
//        if (resourcePath == null) {
//            return null;
//        }
//
//        URL url;
//        String urlString;
//        HttpURLConnection connection = null;
//        MyCards myCards = new MyCards();
//
//        try {
//            urlString = Constants.CARD_REGISTRATION_STATUS_URL +
//                    URLEncoder.encode(resourcePath, "UTF-8");
//
//            Log.i("TAG", "Status request url: " + urlString);
//
//            url = new URL(urlString);
//            connection = (HttpURLConnection) url.openConnection();
//            connection.setConnectTimeout(Constants.CONNECTION_TIMEOUT);
//
//            JsonReader jsonReader = new JsonReader(
//                    new InputStreamReader(connection.getInputStream(), "UTF-8"));
//
//            jsonReader.beginObject();
//
//            while (jsonReader.hasNext()) {
//                String key = jsonReader.nextName();
//                if (key.equals("id")) {
//                    myCards.setToken(jsonReader.nextString());
//                }
//                else if (key.equals("paymentBrand")) {
//                    myCards.setPaymentBrand(jsonReader.nextString());
//                }
//                else if (key.equals("card")){
//                    jsonReader.beginObject();
//                    while (jsonReader.hasNext()){
//                        String innerKey = jsonReader.nextName();
//                        if (innerKey.equals("last4Digits")) {
//                            myCards.setLast4Digits(jsonReader.nextString());
//                        }
//                        else if (innerKey.equals("holder")) {
//                            myCards.setHolderName(jsonReader.nextString());
//                        }
//                        else if (innerKey.equals("expiryMonth")) {
//                            myCards.setExpiryMonth(jsonReader.nextString());
//                        }
//                        else if (innerKey.equals("expiryYear")) {
//                            myCards.setExpiryYear(jsonReader.nextString());
//                        }
//                        else {
//                            jsonReader.skipValue();
//                        }
//                    }
//                    jsonReader.endObject();
//                }
//                else if (key.equals("customPayment")) {
//                    myCards.setPaymentStatus(jsonReader.nextString());
//                }
//                else {
//                    jsonReader.skipValue();
//                }
//            }
//
//            jsonReader.endObject();
//            jsonReader.close();
//
//        } catch (Exception e) {
//            Log.i("TAG", "Error: ", e);
//        } finally {
//            if (connection != null) {
//                connection.disconnect();
//            }
//        }
//
//        return myCards;
//    }
}
