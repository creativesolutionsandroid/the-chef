package com.cs.chef.hyperpay;

import android.os.AsyncTask;
import android.util.JsonReader;
import android.util.Log;
import android.widget.Toast;

import com.cs.chef.Activities.RatingActivity;
import com.cs.chef.Constants;
import com.cs.chef.Models.CheckOutId;
import com.cs.chef.Models.Rating;
import com.cs.chef.R;
import com.cs.chef.Rest.APIInterface;
import com.cs.chef.Rest.ApiClient;
import com.cs.chef.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.HTTP;


/**
 * Represents an async task to request a checkout id from the server.
 */
public class CheckoutIdRequestAsyncTask extends AsyncTask<String, Void, String> {

    private CheckoutIdRequestListener listener;
    private String checkoutId = "";

    public CheckoutIdRequestAsyncTask(CheckoutIdRequestListener listener) {
        this.listener = listener;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(String... params) {
        if (params.length != 6) {
            return null;
        }

        // prepare input json
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("amount", params[0]);
            parentObj.put("shopperResultUrl", params[1]);
            parentObj.put("isCardRegistration", params[2]);
            parentObj.put("merchantTransactionId", params[3]);
            parentObj.put("customerEmail", params[4]);
            parentObj.put("userId", params[5]);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final APIInterface apiService =
                ApiClient.getClient().create(APIInterface.class);

        Call<CheckOutId> call = apiService.generateCheckOutId(
                RequestBody.create(MediaType.parse("application/json"), parentObj.toString()));
        call.enqueue(new Callback<CheckOutId>() {
            @Override
            public void onResponse(Call<CheckOutId> call, Response<CheckOutId> response) {
                Log.d("TAG", "onResponse: " + response);
                if (response.isSuccessful()) {
                    CheckOutId checkOutIdResponse = response.body();
                    try {
                        if (checkOutIdResponse.getStatus()) {
                            checkoutId = checkOutIdResponse.getData().get(0).getId();

                            if (listener != null) {
                                Log.d("TAG", "doInBackground: "+checkoutId);
                                listener.onCheckoutIdReceived(checkoutId);
                            }
                        } else {
                            // status false case
                            checkoutId = "";

                            if (listener != null) {
                                Log.d("TAG", "doInBackground: "+checkoutId);
                                listener.onCheckoutIdReceived(checkoutId);
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        checkoutId = "";

                        if (listener != null) {
                            Log.d("TAG", "doInBackground: "+checkoutId);
                            listener.onCheckoutIdReceived(checkoutId);
                        }
                    }
                } else {
                    checkoutId = "";

                    if (listener != null) {
                        Log.d("TAG", "doInBackground: "+checkoutId);
                        listener.onCheckoutIdReceived(checkoutId);
                    }
                }
            }

            @Override
            public void onFailure(Call<CheckOutId> call, Throwable t) {
                Log.d("TAG", "onFailure: " + t.toString());
                checkoutId = "";

                if (listener != null) {
                    Log.d("TAG", "doInBackground: "+checkoutId);
                    listener.onCheckoutIdReceived(checkoutId);
                }
            }
        });

        return checkoutId;
    }

    @Override
    protected void onPostExecute(String checkoutId) {

    }
}