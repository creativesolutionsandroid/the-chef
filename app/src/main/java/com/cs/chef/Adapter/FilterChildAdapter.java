package com.cs.chef.Adapter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cs.chef.Models.StoresList;
import com.cs.chef.R;

import java.util.ArrayList;

import static com.cs.chef.Fragments.HomeScreenFragment.filterid;

public class FilterChildAdapter extends RecyclerView.Adapter<FilterChildAdapter.MyViewHolder> {

    private Context context;
    private int selectedPosition = 0;
    private ArrayList<StoresList.Filters> data = new ArrayList<>();
    private Activity activity;
    boolean filter = true;
    String language;


    public FilterChildAdapter(Context context, ArrayList<StoresList.Filters> data, String language) {
        this.context = context;
        this.activity = activity;
        this.data = data;
        this.language = language;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;
        if (language.equalsIgnoreCase("En")) {
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.child_list, parent, false);
        } else {
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.child_list, parent, false);
        }
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {

        if (language.equalsIgnoreCase("En")) {
            holder.filter_child_name.setText("" + data.get(position).getFilterName_En());
        } else {
            holder.filter_child_name.setText("" + data.get(position).getFilterName_Ar());
        }

        if (filterid.contains(data.get(position).getFilterId())) {
            holder.filter_child_name.setBackground(context.getResources().getDrawable(R.drawable.child_selected));
            holder.filter_child_name.setTextColor(context.getResources().getColor(R.color.white));

        } else {
            holder.filter_child_name.setBackground(context.getResources().getDrawable(R.drawable.chlid_unselected));
            holder.filter_child_name.setTextColor(context.getResources().getColor(R.color.blue));
        }

        holder.filter_child_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (filterid.contains(data.get(position).getFilterId())) {
                    holder.filter_child_name.setBackground(context.getResources().getDrawable(R.drawable.chlid_unselected));
                    holder.filter_child_name.setTextColor(context.getResources().getColor(R.color.blue));
                    for (int j = 0; j < filterid.size(); j++) {
                        if (filterid.get(j) == data.get(position).getFilterId()) {
                            filterid.remove(j);
                            break;
                        }
                    }
                } else {
                    holder.filter_child_name.setBackground(context.getResources().getDrawable(R.drawable.child_selected));
                    holder.filter_child_name.setTextColor(context.getResources().getColor(R.color.white));
                    filterid.add(data.get(position).getFilterId());
                }

                Log.i("TAG", "onClick: " + filterid.size());
            }
        });

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView filter_child_name;
//        RecyclerView filter_child;

        public MyViewHolder(View itemView) {
            super(itemView);
            filter_child_name = (TextView) itemView.findViewById(R.id.filter_child_name);


        }
    }
}
