package com.cs.chef.Adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cs.chef.Models.CatNames;
import com.cs.chef.Models.Items;
import com.cs.chef.R;

import java.util.ArrayList;
import java.util.List;

public class MenuGridAdapter extends RecyclerView.Adapter<MenuGridAdapter.ViewHolder> {

    ArrayList<CatNames> catNames = new ArrayList<>();
    ArrayList<Items> item = new ArrayList<>();

    private LayoutInflater mInflater;
    String language;
    public static ImageView itemImage;
    Activity parentActivity;
    public Context context;
    ViewPager viewpager;
    boolean dropdown = false;
    List<TextView> cardViewList = new ArrayList<>();
    MenuSubItemsAdapter menuSubItemsAdapter;
    boolean spacing = true;
//    String langauge;


    public MenuGridAdapter(Context context, ArrayList<CatNames> catNames, ArrayList<Items> item, String language) {
        this.mInflater = LayoutInflater.from(context);
        this.item = item;
        this.catNames = catNames;
//        this.hotelname_Ar = hotelname_Ar;
        this.parentActivity = parentActivity;
        this.context = context;
        this.language = language;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        Log.i("TAG", "curst size1 " + orderList.size());
        View view = null;
        if (language.equalsIgnoreCase("En")) {
            view = mInflater.inflate(R.layout.menu_items, parent, false);
        } else if (language.equalsIgnoreCase("Ar")) {
            view = mInflater.inflate(R.layout.menu_items_arabic, parent, false);
        }
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {


        holder.itemview.setLayoutManager(new GridLayoutManager(context.getApplicationContext(), 2));
//        if (spacing){
//            spacing = false;
//            int spacingInPixels = 20;
//            holder.itemview.addItemDecoration(new SpacesItemDecoration(spacingInPixels));
//        }

//        selectedcat = catNames.get(position).getCatname_en();
//        MenuActivity.itemsArrayList.clear();
//        if (selectedcat.equalsIgnoreCase("all")) {
//            for (int i = 1; i < catNames.size(); i++) {
//                for (int j = 0; j < catNames.get(i).getItemsArrayList().size(); j++) {
//                    Items itemname = new Items();
//                    itemname.setItemid(catNames.get(i).getItemsArrayList().get(j).getItemid());
//                    itemname.setItem_name_en(catNames.get(i).getItemsArrayList().get(j).getItem_name_en());
//                    itemname.setItem_name_ar(catNames.get(i).getItemsArrayList().get(j).getItem_name_ar());
//                    itemname.setItem_desc_en(catNames.get(i).getItemsArrayList().get(j).getItem_desc_en());
//                    itemname.setItem_desc_ar(catNames.get(i).getItemsArrayList().get(j).getItem_desc_ar());
//                    itemname.setItem_price(catNames.get(i).getItemsArrayList().get(j).getItem_price());
//                    itemname.setItem_image(catNames.get(i).getItemsArrayList().get(j).getItem_image());
//
//                    itemsArrayList.add(itemname);
//                }
//            }
//        } else {
        if (catNames.get(position).getCatname_en() == "All") {

            holder.hearder_name.setVisibility(View.GONE);
            ArrayList<Items> items = new ArrayList<>();
            menuSubItemsAdapter = new MenuSubItemsAdapter(context, items, language);
            holder.itemview.setAdapter(menuSubItemsAdapter);
            holder.grid_layout.setVisibility(View.GONE);

        } else {
//            if (MenuActivity.filter) {
//                holder.hearder_name.setVisibility(View.VISIBLE);
//                holder.hearder_name.setText("" + MenuActivity.selectedcat);
//                menuSubItemsAdapter = new MenuSubItemsAdapter(context, MenuActivity.itemsArrayList);
//                holder.itemview.setAdapter(menuSubItemsAdapter);
//            } else {
            holder.hearder_name.setVisibility(View.VISIBLE);
            holder.grid_layout.setVisibility(View.VISIBLE);
            if (language.equalsIgnoreCase("En")) {
                holder.hearder_name.setText("" + catNames.get(position).getCatname_en());
            } else {
                holder.hearder_name.setText("" + catNames.get(position).getCatname_ar());
            }
            menuSubItemsAdapter = new MenuSubItemsAdapter(context, catNames.get(position).getItemsArrayList(), language);
            holder.itemview.setAdapter(menuSubItemsAdapter);
//            }

        }

//        LocalBroadcastManager.getInstance(context).registerReceiver(
//                mheader_name, new IntentFilter("HeaderUpdate"));

//        for (int i = 1; i < catNames.size(); i++) {
//            if (catNames.get(i).getCatname_en().equalsIgnoreCase(selectedcat)) {
//                for (int j = 0; j < catNames.get(i).getItemsArrayList().size(); j++) {
//                    Items itemname = new Items();
//                    itemname.setItemid(catNames.get(i).getItemsArrayList().get(j).getItemid());
//                    itemname.setItem_name_en(catNames.get(i).getItemsArrayList().get(j).getItem_name_en());
//                    itemname.setItem_name_ar(catNames.get(i).getItemsArrayList().get(j).getItem_name_ar());
//                    itemname.setItem_desc_en(catNames.get(i).getItemsArrayList().get(j).getItem_desc_en());
//                    itemname.setItem_desc_ar(catNames.get(i).getItemsArrayList().get(j).getItem_desc_ar());
//                    itemname.setItem_price(catNames.get(i).getItemsArrayList().get(j).getItem_price());
//                    itemname.setItem_image(catNames.get(i).getItemsArrayList().get(j).getItem_image());
//
//                    itemsArrayList.add(itemname);
//                }
//                break;
//            }
////            }
//        }


    }


    @Override
    public int getItemCount() {
        return catNames.size();
    }

//    private BroadcastReceiver mheader_name = new BroadcastReceiver() {
//        @Override
//        public void onReceive(Context context, Intent intent) {
//            String b = intent.getStringExtra("cat_name");
//            int c = intent.getIntExtra("pos",0);
//
//            if (b.equalsIgnoreCase("All")){
//                itemview.scrollToPosition(0);
//            }else {
//                itemview.scrollToPosition(c-1);
//            }
//
//        }
//    };

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView hearder_name;
        //        Spinner spinner;
        RecyclerView itemview;
        RelativeLayout grid_layout;

//        ImageView item_img;

        public ViewHolder(View itemView) {
            super(itemView);
//            Log.i("TAG", "curst size4 " + orderList.size());

            hearder_name = itemView.findViewById(R.id.header_name);

            itemview = itemView.findViewById(R.id.grid_view);
            grid_layout = itemView.findViewById(R.id.grid_layout);
//            curstImage = itemView.findViewById(R.id.curst_img_layout);

//            layout.setOnClickListener(this);

//            itemView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//
//                    Intent a = new Intent(context, AdditionalActivity.class);
//                    if (MenuActivity.banner) {
//                        a.putExtra("branchid", MenuActivity.storeData1.get(MenuActivity.pos).getBranchId());
//                    } else {
//                        a.putExtra("branchid", MenuActivity.storeData.get(MenuActivity.pos).getBranchId());
//                    }
////                    Log.i("TAG", "onClick: " + item_id);
//                    a.putExtra("itemid", item.get(getAdapterPosition()).getItemid());
//                    a.putExtra("itemdecs", item.get(getAdapterPosition()).getItem_desc_en());
//                    a.putExtra("itemimage", item.get(getAdapterPosition()).getItem_image());
//                    context.startActivity(a);
//
//                }
//            });
        }

//        @Override
//        public void onClick(View v) {
//
//
//
//            int pos = getAdapterPosition();
//
//            for (int i = 0; i < galleryArrayList.size(); i++) {
//                if (galleryArrayList.get(i).getHotalnameEn().contains(hotelname_En.get(pos))) {
//
//                    if (!city_name_en.contains(galleryArrayList.get(i).getCityNameEn())) {
//                        city_name_en.add(galleryArrayList.get(i).getCityNameEn());
//                    }
//
//                }
//            }
//
//            Log.d("TAG", "adapterpos: " + pos);
//
//            if (hotelname_En.get(pos).contains(hotelname_En.get(pos))) {
//
//                spinner.setBackgroundResource(R.drawable.gallery_footer_selected_bg);
//
//            }
//
//
//        }
    }
}
