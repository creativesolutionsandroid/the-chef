package com.cs.chef.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cs.chef.Activities.MainActivity;
import com.cs.chef.Activities.OfferActivity;
import com.cs.chef.Constants;
import com.cs.chef.DataBaseHelper;
import com.cs.chef.Models.Promotions;
import com.cs.chef.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

//import com.cs.chef.Models.Promotions;

public class OffersAdapter extends BaseAdapter {

    public Context context;
    public LayoutInflater inflater;
    ArrayList<Promotions.Data> data = new ArrayList<>();
    int pos;
    public static String subscriptions = "no";
    String id;
    String language;
    DataBaseHelper myDbHelper;
    float tax;

    ArrayList<String> weekdays = new ArrayList<>();

    PromotionDays promotionDays;
    Activity activity;

    public OffersAdapter(Context context, ArrayList<Promotions.Data> data, String language, Activity activity) {
        this.context = context;
        this.data = data;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.language = language;
        this.activity = activity;
        //DBcontroller = new DatabaseHandler(context);
        myDbHelper = new DataBaseHelper(context);

        //imageLoader=new ImageLoader(context.getApplicationContext());
    }

    public int getCount() {
        return data.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    public static class ViewHolder {

        TextView offer_name, offer_discount, min_spend, offer_desc, discount_upto;
        RecyclerView promo_days;
        LinearLayout offer_layout;

    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        pos = position;
        if (convertView == null) {
            holder = new ViewHolder();
            if (language.equalsIgnoreCase("En")) {
                convertView = inflater.inflate(R.layout.offer_list, null);
            } else {
                convertView = inflater.inflate(R.layout.offer_list_arabic, null);
            }

            holder.offer_layout = (LinearLayout) convertView.findViewById(R.id.offer_layout);
            holder.offer_name = (TextView) convertView.findViewById(R.id.offer_name);
            holder.min_spend = (TextView) convertView.findViewById(R.id.min_spend);
            holder.offer_desc = (TextView) convertView.findViewById(R.id.offer_desc);
            holder.offer_discount = (TextView) convertView.findViewById(R.id.offer_discount);
            holder.promo_days = (RecyclerView) convertView.findViewById(R.id.promotion_days);
            holder.discount_upto = (TextView) convertView.findViewById(R.id.discount_upto);

//            holder.order_addition_item = (TextView) convertView
//                    .findViewById(R.id.order_additional_item);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if (language.equalsIgnoreCase("En")) {
            holder.offer_name.setText("" + data.get(position).getPromoTitle_En());
            holder.offer_desc.setText("" + data.get(position).getMsg_En());
        } else {
            holder.offer_name.setText("" + data.get(position).getPromoTitle_Ar());
            holder.offer_desc.setText("" + data.get(position).getMsg_Ar());
        }
        if (data.get(position).getDiscountType() == 1) {
            holder.offer_discount.setText("" + Constants.decimalFormat.format(data.get(position).getDiscountAmt()) + " SAR OFF");
        } else {
            holder.offer_discount.setText("" + data.get(position).getDiscountAmt() + "% Discount");
        }
        holder.min_spend.setText("Minimum Spend: " + Constants.decimalFormat.format(data.get(position).getMinSpend()) + " SAR");

        holder.discount_upto.setText("Upto Discount " + Constants.decimalFormat.format(data.get(position).getMaxAmount()) + " SAR");

        LinearLayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);

        holder.promo_days.setLayoutManager(layoutManager);

        String dayofweek;
        Calendar calendar = Calendar.getInstance();

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("EEEE", Locale.US);

//        dayofweek = simpleDateFormat.format(calendar.getTime());

        weekdays.clear();

        weekdays.add("S");
        weekdays.add("M");
        weekdays.add("T");
        weekdays.add("W");
        weekdays.add("T");
        weekdays.add("F");
        weekdays.add("S");


        dayofweek = data.get(position).getDayOftheWeek();

        promotionDays = new PromotionDays(context, data.get(position).getPromotionDays(), weekdays);
        holder.promo_days.setAdapter(promotionDays);
        Log.i("TAG", "getView: " + data.get(position).getPromotionDays().size());


        holder.offer_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Constants.confirm) {

                    if (data.get(position).getTodayAvailable()) {
                        tax = myDbHelper.getTotalOrderPrice() * Constants.vatper;
                        if ((myDbHelper.getTotalOrderPrice() + tax) >= data.get(position).getMinSpend()) {

                            Constants.offer_selected = true;

                            Intent intent1 = new Intent(context, MainActivity.class);
                            intent1.putExtra("class", "checkout");
                            Constants.confirm = false;
                            context.startActivity(intent1);

                            Constants.check_out = true;
                            Constants.discount = data.get(position).getDiscountAmt();
                            Constants.offer_name = data.get(position).getPromotionCode();
                            Constants.offer_desc = data.get(position).getDesc_En();
                            Constants.discount_type = data.get(position).getDiscountType();
                            Constants.min_spending = data.get(position).getMinSpend();
                            Constants.max_spending = data.get(position).getMaxAmount();
                        } else {

                            Constants.showOneButtonAlertDialog("Minimum spending must be greater then "+ Constants.decimalFormat.format(data.get(position).getMinSpend()) + " SAR", context.getResources().getString(R.string.app_name), context.getResources().getString(R.string.ok), activity);

                        }

                    } else {

                        Constants.showOneButtonAlertDialog("Sorry there is no promotion avaiable for today", context.getResources().getString(R.string.app_name), context.getResources().getString(R.string.ok), activity);

                    }
                }

            }
        });

        return convertView;
    }
}
