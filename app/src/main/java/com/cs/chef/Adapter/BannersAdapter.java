package com.cs.chef.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.cs.chef.Activities.MenuActivity;
import com.cs.chef.Activities.PromotedViewallScreenActivity;
import com.cs.chef.Constants;
import com.cs.chef.Models.Banners;
import com.cs.chef.R;

import java.util.ArrayList;

//import static com.cs.chef.Activities.ForgotPasswordActivity.TAG;

public class BannersAdapter extends PagerAdapter {

    LayoutInflater mLayoutInflater;
    ArrayList<Banners> bannerList;
    Context context;
    String language;

    public BannersAdapter(Context context, ArrayList<Banners> bannerList, String language) {
        this.context = context;
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.bannerList = bannerList;
        this.language = language;
        notifyDataSetChanged();
    }

    @Override
    public float getPageWidth(int position) {
        float nbPages = 1.0f; // You could display partial pages using a float value
        return (1 / nbPages);
    }

    @Override
    public int getCount() {
        return bannerList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((RelativeLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View itemView;
        if (language.equalsIgnoreCase("En")) {
            itemView = mLayoutInflater.inflate(R.layout.banners_stores, container, false);
        } else {
            itemView = mLayoutInflater.inflate(R.layout.banners_stores, container, false);
        }

        final ImageView store_image = (ImageView) itemView.findViewById(R.id.store_image);

        store_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {
                    if (bannerList.get(position).getBannerList().size() == 0) {

                    } else if (bannerList.get(position).getBannerList().size() > 1) {
                        Intent intent = new Intent(context, PromotedViewallScreenActivity.class);
//                        if (language.equalsIgnoreCase("En")) {
                            intent.putExtra("viewaall", "Banners");
//                        } else {
//                            intent.putExtra("viewaall", "لافتات");
//                        }
                        intent.putExtra("array", bannerList.get(position).getBannerList());
                        intent.putExtra("seletpos", position);
                        context.startActivity(intent);
//                        banners arabic لافتات
                        //                    Log.d("TAG", "bannerlistsize" + bannerList.get(0).getBannerList().size());

                    } else {

                        Intent a = new Intent(context, MenuActivity.class);
                        a.putExtra("array", bannerList.get(position).getBannerList());
                        a.putExtra("pos", 0);
                        a.putExtra("banner", "banner");
                        context.startActivity(a);
                        Constants.store_lat = bannerList.get(position).getBannerList().get(0).getLatitude();
                        Constants.store_longi = bannerList.get(position).getBannerList().get(0).getLongitude();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        RequestOptions requestOptions = new RequestOptions();
        requestOptions.diskCacheStrategy(DiskCacheStrategy.RESOURCE);

        Glide.with(context)
                .load(Constants.IMAGE_URL + bannerList.get(position).getBannerimageEn())
                .into(store_image);

        container.addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((RelativeLayout) object);
    }
}