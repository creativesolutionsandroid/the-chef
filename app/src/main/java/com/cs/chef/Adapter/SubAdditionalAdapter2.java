package com.cs.chef.Adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Paint;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cs.chef.Activities.AdditionalActivity;
import com.cs.chef.Activities.MenuActivity;
import com.cs.chef.Constants;
import com.cs.chef.Models.SubAdditionals;
import com.cs.chef.R;

import java.util.ArrayList;

import static com.cs.chef.Activities.AdditionalActivity.MainOrderPrice;
import static com.cs.chef.Activities.AdditionalActivity.item_add_price;

public class SubAdditionalAdapter2 extends BaseAdapter {
    public Context context;
    public LayoutInflater inflater;
    ArrayList<SubAdditionals.Additems> data = new ArrayList<>();
    int pos;
    public static String subscriptions = "no";
    String id;
    String language;
    //public ImageLoader imageLoader;
    public static Double lat, longi;
    int Qty = 0, add_price;
    float discount_amount;


    int min = 1, max = 5;
    public static float suborderPrice = 0;
    public static float mainsuborderPrice = 0;
    ArrayList<Boolean> mCheckBoxStates = new ArrayList<>();
    boolean sub_item_chk = false;

    public SubAdditionalAdapter2(Context context, ArrayList<SubAdditionals.Additems> data, String language) {
        this.context = context;
        this.data = data;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.language = language;
        //DBcontroller = new DatabaseHandler(context);

        //imageLoader=new ImageLoader(context.getApplicationContext());
    }

    public int getCount() {
        return data.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    public static class ViewHolder {
        CheckBox img;
        TextView maddition, mtotal_price, mdiscount_price, max_item_can_be_selected;
        TextView mins, plus, qty;
        LinearLayout mcount;

    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        pos = position;
        if (convertView == null) {
            holder = new ViewHolder();
            if (language.equalsIgnoreCase("En")) {
                convertView = inflater.inflate(R.layout.sub_items_additionals, null);
            } else {
                convertView = inflater.inflate(R.layout.sub_items_additionals_arabic, null);
            }


            holder.img = convertView
                    .findViewById(R.id.img);
            holder.maddition = convertView
                    .findViewById(R.id.additional);
            holder.mtotal_price = convertView
                    .findViewById(R.id.total_price);
            holder.mdiscount_price = convertView
                    .findViewById(R.id.discount_price);
            holder.mins = convertView
                    .findViewById(R.id.mins);
            holder.plus = convertView
                    .findViewById(R.id.plus);
            holder.qty = convertView
                    .findViewById(R.id.qty);
            holder.mcount = convertView
                    .findViewById(R.id.count);
            holder.max_item_can_be_selected = convertView
                    .findViewById(R.id.max_items_can_be_selected);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if (language.equalsIgnoreCase("En")) {
            holder.maddition.setText("" + data.get(position).getAddtionalName_en());
        } else {
            holder.maddition.setText("" + data.get(position).getAddtionalName_Ar());
        }
        for (int i = 0; i < data.get(position).getAddprice().size(); i++) {
//            int dis_count = 30;
            if (MenuActivity.banner) {
                if (MenuActivity.storeData1.get(MenuActivity.pos).getDiscountAmt() == 0) {
                    holder.mtotal_price.setText("" + Constants.decimalFormat.format(data.get(position).getAddprice().get(i).getAddprice()));
                    holder.mdiscount_price.setVisibility(View.GONE);
//                    RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) holder.mtotal_price.getLayoutParams();
//                    params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
////                    params.addRule(RelativeLayout.CENTER_IN_PARENT);
//                    holder.mtotal_price.setLayoutParams(params); //causes layout update

                } else {
                    holder.mdiscount_price.setVisibility(View.VISIBLE);
                    holder.mtotal_price.setText("" + Constants.decimalFormat.format(data.get(position).getAddprice().get(i).getAddprice()));
                    holder.mtotal_price.setPaintFlags(holder.mtotal_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                    float discount_value = data.get(position).getAddprice().get(i).getAddprice() * MenuActivity.storeData1.get(MenuActivity.pos).getDiscountAmt() / 100;
                    discount_amount = data.get(position).getAddprice().get(i).getAddprice() - discount_value;
                    holder.mdiscount_price.setText("" + Constants.decimalFormat.format(discount_amount));
                }
            } else {
                if (MenuActivity.storeData.get(MenuActivity.pos).getDiscountAmt() == 0) {
                    holder.mtotal_price.setText("" + Constants.decimalFormat.format(data.get(position).getAddprice().get(i).getAddprice()));
                    holder.mdiscount_price.setVisibility(View.GONE);
//                    RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) holder.mtotal_price.getLayoutParams();
//                    params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
//
//                    holder.mtotal_price.setLayoutParams(params); //causes layout update
                } else {
                    holder.mdiscount_price.setVisibility(View.VISIBLE);
                    holder.mtotal_price.setText("" + Constants.decimalFormat.format(data.get(position).getAddprice().get(i).getAddprice()));
                    holder.mtotal_price.setPaintFlags(holder.mtotal_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                    float discount_value = data.get(position).getAddprice().get(i).getAddprice() * MenuActivity.storeData.get(MenuActivity.pos).getDiscountAmt() / 100;
                    discount_amount = data.get(position).getAddprice().get(i).getAddprice() - discount_value;
                    holder.mdiscount_price.setText("" + Constants.decimalFormat.format(discount_amount));
                }
            }

        }

        holder.img.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {


                    if (!AdditionalActivity.max_add_selection.contains(data.get(position).getAddtionalName_en())) {
                        AdditionalActivity.max_add_selection.add(data.get(position).getAddtionalName_en());
                    }

                    if (AdditionalActivity.max_add_selection.size() > AdditionalActivity.max_add_to_be_selected) {

                        isChecked = false;

                        if (language.equalsIgnoreCase("En")) {
                            Constants.showOneButtonAlertDialog("You are done with maximum " + AdditionalActivity.max_add_to_be_selected + " " + AdditionalActivity.sub_additionals_name.getText() + ".", context.getResources().getString(R.string.app_name), "Ok", (Activity) context);
                        } else {
                            Constants.showOneButtonAlertDialog(""+ "."+   AdditionalActivity.sub_additionals_name.getText() + " " + AdditionalActivity.max_add_to_be_selected + "لقد انتهيت من الحد الأقصى " , context.getResources().getString(R.string.app_name_ar), context.getResources().getString(R.string.ok_ar), (Activity) context);
                        }

                    } else {

                        sub_item_chk = true;
                        holder.plus.setClickable(true);
                        Log.i("TAG", "chk_status1: " + sub_item_chk);


                        if (item_add_price == 0) {

                            holder.img.setButtonDrawable(R.drawable.additional_unselected);

                            if (language.equalsIgnoreCase("En")) {

                                Constants.showOneButtonAlertDialog("Please select atleast any size", context.getResources().getString(R.string.app_name), "Ok", (Activity) context);
                            } else {
                                Constants.showOneButtonAlertDialog("Please select atleast any size", context.getResources().getString(R.string.app_name_ar), context.getResources().getString(R.string.ok_ar), (Activity) context);
                            }

                        } else {

//                            suborderPrice = item_add_price;
                            mainsuborderPrice = MainOrderPrice;

                            holder.img.setButtonDrawable(R.drawable.additional_selected);

                            if (data.get(position).getMinSelection() == 0 && data.get(position).getMaxSelection() == 0 || data.get(position).getMinSelection() == 1 && data.get(position).getMaxSelection() == 1) {
//                        if (min == 0 && max == 0 || min == 1 && max == 1) {
                                holder.mcount.setVisibility(View.GONE);
                                data.get(position).setAdd_qty(1);
                                AddItems();


                            } else {

                                holder.mcount.setVisibility(View.VISIBLE);
                                data.get(position).setAdd_qty(1);
                                holder.qty.setText("" + data.get(position).getAdd_qty());
                                AddItems();
                                if (language.equalsIgnoreCase("En")) {
                                    holder.max_item_can_be_selected.setText("Maximum " + data.get(position).getMaxSelection());
                                } else {
                                    holder.max_item_can_be_selected.setText( " الحد الأقصى " + data.get(position).getMaxSelection());
                                }
                            }

                            Log.i("TAG", "onCheckedChanged: ");
                        }

                    }

                } else {

                    if (AdditionalActivity.max_add_selection.contains(data.get(position).getAddtionalName_en())) {
                        AdditionalActivity.max_add_selection.remove(data.get(position).getAddtionalName_en());
                    }

                    sub_item_chk = false;
                    holder.plus.setClickable(false);

                    holder.img.setButtonDrawable(R.drawable.additional_unselected);

                    holder.mcount.setVisibility(View.GONE);

                    data.get(position).setAdd_qty(0);

                    AddItems();
                    holder.qty.setText("" + data.get(position).getAdd_qty());

                }

            }
        });

        holder.plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int localCount = data.get(position).getAdd_qty();

                if (localCount < data.get(position).getMaxSelection()) {

                    data.get(position).setAdd_qty((localCount + 1));
                    holder.qty.setText("" + data.get(position).getAdd_qty());

                    AddItems();
                    Log.i("TAG", "add: ");

                }

                else if (localCount == data.get(position).getMaxSelection()) {

                    if (language.equalsIgnoreCase("En")) {
                        Constants.showOneButtonAlertDialog("You are done with maximum " + data.get(position).getMaxSelection() + " " + data.get(position).getAddtionalName_en() + ".", context.getResources().getString(R.string.app_name), "Ok", (Activity) context);
                    } else {
                        Constants.showOneButtonAlertDialog(""+ "."+   data.get(position).getAddtionalName_Ar() + " " + data.get(position).getMaxSelection() + "لقد انتهيت من الحد الأقصى " , context.getResources().getString(R.string.app_name_ar), context.getResources().getString(R.string.ok_ar), (Activity) context);
                    }

                }
            }
        });


        holder.mins.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Integer.parseInt(holder.qty.getText().toString()) > 1) {

                    int localCount1 = data.get(position).getAdd_qty();

                    data.get(position).setAdd_qty((localCount1 - 1));

                    holder.qty.setText("" + data.get(position).getAdd_qty());

                    AddItems();

                } else {

                    holder.img.setButtonDrawable(R.drawable.additional_unselected);
                    holder.mcount.setVisibility(View.GONE);
//                    int localCount1 = data.get(position).getAdd_qty();
                    data.get(position).setAdd_qty(0);
                    AddItems();

                }

            }
        });

//        notifyDataSetChanged();
        holder.mcount.setVisibility(View.GONE);


        return convertView;
    }

    private void AddItems() {

        float addlPrice = 0, mainPrice = 0;
        for (int i = 0; i < data.size(); i++) {

            if (data.get(i).getAdd_qty() > 0) {

                addlPrice = addlPrice + (data.get(i).getAddprice().get(0).getAddprice() * data.get(i).getAdd_qty());

            }

        }

        addlPrice = item_add_price + addlPrice;
        mainPrice = addlPrice * AdditionalActivity.Qty;

        suborderPrice = addlPrice;

        Log.i("TAG", "Addlprice: " + addlPrice);
        Log.i("TAG", "mainQty: " + AdditionalActivity.Qty);
        Log.i("TAG", "mainPrice: " + addlPrice * AdditionalActivity.Qty);

        if (language.equalsIgnoreCase("En")) {

            AdditionalActivity.price.setText("" + Constants.decimalFormat.format(addlPrice));
            AdditionalActivity.total_price.setText("" + Constants.decimalFormat.format(mainPrice));
        } else {
            AdditionalActivity.price.setText("" + Constants.decimalFormat.format(addlPrice) );
            AdditionalActivity.total_price.setText("" + Constants.decimalFormat.format(mainPrice));
        }
    }
}