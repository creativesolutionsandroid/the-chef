package com.cs.chef.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cs.chef.Activities.AdditionalActivity;
import com.cs.chef.Activities.MenuActivity;
import com.cs.chef.Constants;
import com.cs.chef.Models.Items;
import com.cs.chef.R;

import java.util.ArrayList;
import java.util.List;

public class MenuSubItemsAdapter extends RecyclerView.Adapter<MenuSubItemsAdapter.ViewHolder> {

    ArrayList<Items> item = new ArrayList<>();

    private LayoutInflater mInflater;
    String language;
    public static ImageView itemImage;
    Activity parentActivity;
    public Context context;
    ViewPager viewpager;
    boolean dropdown = false;
    List<TextView> cardViewList = new ArrayList<>();


    public MenuSubItemsAdapter(Context context, ArrayList<Items> item, String language) {
        this.mInflater = LayoutInflater.from(context);
        this.item = item;
//        this.hotelname_Ar = hotelname_Ar;
        this.parentActivity = parentActivity;
        this.context = context;
        this.language = language;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        Log.i("TAG", "curst size1 " + orderList.size());
        View view = null;
        if (language.equalsIgnoreCase("En")) {
        view = mInflater.inflate(R.layout.menu_sub_item_adapter, parent, false);
        } else if (language.equalsIgnoreCase("Ar")) {
            view = mInflater.inflate(R.layout.menu_sub_item_adapter_arabic, parent, false);
        }
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        if (language.equalsIgnoreCase("En")) {
            holder.item_name.setText("" + item.get(position).getItem_name_en());
            holder.item_desc.setText("" + item.get(position).getItem_desc_en());
            holder.item_price.setText("SAR " + Constants.decimalFormat.format(item.get(position).getItem_price()));
        } else {
            holder.item_name.setText("" + item.get(position).getItem_name_ar());
            holder.item_desc.setText("" + item.get(position).getItem_desc_ar());
            holder.item_price.setText("" + Constants.decimalFormat.format(item.get(position).getItem_price())+ " " + context.getResources().getString(R.string.price_format_ar));
        }

        Glide.with(context).load(Constants.ITEM_IMAGE_URL + item.get(position).getItem_image()).into(holder.item_img);

    }


    @Override
    public int getItemCount() {
        return item.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        LinearLayout layout;
        TextView item_name, item_desc, item_price;
//        Spinner spinner;

        ImageView item_img;

        public ViewHolder(View itemView) {
            super(itemView);
//            Log.i("TAG", "curst size4 " + orderList.size());

            item_name = itemView.findViewById(R.id.item_name);
            item_desc = itemView.findViewById(R.id.item_desc);
            item_img = itemView.findViewById(R.id.item_img);
            item_price = itemView.findViewById(R.id.item_price);
            layout = itemView.findViewById(R.id.layout);
//            curstImage = itemView.findViewById(R.id.curst_img_layout);

//            layout.setOnClickListener(this);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent a = new Intent(context, AdditionalActivity.class);
                    if (MenuActivity.banner) {
                        a.putExtra("branchid", MenuActivity.storeData1.get(MenuActivity.pos).getBranchId());
                    } else {
                        a.putExtra("branchid", MenuActivity.storeData.get(MenuActivity.pos).getBranchId());
                    }
//                    Log.i("TAG", "onClick: " + item_id);
                    a.putExtra("itemid", item.get(getAdapterPosition()).getItemid());
                    a.putExtra("itemdecs", item.get(getAdapterPosition()).getItem_desc_en());
                    a.putExtra("itemdecs_ar", item.get(getAdapterPosition()).getItem_desc_ar());
                    a.putExtra("itemimage", item.get(getAdapterPosition()).getItem_image());
                    context.startActivity(a);

                }
            });
        }

//        @Override
//        public void onClick(View v) {
//
//
//
//            int pos = getAdapterPosition();
//
//            for (int i = 0; i < galleryArrayList.size(); i++) {
//                if (galleryArrayList.get(i).getHotalnameEn().contains(hotelname_En.get(pos))) {
//
//                    if (!city_name_en.contains(galleryArrayList.get(i).getCityNameEn())) {
//                        city_name_en.add(galleryArrayList.get(i).getCityNameEn());
//                    }
//
//                }
//            }
//
//            Log.d("TAG", "adapterpos: " + pos);
//
//            if (hotelname_En.get(pos).contains(hotelname_En.get(pos))) {
//
//                spinner.setBackgroundResource(R.drawable.gallery_footer_selected_bg);
//
//            }
//
//
//        }
    }
}
