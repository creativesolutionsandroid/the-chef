package com.cs.chef.Adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.cs.chef.Models.Promotions;
import com.cs.chef.R;

import java.util.ArrayList;

public class PromotionDays extends RecyclerView.Adapter<PromotionDays.ViewHolder> {

    ArrayList<Promotions.PromotionDays> promodays = new ArrayList<>();

    private LayoutInflater mInflater;
    String language;
    public static ImageView itemImage;
    Activity parentActivity;
    public Context context;
    //    String day_of_week;
    ArrayList<String> weekdays = new ArrayList<>();

    public PromotionDays(Context context, ArrayList<Promotions.PromotionDays> promodays, ArrayList<String> day_of_week) {
        this.mInflater = LayoutInflater.from(context);
        this.promodays = promodays;
        this.weekdays = day_of_week;
//        this.hotelname_Ar = hotelname_Ar;
        this.parentActivity = parentActivity;
        this.context = context;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        Log.i("TAG", "curst size1 " + orderList.size());
        View view = null;
//        if (language.equalsIgnoreCase("En")) {
        view = mInflater.inflate(R.layout.promotions_days, parent, false);
//        } else if (language.equalsIgnoreCase("Ar")) {
//            view = mInflater.inflate(R.layout.curst_list, parent, false);
//        }
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        holder.promotion_days.setText("" + weekdays.get(position));

        boolean sunday = false, monday = false, tuesday = false, Wednesday = false, Thursday = false, friday = false, Saturday = false;

        for (int i = 0; i < promodays.size(); i++) {

                if (promodays.get(i).getDayName().equalsIgnoreCase("Sunday")) {

                    sunday = true;

                }


                if (promodays.get(i).getDayName().equalsIgnoreCase("Monday")) {

                    monday = true;

                }

                if (promodays.get(i).getDayName().equalsIgnoreCase("Tuesday")) {

                    tuesday = true;

                }


                if (promodays.get(i).getDayName().equalsIgnoreCase("Wednesday")) {

                    Wednesday = true;

                }


                if (promodays.get(i).getDayName().equalsIgnoreCase("Thursday")) {

                    Thursday = true;

                }


                if (promodays.get(i).getDayName().equalsIgnoreCase("Friday")) {

                    friday = true;

                }



                if (promodays.get(i).getDayName().equalsIgnoreCase("Saturday")) {

                    Saturday = true;

                }

        }


        for (int i = 0; i < promodays.size(); i++) {
            if (position == 0) {

                if (sunday) {

                    holder.promotion_days.setBackground(context.getResources().getDrawable(R.drawable.promotions_days_circle));

                } else {

                    holder.promotion_days.setBackground(context.getResources().getDrawable(R.drawable.promotions_days_circle_red));

                }


            }
            if (position == 1) {

                if (monday) {

                    holder.promotion_days.setBackground(context.getResources().getDrawable(R.drawable.promotions_days_circle));

                } else {

                    holder.promotion_days.setBackground(context.getResources().getDrawable(R.drawable.promotions_days_circle_red));

                }

            }
            if (position == 2) {

                if (tuesday) {

                    holder.promotion_days.setBackground(context.getResources().getDrawable(R.drawable.promotions_days_circle));

                } else {

                    holder.promotion_days.setBackground(context.getResources().getDrawable(R.drawable.promotions_days_circle_red));

                }


            }
            if (position == 3) {

                if (Wednesday) {

                    holder.promotion_days.setBackground(context.getResources().getDrawable(R.drawable.promotions_days_circle));

                } else {

                    holder.promotion_days.setBackground(context.getResources().getDrawable(R.drawable.promotions_days_circle_red));

                }


            }
            if (position == 4) {

                if (Thursday) {

                    holder.promotion_days.setBackground(context.getResources().getDrawable(R.drawable.promotions_days_circle));

                } else {

                    holder.promotion_days.setBackground(context.getResources().getDrawable(R.drawable.promotions_days_circle_red));

                }


            }
            if (position == 5) {

                if (friday) {

                    holder.promotion_days.setBackground(context.getResources().getDrawable(R.drawable.promotions_days_circle));

                } else {

                    holder.promotion_days.setBackground(context.getResources().getDrawable(R.drawable.promotions_days_circle_red));

                }


            }
            if (position == 6) {

                if (Saturday) {

                    holder.promotion_days.setBackground(context.getResources().getDrawable(R.drawable.promotions_days_circle));

                } else {

                    holder.promotion_days.setBackground(context.getResources().getDrawable(R.drawable.promotions_days_circle_red));

                }

            }
        }
//        Log.i("TAG", "onBindViewHolder: " + promodays.get(position).getDayName());


    }


    @Override
    public int getItemCount() {
        return weekdays.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView promotion_days;

        public ViewHolder(View itemView) {
            super(itemView);

            promotion_days = itemView.findViewById(R.id.promo_days);

        }

    }
}
