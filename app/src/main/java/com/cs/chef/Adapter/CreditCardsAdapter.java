package com.cs.chef.Adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.JsonReader;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.chef.Activities.ManageCards;
import com.cs.chef.Constants;
import com.cs.chef.Models.DeleteCardList;
import com.cs.chef.Models.MyCards;
import com.cs.chef.Models.SaveCardToDB;
import com.cs.chef.Models.SavedCards;
import com.cs.chef.R;
import com.cs.chef.Rest.APIInterface;
import com.cs.chef.Rest.ApiClient;
import com.cs.chef.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by SKT on 20-02-2016.
 */
public class CreditCardsAdapter extends BaseAdapter {
    public Context context;
    public LayoutInflater inflater;
    String language;
    List<SavedCards.Data> myGiftsList = new ArrayList<>();
    int pos;
    Activity activity;
    String userId;
    String id;

    public CreditCardsAdapter(Context context, List<SavedCards.Data> myGiftsList, String language, Activity activity, String userId) {
        this.context = context;
        this.myGiftsList = myGiftsList;
        this.activity = activity;
        this.language = language;
        this.userId = userId;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        //DBcontroller = new DatabaseHandler(context);

        //imageLoader=new ImageLoader(context.getApplicationContext());
    }

    public int getCount() {
        return myGiftsList.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    public static class ViewHolder {
        TextView holderName, CardNumber, expiryDate;
        ImageView cardType, cardDelete;

    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        pos = position;
        if (convertView == null) {
            holder = new ViewHolder();
            if(language.equalsIgnoreCase("En")) {
                convertView = inflater.inflate(R.layout.list_manage_card, null);
            }
            else{
                convertView = inflater.inflate(R.layout.list_manage_card_arabic, null);
            }

            holder.holderName = (TextView) convertView
                    .findViewById(R.id.name_on_card);
            holder.CardNumber = (TextView) convertView
                    .findViewById(R.id.card_number);
            holder.expiryDate = (TextView) convertView
                    .findViewById(R.id.card_expiry);
            holder.cardType = (ImageView) convertView.findViewById(R.id.card_type);
            holder.cardDelete = (ImageView) convertView.findViewById(R.id.delete_card);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.holderName.setText(myGiftsList.get(position).getCardHolder());
        holder.CardNumber.setText("**** **** **** "+myGiftsList.get(position).getLast4Digits());
        holder.expiryDate.setText(myGiftsList.get(position).getExpireMonth()+"/"+myGiftsList.get(position).getExpireYear());

        if(myGiftsList.get(position).getCardBrand().equalsIgnoreCase("visa")){
            holder.cardType.setImageDrawable(context.getResources().getDrawable(R.drawable.visa));
        }
        else {
            holder.cardType.setImageDrawable(context.getResources().getDrawable(R.drawable.master));
        }

        holder.cardDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pos = position;

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                String title= "", msg= "", posBtn = "", negBtn = "";
                if(language.equalsIgnoreCase("En")){
                    posBtn = "Yes";
                    negBtn = "No";
                    title = context.getResources().getString(R.string.app_name);
                    msg = "Are you sure to delete the card?";
                }else if(language.equalsIgnoreCase("Ar")){
                    posBtn = "نعم";
                    negBtn = "لا";
                    title = context.getResources().getString(R.string.app_name_ar);
                    msg = "هل انت متأكد من رغبتك في حذف البطاقة ؟";
                }
                // set title
                alertDialogBuilder.setTitle(title);

                // set dialog message
                alertDialogBuilder
                        .setMessage(msg)
                        .setCancelable(false)
                        .setPositiveButton(posBtn, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                                new deleteCardApi().execute(myGiftsList.get(position).getToken(), ""+position);
                            }
                        }).setNegativeButton(negBtn, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();

            }
        });

        return convertView;
    }

    private class deleteCardApi extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Constants.showLoadingDialog(activity);
        }

        @Override
        protected String doInBackground(final String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(context);
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            JSONObject parentObj = new JSONObject();
            try {
                parentObj.put("userId", userId);
                parentObj.put("registrationId", params[0]);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.i("TAG", "doInBackground: " + parentObj.toString());

            Call<DeleteCardList> call = apiService.deleteCard(
                    RequestBody.create(MediaType.parse("application/json"), parentObj.toString()));

            call.enqueue(new Callback<DeleteCardList>() {
                @Override
                public void onResponse(Call<DeleteCardList> call, Response<DeleteCardList> response) {
                    Log.d("TAG", "onResponse: " + response);
                    if (response.isSuccessful()) {
                        DeleteCardList orderItems = response.body();
                        try {
                            if (orderItems.getStatus()) {
                                Log.d("TAG", "onResponse: true ");
                                myGiftsList.remove(Integer.parseInt(params[1]));
                                notifyDataSetChanged();
                            } else {
                                //                          status false case
                                if (language.equalsIgnoreCase("En")) {
                                    String failureResponse = orderItems.getMessage();
                                    Constants.showOneButtonAlertDialog(failureResponse, context.getResources().getString(R.string.app_name),
                                            context.getResources().getString(R.string.ok), activity);
                                } else {
                                    String failureResponse = orderItems.getMessage();
                                    Constants.showOneButtonAlertDialog(failureResponse, context.getResources().getString(R.string.app_name_ar),
                                            context.getResources().getString(R.string.ok_ar), activity);
                                }
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                            if (language.equalsIgnoreCase("En")) {
                                Toast.makeText(activity, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(activity, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                            }
                            Log.i("TAG", "catch: ");
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(activity, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(activity, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                        Log.i("TAG", "responce: ");
                    }

                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<DeleteCardList> call, Throwable t) {
                    Log.d("TAG", "onFailure: " + t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(context, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(context, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(context, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(context, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }

                    Constants.closeLoadingDialog();
                }
            });
            return null;
        }
    }
}