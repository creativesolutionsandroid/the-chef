package com.cs.chef.Adapter;

import android.content.Context;
import android.graphics.Paint;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cs.chef.Activities.AdditionalActivity;
import com.cs.chef.Activities.MenuActivity;
import com.cs.chef.Constants;
import com.cs.chef.DataBaseHelper;
import com.cs.chef.Models.Order;
import com.cs.chef.Models.SubAdditionals;
import com.cs.chef.R;

import java.util.ArrayList;

import static com.cs.chef.Activities.AdditionalActivity.OrderPrice;
import static com.cs.chef.Activities.AdditionalActivity.Qty;
import static com.cs.chef.Activities.AdditionalActivity.boxpos;
import static com.cs.chef.Activities.AdditionalActivity.itemQty;
import static com.cs.chef.Activities.AdditionalActivity.itemfinalQty;
import static com.cs.chef.Activities.AdditionalActivity.itemfinalprice;
import static com.cs.chef.Activities.AdditionalActivity.msubadapter;
import static com.cs.chef.Activities.AdditionalActivity.sub_additional;
import static com.cs.chef.Activities.AdditionalActivity.sub_additional_items;
import static com.cs.chef.Activities.AdditionalActivity.subitemdata;

public class SubAdditionalAdapter extends BaseAdapter {


    public Context context;
    public LayoutInflater inflater;
    ArrayList<SubAdditionals.ItemSizePrice> data = new ArrayList<>();
    ArrayList<Order> orderlist = new ArrayList<>();
    DataBaseHelper myDbHelper;
    int pos;
    public static int sub_item_pos = 0;
    String id;
    String language;
    //public ImageLoader imageLoader;
    public static Double lat, longi;
    boolean checkbox = true;

    private int selectedCheckBoxPosition = -1;

    public int getSelectedCheckBoxPosition() {
        return selectedCheckBoxPosition;
    }

    public SubAdditionalAdapter(Context context, ArrayList<SubAdditionals.ItemSizePrice> data, String language) {
        this.context = context;
        this.data = data;
        this.language = language;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        this.language = language;
        //DBcontroller = new DatabaseHandler(context);

        //imageLoader=new ImageLoader(context.getApplicationContext());
    }

    public int getCount() {
        return data.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    public static class ViewHolder {
        CheckBox img;
        TextView maddition, mtotal_price, mdiscount_price;
        RelativeLayout msub_additionals_layout;
        LinearLayout mcount;

    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        pos = position;
        if (convertView == null) {
            holder = new ViewHolder();
            if (language.equalsIgnoreCase("En")) {
                convertView = inflater.inflate(R.layout.sub_item_size_additionals, null);
            } else {
                convertView = inflater.inflate(R.layout.sub_item_size_additionals_arabic, null);
            }

            holder.msub_additionals_layout = convertView
                    .findViewById(R.id.sub_additionals_layout);
            holder.img = convertView
                    .findViewById(R.id.img);
            holder.maddition = convertView
                    .findViewById(R.id.additional);
            holder.mtotal_price = convertView
                    .findViewById(R.id.total_price);
            holder.mdiscount_price = convertView
                    .findViewById(R.id.discount_price);
            holder.mcount = convertView
                    .findViewById(R.id.count);

            myDbHelper = new DataBaseHelper(context);
            orderlist = myDbHelper.getOrderInfo();

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.mcount.setVisibility(View.GONE);
        if (language.equalsIgnoreCase("En")) {
            holder.maddition.setText("" + data.get(position).getItemSizeName_En());
        } else {
            holder.maddition.setText("" + data.get(position).getItemSizeName_Ar());
        }

//        int dis_count = 30;
        if (language.equalsIgnoreCase("En")) {
            if (MenuActivity.banner) {
                if (MenuActivity.storeData1.get(MenuActivity.pos).getDiscountAmt() == 0) {
                    double itemprice;
                    itemprice = data.get(position).getItemprice();
                    holder.mtotal_price.setText("" + Constants.decimalFormat.format(itemprice));
                    holder.mdiscount_price.setVisibility(View.GONE);
                    RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) holder.mtotal_price.getLayoutParams();
                    params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                    params.addRule(RelativeLayout.CENTER_VERTICAL);
                    holder.mtotal_price.setLayoutParams(params); //causes layout update
                } else {
                    holder.mdiscount_price.setVisibility(View.VISIBLE);
                    double itemprice;
                    itemprice = data.get(position).getItemprice();
                    holder.mtotal_price.setText("" + Constants.decimalFormat.format(itemprice));
                    holder.mtotal_price.setPaintFlags(holder.mtotal_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                    float discount_value = data.get(position).getItemprice() * MenuActivity.storeData1.get(MenuActivity.pos).getDiscountAmt() / 100;
                    float discount_amount = data.get(position).getItemprice() - discount_value;
                    holder.mdiscount_price.setText("" + Constants.decimalFormat.format(discount_amount));
                }
            } else {
                if (MenuActivity.storeData.get(MenuActivity.pos).getDiscountAmt() == 0) {
                    double itemprice;
                    itemprice = data.get(position).getItemprice();
                    holder.mtotal_price.setText("" + Constants.decimalFormat.format(itemprice));
                    holder.mdiscount_price.setVisibility(View.GONE);
                    RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) holder.mtotal_price.getLayoutParams();
                    params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                    params.addRule(RelativeLayout.CENTER_VERTICAL);
                    holder.mtotal_price.setLayoutParams(params); //causes layout update
                } else {
                    holder.mdiscount_price.setVisibility(View.VISIBLE);
                    double itemprice;
                    itemprice = data.get(position).getItemprice();
                    holder.mtotal_price.setText("" + Constants.decimalFormat.format(itemprice));
                    holder.mtotal_price.setPaintFlags(holder.mtotal_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                    float discount_value = data.get(position).getItemprice() * MenuActivity.storeData.get(MenuActivity.pos).getDiscountAmt() / 100;
                    float discount_amount = data.get(position).getItemprice() - discount_value;
                    holder.mdiscount_price.setText("" + Constants.decimalFormat.format(discount_amount));
                }
            }
        } else {
            if (MenuActivity.banner) {
                if (MenuActivity.storeData1.get(MenuActivity.pos).getDiscountAmt() == 0) {
                    double itemprice;
                    itemprice = data.get(position).getItemprice();
                    holder.mtotal_price.setText("" + Constants.decimalFormat.format(itemprice));
                    holder.mdiscount_price.setVisibility(View.GONE);
                    RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) holder.mtotal_price.getLayoutParams();
//                    params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                    params.addRule(RelativeLayout.CENTER_VERTICAL);
                    holder.mtotal_price.setLayoutParams(params); //causes layout update
                } else {
                    holder.mdiscount_price.setVisibility(View.VISIBLE);
                    double itemprice;
                    itemprice = data.get(position).getItemprice();
                    holder.mtotal_price.setText("" + Constants.decimalFormat.format(itemprice));
                    holder.mtotal_price.setPaintFlags(holder.mtotal_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                    float discount_value = data.get(position).getItemprice() * MenuActivity.storeData1.get(MenuActivity.pos).getDiscountAmt() / 100;
                    float discount_amount = data.get(position).getItemprice() - discount_value;
                    holder.mdiscount_price.setText("" + Constants.decimalFormat.format(discount_amount));
                }
            } else {
                if (MenuActivity.storeData.get(MenuActivity.pos).getDiscountAmt() == 0) {
                    double itemprice;
                    itemprice = data.get(position).getItemprice();
                    holder.mtotal_price.setText("" + Constants.decimalFormat.format(itemprice));
                    holder.mdiscount_price.setVisibility(View.GONE);
                    RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) holder.mtotal_price.getLayoutParams();
//                    params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                    params.addRule(RelativeLayout.CENTER_VERTICAL);
                    holder.mtotal_price.setLayoutParams(params); //causes layout update
                } else {
                    holder.mdiscount_price.setVisibility(View.VISIBLE);
                    double itemprice;
                    itemprice = data.get(position).getItemprice();
                    holder.mtotal_price.setText("" + Constants.decimalFormat.format(itemprice));
                    holder.mtotal_price.setPaintFlags(holder.mtotal_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                    float discount_value = data.get(position).getItemprice() * MenuActivity.storeData.get(MenuActivity.pos).getDiscountAmt() / 100;
                    float discount_amount = data.get(position).getItemprice() - discount_value;
                    holder.mdiscount_price.setText("" + Constants.decimalFormat.format(discount_amount));
                }
            }
        }


//        String[] mStringArray = new String[data.size()];
////        mStringArray = data.toArray(mStringArray);
//
//        final String[] finalMStringArray = mStringArray;
//        holder.msub_additionals_layout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                holder.img.setChecked(Boolean.parseBoolean(finalMStringArray[position]));
//                holder.img.setOnClickListener(new View.OnClickListener() {
//
//                    @Override
//                    public void onClick(View v) {
//                        for (int i=0;i<finalMStringArray.length;i++){
//                            if(i==position){
//                                finalMStringArray[i]= String.valueOf(true);
//                            }else{
//                                finalMStringArray[i]= String.valueOf(false);
//                            }
//                        }
//                        notifyDataSetChanged();
//                    }
//                });
//
//            }
//        });

//        if (orderlist.size() != 0) {
//            for (int i = 0; i < orderlist.size(); i++) {
//
//                if (Integer.valueOf(orderlist.get(i).getItemId()) == AdditionalActivity.itemid) {
//
//                    if (Integer.valueOf(orderlist.get(i).getItemTypeId()) == data.get(position).getPriceId()) {
//
//                        holder.img.setChecked(true);
//
//                        if (position == selectedCheckBoxPosition) {
//                            holder.img.setChecked(true);
//                            Log.d("TAG", "checkbox CHECKED at pos: " + position);
//                        } else {
//                            holder.img.setChecked(false);
//                            Log.d("TAG", "checkbox UNCHECKED at pos: " + position);
//                        }
//
//                        if (MenuActivity.banner) {
//                            if (MenuActivity.storeData1.get(MenuActivity.pos).getDiscountAmt() == 0) {
//
//                                AdditionalActivity.OrderPrice = Float.parseFloat(orderlist.get(i).getTotalAmount());
//                                AdditionalActivity.Qty = Integer.parseInt(orderlist.get(i).getQty());
//
//                            } else {
//
//                                float discount_value = data.get(sub_item_pos).getItemprice() * MenuActivity.storeData1.get(MenuActivity.pos).getDiscountAmt() / 100;
//                                float discount_amount = data.get(sub_item_pos).getItemprice() - discount_value;
//
//                                AdditionalActivity.OrderPrice = Float.parseFloat(orderlist.get(i).getTotalAmount());
//                                AdditionalActivity.Qty = Integer.parseInt(orderlist.get(i).getQty());
//
//                            }
//                        } else {
//                            if (MenuActivity.storeData.get(MenuActivity.pos).getDiscountAmt() == 0) {
//
//                                AdditionalActivity.OrderPrice = Float.parseFloat(orderlist.get(i).getTotalAmount());
//                                AdditionalActivity.Qty = Integer.parseInt(orderlist.get(i).getQty());
//
//                            } else {
//                                float discount_value = data.get(sub_item_pos).getItemprice() * MenuActivity.storeData.get(MenuActivity.pos).getDiscountAmt() / 100;
//                                float discount_amount = data.get(sub_item_pos).getItemprice() - discount_value;
//
//                                AdditionalActivity.OrderPrice = Float.parseFloat(orderlist.get(i).getTotalAmount());
//                                AdditionalActivity.Qty = Integer.parseInt(orderlist.get(i).getQty());
//                            }
//                        }
//
//
//                        Log.d("TAG", "onClick() - set selectedCheckBoxPosition = " + position);
//                        if (data.get(sub_item_pos).getAddgrp().size() == 0) {
//                            sub_additional.setVisibility(View.GONE);
//
//                            AdditionalActivity.qty.setText("" + Qty);
//                            AdditionalActivity.price.setText("" + Constants.decimalFormat.format(OrderPrice) + " SR");
//
//                        } else {
//                            sub_additional.setVisibility(View.VISIBLE);
//                            AdditionalActivity.qty.setText("" + Qty);
//                            AdditionalActivity.price.setText("" + Constants.decimalFormat.format(OrderPrice) + " SR");
//                            for (int k = 0; k < data.get(sub_item_pos).getAddgrp().size(); k++) {
//                                AdditionalActivity.sub_additionals_name.setText("" + data.get(sub_item_pos).getAddgrp().get(k).getAddGrpName_En());
//                                AdditionalActivity.mmax_add_to_be_selected.setText("Maximum " + data.get(sub_item_pos).getAddgrp().get(k).getMaxSelection());
//
//                                try {
//                                    SubAdditionalAdapter1.Qty = Integer.parseInt(orderlist.get(i).getAdditionalitemQTY());
//                                    SubAdditionalAdapter1.suborderPrice = Float.parseFloat(orderlist.get(i).getAdditionalsPrice()) * Float.parseFloat(orderlist.get(i).getAdditionalitemQTY());
//                                    boxpos.clear();
//                                    itemQty.clear();
//                                    itemfinalprice.clear();
//                                    itemfinalQty.clear();
//                                    AdditionalActivity.sub_add_items.clear();
//                                } catch (Exception e) {
//                                    e.printStackTrace();
//                                }
//
//                                for (int m = 0; m < data.get(sub_item_pos).getAddgrp().get(k).getAdditems().size(); m++) {
//                                    AdditionalActivity.sub_add_items.add(0);
//                                }
//
//                                subitemdata = data.get(sub_item_pos).getAddgrp().get(k).getAdditems();
//                                msubadapter = new SubAdditionalAdapter1(context, subitemdata);
//                                sub_additional_items.setAdapter(msubadapter);
//
//                            }
//                        }
//                        notifyDataSetChanged();
//
//                        holder.img.setOnClickListener(new View.OnClickListener() {
//                            @Override
//                            public void onClick(View view) {
//                                selectedCheckBoxPosition = position;
//                                sub_item_pos = position;
//
//                                Log.i("TAG", "itemprice: " + data.get(sub_item_pos).getItemprice());
//
//
//                                if (MenuActivity.banner) {
//                                    if (MenuActivity.storeData1.get(MenuActivity.pos).getDiscountAmt() == 0) {
//
//                                        AdditionalActivity.OrderPrice = data.get(sub_item_pos).getItemprice();
//                                        AdditionalActivity.Qty = 1;
//
//                                    } else {
//                                        float discount_value = data.get(sub_item_pos).getItemprice() * MenuActivity.storeData1.get(MenuActivity.pos).getDiscountAmt() / 100;
//                                        float discount_amount = data.get(sub_item_pos).getItemprice() - discount_value;
//
//                                        AdditionalActivity.OrderPrice = discount_amount;
//                                        AdditionalActivity.Qty = 1;
//                                    }
//                                } else {
//                                    if (MenuActivity.storeData.get(MenuActivity.pos).getDiscountAmt() == 0) {
//
//                                        AdditionalActivity.OrderPrice = data.get(sub_item_pos).getItemprice();
//                                        AdditionalActivity.Qty = 1;
//
//                                    } else {
//                                        float discount_value = data.get(sub_item_pos).getItemprice() * MenuActivity.storeData.get(MenuActivity.pos).getDiscountAmt() / 100;
//                                        float discount_amount = data.get(sub_item_pos).getItemprice() - discount_value;
//
//                                        AdditionalActivity.OrderPrice = discount_amount;
//                                        AdditionalActivity.Qty = 1;
//                                    }
//                                }
//
//
//                                Log.d("TAG", "onClick() - set selectedCheckBoxPosition = " + position);
//                                if (data.get(sub_item_pos).getAddgrp().size() == 0) {
//                                    sub_additional.setVisibility(View.GONE);
//
//                                    AdditionalActivity.qty.setText("" + Qty);
//                                    AdditionalActivity.price.setText("" + Constants.decimalFormat.format(OrderPrice) + " SR");
//
//                                } else {
//                                    sub_additional.setVisibility(View.VISIBLE);
//                                    AdditionalActivity.qty.setText("" + Qty);
//                                    AdditionalActivity.price.setText("" + Constants.decimalFormat.format(OrderPrice) + " SR");
//                                    for (int k = 0; k < data.get(sub_item_pos).getAddgrp().size(); k++) {
//                                        AdditionalActivity.sub_additionals_name.setText("" + data.get(sub_item_pos).getAddgrp().get(k).getAddGrpName_En());
//                                        AdditionalActivity.mmax_add_to_be_selected.setText("Maximum " + data.get(sub_item_pos).getAddgrp().get(k).getMaxSelection());
//
//                                        try {
//                                            SubAdditionalAdapter1.Qty = 0;
//                                            SubAdditionalAdapter1.suborderPrice = 0;
//                                            boxpos.clear();
//                                            itemQty.clear();
//                                            itemfinalprice.clear();
//                                            itemfinalQty.clear();
//                                            AdditionalActivity.sub_add_items.clear();
//                                        } catch (Exception e) {
//                                            e.printStackTrace();
//                                        }
//
//                                        for (int m = 0; m < data.get(sub_item_pos).getAddgrp().get(k).getAdditems().size(); m++) {
//                                            AdditionalActivity.sub_add_items.add(0);
//                                        }
//
//                                        subitemdata = data.get(sub_item_pos).getAddgrp().get(k).getAdditems();
//                                        msubadapter = new SubAdditionalAdapter1(context, subitemdata);
//                                        sub_additional_items.setAdapter(msubadapter);
//
//                                    }
//                                }
//                                notifyDataSetChanged();
//                            }
//                        });
//
//                    }
//                }
//
//            }
//        } else {
//
//        }


        if (position == selectedCheckBoxPosition) {
            holder.img.setChecked(true);
            Log.d("TAG", "checkbox CHECKED at pos: " + position);
        } else {
            holder.img.setChecked(false);
            Log.d("TAG", "checkbox UNCHECKED at pos: " + position);
        }

        //viewHolder.checkBox.setText("sample text");

        if (checkbox) {

            if (data.get(position).getItemSizeName_En().equals(data.get(0).getItemSizeName_En())) {

                selectedCheckBoxPosition = position;
                holder.img.setChecked(true);
                checkbox = false;

                sub_item_pos = position;

                Log.i("TAG", "itemprice: " + data.get(sub_item_pos).getItemprice());


                if (MenuActivity.banner) {
                    if (MenuActivity.storeData1.get(MenuActivity.pos).getDiscountAmt() == 0) {

                        AdditionalActivity.OrderPrice = data.get(sub_item_pos).getItemprice();
                        AdditionalActivity.MainOrderPrice = data.get(sub_item_pos).getItemprice();
                        AdditionalActivity.item_add_price = data.get(sub_item_pos).getItemprice();
                        AdditionalActivity.dis_item_add_price = data.get(sub_item_pos).getItemprice();
                        AdditionalActivity.Qty = 1;

                    } else {
                        float discount_value = data.get(sub_item_pos).getItemprice() * MenuActivity.storeData1.get(MenuActivity.pos).getDiscountAmt() / 100;
                        float discount_amount = data.get(sub_item_pos).getItemprice() - discount_value;

                        AdditionalActivity.OrderPrice = discount_amount;
                        AdditionalActivity.MainOrderPrice = data.get(sub_item_pos).getItemprice();
                        AdditionalActivity.item_add_price = data.get(sub_item_pos).getItemprice();
                        AdditionalActivity.dis_item_add_price = discount_amount;
                        AdditionalActivity.Qty = 1;
                    }
                } else {
                    if (MenuActivity.storeData.get(MenuActivity.pos).getDiscountAmt() == 0) {

                        AdditionalActivity.OrderPrice = data.get(sub_item_pos).getItemprice();
                        AdditionalActivity.MainOrderPrice = data.get(sub_item_pos).getItemprice();
                        AdditionalActivity.item_add_price = data.get(sub_item_pos).getItemprice();
                        AdditionalActivity.dis_item_add_price = data.get(sub_item_pos).getItemprice();
                        AdditionalActivity.Qty = 1;

                    } else {
                        float discount_value = data.get(sub_item_pos).getItemprice() * MenuActivity.storeData.get(MenuActivity.pos).getDiscountAmt() / 100;
                        float discount_amount = data.get(sub_item_pos).getItemprice() - discount_value;

                        AdditionalActivity.OrderPrice = discount_amount;
                        AdditionalActivity.MainOrderPrice = data.get(sub_item_pos).getItemprice();
                        AdditionalActivity.item_add_price = data.get(sub_item_pos).getItemprice();
                        AdditionalActivity.dis_item_add_price = discount_amount;
                        AdditionalActivity.Qty = 1;
                    }
                }

                Log.d("TAG", "onClick() - set selectedCheckBoxPosition = " + position);
                if (data.get(sub_item_pos).getAddgrp().size() == 0) {
                    sub_additional.setVisibility(View.GONE);

                    AdditionalActivity.qty.setText("" + Qty);
//                    if (language.equalsIgnoreCase("En")) {
                        AdditionalActivity.price.setText("" + Constants.decimalFormat.format(OrderPrice));
                        AdditionalActivity.total_price.setText("" + Constants.decimalFormat.format(OrderPrice));
//                    } else {
//                        AdditionalActivity.price.setText("" + Constants.decimalFormat.format(OrderPrice) + " " + context.getResources().getString(R.string.price_format_ar));
//                        AdditionalActivity.total_price.setText("" + Constants.decimalFormat.format(OrderPrice) + " " + context.getResources().getString(R.string.price_format_ar));
//                    }

                } else {
                    sub_additional.setVisibility(View.VISIBLE);
                    AdditionalActivity.qty.setText("" + Qty);
//                    if (language.equalsIgnoreCase("En")) {
                        AdditionalActivity.price.setText("" + Constants.decimalFormat.format(OrderPrice));
                        AdditionalActivity.total_price.setText("" + Constants.decimalFormat.format(OrderPrice));
//                    } else {
//                        AdditionalActivity.price.setText("" + Constants.decimalFormat.format(OrderPrice) + " " + context.getResources().getString(R.string.price_format_ar));
//                        AdditionalActivity.total_price.setText("" + Constants.decimalFormat.format(OrderPrice) + " " + context.getResources().getString(R.string.price_format_ar));
//                    }
                    for (int k = 0; k < data.get(sub_item_pos).getAddgrp().size(); k++) {
                        if (language.equalsIgnoreCase("En")) {
                            AdditionalActivity.sub_additionals_name.setText("" + data.get(sub_item_pos).getAddgrp().get(k).getAddGrpName_En());
                            AdditionalActivity.mmax_add_to_be_selected.setText("Maximum " + data.get(sub_item_pos).getAddgrp().get(k).getMaxSelection());
                        } else {
                            AdditionalActivity.sub_additionals_name.setText("" + data.get(sub_item_pos).getAddgrp().get(k).getAddGrpName_Ar());
                            AdditionalActivity.mmax_add_to_be_selected.setText(" الحد الأقصى " + data.get(sub_item_pos).getAddgrp().get(k).getMaxSelection() );
                        }

                        try {
//                            SubAdditionalAdapter2.Qty = 0;
                            SubAdditionalAdapter2.suborderPrice = 0;
                            SubAdditionalAdapter2.mainsuborderPrice = 0;
                            boxpos.clear();
                            itemQty.clear();
                            itemfinalprice.clear();
                            itemfinalQty.clear();
                            AdditionalActivity.sub_add_items.clear();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        for (int m = 0; m < data.get(sub_item_pos).getAddgrp().get(k).getAdditems().size(); m++) {
                            AdditionalActivity.sub_add_items.add(0);
                        }

                        subitemdata = data.get(sub_item_pos).getAddgrp().get(k).getAdditems();
                        msubadapter = new SubAdditionalAdapter2(context, subitemdata, language);
                        sub_additional_items.setAdapter(msubadapter);
                        AdditionalActivity.ListUtils.setDynamicHeight(sub_additional_items);

                    }
                }
                notifyDataSetChanged();

            }
        } else {

            holder.img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    selectedCheckBoxPosition = position;
                    sub_item_pos = position;

                    Log.i("TAG", "itemprice: " + data.get(sub_item_pos).getItemprice());


                    if (MenuActivity.banner) {
                        if (MenuActivity.storeData1.get(MenuActivity.pos).getDiscountAmt() == 0) {

                            AdditionalActivity.OrderPrice = data.get(sub_item_pos).getItemprice();
                            AdditionalActivity.MainOrderPrice = data.get(sub_item_pos).getItemprice();
                            AdditionalActivity.item_add_price = data.get(sub_item_pos).getItemprice();
                            AdditionalActivity.dis_item_add_price = data.get(sub_item_pos).getItemprice();
                            AdditionalActivity.Qty = 1;

                        } else {
                            float discount_value = data.get(sub_item_pos).getItemprice() * MenuActivity.storeData1.get(MenuActivity.pos).getDiscountAmt() / 100;
                            float discount_amount = data.get(sub_item_pos).getItemprice() - discount_value;

                            AdditionalActivity.OrderPrice = discount_amount;
                            AdditionalActivity.MainOrderPrice = data.get(sub_item_pos).getItemprice();
                            AdditionalActivity.item_add_price = data.get(sub_item_pos).getItemprice();
                            AdditionalActivity.dis_item_add_price = discount_amount;
                            AdditionalActivity.Qty = 1;
                        }
                    } else {
                        if (MenuActivity.storeData.get(MenuActivity.pos).getDiscountAmt() == 0) {

                            AdditionalActivity.OrderPrice = data.get(sub_item_pos).getItemprice();
                            AdditionalActivity.MainOrderPrice = data.get(sub_item_pos).getItemprice();
                            AdditionalActivity.item_add_price = data.get(sub_item_pos).getItemprice();
                            AdditionalActivity.dis_item_add_price = data.get(sub_item_pos).getItemprice();
                            AdditionalActivity.Qty = 1;

                        } else {
                            float discount_value = data.get(sub_item_pos).getItemprice() * MenuActivity.storeData.get(MenuActivity.pos).getDiscountAmt() / 100;
                            float discount_amount = data.get(sub_item_pos).getItemprice() - discount_value;

                            AdditionalActivity.OrderPrice = discount_amount;
                            AdditionalActivity.MainOrderPrice = data.get(sub_item_pos).getItemprice();
                            AdditionalActivity.item_add_price = data.get(sub_item_pos).getItemprice();
                            AdditionalActivity.dis_item_add_price = discount_amount;
                            AdditionalActivity.Qty = 1;
                        }
                    }


                    Log.d("TAG", "onClick() - set selectedCheckBoxPosition = " + position);
                    if (data.get(sub_item_pos).getAddgrp().size() == 0) {
                        sub_additional.setVisibility(View.GONE);

                        AdditionalActivity.qty.setText("" + Qty);
//                        if (language.equalsIgnoreCase("En")) {
                            AdditionalActivity.price.setText("" + Constants.decimalFormat.format(OrderPrice));
                            AdditionalActivity.total_price.setText("" + Constants.decimalFormat.format(OrderPrice));
//                        } else {
//                            AdditionalActivity.price.setText("" + Constants.decimalFormat.format(OrderPrice) + " " + context.getResources().getString(R.string.price_format_ar));
//                            AdditionalActivity.total_price.setText("" + Constants.decimalFormat.format(OrderPrice) + " " + context.getResources().getString(R.string.price_format_ar));
//                        }
                    } else {
                        sub_additional.setVisibility(View.VISIBLE);
                        AdditionalActivity.qty.setText("" + Qty);
//                        if (language.equalsIgnoreCase("En")) {
                            AdditionalActivity.price.setText("" + Constants.decimalFormat.format(OrderPrice));
                            AdditionalActivity.total_price.setText("" + Constants.decimalFormat.format(OrderPrice));
//                        } else {
//                            AdditionalActivity.price.setText("" + Constants.decimalFormat.format(OrderPrice) + " " + context.getResources().getString(R.string.price_format_ar));
//                            AdditionalActivity.total_price.setText("" + Constants.decimalFormat.format(OrderPrice) + " " + context.getResources().getString(R.string.price_format_ar));
//                        }
                        for (int k = 0; k < data.get(sub_item_pos).getAddgrp().size(); k++) {
                            if (language.equalsIgnoreCase("En")) {
                                AdditionalActivity.sub_additionals_name.setText("" + data.get(sub_item_pos).getAddgrp().get(k).getAddGrpName_En());
                                AdditionalActivity.mmax_add_to_be_selected.setText("Maximum " + data.get(sub_item_pos).getAddgrp().get(k).getMaxSelection());
                            } else {
                                AdditionalActivity.sub_additionals_name.setText("" + data.get(sub_item_pos).getAddgrp().get(k).getAddGrpName_Ar());
                                AdditionalActivity.mmax_add_to_be_selected.setText(" الحد الأقصى " + data.get(sub_item_pos).getAddgrp().get(k).getMaxSelection() );
                            }

                            try {
//                                SubAdditionalAdapter2.Qty = 0;
                                SubAdditionalAdapter2.suborderPrice = 0;
                                SubAdditionalAdapter2.mainsuborderPrice = 0;
                                boxpos.clear();
                                itemQty.clear();
                                itemfinalprice.clear();
                                itemfinalQty.clear();
                                AdditionalActivity.sub_add_items.clear();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            for (int m = 0; m < data.get(sub_item_pos).getAddgrp().get(k).getAdditems().size(); m++) {
                                AdditionalActivity.sub_add_items.add(0);
                            }

                            subitemdata = data.get(sub_item_pos).getAddgrp().get(k).getAdditems();
                            msubadapter = new SubAdditionalAdapter2(context, subitemdata, language);
                            sub_additional_items.setAdapter(msubadapter);
                            AdditionalActivity.ListUtils.setDynamicHeight(sub_additional_items);

                        }
                    }
                    notifyDataSetChanged();
                }
            });
        }

        return convertView;
    }
}
