package com.cs.chef.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cs.chef.Activities.CheckOutActivity;
import com.cs.chef.Constants;
import com.cs.chef.DataBaseHelper;
import com.cs.chef.Models.Order;
import com.cs.chef.R;

import java.util.ArrayList;

import static com.cs.chef.Activities.CheckOutActivity.coupon_discount;
import static com.cs.chef.Activities.CheckOutActivity.discount;
import static com.cs.chef.Activities.CheckOutActivity.discount_type;
import static com.cs.chef.Activities.CheckOutActivity.empty_card;
import static com.cs.chef.Activities.CheckOutActivity.max_spending;
import static com.cs.chef.Activities.CheckOutActivity.min_order_amount;
import static com.cs.chef.Activities.CheckOutActivity.min_spending;
import static com.cs.chef.Activities.CheckOutActivity.procced;
import static com.cs.chef.Activities.CheckOutActivity.servie_charges;
import static com.cs.chef.Activities.CheckOutActivity.store_avaiable;
import static com.cs.chef.Activities.CheckOutActivity.sub_total;
import static com.cs.chef.Activities.CheckOutActivity.tax;
import static com.cs.chef.Activities.CheckOutActivity.total_price;
import static com.cs.chef.Activities.MainActivity.navigation;
import static com.cs.chef.Activities.MainActivity.removeBadge;
import static com.cs.chef.Activities.MainActivity.showBadge;


public class CheckoutItemAdapter extends BaseAdapter {

    public Context context;
    public LayoutInflater inflater;
    ArrayList<Order> data = new ArrayList<>();
    int pos;
    public static String subscriptions = "no";
    String id;
    String language;
    //public ImageLoader imageLoader;
    public static Double lat, longi;
    private DataBaseHelper myDbHelper;
    int qty;
    float price, main_price;
    int maindiscount;


    public CheckoutItemAdapter(Context context, ArrayList<Order> data, String language) {
        this.context = context;
        this.data = data;
        this.language = language;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        this.language = language;
        //DBcontroller = new DatabaseHandler(context);

        //imageLoader=new ImageLoader(context.getApplicationContext());
        myDbHelper = new DataBaseHelper(context);
    }

    public int getCount() {
        return data.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    public static class ViewHolder {
        TextView item_name, mins, qty, plus, price, disprice;
        ImageView item_image;
        LinearLayout price_layout, dis_price_layout;

    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        pos = position;
        if (convertView == null) {
            holder = new ViewHolder();
            if (language.equalsIgnoreCase("En")) {
                convertView = inflater.inflate(R.layout.card_item, null);
            } else {
                convertView = inflater.inflate(R.layout.card_item_arabic, null);
            }


            holder.item_name = (TextView) convertView
                    .findViewById(R.id.item_name);
            holder.mins = (TextView) convertView
                    .findViewById(R.id.mins);
            holder.qty = (TextView) convertView
                    .findViewById(R.id.qty);
            holder.plus = (TextView) convertView
                    .findViewById(R.id.plus);
            holder.price = (TextView) convertView
                    .findViewById(R.id.price);
            holder.item_image = (ImageView) convertView
                    .findViewById(R.id.item_image);
            holder.disprice = (TextView) convertView
                    .findViewById(R.id.dis_price);
            holder.dis_price_layout = (LinearLayout) convertView
                    .findViewById(R.id.dis_price_layout);
            holder.price_layout = (LinearLayout) convertView
                    .findViewById(R.id.price_layout);


            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

//        if (StoreMenuActivity.banner) {
        if (language.equalsIgnoreCase("En")) {
            if (Integer.parseInt(data.get(position).getDiscount()) == 0) {
                double itemprice;

                itemprice = Double.parseDouble(data.get(position).getTotalAmount());
                holder.price.setText(" " + Constants.decimalFormat.format(itemprice));
                holder.dis_price_layout.setVisibility(View.GONE);
                RelativeLayout.LayoutParams layoutParams =
                        (RelativeLayout.LayoutParams) holder.price_layout.getLayoutParams();
                layoutParams.addRule(RelativeLayout.CENTER_VERTICAL, RelativeLayout.TRUE);
                layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
                holder.price_layout.setLayoutParams(layoutParams);
                holder.price.setTextSize(16);

            } else {
                holder.dis_price_layout.setVisibility(View.VISIBLE);
                double itemprice;
                itemprice = Double.parseDouble(data.get(position).getTotalAmount());
                holder.disprice.setText(" " + Constants.decimalFormat.format(itemprice));
                holder.price.setPaintFlags(holder.price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
//                float mainprice = Float.parseFloat(data.get(position).getPrice()) * myDbHelper.getItemOrderCount(data.get(position).getOrderId());
                double mainitemprice;
                mainitemprice = Double.parseDouble(data.get(position).getMainTotalAmount());
                holder.price.setText(" " + Constants.decimalFormat.format(mainitemprice));
                holder.price.setTextSize(14);
                holder.price.setTextColor(Color.parseColor("#787878"));
            }
        } else {
            if (Integer.parseInt(data.get(position).getDiscount()) == 0) {
                double itemprice;

                itemprice = Double.parseDouble(data.get(position).getTotalAmount());
                holder.price.setText(" " + Constants.decimalFormat.format(itemprice));
                holder.dis_price_layout.setVisibility(View.GONE);
//                RelativeLayout.LayoutParams layoutParams =
//                        (RelativeLayout.LayoutParams) holder.price_layout.getLayoutParams();
//                layoutParams.addRule(RelativeLayout.CENTER_VERTICAL, RelativeLayout.TRUE);
//                layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
//                holder.price_layout.setLayoutParams(layoutParams);
                holder.price.setTextSize(16);

            } else {
                holder.dis_price_layout.setVisibility(View.VISIBLE);
                double itemprice;
                itemprice = Double.parseDouble(data.get(position).getTotalAmount());
                holder.disprice.setText(" " + Constants.decimalFormat.format(itemprice));
                holder.price.setPaintFlags(holder.price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
//                float mainprice = Float.parseFloat(data.get(position).getPrice()) * myDbHelper.getItemOrderCount(data.get(position).getOrderId());
                double mainitemprice;
                mainitemprice = Double.parseDouble(data.get(position).getMainTotalAmount());
                holder.price.setText(" " + Constants.decimalFormat.format(mainitemprice));
                holder.price.setTextSize(14);
                holder.price.setTextColor(Color.parseColor("#787878"));
            }
        }
//        } else {
//            if (StoreMenuActivity.storeData.get(StoreMenuActivity.pos).getDiscountAmt() == 0) {
//                double itemprice;
//                itemprice = Double.parseDouble(data.get(position).getTotalAmount());
//                holder.price.setText("SAR " + Constants.decimalFormat.format(itemprice));
//                holder.dis_price_layout.setVisibility(View.GONE);
//
//            } else {
//                holder.dis_price_layout.setVisibility(View.VISIBLE);
//                double itemprice;
//                itemprice = Double.parseDouble(data.get(position).getTotalAmount());
//                holder.price.setText("SAR " + Constants.decimalFormat.format(itemprice));
//                holder.price.setPaintFlags(holder.price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
//                float discount_value = Float.valueOf(data.get(position).getTotalAmount()) * StoreMenuActivity.storeData.get(StoreMenuActivity.pos).getDiscountAmt() / 100;
//                float discount_amount = Float.valueOf(data.get(position).getTotalAmount()) - discount_value;
//                holder.disprice.setText("SAR " + Constants.decimalFormat.format(discount_amount));
//            }
//        }


        if (language.equalsIgnoreCase("En")) {
            holder.item_name.setText("" + data.get(position).getItemName());
        } else {
            holder.item_name.setText("" + data.get(position).getItemNameAr());
        }

        Glide.with(context).load(Constants.ITEM_IMAGE_URL + data.get(position).getItem_image()).into(holder.item_image);

        holder.qty.setText("" + data.get(position).getQty());

        maindiscount = Integer.parseInt(data.get(position).getDiscount());

        holder.plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("TAG", "getView: " + data.get(position).getQty());
                Log.i("TAG", "onClick: " + position);
                qty = Integer.parseInt(data.get(position).getQty()) + 1;

                if (Integer.parseInt(data.get(position).getDiscount()) == 0) {

                    price = Float.parseFloat(data.get(position).getPrice_with_disc()) + Float.parseFloat(data.get(position).getTotalAmount());
                    main_price = Float.parseFloat(data.get(position).getMain_item_add_price()) + Float.parseFloat(data.get(position).getMainTotalAmount());
                    holder.dis_price_layout.setVisibility(View.GONE);

                } else {
                    holder.dis_price_layout.setVisibility(View.VISIBLE);

                    price = Float.parseFloat(data.get(position).getPrice_with_disc()) + Float.parseFloat(data.get(position).getTotalAmount());
                    main_price = Float.parseFloat(data.get(position).getMain_item_add_price()) + Float.parseFloat(data.get(position).getMainTotalAmount());

                }

//                }
                myDbHelper.updateOrder(String.valueOf(qty), String.valueOf(price), data.get(position).getOrderId());
                myDbHelper.updateMainOrder(String.valueOf(qty), String.valueOf(main_price), data.get(position).getOrderId());
                data = myDbHelper.getOrderInfo();
                notifyDataSetChanged();

                try {
                    CheckOutActivity.item_total.setText("" + Constants.decimalFormat.format(myDbHelper.getTotalOrderPrice()));

//                    float maindiscount_value = myDbHelper.getMainTotalOrderPrice() * Float.parseFloat(data.get(position).getDiscount()) / 100;

//                    CheckOutActivity.offer_discount.setText("- " + Constants.decimalFormat.format(maindiscount_value));

                    tax = myDbHelper.getTotalOrderPrice() * Constants.vatper;
                    if (language.equalsIgnoreCase("En")) {
                        CheckOutActivity.mvat.setText("+ " + Constants.decimalFormat.format(tax));
                    }else {
                        CheckOutActivity.mvat.setText("" + Constants.decimalFormat.format(tax) + " +");
                    }
                    float subtotal;
                    subtotal = (myDbHelper.getTotalOrderPrice() + tax);
                    sub_total.setText("" + Constants.decimalFormat.format(subtotal));


                    Log.i("TAG", "tax1: " + myDbHelper.getTotalOrderPrice() * Constants.vatper);

//                    float discount_value = myDbHelper.getTotalOrderPrice() * ((float) discount / 100);
//            float discount_amount = myDbHelper.getTotalOrderPrice() - discount_value;

//                    Log.i("TAG", "discount: " + discount_value);

//                    CheckOutActivity.coupon_discount.setText("- " + Constants.decimalFormat.format(discount_value));

                    if (discount != 0) {

                        boolean promotion = false, min_orders = false, promotion1 = false, min_orders1 = false;

                        if (data.size() != 0) {
                            if (Float.parseFloat(data.get(position).getMin_amount()) < (myDbHelper.getTotalOrderPrice() + tax)) {
                                Log.i("TAG", "min_amount1: " + Float.parseFloat(data.get(position).getMin_amount()));
                                min_orders = true;

                            } else {

                                min_orders1 = true;
                                Log.i("TAG", "min_amount2: " + Float.parseFloat(data.get(position).getMin_amount()));

                            }
                        }

                        if ((myDbHelper.getTotalOrderPrice() + tax) >= min_spending) {

                            promotion = true;


                        } else {

                            promotion1 = true;

                        }


                        if (promotion && min_orders) {

                            min_order_amount.setVisibility(View.GONE);

                            procced.setBackground(context.getResources().getDrawable(R.drawable.child_selected));
                            procced.setClickable(true);

                        } else if (min_orders) {

                            min_order_amount.setVisibility(View.GONE);

                            procced.setBackground(context.getResources().getDrawable(R.drawable.child_selected));
                            procced.setClickable(true);

                        } else if (promotion) {

                            min_order_amount.setVisibility(View.GONE);

                            procced.setBackground(context.getResources().getDrawable(R.drawable.child_selected));
                            procced.setClickable(true);

                        }

                        if (min_orders1 && promotion1 && store_avaiable) {

                            min_order_amount.setVisibility(View.VISIBLE);
                            double min_order = Double.parseDouble(data.get(0).getMin_amount());
                            min_order_amount.setText("Online Orders not Avaiable");

                            procced.setBackground(context.getResources().getDrawable(R.drawable.check_out_unselected));
                            procced.setClickable(false);

                        } else if (store_avaiable){

                            min_order_amount.setVisibility(View.VISIBLE);
                            double min_order = Double.parseDouble(data.get(0).getMin_amount());
                            min_order_amount.setText("Online Orders not Avaiable");

                            procced.setBackground(context.getResources().getDrawable(R.drawable.check_out_unselected));
                            procced.setClickable(false);

                        } else if (min_orders1) {

                            min_order_amount.setVisibility(View.VISIBLE);
                            double min_order = Double.parseDouble(data.get(position).getMin_amount());
                            min_order_amount.setText("Subtotal must be greater than SAR " + Constants.decimalFormat.format(min_order));

                            procced.setBackground(context.getResources().getDrawable(R.drawable.check_out_unselected));
                            procced.setClickable(false);

                        } else if (promotion1) {

                            min_order_amount.setVisibility(View.VISIBLE);
                            min_order_amount.setText("Promotion applied, cart amount must be greater than " + min_spending + " SAR");

                            procced.setBackground(context.getResources().getDrawable(R.drawable.check_out_unselected));
                            procced.setClickable(false);
                        }


//                        if ((myDbHelper.getTotalOrderPrice() + tax) >= min_spending){
//
//                            min_order_amount.setVisibility(View.GONE);
//
//                            procced.setBackground(context.getResources().getDrawable(R.drawable.menu_header_selected));
//                            procced.setClickable(true);
//
//                        } else {
//
//                            min_order_amount.setVisibility(View.VISIBLE);
//                            min_order_amount.setText("Promotion applied, cart amount must be greater than " + min_spending + " SAR");
//
//                            procced.setBackground(context.getResources().getDrawable(R.drawable.check_out_unselected));
//                            procced.setClickable(false);
//
//                        }

                        if (discount_type == 1) {


                            if ((myDbHelper.getTotalOrderPrice() + tax) < discount) {

                                discount = (myDbHelper.getTotalOrderPrice() + tax);

                            } else {

                                discount = Constants.discount;

                            }

                            if (language.equalsIgnoreCase("En")) {

                                coupon_discount.setText("- " + Constants.decimalFormat.format(discount));
                            } else {
                                coupon_discount.setText("" + Constants.decimalFormat.format(discount) + " -");
                            }

                            float coupon_discount_price1 = (myDbHelper.getTotalOrderPrice() + tax) - discount;

                            if (coupon_discount_price1 > max_spending) {

                                total_price = (myDbHelper.getTotalOrderPrice() + tax) - max_spending;
                                float delivery_charges;
                                if (language.equalsIgnoreCase("En")) {
                                    delivery_charges = Float.parseFloat(servie_charges.getText().toString().replace("+ ", ""));
                                } else {
                                    delivery_charges = Float.parseFloat(servie_charges.getText().toString().replace(" +", ""));
                                }
                                CheckOutActivity.mtotal_price.setText("" + Constants.decimalFormat.format(total_price + delivery_charges));

                            } else {

                                total_price = (myDbHelper.getTotalOrderPrice() + tax) - discount;
                                float delivery_charges;
                                if (language.equalsIgnoreCase("En")) {
                                    delivery_charges = Float.parseFloat(servie_charges.getText().toString().replace("+ ", ""));
                                } else {
                                    delivery_charges = Float.parseFloat(servie_charges.getText().toString().replace(" +", ""));
                                }
                                CheckOutActivity.mtotal_price.setText("" + Constants.decimalFormat.format(total_price + delivery_charges));

                            }

                        } else {

                            float discount_value = (myDbHelper.getTotalOrderPrice() + tax) * ((float) discount / 100);
//            float discount_amount = myDbHelper.getTotalOrderPrice() - discount_value;

                            Log.i("TAG", "discount: " + discount_value);

                            if ((myDbHelper.getTotalOrderPrice() + tax) < discount_value) {

                                discount_value = (myDbHelper.getTotalOrderPrice() + tax);

                            } else {

                                discount_value = (myDbHelper.getTotalOrderPrice() + tax) * ((float) discount / 100);

                            }

                            if (language.equalsIgnoreCase("En")) {
                                coupon_discount.setText("- " + Constants.decimalFormat.format(discount_value));
                            } else {
                                coupon_discount.setText("" + Constants.decimalFormat.format(discount_value) + " -");
                            }

                            float coupon_discount_price1 = (myDbHelper.getTotalOrderPrice() + tax) - discount_value;

                            if (coupon_discount_price1 > max_spending) {

                                total_price = (myDbHelper.getTotalOrderPrice() + tax) - max_spending;
                                float delivery_charges;
                                if (language.equalsIgnoreCase("En")) {
                                    delivery_charges = Float.parseFloat(servie_charges.getText().toString().replace("+ ", ""));
                                } else {
                                    delivery_charges = Float.parseFloat(servie_charges.getText().toString().replace(" +", ""));
                                }
                                CheckOutActivity.mtotal_price.setText("" + Constants.decimalFormat.format(total_price + delivery_charges));

                            } else {

                                total_price = (myDbHelper.getTotalOrderPrice() + tax) - discount_value;
                                float delivery_charges;
                                if (language.equalsIgnoreCase("En")) {
                                    delivery_charges = Float.parseFloat(servie_charges.getText().toString().replace("+ ", ""));
                                } else {
                                    delivery_charges = Float.parseFloat(servie_charges.getText().toString().replace(" +", ""));
                                }
                                CheckOutActivity.mtotal_price.setText("" + Constants.decimalFormat.format(total_price + delivery_charges));

                            }

                        }
                    } else {

                        if (language.equalsIgnoreCase("En")) {
                            coupon_discount.setText("- " + Constants.decimalFormat.format(discount));
                        } else {
                            coupon_discount.setText("" + Constants.decimalFormat.format(discount) + " -");
                        }

                        total_price = (myDbHelper.getTotalOrderPrice() + tax) - discount;
                        float delivery_charges;
                        if (language.equalsIgnoreCase("En")) {
                            delivery_charges = Float.parseFloat(servie_charges.getText().toString().replace("+ ", ""));
                        } else {
                            delivery_charges = Float.parseFloat(servie_charges.getText().toString().replace(" +", ""));
                        }
                        CheckOutActivity.mtotal_price.setText("" + Constants.decimalFormat.format(total_price + delivery_charges));

                    }

//                    CheckOutActivity.final_qty.setText("" + myDbHelper.getTotalOrderQty() + " item");

//                    CheckOutActivity.final_price.setText("SAR " + Constants.decimalFormat.format(total_price));

//                    CheckOutActivity.proceed_to_pay.setBackgroundColor(Color.parseColor("#013950"));
//                    CheckOutActivity.proceed_to_pay.setClickable(true);
//                    min_order_amount.setVisibility(View.GONE);
//                    if (total_price <  min_order_charges){
//
//                        CheckOutActivity.proceed_to_pay.setClickable(false);
//                        CheckOutActivity.proceed_to_pay.setBackgroundColor(Color.parseColor("#778899"));
//                        min_order_amount.setVisibility(View.VISIBLE);
//                        min_order_amount.setText("Subtotal must be greater than SAR " + min_order_charges);
//                    }

                } catch (NullPointerException e) {

                    e.printStackTrace();

                }
            }
        });


        holder.mins.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                qty = Integer.parseInt(data.get(position).getQty()) - 1;
//                }
                if (qty == 0) {
                    myDbHelper.deleteItemFromOrder(data.get(position).getOrderId());


//                } else {
//                    if (orderList.get(position).getCategoryId().equals("4")) {
//                        price = (Float.parseFloat(orderList.get(position).getTotalAmount())) - (Float.parseFloat(orderList.get(position).getPrice()) * 5);
                } else {

                    if (Integer.parseInt(data.get(position).getDiscount()) == 0) {

                        price = Float.parseFloat(data.get(position).getTotalAmount()) - Float.parseFloat(data.get(position).getPrice_with_disc());
                        main_price = Float.parseFloat(data.get(position).getMainTotalAmount()) - Float.parseFloat(data.get(position).getMain_item_add_price());
                        holder.dis_price_layout.setVisibility(View.GONE);

                    } else {
                        holder.dis_price_layout.setVisibility(View.VISIBLE);

                        price = Float.parseFloat(data.get(position).getTotalAmount()) - Float.parseFloat(data.get(position).getPrice_with_disc());
                        main_price = Float.parseFloat(data.get(position).getMainTotalAmount()) - Float.parseFloat(data.get(position).getMain_item_add_price());

                    }

//                }
                    myDbHelper.updateOrder(String.valueOf(qty), String.valueOf(price), data.get(position).getOrderId());
                    myDbHelper.updateMainOrder(String.valueOf(qty), String.valueOf(main_price), data.get(position).getOrderId());
                    float delivery_charges;
                    if (language.equalsIgnoreCase("En")) {
                        delivery_charges = Float.parseFloat(servie_charges.getText().toString().replace("+ ", ""));
                    } else {
                        delivery_charges = Float.parseFloat(servie_charges.getText().toString().replace(" +", ""));
                    }
                    CheckOutActivity.mtotal_price.setText("" + Constants.decimalFormat.format(myDbHelper.getTotalOrderPrice()+ delivery_charges) );

                }


                data = myDbHelper.getOrderInfo();

                notifyDataSetChanged();

                try {
                    CheckOutActivity.item_total.setText("" + Constants.decimalFormat.format(myDbHelper.getTotalOrderPrice()));
//                    float maindiscount_value = myDbHelper.getMainTotalOrderPrice() * maindiscount / 100;

//                    CheckOutActivity.offer_discount.setText("- " + Constants.decimalFormat.format(maindiscount_value));

                    tax = myDbHelper.getTotalOrderPrice() * Constants.vatper;
                    if (language.equalsIgnoreCase("En")) {
                        CheckOutActivity.mvat.setText("+ " + Constants.decimalFormat.format(tax));
                    }else {
                        CheckOutActivity.mvat.setText("" + Constants.decimalFormat.format(tax) + " +");
                    }
                    float subtotal;
                    subtotal = (myDbHelper.getTotalOrderPrice() + tax);
                    sub_total.setText("" + Constants.decimalFormat.format(subtotal));


                    Log.i("TAG", "tax1: " + myDbHelper.getTotalOrderPrice() * Constants.vatper);

//                    float discount_value = myDbHelper.getTotalOrderPrice() * ((float) discount / 100);
//            float discount_amount = myDbHelper.getTotalOrderPrice() - discount_value;

//                    Log.i("TAG", "discount: " + discount_value);

//                    CheckOutActivity.coupon_discount.setText("- " + Constants.decimalFormat.format(discount_value));

                    if (discount != 0) {

                        boolean promotion = false, min_orders = false, promotion1 = false, min_orders1 = false;

                        if (data.size() != 0) {
                            if (Float.parseFloat(data.get(position).getMin_amount()) < (myDbHelper.getTotalOrderPrice() + tax)) {
                                Log.i("TAG", "min_amount1: " + Float.parseFloat(data.get(position).getMin_amount()));
                                min_orders = true;

                            } else {

                                min_orders1 = true;
                                Log.i("TAG", "min_amount2: " + Float.parseFloat(data.get(position).getMin_amount()));

                            }
                        }

                        if ((myDbHelper.getTotalOrderPrice() + tax) >= min_spending) {

                            promotion = true;


                        } else {

                            promotion1 = true;

                        }


                        if (promotion && min_orders) {

                            min_order_amount.setVisibility(View.GONE);

                            procced.setBackground(context.getResources().getDrawable(R.drawable.child_selected));
                            procced.setClickable(true);

                        } else if (min_orders) {

                            min_order_amount.setVisibility(View.GONE);

                            procced.setBackground(context.getResources().getDrawable(R.drawable.child_selected));
                            procced.setClickable(true);

                        } else if (promotion) {

                            min_order_amount.setVisibility(View.GONE);

                            procced.setBackground(context.getResources().getDrawable(R.drawable.child_selected));
                            procced.setClickable(true);

                        }

                        if (min_orders1 && promotion1 && store_avaiable) {

                            min_order_amount.setVisibility(View.VISIBLE);
                            double min_order = Double.parseDouble(data.get(0).getMin_amount());
                            min_order_amount.setText("Online Orders not Avaiable");

                            procced.setBackground(context.getResources().getDrawable(R.drawable.check_out_unselected));
                            procced.setClickable(false);

                        } else if (store_avaiable){

                            min_order_amount.setVisibility(View.VISIBLE);
                            double min_order = Double.parseDouble(data.get(0).getMin_amount());
                            min_order_amount.setText("Online Orders not Avaiable");

                            procced.setBackground(context.getResources().getDrawable(R.drawable.check_out_unselected));
                            procced.setClickable(false);

                        } else if (min_orders1) {

                            min_order_amount.setVisibility(View.VISIBLE);
                            double min_order = Double.parseDouble(data.get(position).getMin_amount());
                            min_order_amount.setText("Subtotal must be greater than SAR " + Constants.decimalFormat.format(min_order));

                            procced.setBackground(context.getResources().getDrawable(R.drawable.check_out_unselected));
                            procced.setClickable(false);

                        } else if (promotion1) {

                            min_order_amount.setVisibility(View.VISIBLE);
                            min_order_amount.setText("Promotion applied, cart amount must be greater than " + min_spending + " SAR");

                            procced.setBackground(context.getResources().getDrawable(R.drawable.check_out_unselected));
                            procced.setClickable(false);
                        }

//                        if ((myDbHelper.getTotalOrderPrice() + tax) >= min_spending){
//
//                            min_order_amount.setVisibility(View.GONE);
//
//                            procced.setBackground(context.getResources().getDrawable(R.drawable.menu_header_selected));
//                            procced.setClickable(true);
//
//                        } else {
//
//                            min_order_amount.setVisibility(View.VISIBLE);
//                            min_order_amount.setText("Promotion applied, cart amount must be greater than " + min_spending + " SAR");
//
//                            procced.setBackground(context.getResources().getDrawable(R.drawable.check_out_unselected));
//                            procced.setClickable(false);
//
//
//                        }

                        if (discount_type == 1) {

                            if ((myDbHelper.getTotalOrderPrice() + tax) < discount) {

                                discount = (myDbHelper.getTotalOrderPrice() + tax);

                            } else {

                                discount = Constants.discount;

                            }

                            if (language.equalsIgnoreCase("En")) {
                                coupon_discount.setText("- " + Constants.decimalFormat.format(discount));
                            } else {
                                coupon_discount.setText("" + Constants.decimalFormat.format(discount) + " -");
                            }

                            float coupon_discount_price1 = (myDbHelper.getTotalOrderPrice() + tax) - discount;

                            if (coupon_discount_price1 > max_spending) {

                                total_price = (myDbHelper.getTotalOrderPrice() + tax) - max_spending;
                                float delivery_charges;
                                if (language.equalsIgnoreCase("En")) {
                                    delivery_charges = Float.parseFloat(servie_charges.getText().toString().replace("+ ", ""));
                                } else {
                                    delivery_charges = Float.parseFloat(servie_charges.getText().toString().replace(" +", ""));
                                }
                                CheckOutActivity.mtotal_price.setText("" + Constants.decimalFormat.format(total_price + delivery_charges));

                            } else {

                                total_price = (myDbHelper.getTotalOrderPrice() + tax) - discount;
                                float delivery_charges;
                                if (language.equalsIgnoreCase("En")) {
                                    delivery_charges = Float.parseFloat(servie_charges.getText().toString().replace("+ ", ""));
                                } else {
                                    delivery_charges = Float.parseFloat(servie_charges.getText().toString().replace(" +", ""));
                                }
                                CheckOutActivity.mtotal_price.setText("" + Constants.decimalFormat.format(total_price + delivery_charges));

                            }

                        } else {

                            float discount_value = (myDbHelper.getTotalOrderPrice() + tax) * ((float) discount / 100);
//            float discount_amount = myDbHelper.getTotalOrderPrice() - discount_value;

                            Log.i("TAG", "discount: " + discount_value);

                            if ((myDbHelper.getTotalOrderPrice() + tax) < discount_value) {

                                discount_value = (myDbHelper.getTotalOrderPrice() + tax);

                            } else {

                                discount_value = (myDbHelper.getTotalOrderPrice() + tax) * ((float) discount / 100);

                            }

                            if (language.equalsIgnoreCase("En")) {
                                coupon_discount.setText("- " + Constants.decimalFormat.format(discount_value));
                            } else {
                                coupon_discount.setText("" + Constants.decimalFormat.format(discount_value) + " -");
                            }

                            float coupon_discount_price1 = (myDbHelper.getTotalOrderPrice() + tax) - discount_value;

                            if (coupon_discount_price1 > max_spending) {

                                total_price = (myDbHelper.getTotalOrderPrice() + tax) - max_spending;
                                float delivery_charges;
                                if (language.equalsIgnoreCase("En")) {
                                    delivery_charges = Float.parseFloat(servie_charges.getText().toString().replace("+ ", ""));
                                } else {
                                    delivery_charges = Float.parseFloat(servie_charges.getText().toString().replace(" +", ""));
                                }
                                CheckOutActivity.mtotal_price.setText("" + Constants.decimalFormat.format(total_price + delivery_charges));

                            } else {

                                total_price = (myDbHelper.getTotalOrderPrice() + tax) - discount_value;
                                float delivery_charges;
                                if (language.equalsIgnoreCase("En")) {
                                    delivery_charges = Float.parseFloat(servie_charges.getText().toString().replace("+ ", ""));
                                } else {
                                    delivery_charges = Float.parseFloat(servie_charges.getText().toString().replace(" +", ""));
                                }
                                CheckOutActivity.mtotal_price.setText("" + Constants.decimalFormat.format(total_price + delivery_charges));

                            }

                        }
                    } else {

                        if (language.equalsIgnoreCase("En")) {
                            coupon_discount.setText("- " + Constants.decimalFormat.format(discount));
                        } else {
                            coupon_discount.setText("" + Constants.decimalFormat.format(discount) + " -");
                        }

                        total_price = (myDbHelper.getTotalOrderPrice() + tax) - discount;
                        float delivery_charges;
                        if (language.equalsIgnoreCase("En")) {
                            delivery_charges = Float.parseFloat(servie_charges.getText().toString().replace("+ ", ""));
                        } else {
                            delivery_charges = Float.parseFloat(servie_charges.getText().toString().replace(" +", ""));
                        }
                        CheckOutActivity.mtotal_price.setText("" + Constants.decimalFormat.format(total_price + delivery_charges));

                    }

//                    CheckOutActivity.final_qty.setText("" + myDbHelper.getTotalOrderQty() + " item");

//                    CheckOutActivity.final_price.setText("SAR " + Constants.decimalFormat.format(total_price));

//                    CheckOutActivity.proceed_to_pay.setBackgroundColor(Color.parseColor("#013950"));
//                    CheckOutActivity.proceed_to_pay.setClickable(true);
//                    min_order_amount.setVisibility(View.GONE);
//                    if (total_price <  min_order_charges){
//
//                        CheckOutActivity.proceed_to_pay.setClickable(false);
//                        CheckOutActivity.proceed_to_pay.setBackgroundColor(Color.parseColor("#778899"));
//                        min_order_amount.setVisibility(View.VISIBLE);
//                        min_order_amount.setText("Subtotal must be greater than SAR " + min_order_charges);
//                    }

                } catch (NullPointerException e) {

                    e.printStackTrace();

                }

                if (data.size() == 0) {
                    empty_card.setVisibility(View.VISIBLE);
                    CheckOutActivity.mcheck_out.setVisibility(View.GONE);
                    discount = 0;
                    discount_type = 0;
                    min_spending = 0;
                    max_spending = 0;
                    Log.i("TAG", "onMin_spend: " + min_spending);
                    Constants.offer_service = false;
                    Constants.offer_selected = false;
                    discount = 0;
                    CheckOutActivity.offer_name = "";
                    CheckOutActivity.offer_desc = "";
                    discount_type = 0;
                    min_spending = 0;
                    max_spending = 0;
                    Menu menu = navigation.getMenu();

                    if (myDbHelper.getTotalOrderQty() == 0) {

                        MenuItem menuItem = menu.getItem(1);
                        removeBadge(navigation, menuItem.getItemId());

                    } else {

                        MenuItem menuItem = menu.getItem(1);
                        showBadge(context, navigation, menuItem.getItemId(), String.valueOf(myDbHelper.getTotalOrderQty()));

                    }
                }

            }
        });


        return convertView;
    }
}
