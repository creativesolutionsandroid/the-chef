package com.cs.chef.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cs.chef.Activities.OrderHistory;
import com.cs.chef.Activities.RatingActivity;
import com.cs.chef.Activities.TrackOrderActivity;
import com.cs.chef.Constants;
import com.cs.chef.Dialogs.TrackListDialog;
import com.cs.chef.Models.OrderHistorySearch;
import com.cs.chef.Models.StoresList;
import com.cs.chef.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class OrderHistoryItemAdapter  extends RecyclerView.Adapter<OrderHistoryItemAdapter.MyViewHolder> {

    public Context context;
    public LayoutInflater inflater;
    ArrayList<OrderHistorySearch> data = new ArrayList<>();
    int pos;
    public static String subscriptions = "no";
    String id;
    String language, userid;


    public OrderHistoryItemAdapter(Context context, ArrayList<OrderHistorySearch> data, String language, String userId){
        this.context = context;
        this.data = data;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.language = language;
        this.userid = userId;
        //DBcontroller = new DatabaseHandler(context);

        //imageLoader=new ImageLoader(context.getApplicationContext());
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;
        if (language.equalsIgnoreCase("En")) {
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.order_history_item, parent, false);
        } else {
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.order_history_item_arabic, parent, false);
        }
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {

        if (language.equalsIgnoreCase("En")) {
            holder.mitem_name.setText("" + data.get(position).getName());
        } else {
            holder.mitem_name.setText("" + data.get(position).getName_ar());
        }

        if (language.equalsIgnoreCase("En")) {
            holder.morder_status.setText("" + data.get(position).getOrder_status());
        } else {
            if (data.get(position).getOrder_status().equalsIgnoreCase("New")) {
                holder.morder_status.setText("جديد");
            } else if (data.get(position).getOrder_status().equalsIgnoreCase("Accept")) {
                holder.morder_status.setText("قبول");
            } else if (data.get(position).getOrder_status().equalsIgnoreCase("Ready")) {
                holder.morder_status.setText("جاهز");
            } else if (data.get(position).getOrder_status().equalsIgnoreCase("Close")) {
                holder.morder_status.setText("مغلق");
            } else if (data.get(position).getOrder_status().equalsIgnoreCase("Cancel")) {
                holder.morder_status.setText("الغاء");
            }
        }


        Glide.with(context)
                .load(Constants.STORE_IMAGE_URL + data.get(position).getImage())
                .into(holder.mitem_img);


        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
        SimpleDateFormat dateFormat1 = new SimpleDateFormat("hh:mm aaa dd-MM-yyyy", Locale.US);

        Date date = null;

        try {

            date = dateFormat.parse(data.get(position).getOrderdate());

        } catch (ParseException e) {
            e.printStackTrace();
        }

        String date1;

        date1 = dateFormat1.format(date);

        holder.mtime_date.setText("" + date1);
        if (language.equalsIgnoreCase("En")) {

            if (data.get(position).getOrder_status().equalsIgnoreCase("Close")) {
                if (data.get(position).getRate() == 0) {
                    holder.mrate.setVisibility(View.VISIBLE);
                    holder.mrating_layout.setVisibility(View.GONE);
                } else {
                    holder.mrate.setVisibility(View.GONE);
                    holder.mrating_layout.setVisibility(View.VISIBLE);
                    Log.i("TAG", "getView: " + data.get(position).getRate());
                    if (data.get(position).getRate() == 1) {
                        Log.i("TAG", "rate: " + data.get(position).getRate());
                        holder.rating1.setImageDrawable(context.getResources().getDrawable(R.drawable.selected_star));
                        holder.rating2.setImageDrawable(context.getResources().getDrawable(R.drawable.unselected_star));
                        holder.rating3.setImageDrawable(context.getResources().getDrawable(R.drawable.unselected_star));
                        holder.rating4.setImageDrawable(context.getResources().getDrawable(R.drawable.unselected_star));
                        holder.rating5.setImageDrawable(context.getResources().getDrawable(R.drawable.unselected_star));
                    } else if (data.get(position).getRate() == 2) {
                        holder.rating1.setImageDrawable(context.getResources().getDrawable(R.drawable.selected_star));
                        holder.rating2.setImageDrawable(context.getResources().getDrawable(R.drawable.selected_star));
                        holder.rating3.setImageDrawable(context.getResources().getDrawable(R.drawable.unselected_star));
                        holder.rating4.setImageDrawable(context.getResources().getDrawable(R.drawable.unselected_star));
                        holder.rating5.setImageDrawable(context.getResources().getDrawable(R.drawable.unselected_star));
                    } else if (data.get(position).getRate() == 3) {
                        holder.rating1.setImageDrawable(context.getResources().getDrawable(R.drawable.selected_star));
                        holder.rating2.setImageDrawable(context.getResources().getDrawable(R.drawable.selected_star));
                        holder.rating3.setImageDrawable(context.getResources().getDrawable(R.drawable.selected_star));
                        holder.rating4.setImageDrawable(context.getResources().getDrawable(R.drawable.unselected_star));
                        holder.rating5.setImageDrawable(context.getResources().getDrawable(R.drawable.unselected_star));
                    } else if (data.get(position).getRate() == 4) {
                        holder.rating1.setImageDrawable(context.getResources().getDrawable(R.drawable.selected_star));
                        holder.rating2.setImageDrawable(context.getResources().getDrawable(R.drawable.selected_star));
                        holder.rating3.setImageDrawable(context.getResources().getDrawable(R.drawable.selected_star));
                        holder.rating4.setImageDrawable(context.getResources().getDrawable(R.drawable.selected_star));
                        holder.rating5.setImageDrawable(context.getResources().getDrawable(R.drawable.unselected_star));
                    } else if (data.get(position).getRate() == 5) {
                        holder.rating1.setImageDrawable(context.getResources().getDrawable(R.drawable.selected_star));
                        holder.rating2.setImageDrawable(context.getResources().getDrawable(R.drawable.selected_star));
                        holder.rating3.setImageDrawable(context.getResources().getDrawable(R.drawable.selected_star));
                        holder.rating4.setImageDrawable(context.getResources().getDrawable(R.drawable.selected_star));
                        holder.rating5.setImageDrawable(context.getResources().getDrawable(R.drawable.selected_star));
                    }
                }
            } else {
                holder.mrate.setVisibility(View.INVISIBLE);
                holder.mrating_layout.setVisibility(View.INVISIBLE);
            }
        } else {
            if (data.get(position).getOrder_status().equalsIgnoreCase("Close")) {
                if (data.get(position).getRate() == 0) {
                    holder.mrate.setVisibility(View.VISIBLE);
                    holder.mrating_layout.setVisibility(View.GONE);
                } else {
                    holder.mrate.setVisibility(View.GONE);
                    holder.mrating_layout.setVisibility(View.VISIBLE);
                    if (data.get(position).getRate() == 1) {
                        holder.rating1.setImageDrawable(context.getResources().getDrawable(R.drawable.unselected_star));
                        holder.rating2.setImageDrawable(context.getResources().getDrawable(R.drawable.unselected_star));
                        holder.rating3.setImageDrawable(context.getResources().getDrawable(R.drawable.unselected_star));
                        holder.rating4.setImageDrawable(context.getResources().getDrawable(R.drawable.unselected_star));
                        holder.rating5.setImageDrawable(context.getResources().getDrawable(R.drawable.selected_star));
                    } else if (data.get(position).getRate() == 2) {
                        holder.rating1.setImageDrawable(context.getResources().getDrawable(R.drawable.unselected_star));
                        holder.rating2.setImageDrawable(context.getResources().getDrawable(R.drawable.unselected_star));
                        holder.rating3.setImageDrawable(context.getResources().getDrawable(R.drawable.unselected_star));
                        holder.rating4.setImageDrawable(context.getResources().getDrawable(R.drawable.selected_star));
                        holder.rating5.setImageDrawable(context.getResources().getDrawable(R.drawable.selected_star));
                    } else if (data.get(position).getRate() == 3) {
                        holder.rating1.setImageDrawable(context.getResources().getDrawable(R.drawable.unselected_star));
                        holder.rating2.setImageDrawable(context.getResources().getDrawable(R.drawable.unselected_star));
                        holder.rating3.setImageDrawable(context.getResources().getDrawable(R.drawable.selected_star));
                        holder.rating4.setImageDrawable(context.getResources().getDrawable(R.drawable.selected_star));
                        holder.rating5.setImageDrawable(context.getResources().getDrawable(R.drawable.selected_star));
                    } else if (data.get(position).getRate() == 4) {
                        holder.rating1.setImageDrawable(context.getResources().getDrawable(R.drawable.unselected_star));
                        holder.rating2.setImageDrawable(context.getResources().getDrawable(R.drawable.selected_star));
                        holder.rating3.setImageDrawable(context.getResources().getDrawable(R.drawable.selected_star));
                        holder.rating4.setImageDrawable(context.getResources().getDrawable(R.drawable.selected_star));
                        holder.rating5.setImageDrawable(context.getResources().getDrawable(R.drawable.selected_star));
                    } else if (data.get(position).getRate() == 5) {
                        holder.rating1.setImageDrawable(context.getResources().getDrawable(R.drawable.selected_star));
                        holder.rating2.setImageDrawable(context.getResources().getDrawable(R.drawable.selected_star));
                        holder.rating3.setImageDrawable(context.getResources().getDrawable(R.drawable.selected_star));
                        holder.rating4.setImageDrawable(context.getResources().getDrawable(R.drawable.selected_star));
                        holder.rating5.setImageDrawable(context.getResources().getDrawable(R.drawable.selected_star));
                    }
                }
            } else {
                holder.mrate.setVisibility(View.INVISIBLE);
                holder.mrating_layout.setVisibility(View.INVISIBLE);
            }
        }

        holder.mrate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                Bundle args = new Bundle();
////                    Log.d("TAG", "storeArrayList "+storesArrayList.size());
////                                            Log.d("TAG", "pos " + getAdapterPosition());
//                args.putInt("UserId", Integer.parseInt(userid));
//                args.putInt("OrderId", data.get(position).getOrderid());
//                args.putFloat("rating", 0);
//                String comment = "";
//                if (comment == null) {
//
//                    comment = "";
//                }
//                args.putString("comment", comment);
//                args.putString("brand_name_ar", data.get(position).getBrand_name_ar());
//                args.putString("brand_name", data.get(position).getBrand_name());
//
//                final TrackListDialog newFragment = TrackListDialog.newInstance();
//                newFragment.setCancelable(true);
//                newFragment.setArguments(args);
//                newFragment.show((TrackOrderActivity.this).getSupportFragmentManager(), "track_order ");

                Intent a = new Intent(context, RatingActivity.class);
                a.putExtra("UserId", userid);
                a.putExtra("OrderId", data.get(position).getOrderid());
                a.putExtra("rating", 0);
                String comment = "";
                if (comment == null) {

                    comment = "";
                }
                a.putExtra("comment", comment);
                a.putExtra("brand_name_ar", data.get(position).getBrand_name_ar());
                a.putExtra("brand_name", data.get(position).getBrand_name());
                context.startActivity(a);


            }
        });


    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView mitem_name, morder_status, mtime_date, mrate;
        ImageView mitem_img;
        LinearLayout mrating_layout;
        ImageView rating1, rating2, rating3, rating4, rating5;

        public MyViewHolder(View itemView) {
            super(itemView);
            mitem_img = (ImageView) itemView
                    .findViewById(R.id.item_img);
            mitem_name = (TextView) itemView
                    .findViewById(R.id.item_name);
            morder_status = (TextView) itemView
                    .findViewById(R.id.order_status);
            mtime_date = (TextView) itemView
                    .findViewById(R.id.time_date);
            mrate = (TextView) itemView
                    .findViewById(R.id.rate);
            mrating_layout = (LinearLayout) itemView
                    .findViewById(R.id.rating);
            rating1 = (ImageView) itemView
                    .findViewById(R.id.rating1);
            rating2 = (ImageView) itemView
                    .findViewById(R.id.rating2);
            rating3 = (ImageView) itemView
                    .findViewById(R.id.rating3);
            rating4 = (ImageView) itemView
                    .findViewById(R.id.rating4);
            rating5 = (ImageView) itemView
                    .findViewById(R.id.rating5);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    int userid = Integer.parseInt(OrderHistory.userId);
                    Intent a = new Intent(context, TrackOrderActivity.class);
                    a.putExtra("userid", userid);
                    a.putExtra("orderid", data.get(getAdapterPosition()).getOrderid());
                    a.putExtra("track_order", "my_order");
                    context.startActivity(a);

                    Log.i("TAG", "orderid: " + OrderHistory.orderid);

                }
            });

        }
    }
}








//        extends BaseAdapter {
//
//    public Context context;
//    public LayoutInflater inflater;
//    ArrayList<OrderHistorySearch> data = new ArrayList<>();
//    int pos;
//    public static String subscriptions = "no";
//    String id;
//    String language, userid;
//
//
//    public OrderHistoryItemAdapter(Context context, ArrayList<OrderHistorySearch> data, String language, String userId) {
//        this.context = context;
//        this.data = data;
//        this.inflater = (LayoutInflater) context
//                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        this.language = language;
//        this.userid = userId;
//        //DBcontroller = new DatabaseHandler(context);
//
//        //imageLoader=new ImageLoader(context.getApplicationContext());
//    }
//
//    public int getCount() {
//        return data.size();
//    }
//
//    public Object getItem(int position) {
//        return null;
//    }
//
//    public long getItemId(int position) {
//        return 0;
//    }
//
//    public static class ViewHolder {
//        TextView mitem_name, morder_status, mtime_date, mrate;
//        ImageView mitem_img;
//        LinearLayout mrating_layout;
//        ImageView rating1, rating2, rating3, rating4, rating5;
//
//    }
//
//    public View getView(final int position, View convertView, ViewGroup parent) {
//        final ViewHolder holder;
//        pos = position;
//        if (convertView == null) {
//            holder = new ViewHolder();
//            if (language.equalsIgnoreCase("En")) {
//                convertView = inflater.inflate(R.layout.order_history_item, null);
//            } else {
//                convertView = inflater.inflate(R.layout.order_history_item_arabic, null);
//            }
//
//
//            holder.mitem_img = (ImageView) convertView
//                    .findViewById(R.id.item_img);
//            holder.mitem_name = (TextView) convertView
//                    .findViewById(R.id.item_name);
//            holder.morder_status = (TextView) convertView
//                    .findViewById(R.id.order_status);
//            holder.mtime_date = (TextView) convertView
//                    .findViewById(R.id.time_date);
//            holder.mrate = (TextView) convertView
//                    .findViewById(R.id.rate);
//            holder.mrating_layout = (LinearLayout) convertView
//                    .findViewById(R.id.rating);
//            holder.rating1 = (ImageView) convertView
//                    .findViewById(R.id.rating1);
//            holder.rating2 = (ImageView) convertView
//                    .findViewById(R.id.rating2);
//            holder.rating3 = (ImageView) convertView
//                    .findViewById(R.id.rating3);
//            holder.rating4 = (ImageView) convertView
//                    .findViewById(R.id.rating4);
//            holder.rating5 = (ImageView) convertView
//                    .findViewById(R.id.rating5);
//
//            convertView.setTag(holder);
//        } else {
//            holder = (ViewHolder) convertView.getTag();
//        }
//
//        if (language.equalsIgnoreCase("En")) {
//            holder.mitem_name.setText("" + data.get(position).getName());
//        } else {
//            holder.mitem_name.setText("" + data.get(position).getName_ar());
//        }
//
//        if (language.equalsIgnoreCase("En")) {
//            holder.morder_status.setText("" + data.get(position).getOrder_status());
//        } else {
//            if (data.get(position).getOrder_status().equalsIgnoreCase("New")) {
//                holder.morder_status.setText("جديد");
//            } else if (data.get(position).getOrder_status().equalsIgnoreCase("Accept")) {
//                holder.morder_status.setText("قبول");
//            } else if (data.get(position).getOrder_status().equalsIgnoreCase("Ready")) {
//                holder.morder_status.setText("جاهز");
//            } else if (data.get(position).getOrder_status().equalsIgnoreCase("Close")) {
//                holder.morder_status.setText("مغلق");
//            } else if (data.get(position).getOrder_status().equalsIgnoreCase("Cancel")) {
//                holder.morder_status.setText("الغاء");
//            }
//        }
//
//
//        Glide.with(context)
//                .load(Constants.STORE_IMAGE_URL + data.get(position).getImage())
//                .into(holder.mitem_img);
//
//
//        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
//        SimpleDateFormat dateFormat1 = new SimpleDateFormat("hh:mm aaa dd-MM-yyyy", Locale.US);
//
//        Date date = null;
//
//        try {
//
//            date = dateFormat.parse(data.get(position).getOrderdate());
//
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//
//        String date1;
//
//        date1 = dateFormat1.format(date);
//
//        holder.mtime_date.setText("" + date1);
//        if (language.equalsIgnoreCase("En")) {
//
//            if (data.get(position).getOrder_status().equalsIgnoreCase("Close")) {
//                if (data.get(position).getRate() == 0) {
//                    holder.mrate.setVisibility(View.VISIBLE);
//                    holder.mrating_layout.setVisibility(View.GONE);
//                } else {
//                    holder.mrate.setVisibility(View.GONE);
//                    holder.mrating_layout.setVisibility(View.VISIBLE);
//                    Log.i("TAG", "getView: " + data.get(position).getRate());
//                    if (data.get(position).getRate() == 1) {
//                        Log.i("TAG", "rate: " + data.get(position).getRate());
//                        holder.rating1.setImageDrawable(context.getResources().getDrawable(R.drawable.selected_star));
//                        holder.rating2.setImageDrawable(context.getResources().getDrawable(R.drawable.unselected_star));
//                        holder.rating3.setImageDrawable(context.getResources().getDrawable(R.drawable.unselected_star));
//                        holder.rating4.setImageDrawable(context.getResources().getDrawable(R.drawable.unselected_star));
//                        holder.rating5.setImageDrawable(context.getResources().getDrawable(R.drawable.unselected_star));
//                    } else if (data.get(position).getRate() == 2) {
//                        holder.rating1.setImageDrawable(context.getResources().getDrawable(R.drawable.selected_star));
//                        holder.rating2.setImageDrawable(context.getResources().getDrawable(R.drawable.selected_star));
//                        holder.rating3.setImageDrawable(context.getResources().getDrawable(R.drawable.unselected_star));
//                        holder.rating4.setImageDrawable(context.getResources().getDrawable(R.drawable.unselected_star));
//                        holder.rating5.setImageDrawable(context.getResources().getDrawable(R.drawable.unselected_star));
//                    } else if (data.get(position).getRate() == 3) {
//                        holder.rating1.setImageDrawable(context.getResources().getDrawable(R.drawable.selected_star));
//                        holder.rating2.setImageDrawable(context.getResources().getDrawable(R.drawable.selected_star));
//                        holder.rating3.setImageDrawable(context.getResources().getDrawable(R.drawable.selected_star));
//                        holder.rating4.setImageDrawable(context.getResources().getDrawable(R.drawable.unselected_star));
//                        holder.rating5.setImageDrawable(context.getResources().getDrawable(R.drawable.unselected_star));
//                    } else if (data.get(position).getRate() == 4) {
//                        holder.rating1.setImageDrawable(context.getResources().getDrawable(R.drawable.selected_star));
//                        holder.rating2.setImageDrawable(context.getResources().getDrawable(R.drawable.selected_star));
//                        holder.rating3.setImageDrawable(context.getResources().getDrawable(R.drawable.selected_star));
//                        holder.rating4.setImageDrawable(context.getResources().getDrawable(R.drawable.selected_star));
//                        holder.rating5.setImageDrawable(context.getResources().getDrawable(R.drawable.unselected_star));
//                    } else if (data.get(position).getRate() == 5) {
//                        holder.rating1.setImageDrawable(context.getResources().getDrawable(R.drawable.selected_star));
//                        holder.rating2.setImageDrawable(context.getResources().getDrawable(R.drawable.selected_star));
//                        holder.rating3.setImageDrawable(context.getResources().getDrawable(R.drawable.selected_star));
//                        holder.rating4.setImageDrawable(context.getResources().getDrawable(R.drawable.selected_star));
//                        holder.rating5.setImageDrawable(context.getResources().getDrawable(R.drawable.selected_star));
//                    }
//                }
//            } else {
//                holder.mrate.setVisibility(View.INVISIBLE);
//                holder.mrating_layout.setVisibility(View.INVISIBLE);
//            }
//        } else {
//            if (data.get(position).getOrder_status().equalsIgnoreCase("Close")) {
//                if (data.get(position).getRate() == 0) {
//                    holder.mrate.setVisibility(View.VISIBLE);
//                    holder.mrating_layout.setVisibility(View.GONE);
//                } else {
//                    holder.mrate.setVisibility(View.GONE);
//                    holder.mrating_layout.setVisibility(View.VISIBLE);
//                    if (data.get(position).getRate() == 1) {
//                        holder.rating1.setImageDrawable(context.getResources().getDrawable(R.drawable.unselected_star));
//                        holder.rating2.setImageDrawable(context.getResources().getDrawable(R.drawable.unselected_star));
//                        holder.rating3.setImageDrawable(context.getResources().getDrawable(R.drawable.unselected_star));
//                        holder.rating4.setImageDrawable(context.getResources().getDrawable(R.drawable.unselected_star));
//                        holder.rating5.setImageDrawable(context.getResources().getDrawable(R.drawable.selected_star));
//                    } else if (data.get(position).getRate() == 2) {
//                        holder.rating1.setImageDrawable(context.getResources().getDrawable(R.drawable.unselected_star));
//                        holder.rating2.setImageDrawable(context.getResources().getDrawable(R.drawable.unselected_star));
//                        holder.rating3.setImageDrawable(context.getResources().getDrawable(R.drawable.unselected_star));
//                        holder.rating4.setImageDrawable(context.getResources().getDrawable(R.drawable.selected_star));
//                        holder.rating5.setImageDrawable(context.getResources().getDrawable(R.drawable.selected_star));
//                    } else if (data.get(position).getRate() == 3) {
//                        holder.rating1.setImageDrawable(context.getResources().getDrawable(R.drawable.unselected_star));
//                        holder.rating2.setImageDrawable(context.getResources().getDrawable(R.drawable.unselected_star));
//                        holder.rating3.setImageDrawable(context.getResources().getDrawable(R.drawable.selected_star));
//                        holder.rating4.setImageDrawable(context.getResources().getDrawable(R.drawable.selected_star));
//                        holder.rating5.setImageDrawable(context.getResources().getDrawable(R.drawable.selected_star));
//                    } else if (data.get(position).getRate() == 4) {
//                        holder.rating1.setImageDrawable(context.getResources().getDrawable(R.drawable.unselected_star));
//                        holder.rating2.setImageDrawable(context.getResources().getDrawable(R.drawable.selected_star));
//                        holder.rating3.setImageDrawable(context.getResources().getDrawable(R.drawable.selected_star));
//                        holder.rating4.setImageDrawable(context.getResources().getDrawable(R.drawable.selected_star));
//                        holder.rating5.setImageDrawable(context.getResources().getDrawable(R.drawable.selected_star));
//                    } else if (data.get(position).getRate() == 5) {
//                        holder.rating1.setImageDrawable(context.getResources().getDrawable(R.drawable.selected_star));
//                        holder.rating2.setImageDrawable(context.getResources().getDrawable(R.drawable.selected_star));
//                        holder.rating3.setImageDrawable(context.getResources().getDrawable(R.drawable.selected_star));
//                        holder.rating4.setImageDrawable(context.getResources().getDrawable(R.drawable.selected_star));
//                        holder.rating5.setImageDrawable(context.getResources().getDrawable(R.drawable.selected_star));
//                    }
//                }
//            } else {
//                holder.mrate.setVisibility(View.INVISIBLE);
//                holder.mrating_layout.setVisibility(View.INVISIBLE);
//            }
//        }
//
//        holder.mrate.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
////                Bundle args = new Bundle();
//////                    Log.d("TAG", "storeArrayList "+storesArrayList.size());
//////                                            Log.d("TAG", "pos " + getAdapterPosition());
////                args.putInt("UserId", Integer.parseInt(userid));
////                args.putInt("OrderId", data.get(position).getOrderid());
////                args.putFloat("rating", 0);
////                String comment = "";
////                if (comment == null) {
////
////                    comment = "";
////                }
////                args.putString("comment", comment);
////                args.putString("brand_name_ar", data.get(position).getBrand_name_ar());
////                args.putString("brand_name", data.get(position).getBrand_name());
////
////                final TrackListDialog newFragment = TrackListDialog.newInstance();
////                newFragment.setCancelable(true);
////                newFragment.setArguments(args);
////                newFragment.show((TrackOrderActivity.this).getSupportFragmentManager(), "track_order ");
//
//                Intent a = new Intent(context, RatingActivity.class);
//                a.putExtra("UserId", userid);
//                a.putExtra("OrderId", data.get(position).getOrderid());
//                a.putExtra("rating", 0);
//                String comment = "";
//                if (comment == null) {
//
//                    comment = "";
//                }
//                a.putExtra("comment", comment);
//                a.putExtra("brand_name_ar", data.get(position).getBrand_name_ar());
//                a.putExtra("brand_name", data.get(position).getBrand_name());
//                context.startActivity(a);
//
//
//            }
//        });
//
//        convertView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                int userid = Integer.parseInt(OrderHistory.userId);
//                Intent a = new Intent(context, TrackOrderActivity.class);
//                a.putExtra("userid", userid);
//                a.putExtra("orderid", data.get(position).getOrderid());
//                a.putExtra("track_order", "my_order");
//                context.startActivity(a);
//
//                Log.i("TAG", "orderid: " + OrderHistory.orderid);
//
//            }
//        });
//
//
//        return convertView;
//    }
//}