package com.cs.chef.Activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.chef.Constants;
import com.cs.chef.Dialogs.ResetPasswordDialog;
import com.cs.chef.Dialogs.VerifyOtpDialog;
import com.cs.chef.Models.VerifyMobileResponse;
import com.cs.chef.R;
import com.cs.chef.Rest.APIInterface;
import com.cs.chef.Rest.ApiClient;
import com.cs.chef.Utils.NetworkUtil;
import com.cs.chef.Utils.TextDrawable;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgotPasswordActivity extends AppCompatActivity implements View.OnClickListener{

    public static boolean isOTPSuccessful = false;
    public static boolean isResetSuccessful = false;
    private TextInputLayout inputLayoutMobile;
    private EditText inputMobile;
    private String strMobile;
    private Button buttonSubmit;
    private String serverOtp;
    Toolbar toolbar;
    Context context;
    AlertDialog loaderDialog = null;

    ImageView mback_btn;

    String language;
    SharedPreferences languagePrefs;

    public static final String TAG = "TAG";
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.activity_forgot_password);
        } else {
            setContentView(R.layout.activity_forgot_password_arabic);
        }
        context = this;


        inputLayoutMobile = (TextInputLayout) findViewById(R.id.input_layout_mobile);
        inputMobile = (EditText) findViewById(R.id.forgot_input_mobile);
        buttonSubmit = (Button) findViewById(R.id.forgot_submit_button);
        mback_btn = (ImageView) findViewById(R.id.back_btn);

        if (language.equalsIgnoreCase("En")) {
            inputMobile.setText(Constants.Country_Code);
            inputMobile.setCursorVisible(false);
        } else {
            inputMobile.setText(Constants.Country_Code);
            inputMobile.setCursorVisible(false);
//            inputMobile.requestFocus();
//            inputMobile.setCompoundDrawablesWithIntrinsicBounds(new TextDrawable(Constants.Country_Code), null, null, null);
        }

        mback_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();

            }
        });

        setTypeFace();

        buttonSubmit.setOnClickListener(this);
        inputMobile.addTextChangedListener(new MyTextWatcher(inputMobile));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.forgot_submit_button:
                if(validations()) {
                    String networkStatus = NetworkUtil.getConnectivityStatusString(ForgotPasswordActivity.this);
                    if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        new ForgotPasswordApi().execute();
                    }
                    else{
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(getApplicationContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getApplicationContext(), R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }
                    }
                }
                break;
        }
    }

    private void setTypeFace(){
        inputMobile.setTypeface(Constants.getTypeFace(context));
        buttonSubmit.setTypeface(Constants.getTypeFace(context));
        ((TextView) findViewById(R.id.forgot_body)).setTypeface(Constants.getTypeFace(context));
    }

    private boolean validations(){
        strMobile = inputMobile.getText().toString();
        strMobile = strMobile.replace("+966 ", "");


        if (strMobile.length() == 0){
            if (language.equalsIgnoreCase("En")) {
                inputLayoutMobile.setError(getResources().getString(R.string.signup_msg_enter_mobile));
            } else {
                inputLayoutMobile.setError(getResources().getString(R.string.signup_msg_enter_mobile_ar));
            }
            Constants.requestEditTextFocus(inputMobile, ForgotPasswordActivity.this);
            return false;
        }
        else if (strMobile.length() != 9){
            if (language.equalsIgnoreCase("En")) {
                inputLayoutMobile.setError(getResources().getString(R.string.signup_msg_invalid_mobile));
            } else {
                inputLayoutMobile.setError(getResources().getString(R.string.signup_msg_invalid_mobile_ar));
            }
            Constants.requestEditTextFocus(inputMobile, ForgotPasswordActivity.this);
            return false;
        }
        return true;
    }

    private void clearErrors(){
        inputLayoutMobile.setErrorEnabled(false);
    }

    private class MyTextWatcher implements TextWatcher {
        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.forgot_input_mobile:
                    inputMobile.setCursorVisible(true);

                        String enteredMobile = editable.toString();
                        if (!enteredMobile.contains(Constants.Country_Code)) {
                            if (enteredMobile.length() > Constants.Country_Code.length()) {
                                enteredMobile = enteredMobile.substring((Constants.Country_Code.length() - 1));
                                inputMobile.setText(Constants.Country_Code + enteredMobile);
                            } else {
                                inputMobile.setText(Constants.Country_Code);
                            }
                            inputMobile.setSelection(inputMobile.length());
                        }
                    clearErrors();
                    break;
            }
        }
    }

    private void displayVerifyOTPDialog(){
        Bundle args = new Bundle();
        args.putString("mobile", strMobile);
        args.putString("otp", serverOtp);

        isOTPSuccessful = false;

        final VerifyOtpDialog newFragment = VerifyOtpDialog.newInstance();
        newFragment.setCancelable(false);
        newFragment.setArguments(args);
        newFragment.show(getSupportFragmentManager(), "forgot");

        getSupportFragmentManager().executePendingTransactions();
        newFragment.getDialog().setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                //do whatever you want when dialog is dismissed

                if(isOTPSuccessful){
                    displayResetPasswordDiaolg();
                }

                Log.i(TAG, "onDismiss: " + isOTPSuccessful);

                if (newFragment != null) {
                    newFragment.dismiss();
                }
            }
        });
    }

    private void displayResetPasswordDiaolg(){
        Bundle args = new Bundle();
        args.putString("mobile", strMobile);

        Log.i(TAG, "displayResetPasswordDiaolg: " + strMobile);

        isResetSuccessful = false;

        final ResetPasswordDialog newFragment = ResetPasswordDialog.newInstance();
        newFragment.setCancelable(false);
        newFragment.setArguments(args);
        newFragment.show(getSupportFragmentManager(), "forgot");

        getSupportFragmentManager().executePendingTransactions();
        newFragment.getDialog().setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                //do whatever you want when dialog is dismissed


                if(isResetSuccessful){
                    setResult(RESULT_OK);
                    finish();
                }

                if (newFragment != null) {
                    newFragment.dismiss();
                }
            }
        });
    }

    private String prepareForgotPasswordJson(){
        JSONObject parentObj = new JSONObject();
        JSONObject forgotPasswordObj = new JSONObject();

        try {
            forgotPasswordObj.put("Mobile","966"+strMobile);
            parentObj.put("VerifyMobile", forgotPasswordObj);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.i(TAG, "prepareResetPasswordJson: "+parentObj.toString());
        return parentObj.toString();
    }

//    public void showloaderAlertDialog(){
//        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(ForgotPasswordActivity.this);
//        // ...Irrelevant code for customizing the buttons and title
//        LayoutInflater inflater = getLayoutInflater();
//        int layout = R.layout.loder_dialog;
//        View dialogView = inflater.inflate(layout, null);
//        dialogBuilder.setView(dialogView);
//        dialogBuilder.setCancelable(true);
//
//        loaderDialog = dialogBuilder.create();
//        loaderDialog.show();
//        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
//        Window window = loaderDialog.getWindow();
//        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        lp.copyFrom(window.getAttributes());
//        //This makes the dialog take up the full width
//        Display display = getWindowManager().getDefaultDisplay();
//        Point size = new Point();
//        display.getSize(size);
//        int screenWidth = size.x;
//
//        double d = screenWidth*0.85;
//        lp.width = (int) d;
//        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
//        window.setAttributes(lp);
//    }

    private class ForgotPasswordApi extends AsyncTask<String, Integer, String> {

//        ACProgressFlower dialog;
        String inputStr;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareForgotPasswordJson();
//            dialog = new ACProgressFlower.Builder(ForgotPasswordActivity.this)
//                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
//                    .themeColor(Color.WHITE)
//                    .fadeColor(Color.DKGRAY).build();
//            dialog.show();
            Constants.showLoadingDialog(ForgotPasswordActivity.this);
//            showloaderAlertDialog();
        }

        @Override
        protected String doInBackground(String... strings) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(ForgotPasswordActivity.this);
            APIInterface apiService = ApiClient.getClient().create(APIInterface.class);

            Call<VerifyMobileResponse> call = apiService.forgotPassword(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<VerifyMobileResponse>() {
                @Override
                public void onResponse(Call<VerifyMobileResponse> call, Response<VerifyMobileResponse> response) {
                    if(response.isSuccessful()){
                        VerifyMobileResponse forgotPasswordResponse = response.body();
                        try {
                            if(forgotPasswordResponse.getStatus()){
                                serverOtp = forgotPasswordResponse.getData().getOtp();
                                Log.i(TAG, "onResponse: "+serverOtp);
                                displayVerifyOTPDialog();
                            }
                            else {
                                //                          status false case
                                if (language.equalsIgnoreCase("En")) {
                                    String failureResponse = forgotPasswordResponse.getMessage();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name),
                                            getResources().getString(R.string.ok), ForgotPasswordActivity.this);
                                } else {
                                    String failureResponse = forgotPasswordResponse.getMessageAr();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name_ar),
                                            getResources().getString(R.string.ok_ar), ForgotPasswordActivity.this);
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            if (language.equalsIgnoreCase("En")) {
                                Toast.makeText(ForgotPasswordActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(ForgotPasswordActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                    else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(ForgotPasswordActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(ForgotPasswordActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }

                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<VerifyMobileResponse> call, Throwable t) {
                    if(networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(ForgotPasswordActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(ForgotPasswordActivity.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }
                    }
                    else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(ForgotPasswordActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(ForgotPasswordActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }

                    Constants.closeLoadingDialog();
                }
            });
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Constants.closeLoadingDialog();
        }
    }
}
