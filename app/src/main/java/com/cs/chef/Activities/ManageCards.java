package com.cs.chef.Activities;

import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.chef.Adapter.CreditCardsAdapter;
import com.cs.chef.Constants;
import com.cs.chef.Models.CancelOrder;
import com.cs.chef.Models.CardRegistration;
import com.cs.chef.Models.MyCards;
import com.cs.chef.Models.SaveCardToDB;
import com.cs.chef.Models.SavedCards;
import com.cs.chef.R;
import com.cs.chef.Rest.APIInterface;
import com.cs.chef.Rest.ApiClient;
import com.cs.chef.Utils.NetworkUtil;
import com.cs.chef.hyperpay.CardRegistrationStatusRequestAsyncTask;
import com.cs.chef.hyperpay.CardRegistrationStatusRequestListener;
import com.cs.chef.hyperpay.CheckoutIdRequestAsyncTask;
import com.cs.chef.hyperpay.CheckoutIdRequestListener;
import com.oppwa.mobile.connect.checkout.dialog.CheckoutActivity;
import com.oppwa.mobile.connect.checkout.meta.CheckoutSettings;
import com.oppwa.mobile.connect.checkout.meta.CheckoutStorePaymentDetailsMode;
import com.oppwa.mobile.connect.exception.PaymentError;
import com.oppwa.mobile.connect.exception.PaymentException;
import com.oppwa.mobile.connect.provider.Connect;
import com.oppwa.mobile.connect.provider.Transaction;
import com.oppwa.mobile.connect.provider.TransactionType;
import com.oppwa.mobile.connect.service.ConnectService;
import com.oppwa.mobile.connect.service.IProviderBinder;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ManageCards extends AppCompatActivity implements CheckoutIdRequestListener, CardRegistrationStatusRequestListener {
    Button addCardBtn;
    ImageView backbtn;
    String language;
    SharedPreferences languagePrefs;
    private CardRegistration myRegistrationCardDetails = new CardRegistration();
    private AlertDialog loadingDialog;
    private ListView cardsListView;
    private CreditCardsAdapter creditCardsAdapter;

    private static final String STATE_RESOURCE_PATH = "STATE_RESOURCE_PATH";

    protected IProviderBinder providerBinder;
    protected String resourcePath;
    String merchantId = "";
    String CheckoutId = "";
    SharedPreferences userPrefs;
    String userId;

    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            ManageCards.this.onServiceConnected(service);
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            providerBinder = null;
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.cardsactivity);
        } else {
            setContentView(R.layout.cardsactivity_arabic);
        }

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userId = userPrefs.getString("userId", "0");

        backbtn = (ImageView) findViewById(R.id.back_btn);
        addCardBtn = (Button) findViewById(R.id.add_card_btn);
        cardsListView = (ListView) findViewById(R.id.cards_list);

        new getAllSavedCards().execute();

        generateMerchantId();

        backbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        addCardBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    requestCheckoutId(Constants.SHOPPER_RESULT_URL);
                } catch (Exception e) {
                    Log.e("connect", "error creating the payment page", e);
                }
            }
        });
    }

    @SuppressWarnings("unchecked")
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CheckoutActivity.CHECKOUT_ACTIVITY) {
            switch (resultCode) {
                case CheckoutActivity.RESULT_OK:
                    /* Transaction completed. */
                    Transaction transaction = data.getParcelableExtra(CheckoutActivity.CHECKOUT_RESULT_TRANSACTION);

                    resourcePath = data.getStringExtra(CheckoutActivity.CHECKOUT_RESULT_RESOURCE_PATH);
                    Log.d("TAG", "onActivityResult: "+resourcePath);

                    /* Check the transaction type. */
                    if (transaction.getTransactionType() == TransactionType.SYNC) {
                        /* Check the status of synchronous transaction. */
                        requestPaymentStatus(resourcePath);
                    } else {
                        /* The on onNewIntent method may be called before onActivityResult
                           if activity was destroyed in the background, so check
                           if the intent already has the callback scheme */
                        if (hasCallbackScheme(this.getIntent())) {
                            requestPaymentStatus(resourcePath);
                        } else {
                            /* The on onNewIntent method wasn't called yet,
                               wait for the callback. */
                            showProgressDialog(R.string.progress_message_please_wait);
                        }
                    }

                    break;
                case CheckoutActivity.RESULT_CANCELED:
                    hideProgressDialog();

                    break;
                case CheckoutActivity.RESULT_ERROR:
                    PaymentError error = data.getParcelableExtra(
                            CheckoutActivity.CHECKOUT_RESULT_ERROR);
                    Log.d("TAG", "onActivityResult: error "+error.getErrorInfo());
                    Log.d("TAG", "onActivityResult: error "+error.getErrorMessage());
                    Log.d("TAG", "onActivityResult: error "+error.getErrorCode());
                    showAlertDialog(R.string.error_message);
            }
        }
    }

    protected boolean hasCallbackScheme(Intent intent) {
        String scheme = intent.getScheme();

        return Constants.SHOPPER_RESULT_URL.equals(scheme+"://result");
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d("lifecycle", "cards fragment onStart: ");
        Intent intent = new Intent(ManageCards.this, ConnectService.class);

        ManageCards.this.startService(intent);
        ManageCards.this.bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d("lifecycle", "cards fragment onStop: ");
        ManageCards.this.unbindService(serviceConnection);
        ManageCards.this.stopService(new Intent(ManageCards.this, ConnectService.class));
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putString(STATE_RESOURCE_PATH, resourcePath);
    }

    protected void onServiceConnected(IBinder service) {
        providerBinder = (IProviderBinder) service;

        /* We have a connection to the service. */
        try {
            /* Initialize Payment Provider with test mode. */
            providerBinder.initializeProvider(Connect.ProviderMode.TEST);
        } catch (PaymentException e) {
            e.printStackTrace();
            showAlertDialog(R.string.error_message);
        }
    }

    protected void requestCheckoutId(String callbackScheme) {
        showProgressDialog(R.string.progress_message_checkout_id);

        new CheckoutIdRequestAsyncTask(this)
                .execute("0.00", Constants.SHOPPER_RESULT_URL, "true",
                        merchantId, userPrefs.getString("email", ""), userId);
    }

    @Override
    public void onCheckoutIdReceived(String checkoutId) {
        hideProgressDialog();

        if (checkoutId == null) {
            Log.d("TAG", "onCheckoutIdReceived: ");
            showAlertDialog(R.string.error_message);
        }

        if (checkoutId != null) {
            CheckoutId = checkoutId;
            openCheckoutUI(checkoutId);
        }
    }

    private void openCheckoutUI(String checkoutId) {
        CheckoutSettings checkoutSettings = createCheckoutSettings(checkoutId);

        /* Set up the Intent and start the checkout activity. */
        Intent intent = new Intent(ManageCards.this, CheckoutActivity.class);
        intent.putExtra(CheckoutActivity.CHECKOUT_SETTINGS, checkoutSettings);

        startActivityForResult(intent, CheckoutActivity.CHECKOUT_ACTIVITY);
    }

    @Override
    public void onErrorOccurred() {
        hideProgressDialog();
        Log.d("TAG", "onErrorOccurred: ");
        showAlertDialog(R.string.error_message);
    }

    @Override
    public void onCardRegistrationStatusReceived(CardRegistration paymentStatus) {
        Log.d("TAG", "onCardRegistrationStatusReceived: "+paymentStatus);
        hideProgressDialog();
        myRegistrationCardDetails = paymentStatus;
        new saveCardToDb().execute();
    }

    protected void requestPaymentStatus(String resourcePath) {
        showProgressDialog(R.string.progress_message_payment_status);
        Log.d("TAG", "requestPaymentStatus: "+resourcePath);
        resourcePath = resourcePath.replace("registration","payment");
        Log.d("TAG", "requestPaymentStatus: "+resourcePath);
        new CardRegistrationStatusRequestAsyncTask(this).execute(resourcePath);
    }

    /**
     * Creates the new instance of {@link CheckoutSettings}
     * to instantiate the {@link CheckoutActivity}.
     *
     * @param checkoutId the received checkout id
     * @return the new instance of {@link CheckoutSettings}
     */
    protected CheckoutSettings createCheckoutSettings(String checkoutId) {
        return new CheckoutSettings(checkoutId, Constants.Config.PAYMENT_BRANDS)
                .setStorePaymentDetailsMode(CheckoutStorePaymentDetailsMode.PROMPT)
                .setShopperResultUrl(Constants.SHOPPER_RESULT_URL)
                .setWindowSecurityEnabled(false);
    }

    public void generateMerchantId(){
        SimpleDateFormat dateFormater = new SimpleDateFormat("ddMMyyyy", Locale.US);
        SimpleDateFormat timeFormater = new SimpleDateFormat("HHmm", Locale.US);

        Calendar calendar = Calendar.getInstance();
        String timeStr = timeFormater.format(calendar.getTimeInMillis());
        String dateStr = dateFormater.format(calendar.getTimeInMillis());

        merchantId = userId + dateStr + timeStr;
    }

    protected void showProgressDialog(int messageId) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(ManageCards.this);
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getLayoutInflater();
        int layout = R.layout.progress_bar_alert;
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);

        TextView loadingText = (TextView) dialogView.findViewById(R.id.loading_text);
        loadingText.setText(getString(messageId));

        loadingDialog = dialogBuilder.create();
        loadingDialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = loadingDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the progressDialog take up the full width
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x;

        double d = screenWidth * 0.45;
        lp.width = (int) d;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

    protected void hideProgressDialog() {
        if (loadingDialog == null) {
            return;
        }

        loadingDialog.dismiss();
    }

    protected void showAlertDialog(String message) {
        new AlertDialog.Builder(this)
                .setMessage(message)
                .setPositiveButton(R.string.button_ok, null)
                .setCancelable(false)
                .show();
    }

    protected void showAlertDialog(int messageId) {
        showAlertDialog(getString(messageId));
    }

    private class getAllSavedCards extends AsyncTask<String, String, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareGetCardsJSON();
            Constants.showLoadingDialog(ManageCards.this);
        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(ManageCards.this);
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<SavedCards> call = apiService.GetAllSavedCards(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<SavedCards>() {
                @Override
                public void onResponse(Call<SavedCards> call, Response<SavedCards> response) {
                    Log.d("TAG", "onResponse: " + response);
                    if (response.isSuccessful()) {
                        SavedCards orderItems = response.body();
                        try {
                            if (orderItems.getStatus()) {
                                Constants.closeLoadingDialog();
                                creditCardsAdapter = new CreditCardsAdapter(ManageCards.this, orderItems.getData(), language, ManageCards.this, userId);
                                cardsListView.setAdapter(creditCardsAdapter);
                            } else {
                                Constants.closeLoadingDialog();
                                //                          status false case
                                if (language.equalsIgnoreCase("En")) {
                                    String failureResponse = orderItems.getMessage();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name),
                                            getResources().getString(R.string.ok), ManageCards.this);
                                } else {
                                    String failureResponse = orderItems.getMessageAr();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name_ar),
                                            getResources().getString(R.string.ok_ar), ManageCards.this);
                                }
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                            Constants.closeLoadingDialog();
                            if (language.equalsIgnoreCase("En")) {
                                Toast.makeText(ManageCards.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(ManageCards.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                            }
                        }
                    } else {
                        Constants.closeLoadingDialog();
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(ManageCards.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(ManageCards.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }

                }

                @Override
                public void onFailure(Call<SavedCards> call, Throwable t) {
                    Log.d("TAG", "onFailure: " + t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(ManageCards.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(ManageCards.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(ManageCards.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(ManageCards.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }

                    Constants.closeLoadingDialog();
                }
            });
            return null;
        }

        private String prepareGetCardsJSON() {
            JSONObject parentObj = new JSONObject();
            try {
                parentObj.put("userId", userId);
               } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.d("TAG", "prepareVerifyMobileJson: " + parentObj);
            return parentObj.toString();
        }
    }

    private class saveCardToDb extends AsyncTask<String, String, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Log.d("TAG", "onPreExecute: ");
            inputStr = prepareSaveCardJSON();
            Constants.showLoadingDialog(ManageCards.this);
        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(ManageCards.this);
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<SaveCardToDB> call = apiService.insertCreditCard(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<SaveCardToDB>() {
                @Override
                public void onResponse(Call<SaveCardToDB> call, Response<SaveCardToDB> response) {
                    Log.d("TAG", "onResponse: " + response);
                    Constants.closeLoadingDialog();
                    if (response.isSuccessful()) {
                        SaveCardToDB orderItems = response.body();
                        try {
                            if (orderItems.getStatusCode().equals("1")) {
                                new getAllSavedCards().execute();

                            } else {
                                //                          status false case
                                if (language.equalsIgnoreCase("En")) {
                                    String failureResponse = orderItems.getStatusMessage();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name),
                                            getResources().getString(R.string.ok), ManageCards.this);
                                } else {
                                    String failureResponse = orderItems.getStatusMessage();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name_ar),
                                            getResources().getString(R.string.ok_ar), ManageCards.this);
                                }
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                            if (language.equalsIgnoreCase("En")) {
                                Toast.makeText(ManageCards.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(ManageCards.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                            }
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(ManageCards.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(ManageCards.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }
                }

                @Override
                public void onFailure(Call<SaveCardToDB> call, Throwable t) {
                    Log.d("TAG", "onFailure: " + t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(ManageCards.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(ManageCards.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(ManageCards.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(ManageCards.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }

                    Constants.closeLoadingDialog();
                }
            });
            return null;
        }

        private String prepareSaveCardJSON() {
            JSONObject parentObj = new JSONObject();
            try {
                parentObj.put("userId", userId);
                for (int i = 0; i < myRegistrationCardDetails.getData().size(); i++) {
                    for (int j = 0; j < myRegistrationCardDetails.getData().get(i).getJArray().size(); j++) {
                        if(myRegistrationCardDetails.getData().get(i).getJArray().get(j).getKey().equals("id")) {
                            parentObj.put("token", myRegistrationCardDetails.getData().get(i).getJArray().get(j).getValue());
                        }
                        else if(myRegistrationCardDetails.getData().get(i).getJArray().get(j).getKey().equals("last4Digits")) {
                            parentObj.put("last4Digits", myRegistrationCardDetails.getData().get(i).getJArray().get(j).getValue());
                        }
                        else if(myRegistrationCardDetails.getData().get(i).getJArray().get(j).getKey().equals("holder")) {
                            parentObj.put("cardHolder", myRegistrationCardDetails.getData().get(i).getJArray().get(j).getValue());
                        }
                        else if(myRegistrationCardDetails.getData().get(i).getJArray().get(j).getKey().equals("expiryMonth")) {
                            parentObj.put("expireMonth", myRegistrationCardDetails.getData().get(i).getJArray().get(j).getValue());
                        }
                        else if(myRegistrationCardDetails.getData().get(i).getJArray().get(j).getKey().equals("expiryYear")) {
                            parentObj.put("expireYear", myRegistrationCardDetails.getData().get(i).getJArray().get(j).getValue());
                        }
                        else if(myRegistrationCardDetails.getData().get(i).getJArray().get(j).getKey().equals("paymentBrand")) {
                            parentObj.put("cardBrand", myRegistrationCardDetails.getData().get(i).getJArray().get(j).getValue());
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.d("TAG", "prepareVerifyMobileJson: " + parentObj.toString());
            return parentObj.toString();
        }
    }
}
