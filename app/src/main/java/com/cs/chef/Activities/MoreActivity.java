package com.cs.chef.Activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.cs.chef.R;

public class MoreActivity extends AppCompatActivity {
    Context context;
    ImageView backbtn,twitter,privicy,trems;
    String language;
    SharedPreferences languagePrefs;
    TextView arabspace;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.moreactivity);
        } else {
            setContentView(R.layout.moreactivity_arabic);
        }
        context = this;

        backbtn=(ImageView)findViewById(R.id.back_btn);
        twitter=(ImageView)findViewById(R.id.twitterimage);
        privicy=(ImageView)findViewById(R.id.more_privacypolicy);
        trems=(ImageView)findViewById(R.id.more_trems);
        arabspace=(TextView)findViewById(R.id.arabspacelink);
        backbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        twitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent fbIntent = new Intent(MoreActivity.this, WebViewActivity.class);
                fbIntent.putExtra("title", "Twitter");
                fbIntent.putExtra("url", " https://www.twitter.com/thechefapp");
                startActivity(fbIntent);
            }
        });
        arabspace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent fbIntent = new Intent(MoreActivity.this, WebViewActivity.class);
                fbIntent.putExtra("title", "Arab Space");
                fbIntent.putExtra("url", " https://www.Arab-space.com.sa");
                startActivity(fbIntent);
            }
        });
    }
}