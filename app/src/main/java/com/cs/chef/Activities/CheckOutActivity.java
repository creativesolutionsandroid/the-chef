package com.cs.chef.Activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.chef.Adapter.BannersAdapter;
import com.cs.chef.Adapter.CheckoutItemAdapter;
import com.cs.chef.Adapter.StoresAdapter;
import com.cs.chef.ChromeHelpPopup;
import com.cs.chef.ChromeHelpPopup1;
import com.cs.chef.Constants;
import com.cs.chef.CustomListView;
import com.cs.chef.DataBaseHelper;
import com.cs.chef.Models.Banners;
import com.cs.chef.Models.Brands;
import com.cs.chef.Models.Filter;
import com.cs.chef.Models.InsertOrder;
import com.cs.chef.Models.OffersDiscount;
import com.cs.chef.Models.Order;
import com.cs.chef.Models.Promotions;
import com.cs.chef.Models.StoresList;
import com.cs.chef.R;
import com.cs.chef.Rest.APIInterface;
import com.cs.chef.Rest.ApiClient;
import com.cs.chef.Utils.NetworkUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.cs.chef.Activities.MainActivity.navigation;
import static com.cs.chef.Activities.MainActivity.removeBadge;
import static com.cs.chef.Activities.MainActivity.showBadge;
import static com.cs.chef.Constants.currentLatitude;
import static com.cs.chef.Constants.currentLongitude;

//import com.cs.chef.Fragments.AccountFragment;
//import com.cs.chef.Fragments.SearchFragment;

public class CheckOutActivity extends Fragment implements OnMapReadyCallback {

    private TextView title_name, branch_name, store_address, order_type_txt, expected_time;
    public static String check_ordertype = "Pick Up";
    public static String check_ordertype_ar = "طريقة التوصيل";
    public static TextView service;
    public static TextView item_total, sub_total, mvat, servie_charges, mtotal_price, coupon_discount, min_order_amount;
    public static Button procced;
    //    public static TextView total_price;
    ImageView back_btn;
    Spinner order_type_spinner;
    //    public static TextView item_total, offer_discount, mvat, final_price, final_qty, mtotal_price, coupon_discount, moffer, min_order_amount;
//    public static TextView order_type_txt, proceed_to_pay;
    public static float total_price, tax;
    //    TextView delivery_change, brand_address;
//    ImageView mbrand_logo, map;
//    public static RecyclerView morder_type_txt_view;
//    public static RelativeLayout order_type_txt_layout;
    private ArrayAdapter<String> SpinnerAdapter;
    //    LinearLayout expected_layout;
//    RelativeLayout order_s;
    CustomListView mitem_list;
    CheckoutItemAdapter mAdapter;
    String format;
    String[] order_type_txt_count;
//    OrderTypeListAdapter order_type_txt_adapter;
//    public static RelativeLayout complete_bg;

    private int mYear, mMonth, mDay, mHour, mMinute;

    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefEditor;
    String userId;

    boolean isToday;
    String selectedDate, selectedTime, time1;


//    CheckBox mcash, mcard;
//    RelativeLayout mcash_layout, mcard_layout;

    public static TextView moffer_name;
//    TextView moffer_desc;

    float vat = (float) 5.0;

    public static float discount = 0;
    public static int min_order_charges = 0, discount_type = 0, min_spending = 0, max_spending = 0;
    int maindiscount = 0;
    public static String offer_name = "", offer_desc;
    ArrayList<Order> orderList = new ArrayList<>();
    private DataBaseHelper myDbHelper;
    boolean offer;

    private static final int Date_id = 1;
    private static final int Time_id = 0;

    int PaymentType = 0;

    public static RelativeLayout mcheck_out, empty_card;
//    AlertDialog loaderDialog = null;

    Date cal_date1 = null, cal_time1 = null, cal_date3 = null;
    RelativeLayout expected_layout;
    public static TextView exp_date, exp_time;
    View rootView;

    ArrayList<OffersDiscount> offersDiscounts = new ArrayList<>();
    ArrayList<Promotions.Data> data = new ArrayList<Promotions.Data>();

    String language;
    SharedPreferences languagePrefs;

    SupportMapFragment mapFragment;
    private GoogleMap mMap;
    double lat, longi;
    //    RelativeLayout map_layout;
    RelativeLayout offer_layout;
    TextView cancel_offer;
    public static boolean store_avaiable = false;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        languagePrefs = getActivity().getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            rootView = inflater.inflate(R.layout.checkout, container, false);
        } else if (language.equalsIgnoreCase("Ar")) {
            rootView = inflater.inflate(R.layout.check_out_arabic, container, false);
        }
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        mcheck_out = rootView.findViewById(R.id.check_out);
        empty_card = rootView.findViewById(R.id.empty_card);
//        map_layout = rootView.findViewById(R.id.map_layout);

        mcheck_out.setVisibility(View.VISIBLE);

        myDbHelper = new DataBaseHelper(getActivity());

        orderList = myDbHelper.getOrderInfo();

        offersDiscounts.clear();

        userPrefs = getActivity().getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userId = userPrefs.getString("userId", "0");

        Log.i("TAG", "onMin_spend: " + min_spending);

        if (myDbHelper.getTotalOrderQty() == 0) {

            empty_card.setVisibility(View.VISIBLE);
            mcheck_out.setVisibility(View.GONE);

        } else {

            mcheck_out.setVisibility(View.VISIBLE);
            empty_card.setVisibility(View.GONE);

        }

        branch_name = rootView.findViewById(R.id.branch_name);
        title_name = rootView.findViewById(R.id.title_name);
        store_address = rootView.findViewById(R.id.store_address);
        order_type_txt = rootView.findViewById(R.id.order_type_txt);
        item_total = rootView.findViewById(R.id.item_total);
        sub_total = rootView.findViewById(R.id.sub_total);

        servie_charges = rootView.findViewById(R.id.service_charge);
        service = rootView.findViewById(R.id.service);
        mvat = rootView.findViewById(R.id.vat);
        coupon_discount = rootView.findViewById(R.id.coupon_discount);
//        delivery_change = rootView.findViewById(R.id.delivery_charges);
//        final_price = rootView.findViewById(R.id.final_price);
//        final_qty = rootView.findViewById(R.id.final_qty);
        exp_date = rootView.findViewById(R.id.exp_date);
        exp_time = rootView.findViewById(R.id.exp_time);
        expected_layout = rootView.findViewById(R.id.expected_layout);
        mtotal_price = rootView.findViewById(R.id.total_price);
        procced = rootView.findViewById(R.id.proceed);
        min_order_amount = rootView.findViewById(R.id.min_order_amount);

//        order_type_spinner = rootView.findViewById(R.id.order_type_spinner);

//        order_type_txt_layout = rootView.findViewById(R.id.order_status_layout);


        offer_layout = rootView.findViewById(R.id.offer_layout);
        moffer_name = rootView.findViewById(R.id.offer_name);
//        moffer_desc = rootView.findViewById(R.id.offer_desc);

        cancel_offer = rootView.findViewById(R.id.cancel_offer);


        mitem_list = rootView.findViewById(R.id.item_list);

        min_order_amount.setVisibility(View.GONE);


//        back_btn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                getActivity().finish();
//
//            }
//        });


//        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
//        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
//        disableShiftMode(navigation);


//        Log.i("TAG", "offer: " + OfferActivity.offer_selected);

//        Constants.confirm = true;

        if (Constants.offer_selected) {

            discount = Constants.discount;
            offer_name = Constants.offer_name;
            offer_desc = Constants.offer_desc;
            discount_type = Constants.discount_type;
            min_spending = Constants.min_spending;
            max_spending = Constants.max_spending;
            Log.i("TAG", "onMin_spend4: " + min_spending);
        }
        Log.i("TAG", "onMin_spend1: " + min_spending);

        mAdapter = new CheckoutItemAdapter(getActivity(), orderList, language);
        mitem_list.setAdapter(mAdapter);

        offer_layout.setVisibility(View.VISIBLE);

        offer_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.i("TAG", "onClick: " + offer);

//                if (discount != 0 ) {

                if (userId.equalsIgnoreCase("0")) {

                    Intent a = new Intent(getActivity(), SignInActivity.class);
                    startActivity(a);

                } else {

                    Constants.confirm = true;
                    Intent a = new Intent(getActivity(), OfferActivity.class);
                    startActivity(a);

                }
//                }

            }
        });

        Log.i("TAG", "discount: " + discount);
        if (discount != 0) {
            Log.i("TAG", "discount1: " + discount);


            moffer_name.setText("" + offer_name);
//            moffer_desc.setText("" + offer_desc);

            cancel_offer.setText("X");

        } else {

            if (language.equalsIgnoreCase("En")) {
                cancel_offer.setText(">");
            } else {
                cancel_offer.setText("<");
            }

        }

        cancel_offer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (cancel_offer.getText().toString().equalsIgnoreCase("X")) {

                    discount = 0;

                    if (language.equalsIgnoreCase("En")) {
                        moffer_name.setText("Apply Promo Code");
                    } else {
                        moffer_name.setText("غير متوفر");

                    }

                    if (language.equalsIgnoreCase("En")) {
                        cancel_offer.setText(">");
                    } else {
                        cancel_offer.setText("<");
                    }

                    min_order_amount.setVisibility(View.GONE);

                    procced.setBackground(getResources().getDrawable(R.drawable.menu_header_selected));
                    procced.setClickable(true);

                    if (language.equalsIgnoreCase("En")) {
                        coupon_discount.setText("- " + Constants.decimalFormat.format(discount));
                    } else {
                        coupon_discount.setText("" + Constants.decimalFormat.format(discount) + " -");
                    }


                    total_price = (myDbHelper.getTotalOrderPrice() + tax) - discount;
                    float delivery_charges;
                    if (language.equalsIgnoreCase("En")) {
                        delivery_charges = Float.parseFloat(servie_charges.getText().toString().replace("+ ", ""));
                    } else {
                        delivery_charges = Float.parseFloat(servie_charges.getText().toString().replace(" +", ""));
                    }
                    mtotal_price.setText("" + Constants.decimalFormat.format(total_price + delivery_charges));

                }

            }
        });

//        for (int i = 0; i < orderList.size(); i++) {
//            item_total.setText(orderList.get(i).getTotalAmount());
//
//        }

        String orderstatus = null;
//        if (StoreMenuActivity.banner) {
//            brand_address.setText("" + StoreMenuActivity.storeData1.get(StoreMenuActivity.pos).getAddress());
//            Glide.with(getActivity()).load(Constants.IMAGE_URL + StoreMenuActivity.storeData1.get(StoreMenuActivity.pos).getStoreLogo_En()).into(mbrand_logo);
//
//            brand_name.setText("" + StoreMenuActivity.storeData1.get(StoreMenuActivity.pos).getBrandName_En());
//
//            map.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    String geoUri = "http://maps.google.com/maps?q=loc:" + StoreMenuActivity.storeData1.get(StoreMenuActivity.pos).getLatitude() + "," + StoreMenuActivity.storeData1.get(StoreMenuActivity.pos).getLongitude() + " (" + StoreMenuActivity.storeData1.get(StoreMenuActivity.pos).getBranchName_En() + ")";
//                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(geoUri));
//                    startActivity(intent);
//                }
//            });
//
//
//            orderstatus = MenuActivity.storeData1.get(MenuActivity.pos).getOrderTypes();
//        } else {

        if (orderList.size() != 0) {
            for (int i = 0; i < orderList.size(); i++) {
                float delivery_charges;
                if (language.equalsIgnoreCase("En")) {
                    delivery_charges = Float.parseFloat(servie_charges.getText().toString().replace("+ ", ""));
                } else {
                    delivery_charges = Float.parseFloat(servie_charges.getText().toString().replace(" +", ""));
                }
                mtotal_price.setText("" + Constants.decimalFormat.format(myDbHelper.getTotalOrderPrice() + delivery_charges));
                store_address.setText("" + orderList.get(i).getAddress());
                if (language.equalsIgnoreCase("En")) {
                    title_name.setText("" + orderList.get(i).getBranchName());
                    branch_name.setText("" + orderList.get(i).getBranchName());
                } else {
                    title_name.setText("" + orderList.get(i).getBranchName_Ar());
                    branch_name.setText("" + orderList.get(i).getBranchName_Ar());
                }
                item_total.setText("" + Constants.decimalFormat.format(myDbHelper.getTotalOrderPrice()));
                if (language.equalsIgnoreCase("En")) {
                    servie_charges.setText("+ 0.00");
                    coupon_discount.setText("- 0.00");
                } else {
                    servie_charges.setText("0.00 +");
                    coupon_discount.setText("0.00 -");
                }
                lat = Double.parseDouble(orderList.get(i).getLatitute());
                longi = Double.parseDouble(orderList.get(i).getLongitute());

                Constants.branch_id = Integer.parseInt(orderList.get(i).getSubCatId());
                Constants.brand_id = Integer.parseInt(orderList.get(i).getBrandId());

//                final int finalI = i;
//                map_layout.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        String geoUri = "http://maps.google.com/maps?q=loc:" + orderList.get(finalI).getLatitute() + "," + orderList.get(finalI).getLongitute() + " (" + orderList.get(finalI).getBranchName() + ")";
//                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(geoUri));
//                        startActivity(intent);
//                        Log.i("TAG", "onClick: " + orderList.get(finalI).getBranchName());
//                    }
//                });

                mapFragment = (SupportMapFragment) getChildFragmentManager()
                        .findFragmentById(R.id.map);
                mapFragment.getMapAsync(this);

//            Glide.with(getActivity()).load(Constants.IMAGE_URL + orderList.get(i).getImage()).into(mbrand_logo);

//            brand_name.setText("" + orderList.get(i).getBrandName());
//
//            final int finalI = i;
//            map.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    String geoUri = "http://maps.google.com/maps?q=loc:" + orderList.get(finalI).getLatitute() + "," + orderList.get(finalI).getLongitute() + " (" + orderList.get(finalI).getBranchName() + ")";
//                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(geoUri));
//                    startActivity(intent);
//                    Log.i("TAG", "onClick: " + orderList.get(finalI).getBranchName());
//                }
//            });
//
                orderstatus = orderList.get(i).getOrderType();
//            maindiscount = Integer.parseInt(orderList.get(i).getDiscount());
//            min_order_charges = Integer.parseInt(orderList.get(i).getMin_amount()) - 1;


            }

//        }

            String[] orderstatus3 = orderstatus.split(",");


            Log.i("TAG", "orderstatus: " + orderstatus);
            String[] orderstatus2 = orderstatus.split(", ");
            String order1 = null, order2 = null, order3 = null;
            if (orderstatus2[0].contains("TakeAway")) {
                order1 = orderstatus2[0].replace("TakeAway", "Pick Up");
            } else {
                order1 = orderstatus2[0];
            }
            if (orderstatus2[1].contains("TakeAway")) {
                order2 = orderstatus2[1].replace("TakeAway", "Pick Up");
            } else {
                order2 = orderstatus2[1];
            }
            if (orderstatus2[2].contains("TakeAway")) {
                order3 = orderstatus2[2].replace("TakeAway", "Pick Up");
                Log.i("TAG", "takeaway: " + order3);
            } else {
                order3 = orderstatus2[2];
            }
            String order = order1 + "," + order2 + "," + order3;
            Log.i("TAG", "orderstatus1: " + order);
            String[] orderstatus1 = order.split(",");
            if (orderstatus1.length == 1) {
//            order_type_txt_layout.setClickable(false);
                order_type_txt_count = new String[]{orderstatus1[0]};
            } else if (orderstatus1.length == 2) {
//            order_type_txt_layout.setClickable(true);
                order_type_txt_count = new String[]{orderstatus1[0], orderstatus1[1]};
            } else if (orderstatus1.length == 3) {
//            order_type_txt_layout.setClickable(true);
                order_type_txt_count = new String[]{orderstatus1[0], orderstatus1[1], orderstatus1[2]};
            }

//        order_type_txt_layout.setClickable(true);
            if (language.equalsIgnoreCase("En")) {
                order_type_txt_count = new String[]{"Pick Up", "Delivery"};
            } else {
                order_type_txt_count = new String[]{"طريقة التوصيل", "استلام"};
            }
//
//        complete_bg.setVisibility(View.GONE);
//        morder_type_txt_view.setVisibility(View.GONE);
//
            if (language.equalsIgnoreCase("En")) {
                order_type_txt.setText("" + check_ordertype);
                if (check_ordertype.equalsIgnoreCase("Pick Up")) {
                    service.setText("Service Charges");
                    servie_charges.setText("+ 0.00");
                } else {
                    service.setText("Delivery Charges");
                    servie_charges.setText("+ " + Constants.decimalFormat.format(Float.parseFloat(orderList.get(0).getDelivery_charges())));
                }
                float delivery_charges = Float.parseFloat(servie_charges.getText().toString().replace("+ ", ""));
                mtotal_price.setText("" + Constants.decimalFormat.format(total_price + delivery_charges));
            } else {
                order_type_txt.setText("" + check_ordertype_ar);
                if (check_ordertype_ar.equalsIgnoreCase("طريقة التوصيل")) {
                    service.setText("رسوم الخدمات");
                    servie_charges.setText("0.00 +");
                } else {
                    service.setText("رسوم التوصيل");
                    servie_charges.setText("" + Constants.decimalFormat.format(Float.parseFloat(orderList.get(0).getDelivery_charges())) + " +");
                }
                float delivery_charges = Float.parseFloat(servie_charges.getText().toString().replace(" +", ""));
                mtotal_price.setText("" + Constants.decimalFormat.format(total_price + delivery_charges));
            }
            order_type_txt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (language.equalsIgnoreCase("En")) {
                        ChromeHelpPopup chromeHelpPopup = new ChromeHelpPopup(getActivity(), language);
                        chromeHelpPopup.show(view);
                    } else {
                        ChromeHelpPopup1 chromeHelpPopup = new ChromeHelpPopup1(getActivity(), language);
                        chromeHelpPopup.show(view);
                    }

                }
            });

            LocalBroadcastManager.getInstance(getActivity()).registerReceiver(
                    mheader_name, new IntentFilter("OrdertypeUpdate"));


//
//        complete_bg.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                complete_bg.setVisibility(View.GONE);
//
//            }
//        });
//

//            SpinnerAdapter = new ArrayAdapter<String>(getActivity().getApplicationContext(), R.layout.list_spinner, order_type_txt_count) {
//                public View getView(int position, View convertView, ViewGroup parent) {
//                    View v = super.getView(position, convertView, parent);
//
//                    ((TextView) v).setTextSize(1);
//                    ((TextView) v).setTextColor(getResources().getColorStateList(R.color.white));
//
//                    return v;
//                }
//
//                public View getDropDownView(int position, View convertView, ViewGroup parent) {
//                    View v = super.getDropDownView(position, convertView, parent);
//                    v.setBackgroundResource(R.color.white);
//                    ((TextView) v).setTextSize(15);
//                    ((TextView) v).setGravity(Gravity.LEFT);
//                    ((TextView) v).setTextColor(getResources().getColorStateList(R.color.black));
//
//                    return v;
//                }
//            };
//
//            order_type_spinner.setAdapter(SpinnerAdapter);
//
//
//            order_type_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//                @Override
//                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//
//                    order_type_txt.setText(order_type_txt_count[i]);
//
//                }
//
//                @Override
//                public void onNothingSelected(AdapterView<?> adapterView) {
//
//                }
//            });
        }

        String currentDate, currentTime;

        final Calendar c = Calendar.getInstance();
        SimpleDateFormat df3 = new SimpleDateFormat("dd MMM yyyy", Locale.US);
        SimpleDateFormat df4 = new SimpleDateFormat("hh:mm a", Locale.US);
        SimpleDateFormat df5 = new SimpleDateFormat("HH:mm", Locale.US);
        currentDate = df3.format(c.getTime());
        currentTime = df5.format(c.getTime());

        Date time = null;

        try {
            time = df5.parse(currentTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        c.setTime(time);
        c.add(Calendar.MINUTE, 30);
        String pre_mins = df4.format(c.getTime());

        exp_date.setText("" + currentDate);
        exp_time.setText("" + pre_mins);

        expected_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);


                DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {

                                if (year == mYear && monthOfYear == mMonth && dayOfMonth == mDay) {
                                    isToday = true;
                                } else {
                                    isToday = false;
                                }
                                mYear = year;
                                mDay = dayOfMonth;
                                mMonth = monthOfYear;

                                final int hour = c.get(Calendar.HOUR_OF_DAY);
                                final int minute = c.get(Calendar.MINUTE);

                                TimePickerDialog tpd = TimePickerDialog.newInstance
                                        (new TimePickerDialog.OnTimeSetListener() {
                                             @Override
                                             public void onTimeSet(RadialPickerLayout view, int selectedHour, int selectedMinute, int second) {
                                                 if (selectedHour == 0) {
//
                                                     selectedHour += 12;

                                                     format = "AM";
                                                 } else if (selectedHour == 12) {

                                                     format = "PM";

                                                 } else if (selectedHour > 12) {

                                                     selectedHour -= 12;

                                                     format = "PM";

                                                 } else {

                                                     format = "AM";
                                                 }
                                                 if (mMonth < 9) {
                                                     selectedDate = mDay + "-0" + (mMonth + 1) + "-" + mYear;
                                                 } else {
                                                     selectedDate = mDay + "-" + (mMonth + 1) + "-" + mYear;
                                                 }
                                                 selectedTime = String.format("%02d:%02d", selectedHour, selectedMinute) + " " + format;
                                                 time1 = String.format("%02d:%02d:%02d", selectedHour, selectedMinute, second) + " " + format;

                                                 SimpleDateFormat df6 = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
                                                 SimpleDateFormat df3 = new SimpleDateFormat("dd MMM yyyy", Locale.US);

                                                 try {
                                                     cal_date1 = df6.parse(selectedDate);
                                                 } catch (ParseException e) {
                                                     e.printStackTrace();
                                                 }

                                                 selectedDate = df3.format(cal_date1);

                                                 exp_time.setText(selectedTime);
                                                 exp_date.setText(selectedDate);
                                             }
                                         },
                                                hour,
                                                minute,
                                                false
                                        );
                                tpd.setThemeDark(true);
                                tpd.vibrate(false);
                                tpd.setAccentColor(Color.parseColor("#76C8FC"));
                                tpd.setOnCancelListener(new DialogInterface.OnCancelListener() {
                                    @Override
                                    public void onCancel(DialogInterface dialogInterface) {
                                        Log.d("TimePicker", "Dialog was cancelled");
                                    }
                                });
                                if (isToday) {
                                    if (minute > 30) {
                                        tpd.setMinTime(hour + 1, minute - 30, 00);
                                    } else {
                                        tpd.setMinTime(hour, (minute + 30), 00);
                                        Log.i("TAG", "onDateSet: " + minute);
                                        Log.i("TAG", "onDateSet1: " + (minute + 30));
                                    }
                                } else {
                                    tpd.setMinTime(0, 0, 0);
                                }
                                tpd.show(getActivity().getFragmentManager(), "Timepickerdialog");


                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.getDatePicker().setMinDate(c.getTimeInMillis());
                int max_day = Integer.parseInt(orderList.get(0).getMax_days());
                int max_days = max_day;
                long max = TimeUnit.DAYS.toMillis(Long.parseLong(String.valueOf(max_days)));
                Log.i("TAG", "maxdays+1: " + max_days);
                Log.i("TAG", "maxdays: " + orderList.get(0).getMax_days());
                datePickerDialog.getDatePicker().setMaxDate(c.getTimeInMillis() + max);
                datePickerDialog.setTitle("Select Date");
                datePickerDialog.show();

//                showDialog(Time_id);
//                showDialog(Date_id);

            }
        });

        float delivery_charges;
        if (language.equalsIgnoreCase("En")) {
            delivery_charges = Float.parseFloat(servie_charges.getText().toString().replace("+ ", ""));
        } else {
            delivery_charges = Float.parseFloat(servie_charges.getText().toString().replace(" +", ""));
        }
        mtotal_price.setText("" + Constants.decimalFormat.format(myDbHelper.getTotalOrderPrice() + delivery_charges));
//        if (maindiscount != 0) {
//            float maindiscount_value = 0;
//            for (int i = 0; i < orderList.size(); i++) {
//                maindiscount_value = myDbHelper.getMainTotalOrderPrice() * Float.parseFloat(orderList.get(i).getDiscount()) / 100;
//            }
////            offer_discount.setVisibility(View.VISIBLE);
////            moffer.setVisibility(View.VISIBLE);
//
////            CheckOutActivity.offer_discount.setText("- " + Constants.decimalFormat.format(maindiscount_value));
//        } else {
//
//            offer_discount.setVisibility(View.GONE);
//            moffer.setVisibility(View.GONE);
//
//        }
//
//
        tax = myDbHelper.getTotalOrderPrice() * Constants.vatper;
        if (language.equalsIgnoreCase("En")) {
            mvat.setText("+ " + Constants.decimalFormat.format(tax));
        } else {
            mvat.setText("" + Constants.decimalFormat.format(tax) + " +");
        }
        float subtotal;
        subtotal = (myDbHelper.getTotalOrderPrice() + tax);
        sub_total.setText("" + Constants.decimalFormat.format(subtotal));

//        coupon_discount.setText("- " + Constants.decimalFormat.format(discount));


        Log.i("TAG", "tax1: " + myDbHelper.getTotalOrderPrice() * Constants.vatper);
//
//        float discount_value = myDbHelper.getTotalOrderPrice() * ((float) discount / 100);
////            float discount_amount = myDbHelper.getTotalOrderPrice() - discount_value;
//
//        Log.i("TAG", "discount: " + discount_value);
//
//        coupon_discount.setText("- " + Constants.decimalFormat.format(discount_value));
//
//        float coupon_discount_price = (myDbHelper.getTotalOrderPrice() + tax) - discount;
//
//        if (coupon_discount_price > max_spending) {
//
//            total_price = (myDbHelper.getTotalOrderPrice() + tax) - max_spending;
//            mtotal_price.setText("" + Constants.decimalFormat.format(total_price));
//
//        } else {
//
//            total_price = (myDbHelper.getTotalOrderPrice() + tax) - discount;
//            mtotal_price.setText("" + Constants.decimalFormat.format(total_price));
//
//        }

//
//        final_qty.setText("" + myDbHelper.getTotalOrderQty() + " item");
//
//        final_price.setText("SAR " + Constants.decimalFormat.format(total_price));
//
//        proceed_to_pay.setBackgroundColor(Color.parseColor("#013950"));
//        proceed_to_pay.setClickable(true);
//        min_order_amount.setVisibility(View.GONE);
//        if (total_price < min_order_charges) {
//
//            proceed_to_pay.setClickable(false);
//            proceed_to_pay.setBackgroundColor(Color.parseColor("#778899"));
//            min_order_amount.setVisibility(View.VISIBLE);
//            min_order_amount.setText("Subtotal must be greater than SAR " + min_order_charges);
//        }

        if (discount != 0) {

            if ((myDbHelper.getTotalOrderPrice() + tax) >= min_spending) {

                if (discount_type == 1) {

                    if ((myDbHelper.getTotalOrderPrice() + tax) < discount) {

                        discount = (myDbHelper.getTotalOrderPrice() + tax);

                    } else {

                        discount = Constants.discount;

                    }

                    if (language.equalsIgnoreCase("En")) {
                        coupon_discount.setText("- " + Constants.decimalFormat.format(discount));
                    } else {
                        coupon_discount.setText("" + Constants.decimalFormat.format(discount) + " -");
                    }

                    float coupon_discount_price1 = (myDbHelper.getTotalOrderPrice() + tax) - discount;

                    if (coupon_discount_price1 > max_spending) {

                        total_price = (myDbHelper.getTotalOrderPrice() + tax) - max_spending;
                        float delivery_charges1;
                        if (language.equalsIgnoreCase("En")) {
                            delivery_charges1 = Float.parseFloat(servie_charges.getText().toString().replace("+ ", ""));
                        } else {
                            delivery_charges1 = Float.parseFloat(servie_charges.getText().toString().replace(" +", ""));
                        }
                        mtotal_price.setText("" + Constants.decimalFormat.format(total_price + delivery_charges1));

                    } else {

                        total_price = (myDbHelper.getTotalOrderPrice() + tax) - discount;
                        float delivery_charges2;
                        if (language.equalsIgnoreCase("En")) {
                            delivery_charges2 = Float.parseFloat(servie_charges.getText().toString().replace("+ ", ""));
                        } else {
                            delivery_charges2 = Float.parseFloat(servie_charges.getText().toString().replace(" +", ""));
                        }
                        mtotal_price.setText("" + Constants.decimalFormat.format(total_price + delivery_charges2));

                    }

                    if ((myDbHelper.getTotalOrderPrice() + tax) >= min_spending) {

                        min_order_amount.setVisibility(View.GONE);

                        procced.setBackground(getResources().getDrawable(R.drawable.menu_header_selected));
                        procced.setClickable(true);

                    } else {

                        min_order_amount.setVisibility(View.VISIBLE);
                        min_order_amount.setText("Promotion applied, cart amount must be greater than " + min_spending + " SAR");

                        procced.setBackground(getResources().getDrawable(R.drawable.check_out_unselected));
                        procced.setClickable(false);

                    }

                } else {

                    float discount_value = (myDbHelper.getTotalOrderPrice() + tax) * ((float) discount / 100);
//            float discount_amount = myDbHelper.getTotalOrderPrice() - discount_value;

                    Log.i("TAG", "discount: " + discount_value);

                    if ((myDbHelper.getTotalOrderPrice() + tax) < discount_value) {

                        discount_value = (myDbHelper.getTotalOrderPrice() + tax);

                    } else {

                        discount_value = (myDbHelper.getTotalOrderPrice() + tax) * ((float) discount / 100);

                    }

                    if (language.equalsIgnoreCase("En")) {
                        coupon_discount.setText("- " + Constants.decimalFormat.format(discount_value));
                    } else {
                        coupon_discount.setText("" + Constants.decimalFormat.format(discount_value) + " -");
                    }

                    float coupon_discount_price1 = (myDbHelper.getTotalOrderPrice() + tax) - discount_value;

                    if (coupon_discount_price1 > max_spending) {

                        total_price = (myDbHelper.getTotalOrderPrice() + tax) - max_spending;
                        float delivery_charges1;
                        if (language.equalsIgnoreCase("En")) {
                            delivery_charges1 = Float.parseFloat(servie_charges.getText().toString().replace("+ ", ""));
                        } else {
                            delivery_charges1 = Float.parseFloat(servie_charges.getText().toString().replace(" +", ""));
                        }
                        mtotal_price.setText("" + Constants.decimalFormat.format(total_price + delivery_charges1));

                    } else {

                        total_price = (myDbHelper.getTotalOrderPrice() + tax) - discount_value;
                        float delivery_charges2;
                        if (language.equalsIgnoreCase("En")) {
                            delivery_charges2 = Float.parseFloat(servie_charges.getText().toString().replace("+ ", ""));
                        } else {
                            delivery_charges2 = Float.parseFloat(servie_charges.getText().toString().replace(" +", ""));
                        }
                        mtotal_price.setText("" + Constants.decimalFormat.format(total_price + delivery_charges2));

                    }

                    if ((myDbHelper.getTotalOrderPrice() + tax) >= min_spending) {

                        min_order_amount.setVisibility(View.GONE);

                        procced.setBackground(getResources().getDrawable(R.drawable.menu_header_selected));
                        procced.setClickable(true);

                    } else {

                        min_order_amount.setVisibility(View.VISIBLE);
                        min_order_amount.setText("Promotion applied, cart amount must be greater than " + min_spending + " SAR");

                        procced.setBackground(getResources().getDrawable(R.drawable.check_out_unselected));
                        procced.setClickable(false);

                    }

                }
            }
        } else {

            if (language.equalsIgnoreCase("En")) {
                coupon_discount.setText("- " + Constants.decimalFormat.format(discount));
            } else {
                coupon_discount.setText("" + Constants.decimalFormat.format(discount) + " -");
            }

            total_price = (myDbHelper.getTotalOrderPrice() + tax) - discount;
            float delivery_charges1;
            if (language.equalsIgnoreCase("En")) {
                delivery_charges1 = Float.parseFloat(servie_charges.getText().toString().replace("+ ", ""));
            } else {
                delivery_charges1 = Float.parseFloat(servie_charges.getText().toString().replace(" +", ""));
            }
            mtotal_price.setText("" + Constants.decimalFormat.format(total_price + delivery_charges1));

        }

        procced.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                userId = userPrefs.getString("userId", "0");
                if (userId.equals("0")) {

                    Intent a = new Intent(getActivity(), SignInActivity.class);
                    startActivity(a);

                } else {

                    if (language.equalsIgnoreCase("En")) {
                        check_ordertype = order_type_txt.getText().toString();
                    } else {
                        if (order_type_txt.getText().toString().equalsIgnoreCase("طريقة التوصيل")) {
                            check_ordertype = "Pick Up";
                        } else if (order_type_txt.getText().toString().equalsIgnoreCase("استلام")) {
                            check_ordertype = "Delivery";
                        }
                    }
                    Intent a = new Intent(getActivity(), ConfirmationActivity.class);
//                    a.putExtra("ordertype", ordertype1);
//                    a.putExtra("deliverytime", expected_time.getText().toString());

                    startActivity(a);

                }

            }
        });


        if (getActivity() != null) {
            if (!userId.equalsIgnoreCase("0")) {
                if (Constants.offer_service) {

                    Constants.offer_service = false;
                    Log.i("TAG", "onCreateView: ");
                    String networkStatus1 = NetworkUtil.getConnectivityStatusString(getActivity());
                    if (!networkStatus1.equalsIgnoreCase("Not connected to Internet")) {
                        new OfferApi().execute();
                    } else {
                        Toast.makeText(getActivity(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    }

                } else {
                    for (int i = 0; i < data.size(); i++) {
                        Log.i("TAG", "minspen: " + data.get(i).getMinSpend());

                        if ((myDbHelper.getTotalOrderPrice() + tax) >= data.get(i).getMinSpend()) {

                            Log.i("TAG", "exp_time11: " + exp_time.getText().toString());
                            String networkStatus1 = NetworkUtil.getConnectivityStatusString(getActivity());
                            if (!networkStatus1.equalsIgnoreCase("Not connected to Internet")) {
                                new OfferApi().execute();
                            } else {
                                Toast.makeText(getActivity(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                            }

                        }
                    }
                }
            }
            String networkStatus1 = NetworkUtil.getConnectivityStatusString(getActivity());
            if (!networkStatus1.equalsIgnoreCase("Not connected to Internet")) {
                new GetStoresApi().execute();
            } else {
                Toast.makeText(getActivity(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
            }


        }

        Log.i("TAG", "exp_time: " + exp_time.getText().toString());
        return rootView;
    }

    private BroadcastReceiver mheader_name = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String b = intent.getStringExtra("ordertype");
            String ar = intent.getStringExtra("ordertype_ar");

            if (language.equalsIgnoreCase("En")) {
                order_type_txt.setText("" + b);
                if (b.equalsIgnoreCase("Pick Up")) {
                    service.setText("Service Charges");
                    servie_charges.setText("+ 0.00");
                } else {
                    service.setText("Delivery Charges");
                    servie_charges.setText("+ " + Constants.decimalFormat.format(Float.parseFloat(orderList.get(0).getDelivery_charges())));
                }
                float delivery_charges = Float.parseFloat(servie_charges.getText().toString().replace("+ ", ""));
                mtotal_price.setText("" + Constants.decimalFormat.format(total_price + delivery_charges));
            } else {
                order_type_txt.setText("" + ar);
                if (ar.equalsIgnoreCase("طريقة التوصيل")) {
                    service.setText("رسوم الخدمات");
                    servie_charges.setText("0.00 +");
                } else {
                    service.setText("رسوم التوصيل");
                    servie_charges.setText("" + Constants.decimalFormat.format(Float.parseFloat(orderList.get(0).getDelivery_charges())) + " +");
                }
                float delivery_charges = Float.parseFloat(servie_charges.getText().toString().replace(" +", ""));
                mtotal_price.setText("" + Constants.decimalFormat.format(total_price + delivery_charges));
            }
        }
    };

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        final LatLng store = new LatLng(lat, longi);

        mMap.moveCamera(CameraUpdateFactory.newLatLng(store));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(16.0f));
        mMap.setOnCameraMoveListener(new GoogleMap.OnCameraMoveListener() {
            @Override
            public void onCameraMove() {
                mMap.moveCamera(CameraUpdateFactory.newLatLng(store));
            }
        });
    }


//    @Override
//    public void onTimeSet(RadialPickerLayout view, int selectedHour, int selectedMinute, int second) {
//
//        if (selectedHour == 0) {
////
//            selectedHour += 12;
//
//            format = "AM";
//        } else if (selectedHour == 12) {
//
//            format = "PM";
//
//        } else if (selectedHour > 12) {
//
//            selectedHour -= 12;
//
//            format = "PM";
//
//        } else {
//
//            format = "AM";
//        }
//        if (mMonth < 9) {
//            selectedDate = mDay + "-0" + (mMonth + 1) + "-" + mYear;
//        } else {
//            selectedDate = mDay + "-" + (mMonth + 1) + "-" + mYear;
//        }
//        selectedTime = String.format("%02d:%02d", selectedHour, selectedMinute) + " " + format;
//        time1 = String.format("%02d:%02d:%02d", selectedHour, selectedMinute, second) + " " + format;
//
//        SimpleDateFormat df6 = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
//        SimpleDateFormat df3 = new SimpleDateFormat("dd MMM yyyy", Locale.US);
//
//        try {
//            cal_date1 = df6.parse(selectedDate);
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//
//        selectedDate = df3.format(cal_date1);
//
//        exp_time.setText(selectedTime);
//        exp_date.setText(selectedDate);
//
//        Log.i("TAG", "onCreateView: " + exp_time.getText().toString());
//
////        if(alertDialog1 != null){
////            alertDialog1.dismiss();
//////                        showDialog1(secs, false, "");
////        }
////
////        if(alertDialog != null){
////            alertDialog.dismiss();
//////                        showDialog1(secs, false, "");
////        }
//
//
//    }

//    public void showloaderAlertDialog(){
//        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
//        // ...Irrelevant code for customizing the buttons and title
//        LayoutInflater inflater = getLayoutInflater();
//        int layout = R.layout.loder_dialog;
//        View dialogView = inflater.inflate(layout, null);
//        dialogBuilder.setView(dialogView);
//        dialogBuilder.setCancelable(true);
//
//        loaderDialog = dialogBuilder.create();
//        loaderDialog.show();
//        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
//        Window window = loaderDialog.getWindow();
//        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        lp.copyFrom(window.getAttributes());
//        //This makes the dialog take up the full width
//        Display display = getWindowManager().getDefaultDisplay();
//        Point size = new Point();
//        display.getSize(size);
//        int screenWidth = size.x;
//
//        double d = screenWidth*0.85;
//        lp.width = (int) d;
//        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
//        window.setAttributes(lp);
//    }

    private class InsertOrderApi extends AsyncTask<String, String, String> {

        //        ACProgressFlower dialog;
        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareVerifyMobileJson();
//            dialog = new ACProgressFlower.Builder(getActivity())
//                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
//                    .themeColor(Color.WHITE)
//                    .fadeColor(Color.DKGRAY).build();
//            dialog.show();
            Constants.showLoadingDialog(getActivity());
//            showloaderAlertDialog();
        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<InsertOrder> call = apiService.getInsertOrder(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<InsertOrder>() {
                @Override
                public void onResponse(Call<InsertOrder> call, Response<InsertOrder> response) {
                    Log.d("TAG", "onResponse: " + response);
                    if (response.isSuccessful()) {
                        InsertOrder orderItems = response.body();
                        try {
                            if (orderItems.getStatus()) {
                                String message = orderItems.getMessage();


                                myDbHelper.deleteOrderTable();
//                                Intent a = new Intent(getActivity(), ConfirmationActivity.class);
//                                a.putExtra("userid", orderItems.getData().getUserId());
//                                a.putExtra("orderid", orderItems.getData().getOrderId());
//                                a.putExtra("track_order", "confirmation");

//                                startActivity(a);


                            } else {
                                //                          status false case
                                String failureResponse = orderItems.getMessage();
                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name),
                                        getResources().getString(R.string.ok), getActivity());
                            }
//                            Log.i("TAG", "onResponse: " + data.size());
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(getActivity(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(getActivity(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }

                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<InsertOrder> call, Throwable t) {
                    Log.d("TAG", "onFailure: " + t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(getActivity(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getActivity(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }

                    Constants.closeLoadingDialog();
                }
            });
            return null;
        }


        private String prepareVerifyMobileJson() {

            Log.i("TAG", "userid json: " + userId);
            JSONObject parentObj = new JSONObject();
            try {

                JSONObject OrderDetails = new JSONObject();

                PackageInfo pInfo = null;
                try {
                    pInfo = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0);
                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                }
                String version = pInfo.versionName;
                JSONArray orderitem = new JSONArray();
                Log.i("TAG", "orderitem: " + orderitem.length());
                Log.i("TAG", "orderList: " + orderList.size());
//                for (int i = 0; i < orderitem.length(); i++) {
                for (int i = 0; i < orderList.size(); i++) {
                    JSONObject jo = new JSONObject();

                    jo.put("ItemId", orderList.get(i).getItemId());
                    jo.put("Comments", orderList.get(i).getComment());
                    jo.put("ItemPrice", orderList.get(i).getDis_item_add_price());

                    JSONArray Additionalitem = new JSONArray();
                    if (orderList.get(i).getAdditionalsTypeId() != null) {
                        String ids = orderList.get(i).getAdditionalsTypeId();
                        String qty = orderList.get(i).getAdditionalitemQTY();
                        String price = orderList.get(i).getAdditionalsPrice();
                        List<String> idsList = Arrays.asList(ids.split(","));
                        List<String> qtyList = Arrays.asList(qty.split(","));
                        List<String> priceList = Arrays.asList(price.split(","));
                        for (int j = 0; j < idsList.size(); j++) {
                            if (!idsList.get(j).equals("")) {

                                JSONObject jo1 = new JSONObject();

                                jo1.put("AddItemId", idsList.get(j));
                                jo1.put("AddItemPrice", priceList.get(j));
                                jo1.put("Quantity", qtyList.get(j));

                                Additionalitem.put(jo1);
                            }
                        }
                    }


                    jo.put("Quantity", orderList.get(i).getQty());
                    jo.put("SizeId", orderList.get(i).getItemType());


                    jo.put("AdditionalItems", Additionalitem);
                    orderitem.put(jo);
                }
//                }

                int ordertype = 0;
                if (order_type_txt.getText().toString().equals("Dine-In")) {
                    ordertype = 1;
                } else if (order_type_txt.getText().toString().equals("Pick Up")) {
                    ordertype = 2;
                } else if (order_type_txt.getText().toString().equals("Delivery")) {
                    ordertype = 3;
                }

                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
                SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
                SimpleDateFormat dateFormat2 = new SimpleDateFormat("HH:mm:ss", Locale.US);
                SimpleDateFormat df3 = new SimpleDateFormat("dd MMM yyyy hh:mm a", Locale.US);
                SimpleDateFormat df4 = new SimpleDateFormat("hh:mm a", Locale.US);

                String expected_date, expected_time1;

                Date expect_date = null, expect_time = null;

//                expected_date = exp_date.getText().toString();
                expected_time1 = expected_time.getText().toString();

//                Log.i("TAG", "expectedtime1: " + expected_date);

                expected_date = expected_time1;

                Log.i("TAG", "expectedtime2: " + expected_date);

                try {
                    expect_date = df4.parse(expected_date);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                expected_date = df4.format(expect_date);

                Log.i("TAG", "expectedtime3: " + expected_date);


                JSONObject OrderDetails1 = new JSONObject();
                OrderDetails1.put("Version", "Android v" + version);
                OrderDetails1.put("VatPercentage", Constants.vatper);
                OrderDetails1.put("TotalPrice", total_price);
                OrderDetails1.put("ExpectedTime", expected_date);
                OrderDetails1.put("UserId", userId);
                OrderDetails1.put("AddressID", "");
                OrderDetails1.put("OrderType", ordertype);
//                if (StoreMenuActivity.banner) {
//                    OrderDetails1.put("BrandId", StoreMenuActivity.storeData1.get(StoreMenuActivity.pos).getBrandId());
//                    OrderDetails1.put("BranchId", StoreMenuActivity.storeData1.get(StoreMenuActivity.pos).getBranchId());
//                } else {
                for (int i = 0; i < orderList.size(); i++) {
                    OrderDetails1.put("BrandId", orderList.get(i).getBrandId());
                    OrderDetails1.put("BranchId", orderList.get(i).getSubCatId());
                }
//                }
                OrderDetails1.put("SubTotal", myDbHelper.getTotalOrderPrice());
                OrderDetails1.put("Devicetoken", SplashScreenActivity.regId);
                OrderDetails1.put("PaymentMode", PaymentType);
//                OrderDetails1.put("VatCharges", tax);

                OrderDetails.put("OrderItems", orderitem);
                OrderDetails.put("OrderDetails", OrderDetails1);
                parentObj.put("OrderDetails", OrderDetails);

            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.d("TAG", "prepareVerifyMobileJson: " + parentObj);
            return parentObj.toString();
        }
    }

    protected Dialog onCreateDialog(int id) {

        // Get the calander
        Calendar c = Calendar.getInstance();

        // From calander get the year, month, day, hour, minute
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);
        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);


//        int day1 = c.get(Calendar.DAY_OF_MONTH + 1);

        Log.i("TAG", "onCreateDialog: " + day + "/" + month + "/" + year);
        Log.i("TAG", "onCreateDialog: " + hour + ":" + minute);

        String cal_date, cal_date2, cal_time;

        cal_date = String.valueOf(month) + "/" + String.valueOf(day)
                + "/" + String.valueOf(year);
        cal_time = String.valueOf(hour) + ":" + String.valueOf(minute);

        SimpleDateFormat df6 = new SimpleDateFormat("MM/dd/yyyy", Locale.US);
        SimpleDateFormat df5 = new SimpleDateFormat("HH:mm", Locale.US);

        try {
            cal_date1 = df6.parse(cal_date);
//            cal_date3 = df6.parse(cal_date2);
            cal_time1 = df5.parse(cal_time);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm", Locale.US);
        c.setTime(cal_time1);
        c.add(Calendar.MINUTE, 30);
        String pre_mins = simpleDateFormat.format(c.getTime());

        String[] time = pre_mins.split(":");

        hour = Integer.parseInt(time[0]);
        minute = Integer.parseInt(time[1]);

        c.setTime(cal_date1);
        c.add(Calendar.DAY_OF_MONTH, 1);
        String date = df6.format(c.getTime());

        try {
            cal_date3 = df6.parse(date);
            cal_time1 = df5.parse(pre_mins);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Log.i("TAG", "pre_mins: " + pre_mins);

        switch (id) {

            case Time_id:

                // Open the timepicker dialog
//                return new TimePickerDialog(getActivity(), time_listener, hour,
//                        minute, false);

            case Date_id:

                // Open the datepicker dialog
                return new DatePickerDialog(getActivity(), date_listener, year,
                        month, day);

        }
        return null;
    }


    // Date picker dialog
    DatePickerDialog.OnDateSetListener date_listener = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int month, int day) {
            // store the data in one string and set it to text
            String date1 = String.valueOf(month) + "/" + String.valueOf(day)
                    + "/" + String.valueOf(year);
            String date2 = String.valueOf(month) + "/" + String.valueOf(day + 1)
                    + "/" + String.valueOf(year);

            Log.i("TAG", "ondate: " + date2);

            Date date_picker = null, date_picker1 = null;

            SimpleDateFormat df6 = new SimpleDateFormat("MM/dd/yyyy", Locale.US);
            SimpleDateFormat df3 = new SimpleDateFormat("dd MMM yyyy", Locale.US);

            try {
                date_picker = df6.parse(date1);
                date_picker1 = df6.parse(date2);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            Date date = null, date3 = null;

            Log.i("TAG", "date_picker: " + date_picker);
            Log.i("TAG", "cal_date: " + cal_date1);
            Log.i("TAG", "cal_date+1: " + cal_date3);

            if (date_picker.before(cal_date1) || date_picker.after(cal_date3)) {

                AlertDialog customDialog = null;
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
                // ...Irrelevant code for customizing the buttons and title
                LayoutInflater inflater = getLayoutInflater();
                int layout = R.layout.alert_dialog;
                View dialogView = inflater.inflate(layout, null);
                dialogBuilder.setView(dialogView);
                dialogBuilder.setCancelable(false);

                TextView title = (TextView) dialogView.findViewById(R.id.title);
                TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                View vert = (View) dialogView.findViewById(R.id.vert_line);

                no.setVisibility(View.GONE);
                vert.setVisibility(View.GONE);

                title.setText(getResources().getString(R.string.app_name));
                yes.setText(getResources().getString(R.string.ok));
                desc.setText("Order can't be processed for selected date");

                customDialog = dialogBuilder.create();
                customDialog.show();

                final AlertDialog finalCustomDialog = customDialog;
                yes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        finalCustomDialog.dismiss();
                    }
                });

                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                Window window = customDialog.getWindow();
                window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                lp.copyFrom(window.getAttributes());
                //This makes the dialog take up the full width
                Display display = getActivity().getWindowManager().getDefaultDisplay();
                Point size = new Point();
                display.getSize(size);
                int screenWidth = size.x;

                double d = screenWidth * 0.85;
                lp.width = (int) d;
                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                window.setAttributes(lp);

            } else {

                try {
                    date = df6.parse(date1);
                } catch (ParseException e) {
                    e.printStackTrace();
                }


                date1 = df3.format(date);
                exp_date.setText(date1);

            }

        }
    };
////    TimePickerDialog.OnTimeSetListener time_listener = new TimePickerDialog.OnTimeSetListener() {
////
////        @Override
////        public void onTimeSet(TimePicker view, int hour, int minute) {
////            // store the data in one string and set it to text
////            String time1 = String.valueOf(hour) + ":" + String.valueOf(minute);
//////            String time2 = String.valueOf(hour) + ":" + String.valueOf(minute + 30);
////
////            Date time_picker = null, time_picker1 = null;
////
////            SimpleDateFormat df5 = new SimpleDateFormat("HH:mm", Locale.US);
////            SimpleDateFormat df4 = new SimpleDateFormat("hh:mm a", Locale.US);
////
////            try {
////                time_picker = df5.parse(time1);
//////                date_picker1 = df6.parse(date2);
////            } catch (ParseException e) {
////                e.printStackTrace();
////            }
////
////            Date date = null, date3 = null;
////
////            Log.i("TAG", "onTimeSet: " + time_picker);
////            Log.i("TAG", "onTimeSet1: " + cal_time1);
////
////            if (time_picker.before(cal_time1)) {
////
////                AlertDialog customDialog = null;
////                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
////                // ...Irrelevant code for customizing the buttons and title
////                LayoutInflater inflater = getLayoutInflater();
////                int layout = R.layout.alert_dialog;
////                View dialogView = inflater.inflate(layout, null);
////                dialogBuilder.setView(dialogView);
////                dialogBuilder.setCancelable(false);
////
////                TextView title = (TextView) dialogView.findViewById(R.id.title);
////                TextView desc = (TextView) dialogView.findViewById(R.id.desc);
////                TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
////                TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
////                View vert = (View) dialogView.findViewById(R.id.vert_line);
////
////                no.setVisibility(View.GONE);
////                vert.setVisibility(View.GONE);
////
////                title.setText(getResources().getString(R.string.app_name));
////                yes.setText(getResources().getString(R.string.ok));
////                desc.setText("Order can't be processed for selected time");
////
////                customDialog = dialogBuilder.create();
////                customDialog.show();
////
////                final AlertDialog finalCustomDialog = customDialog;
////                yes.setOnClickListener(new View.OnClickListener() {
////                    @Override
////                    public void onClick(View view) {
////                        finalCustomDialog.dismiss();
////                    }
////                });
////
////                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
////                Window window = customDialog.getWindow();
////                window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
////                lp.copyFrom(window.getAttributes());
////                //This makes the dialog take up the full width
////                Display display = getActivity().getWindowManager().getDefaultDisplay();
////                Point size = new Point();
////                display.getSize(size);
////                int screenWidth = size.x;
////
////                double d = screenWidth * 0.85;
////                lp.width = (int) d;
////                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
////                window.setAttributes(lp);
////
////            } else {
////
////                try {
////                    date = df5.parse(time1);
////                } catch (ParseException e) {
////                    e.printStackTrace();
////                }
////
////
////                time1 = df4.format(date);
////                exp_time.setText(time1);
////
////            }
////
////
////            Log.i("TAG", "onTimeSet: " + time1);
////            Log.i("TAG", "onTimeSet1: " + time2);
//
////            SimpleDateFormat df5 = new SimpleDateFormat("HH:mm", Locale.US);
////            SimpleDateFormat df4 = new SimpleDateFormat("hh:mm a", Locale.US);
////            Date time = null;
////
////            try {
////                time = df5.parse(time1);
////            } catch (ParseException e) {
////                e.printStackTrace();
////            }
////            time1 = df4.format(time);
////            exp_time.setText(time1);
//        }
//    };

    private class OfferApi extends AsyncTask<String, String, String> {

        AlertDialog customDialog = null;
        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareVerifyMobileJson1();
//            dialog = new ACProgressFlower.Builder(getActivity())
//                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
//                    .themeColor(Color.WHITE)
//                    .fadeColor(Color.DKGRAY).build();
//            dialog.show();

            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getContext());
            // ...Irrelevant code for customizing the buttons and title
            LayoutInflater inflater = getActivity().getLayoutInflater();
            int layout = R.layout.progress_bar_alert;
            View dialogView = inflater.inflate(layout, null);
            dialogBuilder.setView(dialogView);
            dialogBuilder.setCancelable(false);

            customDialog = dialogBuilder.create();
            customDialog.show();
            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            Window window = customDialog.getWindow();
            window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            lp.copyFrom(window.getAttributes());
            //This makes the progressDialog take up the full width
            Display display = getActivity().getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            int screenWidth = size.x;

            double d = screenWidth * 0.45;
            lp.width = (int) d;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
            window.setAttributes(lp);

//            Constants.showLoadingDialog(getActivity());
        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<Promotions> call = apiService.getPromotions(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<Promotions>() {
                @Override
                public void onResponse(Call<Promotions> call, Response<Promotions> response) {
                    Log.d("TAG", "onResponse: " + response);
                    if (response.isSuccessful()) {
                        Promotions storeMenuItems = response.body();
                        data = storeMenuItems.getData();

                        ArrayList<Integer> min_spend = new ArrayList<>();
                        for (int i = 0; i < data.size(); i++) {
                            min_spend.add(data.get(i).getMinSpend());
                        }

                        for (int j = 0; j < min_spend.size(); j++) {
                            if ((myDbHelper.getTotalOrderPrice() + tax) >= min_spend.get(j)) {

                                try {
                                    if (storeMenuItems.getStatus()) {
                                        String message = storeMenuItems.getMessage();

                                        for (int i = 0; i < data.size(); i++) {
                                            if (data.get(i).getTodayAvailable()) {
                                                OffersDiscount offers = new OffersDiscount();
                                                if ((myDbHelper.getTotalOrderPrice() + tax) >= data.get(i).getMinSpend()) {
                                                    offers.setPromoTitle_Ar(data.get(i).getPromoTitle_Ar());
                                                    offers.setPromoTitle_En(data.get(i).getPromotionCode());
                                                    offers.setDesc_Ar(data.get(i).getDesc_Ar());
                                                    offers.setDesc_En(data.get(i).getDesc_En());
                                                    offers.setDiscountType(data.get(i).getDiscountType());
                                                    offers.setDiscount_main(data.get(i).getDiscountAmt());

                                                    if (data.get(i).getDiscountType() == 1) {

                                                        float coupon_discount_price1 = (myDbHelper.getTotalOrderPrice() + tax) - ((float) data.get(i).getDiscountAmt());

                                                        if (coupon_discount_price1 > data.get(i).getMaxAmount()) {

                                                            offers.setDiscountAmt(Float.parseFloat(String.valueOf(data.get(i).getMaxAmount())));

                                                        } else {

                                                            float discountvalue = (myDbHelper.getTotalOrderPrice() + tax) - ((float) data.get(i).getDiscountAmt());
                                                            offers.setDiscountAmt(discountvalue);

                                                        }


                                                    } else {

                                                        float discount_value = (myDbHelper.getTotalOrderPrice() + tax) * ((float) data.get(i).getDiscountAmt() / 100);
//            float discount_amount = myDbHelper.getTotalOrderPrice() - discount_value;

                                                        Log.i("TAG", "discount: " + discount_value);

                                                        float coupon_discount_price = (myDbHelper.getTotalOrderPrice() + tax) - discount_value;

                                                        if (coupon_discount_price > max_spending) {

                                                            offers.setDiscountAmt(Float.parseFloat(String.valueOf(data.get(i).getMaxAmount())));

                                                        } else {

                                                            float discountvalue = (myDbHelper.getTotalOrderPrice() + tax) * ((float) data.get(i).getDiscountAmt() / 100);
                                                            offers.setDiscountAmt(discountvalue);

                                                        }

                                                    }

                                                    Log.i("TAG", "offeramtlist: " + offers.getDiscountAmt());
                                                    offers.setTodaysavailable(data.get(i).getTodayAvailable());
                                                    offers.setMin_spending(data.get(i).getMinSpend());
                                                    offers.setMax_spending(data.get(i).getMaxAmount());

                                                    offersDiscounts.add(offers);
                                                }


                                            }

                                            Log.d("TAG", "" + offersDiscounts.size());
                                        }

//                                Log.d("TAG", "offersDiscounts.get(0).getMin_spending()" + offersDiscounts.get(0).getMin_spending());

                                        Comparator<OffersDiscount> DiscountSort = new Comparator<OffersDiscount>() {

                                            public int compare(OffersDiscount s1, OffersDiscount s2) {

                                                float discount1 = (s1.getDiscountAmt());
                                                float discount2 = (s2.getDiscountAmt());

                                                /*For descending order*/
//                                        return Double.compare(discount2, discount1);

                                                /*For ascending order*/
                                                return Double.compare(discount1, discount2);//rollno1-rollno2;
                                            }
                                        };

                                        Collections.sort(offersDiscounts, DiscountSort);

                                        if (Constants.discount == 0) {

//                                    if ((myDbHelper.getTotalOrderPrice() + tax) >= offersDiscounts.get(0).getMin_spending()) {
                                            discount = offersDiscounts.get(0).getDiscount_main();
                                            discount_type = offersDiscounts.get(0).getDiscountType();
                                            min_spending = offersDiscounts.get(0).getMin_spending();
                                            max_spending = offersDiscounts.get(0).getMax_spending();
//                                    }
//                                    moffer_name.setText("" + offersDiscounts.get(0).getPromoTitle_En());
//                                    moffer_desc.setText("" + offersDiscounts.get(0).getDesc_En());
                                            Log.i("TAG", "offertype: " + discount_type);
                                            Log.i("TAG", "minSpend: " + offersDiscounts.get(0).getMin_spending());
                                            Log.i("TAG", "minSpend: " + offersDiscounts.get(0).getPromoTitle_En());
                                            Log.i("TAG", "offerdiscount: " + discount);
                                            Log.i("TAG", "offeramt: " + offersDiscounts.get(0).getDiscountAmt());

                                        }

                                        Log.i("TAG", "onMin_spend3: " + min_spending);
                                        if (discount != 0) {
                                            if ((myDbHelper.getTotalOrderPrice() + tax) >= min_spending) {


//                                                if (language.equalsIgnoreCase("En")) {
                                                moffer_name.setText("" + offersDiscounts.get(0).getPromoTitle_En());
//                                                } else {
//                                                    moffer_name.setText("" + offersDiscounts.get(0).getPromoTitle_Ar());
//                                                }

                                                cancel_offer.setText("X");

                                                if (discount_type == 1) {

                                                    if ((myDbHelper.getTotalOrderPrice() + tax) < discount) {

                                                        discount = (myDbHelper.getTotalOrderPrice() + tax);

                                                    } else {

                                                        discount = Constants.discount;

                                                    }

                                                    if (language.equalsIgnoreCase("En")) {
                                                        coupon_discount.setText("- " + Constants.decimalFormat.format(discount));
                                                    } else {
                                                        coupon_discount.setText("" + Constants.decimalFormat.format(discount) + " -");
                                                    }

                                                    float coupon_discount_price1 = (myDbHelper.getTotalOrderPrice() + tax) - discount;

                                                    if (coupon_discount_price1 > max_spending) {

                                                        total_price = (myDbHelper.getTotalOrderPrice() + tax) - max_spending;
                                                        float delivery_charges1;
                                                        if (language.equalsIgnoreCase("En")) {
                                                            delivery_charges1 = Float.parseFloat(servie_charges.getText().toString().replace("+ ", ""));
                                                        } else {
                                                            delivery_charges1 = Float.parseFloat(servie_charges.getText().toString().replace(" +", ""));
                                                        }
                                                        mtotal_price.setText("" + Constants.decimalFormat.format(total_price + delivery_charges1));

                                                    } else {
                                                        float delivery_charges1;
                                                        if (language.equalsIgnoreCase("En")) {
                                                            delivery_charges1 = Float.parseFloat(servie_charges.getText().toString().replace("+ ", ""));
                                                        } else {
                                                            delivery_charges1 = Float.parseFloat(servie_charges.getText().toString().replace(" +", ""));
                                                        }
                                                        total_price = (myDbHelper.getTotalOrderPrice() + tax) - discount;
                                                        mtotal_price.setText("" + Constants.decimalFormat.format(total_price + delivery_charges1));

                                                    }

                                                } else {

                                                    float discount_value = (myDbHelper.getTotalOrderPrice() + tax) * ((float) discount / 100);
//            float discount_amount = myDbHelper.getTotalOrderPrice() - discount_value;

                                                    Log.i("TAG", "discount: " + discount_value);

                                                    if ((myDbHelper.getTotalOrderPrice() + tax) < discount_value) {

                                                        discount_value = (myDbHelper.getTotalOrderPrice() + tax);

                                                    } else {

                                                        discount_value = (myDbHelper.getTotalOrderPrice() + tax) * ((float) discount / 100);

                                                    }

                                                    if (language.equalsIgnoreCase("En")) {
                                                        coupon_discount.setText("- " + Constants.decimalFormat.format(discount_value));
                                                    } else {
                                                        coupon_discount.setText("" + Constants.decimalFormat.format(discount_value) + " -");
                                                    }

                                                    float coupon_discount_price1 = (myDbHelper.getTotalOrderPrice() + tax) - discount_value;

                                                    if (coupon_discount_price1 > max_spending) {

                                                        total_price = (myDbHelper.getTotalOrderPrice() + tax) - max_spending;
                                                        float delivery_charges1;
                                                        if (language.equalsIgnoreCase("En")) {
                                                            delivery_charges1 = Float.parseFloat(servie_charges.getText().toString().replace("+ ", ""));
                                                        } else {
                                                            delivery_charges1 = Float.parseFloat(servie_charges.getText().toString().replace(" +", ""));
                                                        }
                                                        mtotal_price.setText("" + Constants.decimalFormat.format(total_price + delivery_charges1));

                                                    } else {

                                                        total_price = (myDbHelper.getTotalOrderPrice() + tax) - discount_value;
                                                        float delivery_charges1;
                                                        if (language.equalsIgnoreCase("En")) {
                                                            delivery_charges1 = Float.parseFloat(servie_charges.getText().toString().replace("+ ", ""));
                                                        } else {
                                                            delivery_charges1 = Float.parseFloat(servie_charges.getText().toString().replace(" +", ""));
                                                        }
                                                        mtotal_price.setText("" + Constants.decimalFormat.format(total_price + delivery_charges1));

                                                    }

                                                }
                                            }
                                        } else {

                                            if (language.equalsIgnoreCase("En")) {
                                                coupon_discount.setText("- " + Constants.decimalFormat.format(discount));
                                            } else {
                                                coupon_discount.setText("" + Constants.decimalFormat.format(discount) + " -");
                                            }

                                            total_price = (myDbHelper.getTotalOrderPrice() + tax) - discount;
                                            float delivery_charges1;
                                            if (language.equalsIgnoreCase("En")) {
                                                delivery_charges1 = Float.parseFloat(servie_charges.getText().toString().replace("+ ", ""));
                                            } else {
                                                delivery_charges1 = Float.parseFloat(servie_charges.getText().toString().replace(" +", ""));
                                            }
                                            mtotal_price.setText("" + Constants.decimalFormat.format(total_price + delivery_charges1));

                                        }


                                    } else {
                                        //                          status false case
                                    }
                                    Log.i("TAG", "onResponse: " + data.size());
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    Toast.makeText(getActivity(), getResources().getString(R.string.cannot_reach_server), Toast.LENGTH_SHORT).show();
                                    Log.i("TAG", "cannot");
                                }
                            }
                        }
                    } else {
                        Toast.makeText(getActivity(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        Log.i("TAG", "cannot reach " + data.size());
                    }

                    if (customDialog != null) {
                        customDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<Promotions> call, Throwable t) {
                    Log.d("TAG", "onFailure: " + t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(getActivity(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getActivity(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }

                    if (customDialog != null) {
                        customDialog.dismiss();
                    }
//                    Constants.closeLoadingDialog();
                }
            });
            return null;
        }

        private String prepareVerifyMobileJson1() {
            JSONObject parentObj = new JSONObject();
            try {
                parentObj.put("BranchId", orderList.get(0).getSubCatId());
                parentObj.put("BrandId", orderList.get(0).getBrandId());
                parentObj.put("DeviceToken", SplashScreenActivity.regId);
                parentObj.put("UserId", userId);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.d("TAG", "prepareVerifyMobileJson1: " + parentObj);
            return parentObj.toString();
        }
    }


    private class GetStoresApi extends AsyncTask<String, Integer, String> {

        String inputStr;
//        AlertDialog loaderDialog = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareGetStoresJSON();
//            dialog = new ACProgressFlower.Builder(getContext())
//                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
//                    .themeColor(Color.WHITE)
//                    .fadeColor(Color.DKGRAY).build();
//            dialog.show();
            Constants.showLoadingDialog(getActivity());
//            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
//            // ...Irrelevant code for customizing the buttons and title
//            LayoutInflater inflater = getLayoutInflater();
//            int layout = R.layout.loder_dialog;
//            View dialogView = inflater.inflate(layout, null);
//            dialogBuilder.setView(dialogView);
//            dialogBuilder.setCancelable(true);
//
//            loaderDialog = dialogBuilder.create();
//            loaderDialog.show();
//            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
//            Window window = loaderDialog.getWindow();
//            window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//            lp.copyFrom(window.getAttributes());
//            //This makes the dialog take up the full width
//            Display display = getActivity().getWindowManager().getDefaultDisplay();
//            Point size = new Point();
//            display.getSize(size);
//            int screenWidth = size.x;
//
//            double d = screenWidth*0.85;
//            lp.width = (int) d;
//            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
//            window.setAttributes(lp);
        }

        @Override
        protected String doInBackground(final String... strings) {
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<StoresList> call = apiService.getStoresList(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<StoresList>() {
                @Override
                public void onResponse(Call<StoresList> call, Response<StoresList> response) {
                    Log.i("TAG", "store servies responce " + response);
                    if (response.isSuccessful()) {

                        StoresList stores = response.body();
                        if (stores.getStatus()) {


                        } else {

                            boolean promotion = false, min_orders = false, promotion1 = false, min_orders1 = false;

                            store_avaiable = true;

                            if (orderList.size() != 0) {
                                Log.i("TAG", "min_amount: " + Float.parseFloat(orderList.get(0).getMin_amount()));
                                if (Float.parseFloat(orderList.get(0).getMin_amount()) < (myDbHelper.getTotalOrderPrice() + tax)) {
                                    Log.i("TAG", "min_amount1: " + Float.parseFloat(orderList.get(0).getMin_amount()));
                                    min_orders = true;

                                } else {

                                    min_orders1 = true;
                                    Log.i("TAG", "min_amount2: " + Float.parseFloat(orderList.get(0).getMin_amount()));

                                }
                            }

                            if ((myDbHelper.getTotalOrderPrice() + tax) >= min_spending) {

                                promotion = true;


                            } else {

                                promotion1 = true;

                            }


                            if (promotion && min_orders) {

                                min_order_amount.setVisibility(View.GONE);

                                procced.setBackground(getResources().getDrawable(R.drawable.child_selected));
                                procced.setClickable(true);

                            } else if (min_orders) {

                                min_order_amount.setVisibility(View.GONE);

                                procced.setBackground(getResources().getDrawable(R.drawable.child_selected));
                                procced.setClickable(true);

                            } else if (promotion) {

                                min_order_amount.setVisibility(View.GONE);

                                procced.setBackground(getResources().getDrawable(R.drawable.child_selected));
                                procced.setClickable(true);

                            }

                            if (min_orders1 && promotion1 && !stores.getStatus()) {

                                min_order_amount.setVisibility(View.VISIBLE);
                                double min_order = Double.parseDouble(orderList.get(0).getMin_amount());
                                min_order_amount.setText("Online orders not avaiable");

                                procced.setBackground(getResources().getDrawable(R.drawable.check_out_unselected));
                                procced.setClickable(false);

                            } else if (!stores.getStatus()) {

                                min_order_amount.setVisibility(View.VISIBLE);
                                double min_order = Double.parseDouble(orderList.get(0).getMin_amount());
                                min_order_amount.setText("Online orders not avaiable");

                                procced.setBackground(getResources().getDrawable(R.drawable.check_out_unselected));
                                procced.setClickable(false);

                            } else if (min_orders1) {

                                min_order_amount.setVisibility(View.VISIBLE);
                                double min_order = Double.parseDouble(orderList.get(0).getMin_amount());
                                min_order_amount.setText("Subtotal must be greater than SAR " + Constants.decimalFormat.format(min_order));

                                procced.setBackground(getResources().getDrawable(R.drawable.check_out_unselected));
                                procced.setClickable(false);

                            } else if (promotion1) {

                                min_order_amount.setVisibility(View.VISIBLE);
                                min_order_amount.setText("Promotion applied, cart amount must be greater than " + min_spending + " SAR");

                                procced.setBackground(getResources().getDrawable(R.drawable.check_out_unselected));
                                procced.setClickable(false);

                                if (discount_type == 1) {

                                    if ((myDbHelper.getTotalOrderPrice() + tax) < discount) {

                                        discount = (myDbHelper.getTotalOrderPrice() + tax);

                                    } else {

                                        discount = Constants.discount;

                                    }

                                    if (language.equalsIgnoreCase("En")) {
                                        coupon_discount.setText("- " + Constants.decimalFormat.format(discount));
                                    } else {
                                        coupon_discount.setText("" + Constants.decimalFormat.format(discount) + " -");
                                    }
                                } else {

                                    float discount_value = (myDbHelper.getTotalOrderPrice() + tax) * ((float) discount / 100);
//            float discount_amount = myDbHelper.getTotalOrderPrice() - discount_value;

                                    Log.i("TAG", "discount: " + discount_value);

                                    if ((myDbHelper.getTotalOrderPrice() + tax) < discount_value) {

                                        discount_value = (myDbHelper.getTotalOrderPrice() + tax);

                                    } else {

                                        discount_value = (myDbHelper.getTotalOrderPrice() + tax) * ((float) discount / 100);

                                    }

                                    if (language.equalsIgnoreCase("En")) {
                                        coupon_discount.setText("- " + Constants.decimalFormat.format(discount_value));
                                    } else {
                                        coupon_discount.setText("" + Constants.decimalFormat.format(discount_value) + " -");
                                    }
                                }

                            }

                        }

                    }

                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<StoresList> call, Throwable t) {
                    Log.i("TAG", "onFailure: " + t);
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(getContext());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(getContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getContext(), R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(getContext(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getContext(), R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }

                    boolean promotion = false, min_orders = false, promotion1 = false, min_orders1 = false;

//                    if (orderList.size() != 0) {
//                        Log.i("TAG", "min_amount: " + Float.parseFloat(orderList.get(0).getMin_amount()));
//                        if (Float.parseFloat(orderList.get(0).getMin_amount()) < (myDbHelper.getTotalOrderPrice() + tax)) {
//                            Log.i("TAG", "min_amount1: " + Float.parseFloat(orderList.get(0).getMin_amount()));
//                            min_orders = true;
//
//                        } else {
//
//                            min_orders1 = true;
//                            Log.i("TAG", "min_amount2: " + Float.parseFloat(orderList.get(0).getMin_amount()));
//
//                        }
//                    }
//
//                    if ((myDbHelper.getTotalOrderPrice() + tax) >= min_spending) {
//
//                        promotion = true;
//
//
//                    } else {
//
//                        promotion1 = true;
//
//                    }


//                    if (promotion && min_orders) {
//
//                        min_order_amount.setVisibility(View.GONE);
//
//                        procced.setBackground(getResources().getDrawable(R.drawable.child_selected));
//                        procced.setClickable(true);
//
//                    } else if (min_orders) {
//
//                        min_order_amount.setVisibility(View.GONE);
//
//                        procced.setBackground(getResources().getDrawable(R.drawable.child_selected));
//                        procced.setClickable(true);
//
//                    } else if (promotion) {
//
//                        min_order_amount.setVisibility(View.GONE);
//
//                        procced.setBackground(getResources().getDrawable(R.drawable.child_selected));
//                        procced.setClickable(true);
//
//                    }

//                    if (min_orders1 && promotion1) {
//
                    store_avaiable = true;
                    min_order_amount.setVisibility(View.VISIBLE);
                    double min_order = Double.parseDouble(orderList.get(0).getMin_amount());
                    min_order_amount.setText("Online orders not avaiable");

                    procced.setBackground(getResources().getDrawable(R.drawable.check_out_unselected));
                    procced.setClickable(false);
//
//                    }
//                    else if (!stores.getStatus()){
//
//                        min_order_amount.setVisibility(View.VISIBLE);
//                        double min_order = Double.parseDouble(orderList.get(0).getMin_amount());
//                        min_order_amount.setText("Online orders not avaiable");
//
//                        procced.setBackground(getResources().getDrawable(R.drawable.check_out_unselected));
//                        procced.setClickable(false);
//
//                    } else if (min_orders1) {
//
//                        min_order_amount.setVisibility(View.VISIBLE);
//                        double min_order = Double.parseDouble(orderList.get(0).getMin_amount());
//                        min_order_amount.setText("Subtotal must be greater than SAR " + Constants.decimalFormat.format(min_order));
//
//                        procced.setBackground(getResources().getDrawable(R.drawable.check_out_unselected));
//                        procced.setClickable(false);
//
//                    } else if (promotion1) {
//
//                        min_order_amount.setVisibility(View.VISIBLE);
//                        min_order_amount.setText("Promotion applied, cart amount must be greater than " + min_spending + " SAR");
//
//                        procced.setBackground(getResources().getDrawable(R.drawable.check_out_unselected));
//                        procced.setClickable(false);
//
//                        if (discount_type == 1) {
//
//                            if ((myDbHelper.getTotalOrderPrice() + tax) < discount) {
//
//                                discount = (myDbHelper.getTotalOrderPrice() + tax);
//
//                            } else {
//
//                                discount = Constants.discount;
//
//                            }
//
//                            if (language.equalsIgnoreCase("En")) {
//                                coupon_discount.setText("- " + Constants.decimalFormat.format(discount));
//                            } else {
//                                coupon_discount.setText("" + Constants.decimalFormat.format(discount) + " -");
//                            }
//                        } else {
//
//                            float discount_value = (myDbHelper.getTotalOrderPrice() + tax) * ((float) discount / 100);
////            float discount_amount = myDbHelper.getTotalOrderPrice() - discount_value;
//
//                            Log.i("TAG", "discount: " + discount_value);
//
//                            if ((myDbHelper.getTotalOrderPrice() + tax) < discount_value) {
//
//                                discount_value = (myDbHelper.getTotalOrderPrice() + tax);
//
//                            } else {
//
//                                discount_value = (myDbHelper.getTotalOrderPrice() + tax) * ((float) discount / 100);
//
//                            }
//
//                            if (language.equalsIgnoreCase("En")) {
//                                coupon_discount.setText("- " + Constants.decimalFormat.format(discount_value));
//                            } else {
//                                coupon_discount.setText("" + Constants.decimalFormat.format(discount_value) + " -");
//                            }
//                        }
//
//                    }
                    Log.i("TAG", "onFailure: " + t);

                    Constants.closeLoadingDialog();
                }
            });
            return null;
        }


    }

    private String prepareGetStoresJSON() {
        JSONObject parentObj = new JSONObject();

        try {
            parentObj.put("Latitude", currentLatitude);
            parentObj.put("Longitude", currentLongitude);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.d("TAG", "prepareVerifyMobileJson: " + parentObj);
        return parentObj.toString();
    }


    @Override
    public void onResume() {
        super.onResume();

        Log.i("TAG", "onMin_spend2: " + min_spending);

        Log.i("TAG", "total_amount: " + (myDbHelper.getTotalOrderPrice() + tax));

        userPrefs = getActivity().getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userId = userPrefs.getString("userId", "0");

        boolean promotion = false, min_orders = false, promotion1 = false, min_orders1 = false;

        if (orderList.size() != 0) {
            Log.i("TAG", "min_amount: " + Float.parseFloat(orderList.get(0).getMin_amount()));
            if (Float.parseFloat(orderList.get(0).getMin_amount()) < (myDbHelper.getTotalOrderPrice() + tax)) {
                Log.i("TAG", "min_amount1: " + Float.parseFloat(orderList.get(0).getMin_amount()));
                min_orders = true;

            } else {

                min_orders1 = true;
                Log.i("TAG", "min_amount2: " + Float.parseFloat(orderList.get(0).getMin_amount()));

            }
        }

        if ((myDbHelper.getTotalOrderPrice() + tax) >= min_spending) {

            promotion = true;


        } else {

            promotion1 = true;

        }


        if (promotion && min_orders) {

            min_order_amount.setVisibility(View.GONE);

            procced.setBackground(getResources().getDrawable(R.drawable.child_selected));
            procced.setClickable(true);

        } else if (min_orders) {

            min_order_amount.setVisibility(View.GONE);

            procced.setBackground(getResources().getDrawable(R.drawable.child_selected));
            procced.setClickable(true);

        } else if (promotion) {

            min_order_amount.setVisibility(View.GONE);

            procced.setBackground(getResources().getDrawable(R.drawable.child_selected));
            procced.setClickable(true);

        }

        if (min_orders1 && promotion1 && store_avaiable) {

            min_order_amount.setVisibility(View.VISIBLE);
            double min_order = Double.parseDouble(orderList.get(0).getMin_amount());
            min_order_amount.setText("Online Orders not Avaiable");

            procced.setBackground(getResources().getDrawable(R.drawable.check_out_unselected));
            procced.setClickable(false);

        } else if (store_avaiable){

            min_order_amount.setVisibility(View.VISIBLE);
            double min_order = Double.parseDouble(orderList.get(0).getMin_amount());
            min_order_amount.setText("Online Orders not Avaiable");

            procced.setBackground(getResources().getDrawable(R.drawable.check_out_unselected));
            procced.setClickable(false);

        } else if (min_orders1) {

            min_order_amount.setVisibility(View.VISIBLE);
            double min_order = Double.parseDouble(orderList.get(0).getMin_amount());
            min_order_amount.setText("Subtotal must be greater than SAR " + Constants.decimalFormat.format(min_order));

            procced.setBackground(getResources().getDrawable(R.drawable.check_out_unselected));
            procced.setClickable(false);

        } else if (promotion1) {

            min_order_amount.setVisibility(View.VISIBLE);
            min_order_amount.setText("Promotion applied, cart amount must be greater than " + min_spending + " SAR");

            procced.setBackground(getResources().getDrawable(R.drawable.check_out_unselected));
            procced.setClickable(false);

            if (discount_type == 1) {

                if ((myDbHelper.getTotalOrderPrice() + tax) < discount) {

                    discount = (myDbHelper.getTotalOrderPrice() + tax);

                } else {

                    discount = Constants.discount;

                }

                if (language.equalsIgnoreCase("En")) {
                    coupon_discount.setText("- " + Constants.decimalFormat.format(discount));
                } else {
                    coupon_discount.setText("" + Constants.decimalFormat.format(discount) + " -");
                }
            } else {

                float discount_value = (myDbHelper.getTotalOrderPrice() + tax) * ((float) discount / 100);
//            float discount_amount = myDbHelper.getTotalOrderPrice() - discount_value;

                Log.i("TAG", "discount: " + discount_value);

                if ((myDbHelper.getTotalOrderPrice() + tax) < discount_value) {

                    discount_value = (myDbHelper.getTotalOrderPrice() + tax);

                } else {

                    discount_value = (myDbHelper.getTotalOrderPrice() + tax) * ((float) discount / 100);

                }

                if (language.equalsIgnoreCase("En")) {
                    coupon_discount.setText("- " + Constants.decimalFormat.format(discount_value));
                } else {
                    coupon_discount.setText("" + Constants.decimalFormat.format(discount_value) + " -");
                }
            }

        }


        Menu menu = navigation.getMenu();

        if (myDbHelper.getTotalOrderQty() == 0) {

            MenuItem menuItem = menu.getItem(1);
            removeBadge(navigation, menuItem.getItemId());

        } else {

            MenuItem menuItem = menu.getItem(1);
            showBadge(getActivity(), navigation, menuItem.getItemId(), String.valueOf(myDbHelper.getTotalOrderQty()));

        }

    }

}
