package com.cs.chef.Activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.chef.Adapter.OrderHistoryItemAdapter;
import com.cs.chef.Constants;
import com.cs.chef.Models.MyOrderItems;
import com.cs.chef.Models.Order;
import com.cs.chef.Models.OrderHistorySearch;
import com.cs.chef.R;
import com.cs.chef.Rest.APIInterface;
import com.cs.chef.Rest.ApiClient;
import com.cs.chef.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cc.cloudist.acplibrary.ACProgressFlower;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderHistory extends AppCompatActivity {

    ImageView msearch;
    RecyclerView morder_list;
    ArrayList<MyOrderItems.Data> data = new ArrayList<>();
    SharedPreferences userPrefs;
    public static String userId;
    int page_no = 1, page_size = 10;
    OrderHistoryItemAdapter mAdapter;
    EditText search_text;
    RelativeLayout search_layout;
    String search = "";
    boolean searchclick;
    ImageView back_btn;

    String language;
    SharedPreferences languagePrefs;

    int lastVisibleItem, no_of_rows;

    ArrayList<OrderHistorySearch> orderHistorySearches = new ArrayList<>();
    public static int userid = 0, orderid = 0;
    TextView emptyView;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.order_history);
        } else {
            setContentView(R.layout.order_history_arabic);
        }

        orderHistorySearches.clear();

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userId = userPrefs.getString("userId", "0");


        msearch = findViewById(R.id.search);
        morder_list = findViewById(R.id.order_list);
        search_text = findViewById(R.id.search_text);
        back_btn = findViewById(R.id.back_btn);
        emptyView = (TextView) findViewById(R.id.empty_view);
        search_layout = (RelativeLayout) findViewById(R.id.search_layout);

        emptyView.setVisibility(View.GONE);

        search_layout.setVisibility(View.GONE);

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();

            }
        });

        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(OrderHistory.this, LinearLayoutManager.VERTICAL, false);
        morder_list.setLayoutManager(linearLayoutManager);


//        LocalBroadcastManager.getInstance(OrderHistory.this).registerReceiver(
//                mTrackOrder, new IntentFilter("TrackOrderUpdate"));

        msearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.i("TAG", "onClick: " + searchclick);

                if (searchclick) {
                    search_layout.setVisibility(View.GONE);
                    searchclick = false;
                } else {
                    search_layout.setVisibility(View.VISIBLE);
                    searchclick = true;
                }

            }
        });

        morder_list.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);


                lastVisibleItem = linearLayoutManager
                        .findLastVisibleItemPosition();

                if (page_size != no_of_rows) {
                    if (lastVisibleItem == (page_size - 1)) {

                        page_size = page_size + 10;
                        page_no = page_no + 1;
                        new MyorderApi().execute();


                    }
                }
            }
        });

        search_text.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int j, int i1, int i2) {
//                if (MenuFragment.mAdapter != null) {
//                    MenuFragment.mAdapter.getFilter().filter(charSequence);
//                }

                search = search_text.getText().toString().toLowerCase();

                orderHistorySearches.clear();
                for (int i = 0; i < data.size(); i++) {

                    if (!search.equals("")) {
                        if (data.get(i).getOrderMain().getBranchName_En().toLowerCase().contains(search) ||
                                data.get(i).getOrderMain().getBranchName_Ar().contains(search)) {

                            OrderHistorySearch historySearch1 = new OrderHistorySearch();


                            historySearch1.setOrderid(data.get(i).getOrderMain().getOrderId());
                            historySearch1.setImage(data.get(i).getOrderMain().getBrandLogo());
                            historySearch1.setName(data.get(i).getOrderMain().getBranchName_En());
                            historySearch1.setName_ar(data.get(i).getOrderMain().getBranchName_Ar());
                            historySearch1.setOrder_status(data.get(i).getOrderMain().getOrderStatus());
                            historySearch1.setOrderdate(data.get(i).getOrderMain().getOrderDate());
                            historySearch1.setRate(data.get(i).getOrderMain().getRating());
                            historySearch1.setBrand_name(data.get(i).getOrderMain().getBrandName_En());
                            historySearch1.setBrand_name_ar(data.get(i).getOrderMain().getBrandName_Ar());

                            orderHistorySearches.add(historySearch1);


                        }
                    } else {

                        OrderHistorySearch historySearch2 = new OrderHistorySearch();

                        historySearch2.setOrderid(data.get(i).getOrderMain().getOrderId());
                        historySearch2.setImage(data.get(i).getOrderMain().getBrandLogo());
                        historySearch2.setName(data.get(i).getOrderMain().getBranchName_En());
                        historySearch2.setName_ar(data.get(i).getOrderMain().getBranchName_Ar());
                        historySearch2.setOrder_status(data.get(i).getOrderMain().getOrderStatus());
                        historySearch2.setOrderdate(data.get(i).getOrderMain().getOrderDate());
                        historySearch2.setRate(data.get(i).getOrderMain().getRating());
                        historySearch2.setBrand_name(data.get(i).getOrderMain().getBrandName_En());
                        historySearch2.setBrand_name_ar(data.get(i).getOrderMain().getBrandName_Ar());

                        orderHistorySearches.add(historySearch2);

                    }


                }

                if (orderHistorySearches.size() == 0) {

                    morder_list.setVisibility(View.GONE);
                    emptyView.setVisibility(View.VISIBLE);

                } else {

                    emptyView.setVisibility(View.GONE);
                    morder_list.setVisibility(View.VISIBLE);
                    mAdapter = new OrderHistoryItemAdapter(OrderHistory.this, orderHistorySearches, language, userId);
                    morder_list.setAdapter(mAdapter);

                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


//        String networkStatus = NetworkUtil.getConnectivityStatusString(OrderHistory.this);
//        if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
//            new MyorderApi().execute();
//        } else {
//            if (language.equalsIgnoreCase("En")) {
//                Toast.makeText(OrderHistory.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
//            } else {
//                Toast.makeText(OrderHistory.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
//            }
//        }


    }

    private class MyorderApi extends AsyncTask<String, String, String> {

        ACProgressFlower dialog;
        AlertDialog loaderDialog = null;
        String inputStr;


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareVerifyMobileJson();
//            dialog = new ACProgressFlower.Builder(OrderHistory.this)
//                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
//                    .themeColor(Color.WHITE)
//                    .fadeColor(Color.DKGRAY).build();
//            dialog.show();
            Constants.showLoadingDialog(OrderHistory.this);
//            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(OrderHistory.this);
//            // ...Irrelevant code for customizing the buttons and title
//            LayoutInflater inflater = getLayoutInflater();
//            int layout = R.layout.loder_dialog;
//            View dialogView = inflater.inflate(layout, null);
//            dialogBuilder.setView(dialogView);
//            dialogBuilder.setCancelable(true);
//
//            loaderDialog = dialogBuilder.create();
//            loaderDialog.show();
//            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
//            Window window = loaderDialog.getWindow();
//            window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//            lp.copyFrom(window.getAttributes());
//            //This makes the dialog take up the full width
//            Display display = OrderHistory.this.getWindowManager().getDefaultDisplay();
//            Point size = new Point();
//            display.getSize(size);
//            int screenWidth = size.x;
//
//            double d = screenWidth * 0.85;
//            lp.width = (int) d;
//            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
//            window.setAttributes(lp);
        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(OrderHistory.this);
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<MyOrderItems> call = apiService.getMyorder(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<MyOrderItems>() {
                @Override
                public void onResponse(Call<MyOrderItems> call, Response<MyOrderItems> response) {
                    Log.d("TAG", "onResponse: " + response);
                    if (response.isSuccessful()) {
                        MyOrderItems orderItems = response.body();
                        data = orderItems.getData();

                        if (data.size() == 0) {

                            emptyView.setVisibility(View.VISIBLE);
                            morder_list.setVisibility(View.GONE);
                            msearch.setVisibility(View.GONE);

                        } else {

                            emptyView.setVisibility(View.GONE);
                            morder_list.setVisibility(View.VISIBLE);
                            msearch.setVisibility(View.VISIBLE);
                        }
                        orderHistorySearches.clear();
                        try {
                            if (orderItems.getStatus()) {
                                String message = orderItems.getMessage();


                                for (int i = 0; i < data.size(); i++) {

                                    userid = data.get(i).getOrderMain().getUserId();

                                    OrderHistorySearch historySearch = new OrderHistorySearch();

                                    historySearch.setOrderid(data.get(i).getOrderMain().getOrderId());
                                    historySearch.setImage(data.get(i).getOrderMain().getBrandLogo());
                                    historySearch.setName(data.get(i).getOrderMain().getBranchName_En());
                                    historySearch.setName_ar(data.get(i).getOrderMain().getBranchName_Ar());
                                    historySearch.setOrder_status(data.get(i).getOrderMain().getOrderStatus());
                                    historySearch.setOrderdate(data.get(i).getOrderMain().getOrderDate());
                                    historySearch.setRate(data.get(i).getOrderMain().getRating());
                                    historySearch.setBrand_name(data.get(i).getOrderMain().getBrandName_En());
                                    historySearch.setBrand_name_ar(data.get(i).getOrderMain().getBrandName_Ar());

                                    orderHistorySearches.add(historySearch);

                                    no_of_rows = data.get(i).getRows();


                                }

                                mAdapter = new OrderHistoryItemAdapter(OrderHistory.this, orderHistorySearches, language, userId);
                                morder_list.setAdapter(mAdapter);
//                                mscrollview.smoothScrollTo(0, 0);
//                                bids_count.setText(""+data.size());

                            } else {
                                //                          status false case
                                if (language.equalsIgnoreCase("En")) {
                                    String failureResponse = orderItems.getMessage();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name),
                                            getResources().getString(R.string.ok), OrderHistory.this);
                                } else {
                                    String failureResponse = orderItems.getMessageAr();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name_ar),
                                            getResources().getString(R.string.ok_ar), OrderHistory.this);
                                }
                            }
                            Log.i("TAG", "onResponse: " + data.size());
                        } catch (Exception e) {
                            e.printStackTrace();
                            if (language.equalsIgnoreCase("En")) {
                                Toast.makeText(OrderHistory.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(OrderHistory.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                            }
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(OrderHistory.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(OrderHistory.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }

                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<MyOrderItems> call, Throwable t) {
                    Log.d("TAG", "onFailure: " + t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(OrderHistory.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(OrderHistory.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(OrderHistory.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(OrderHistory.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }

                    Constants.closeLoadingDialog();
                }
            });
            return null;
        }

        private String prepareVerifyMobileJson() {
            JSONObject parentObj = new JSONObject();
            try {

                parentObj.put("UserId", userId);
                parentObj.put("PageNumber", page_no);
                parentObj.put("PageSize", page_size);
                parentObj.put("BranchId", 0);


            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.d("TAG", "prepareVerifyMobileJson: " + parentObj);
            return parentObj.toString();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        String networkStatus = NetworkUtil.getConnectivityStatusString(OrderHistory.this);
        if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {

            new MyorderApi().execute();

            emptyView.setVisibility(View.GONE);
            morder_list.setVisibility(View.VISIBLE);
            msearch.setVisibility(View.VISIBLE);

        } else {
            emptyView.setVisibility(View.VISIBLE);
            morder_list.setVisibility(View.GONE);
            msearch.setVisibility(View.GONE);
            if (language.equalsIgnoreCase("En")) {
                Toast.makeText(OrderHistory.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(OrderHistory.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
            }
        }

    }

}
