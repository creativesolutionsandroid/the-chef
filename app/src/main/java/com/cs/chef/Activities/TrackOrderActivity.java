package com.cs.chef.Activities;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.cs.chef.Adapter.TrackOrderItemsAdapter;
import com.cs.chef.Constants;
import com.cs.chef.CustomListView;
import com.cs.chef.Dialogs.TrackListDialog;
import com.cs.chef.Models.CancelOrder;
import com.cs.chef.Models.Favourite;
import com.cs.chef.Models.TrackOrder;
import com.cs.chef.R;
import com.cs.chef.Rest.APIInterface;
import com.cs.chef.Rest.ApiClient;
import com.cs.chef.Utils.NetworkUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import cc.cloudist.acplibrary.ACProgressFlower;
import me.zhanghai.android.materialratingbar.MaterialRatingBar;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TrackOrderActivity extends AppCompatActivity implements OnMapReadyCallback {

    public static TextView item_total, sub_total, mvat, servie_charges, mtotal_price, coupon_discount, service;
    LinearLayout order_status, order_summary;
    TextView order_status_txt, order_summary_txt, date;
    View order_status_view, order_status_view1, order_summary_view, order_summary_view1;
    RelativeLayout order_status_data, order_summary_data;
    TextView submit_time, submit_date, submit_txt, accept_title, accept_time, accept_date, accept_txt, ready_title, ready_time, ready_date, ready_txt, delivery_title, delivery_time, delivery_date, address_txt;
    ImageView accept_img, ready_img, delivery_img;
    LinearLayout layout_ready, layout_delivery;

    TextView summary_address_store, bill_no, date_delivery, total_price, order_cancel, branch_name, order_type_address;
    CustomListView list_view;

    MaterialRatingBar ratingBar1;
    View view;

    ArrayList<TrackOrder.Data> data = new ArrayList<>();
    boolean order = false, confirm_order = false, ready_order = false, delivered_order = false, cancel_order = false, cancel_order1 = false;
    ArrayList<Favourite> data1 = new ArrayList<>();
    ArrayList<String> status = new ArrayList<>();

    int userid = 0, orderid = 0;

    private Timer timer = new Timer();

    ImageView back_btn, store_img;
    //    MaterialRatingBar ratingBar1;
//    View view;
    float rating;

    TrackOrderItemsAdapter mAdapter;
//    LinearLayout bottom_bar;

    boolean isfav;

    FragmentManager fragmentManager = getSupportFragmentManager();

//    int UserId = 3, OrderId = 69;

    private static final String[] PHONE_PERMS = {
            android.Manifest.permission.CALL_PHONE
    };
    private static final int PHONE_REQUEST = 3;

    String phone_no;

    String language;
    SharedPreferences languagePrefs;

    SupportMapFragment mapFragment, mapFragment1;
    private GoogleMap mMap;
    double userlat, userlongi, storelat, storelongi;
    int order_type;
    RelativeLayout map_layout;
    LinearLayout submit_layout, accept_layout, ready_layout, delivery_layout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.track_order_screen);
        } else {
            setContentView(R.layout.track_order_screen_arabic);
        }

        branch_name = findViewById(R.id.branch_name);
        order_type_address = findViewById(R.id.order_type_address);
        order_status = findViewById(R.id.order_status);
        order_summary = findViewById(R.id.order_summary);

        order_status_txt = findViewById(R.id.order_status_txt);
        order_summary_txt = findViewById(R.id.order_summary_txt);

        order_status_view = findViewById(R.id.order_status_view);
        order_status_view1 = findViewById(R.id.order_status_view1);
        order_summary_view = findViewById(R.id.order_summary_view);
        order_summary_view1 = findViewById(R.id.order_summary_view1);

        order_status_data = findViewById(R.id.order_status_data);
        order_summary_data = findViewById(R.id.order_summary_data);
        date = findViewById(R.id.date);

        submit_time = findViewById(R.id.submit_time);
        submit_date = findViewById(R.id.submit_date);
        submit_txt = findViewById(R.id.submit_txt);
        accept_title = findViewById(R.id.accept_title);
        accept_time = findViewById(R.id.accept_time);
        accept_date = findViewById(R.id.accept_date);
        accept_txt = findViewById(R.id.accept_txt);
        ready_title = findViewById(R.id.ready_title);
        ready_time = findViewById(R.id.ready_time);
        ready_date = findViewById(R.id.ready_date);
        ready_txt = findViewById(R.id.ready_txt);
        delivery_title = findViewById(R.id.delivery_title);
        delivery_time = findViewById(R.id.delivery_time);
        delivery_date = findViewById(R.id.delivery_date);
        address_txt = findViewById(R.id.address_txt);

        accept_img = findViewById(R.id.accept_img);
        ready_img = findViewById(R.id.ready_img);
        delivery_img = findViewById(R.id.delivery_img);

        summary_address_store = findViewById(R.id.summary_address);
        list_view = findViewById(R.id.order_summary_list);
        bill_no = findViewById(R.id.order_summary_bill_no);
        date_delivery = findViewById(R.id.order_summary_delivery_date);
        item_total = findViewById(R.id.item_total);
        sub_total = findViewById(R.id.sub_total);
        servie_charges = findViewById(R.id.service_charge);
        service = findViewById(R.id.service);

        mvat = findViewById(R.id.vat);
        coupon_discount = findViewById(R.id.coupon_discount);
        mtotal_price = findViewById(R.id.total_price);

        order_cancel = findViewById(R.id.order_cancel);
//        fav_order = findViewById(R.id.fav_order);

        view = findViewById(R.id.view1);
        ratingBar1 = findViewById(R.id.ratingBar1);

        layout_ready = findViewById(R.id.ready);
        layout_delivery = findViewById(R.id.delivered);

        back_btn = findViewById(R.id.back_btn);

        submit_layout = findViewById(R.id.submit_layout);
        accept_layout = findViewById(R.id.accept_layout);
        ready_layout = findViewById(R.id.ready_layout);
        delivery_layout = findViewById(R.id.delivery_layout);

//        map_layout = findViewById(R.id.map_layout);

        userid = getIntent().getIntExtra("userid", 0);
        orderid = getIntent().getIntExtra("orderid", 0);

        Log.i("TAG", "userid: " + userid);
        Log.i("TAG", "orderid: " + orderid);


        order_status.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                order_status_data.setVisibility(View.VISIBLE);
                order_summary_data.setVisibility(View.GONE);

                order_status_txt.setTextColor(getResources().getColor(R.color.black));
                order_summary_txt.setTextColor(getResources().getColor(R.color.track));

                order_status_view.setVisibility(View.VISIBLE);
                order_status_view1.setVisibility(View.GONE);

                order_summary_view.setVisibility(View.GONE);
                order_summary_view1.setVisibility(View.VISIBLE);

            }
        });

        order_summary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                order_status_data.setVisibility(View.GONE);
                order_summary_data.setVisibility(View.VISIBLE);

                order_status_txt.setTextColor(getResources().getColor(R.color.track));
                order_summary_txt.setTextColor(getResources().getColor(R.color.black));

                order_status_view.setVisibility(View.GONE);
                order_status_view1.setVisibility(View.VISIBLE);

                order_summary_view.setVisibility(View.VISIBLE);
                order_summary_view1.setVisibility(View.GONE);

            }
        });

        if (language.equalsIgnoreCase("En")) {
            String name = getColoredSpanned("Your order has been send to", "#888888");
            String surName = getColoredSpanned("EAT EGYPT", "#000000");

            submit_txt.setText(Html.fromHtml(name + " " + surName));
        } else {
            String name = getColoredSpanned("تم ارسال طلبك الى", "#888888");
            String surName = getColoredSpanned("EAT EGYPT", "#000000");

            submit_txt.setText(Html.fromHtml(name + " " + surName));
        }

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (getIntent().getStringExtra("track_order").equals("my_order")) {

                    finish();

                } else if (getIntent().getStringExtra("track_order").equals("check_out")) {

//            Fragment accountFragment = new HomeScreenFragment();
//            fragmentManager.beginTransaction().replace(R.id.fragment_layout, accountFragment).commit();

//            Fragment accountFragment = new HomeScreenFragment();
//            FragmentTransaction ft = fragmentManager.beginTransaction().replace(R.id.fragment_layout, accountFragment);
//            ft.commit();

                    Intent a = new Intent(TrackOrderActivity.this, MainActivity.class);
                    a.putExtra("class", "splash");
                    startActivity(a);

                }

            }
        });

        order_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog customDialog = null;
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(TrackOrderActivity.this);
                // ...Irrelevant code for customizing the buttons and title
                LayoutInflater inflater = TrackOrderActivity.this.getLayoutInflater();
                int layout;
                if (language.equalsIgnoreCase("En")) {
                    layout = R.layout.alert_dialog;
                } else {
                    layout = R.layout.alert_dialog_arabic;
                }
                View dialogView = inflater.inflate(layout, null);
                dialogBuilder.setView(dialogView);
                dialogBuilder.setCancelable(false);

                TextView title = (TextView) dialogView.findViewById(R.id.title);
                TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                View vert = (View) dialogView.findViewById(R.id.vert_line);

//                no.setVisibility(View.GONE);
//                vert.setVisibility(View.GONE);

                if (language.equalsIgnoreCase("En")) {
                    title.setText(getResources().getString(R.string.app_name));
                    yes.setText(getResources().getString(R.string.yes));
                    no.setText(getResources().getString(R.string.no));
                    desc.setText("Do u really want to cancel your order");
                } else {
                    title.setText(getResources().getString(R.string.app_name_ar));
                    yes.setText(getResources().getString(R.string.yes_ar));
                    no.setText(getResources().getString(R.string.no_ar));
                    desc.setText("هل ترغب فعلاً بألغاء طلبك؟");
                }

                customDialog = dialogBuilder.create();
                customDialog.show();

                final AlertDialog finalCustomDialog = customDialog;
                yes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String networkStatus = NetworkUtil.getConnectivityStatusString(TrackOrderActivity.this);
                        if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                            new CancelOrderApi().execute();
                        } else {
                            if (language.equalsIgnoreCase("En")) {
                                Toast.makeText(TrackOrderActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(TrackOrderActivity.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                            }
                        }
                        finalCustomDialog.dismiss();
                    }
                });

                no.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        finalCustomDialog.dismiss();

                    }
                });

                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                Window window = customDialog.getWindow();
                window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                lp.copyFrom(window.getAttributes());
                //This makes the dialog take up the full width
                Display display = getWindowManager().getDefaultDisplay();
                Point size = new Point();
                display.getSize(size);
                int screenWidth = size.x;

                double d = screenWidth * 0.85;
                lp.width = (int) d;
                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                window.setAttributes(lp);


            }
        });

        timer.schedule(new MyTimerTask(), 30000, 30000);

        String networkStatus = NetworkUtil.getConnectivityStatusString(TrackOrderActivity.this);
        if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
            new TrackOrderApi().execute();
        } else {
            if (language.equalsIgnoreCase("En")) {
                Toast.makeText(TrackOrderActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(TrackOrderActivity.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
            }
        }

    }

    @Override
    public void onBackPressed() {

        if (getIntent().getStringExtra("track_order").equals("my_order")) {

            Intent intent = new Intent("TrackOrderUpdate");
            LocalBroadcastManager.getInstance(TrackOrderActivity.this).sendBroadcast(intent);
            finish();

        } else if (getIntent().getStringExtra("track_order").equals("check_out")) {

//            Fragment accountFragment = new HomeScreenFragment();
//            fragmentManager.beginTransaction().replace(R.id.fragment_layout, accountFragment).commit();

//            Fragment accountFragment = new HomeScreenFragment();
//            FragmentTransaction ft = fragmentManager.beginTransaction().replace(R.id.fragment_layout, accountFragment);
//            ft.commit();

            Intent a = new Intent(TrackOrderActivity.this, MainActivity.class);
            a.putExtra("class", "splashscreen");
            startActivity(a);

        }
//        else if (getIntent().getStringExtra("track_order").equals("home_screen")) {
//
//            finish();
//
//        }
        //Include the code here
        return;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;
//        LatLng driver = new LatLng(driverLat, driverLong);
//        if ()
        if (order_type == 1 || order_type == 2) {
            final LatLng store = new LatLng(storelat, storelongi);
            mMap.addMarker(new MarkerOptions().position(store).title("").icon(BitmapDescriptorFactory.fromResource(R.drawable.map_marker)));
            mMap.moveCamera(CameraUpdateFactory.newLatLng(store));
            mMap.animateCamera(CameraUpdateFactory.zoomTo(15.0f));
            mMap.setOnCameraMoveListener(new GoogleMap.OnCameraMoveListener() {
                @Override
                public void onCameraMove() {
                    mMap.moveCamera(CameraUpdateFactory.newLatLng(store));
                }
            });
            Log.i("TAG", "onMapReady: " + store);
        } else {
            final LatLng user = new LatLng(userlat, userlongi);

            mMap.addMarker(new MarkerOptions().position(user).title("").icon(BitmapDescriptorFactory.fromResource(R.drawable.map_marker)));
            mMap.moveCamera(CameraUpdateFactory.newLatLng(user));
            mMap.animateCamera(CameraUpdateFactory.zoomTo(15.0f));
            mMap.setOnCameraMoveListener(new GoogleMap.OnCameraMoveListener() {
                @Override
                public void onCameraMove() {
                    mMap.moveCamera(CameraUpdateFactory.newLatLng(user));
                }
            });
            Log.i("TAG", "onMapReady: " + user);
        }

//        mMap.addMarker(new MarkerOptions().position(driver).title("").icon(BitmapDescriptorFactory.fromResource(R.drawable.direction_marker)));


    }

    private class MyTimerTask extends TimerTask {

        @Override
        public void run() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    String networkStatus = NetworkUtil.getConnectivityStatusString(TrackOrderActivity.this);
                    if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        new TrackOrderApi().execute();
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(TrackOrderActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(TrackOrderActivity.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            });
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        timer.cancel();
        Constants.closeLoadingDialog();
    }

    private class CancelOrderApi extends AsyncTask<String, String, String> {

        //        ACProgressFlower dialog;
        //        AlertDialog  loaderDialog = null;
        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareVerifyMobileJson();
//            dialog = new ACProgressFlower.Builder(TrackOrderActivity.this)
//                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
//                    .themeColor(Color.WHITE)
//                    .fadeColor(Color.DKGRAY).build();
//            dialog.show();
            Constants.showLoadingDialog(TrackOrderActivity.this);
//            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(TrackOrderActivity.this);
//            // ...Irrelevant code for customizing the buttons and title
//            LayoutInflater inflater = getLayoutInflater();
//            int layout = R.layout.loder_dialog;
//            View dialogView = inflater.inflate(layout, null);
//            dialogBuilder.setView(dialogView);
//            dialogBuilder.setCancelable(true);
//
//            loaderDialog = dialogBuilder.create();
//            loaderDialog.show();
//            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
//            Window window = loaderDialog.getWindow();
//            window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//            lp.copyFrom(window.getAttributes());
//            //This makes the dialog take up the full width
//            Display display = getWindowManager().getDefaultDisplay();
//            Point size = new Point();
//            display.getSize(size);
//            int screenWidth = size.x;
//
//            double d = screenWidth*0.85;
//            lp.width = (int) d;
//            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
//            window.setAttributes(lp);
        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(TrackOrderActivity.this);
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<CancelOrder> call = apiService.getCancelOrder(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<CancelOrder>() {
                @Override
                public void onResponse(Call<CancelOrder> call, Response<CancelOrder> response) {
                    Log.d("TAG", "onResponse: " + response);
                    if (response.isSuccessful()) {
                        CancelOrder orderItems = response.body();
                        try {
                            if (orderItems.getStatus()) {
                                String message = orderItems.getMessage();

//                                cancel_order = true;
                                String networkStatus = NetworkUtil.getConnectivityStatusString(TrackOrderActivity.this);
                                if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                                    new TrackOrderApi().execute();
                                } else {
                                    if (language.equalsIgnoreCase("En")) {
                                        Toast.makeText(TrackOrderActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                                    } else {
                                        Toast.makeText(TrackOrderActivity.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                                    }
                                }

                            } else {
                                //                          status false case
                                if (language.equalsIgnoreCase("En")) {
                                    String failureResponse = orderItems.getMessage();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name),
                                            getResources().getString(R.string.ok), TrackOrderActivity.this);
                                } else {
                                    String failureResponse = orderItems.getMessageAr();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name_ar),
                                            getResources().getString(R.string.ok_ar), TrackOrderActivity.this);
                                }
                            }
                            Log.i("TAG", "onResponse: " + data.size());
                        } catch (Exception e) {
                            e.printStackTrace();
                            if (language.equalsIgnoreCase("En")) {
                                Toast.makeText(TrackOrderActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(TrackOrderActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                            }
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(TrackOrderActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(TrackOrderActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }

                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<CancelOrder> call, Throwable t) {
                    Log.d("TAG", "onFailure: " + t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(TrackOrderActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(TrackOrderActivity.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(TrackOrderActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(TrackOrderActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }

                    Constants.closeLoadingDialog();
                }
            });
            return null;
        }

        private String prepareVerifyMobileJson() {
            JSONObject parentObj = new JSONObject();
            try {

                parentObj.put("OrderId", orderid);

            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.d("TAG", "prepareVerifyMobileJson: " + parentObj);
            return parentObj.toString();
        }
    }

    private class FavOrderApi extends AsyncTask<String, String, String> {

        ACProgressFlower dialog;
        String inputStr;
//        AlertDialog loaderDialog = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareVerifyMobileJson();
//            dialog = new ACProgressFlower.Builder(TrackOrderActivity.this)
//                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
//                    .themeColor(Color.WHITE)
//                    .fadeColor(Color.DKGRAY).build();
//            dialog.show();
            Constants.showLoadingDialog(TrackOrderActivity.this);
//            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(TrackOrderActivity.this);
//            // ...Irrelevant code for customizing the buttons and title
//            LayoutInflater inflater = getLayoutInflater();
//            int layout = R.layout.loder_dialog;
//            View dialogView = inflater.inflate(layout, null);
//            dialogBuilder.setView(dialogView);
//            dialogBuilder.setCancelable(true);
//
//            loaderDialog = dialogBuilder.create();
//            loaderDialog.show();
//            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
//            Window window = loaderDialog.getWindow();
//            window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//            lp.copyFrom(window.getAttributes());
//            //This makes the dialog take up the full width
//            Display display = getWindowManager().getDefaultDisplay();
//            Point size = new Point();
//            display.getSize(size);
//            int screenWidth = size.x;
//
//            double d = screenWidth * 0.85;
//            lp.width = (int) d;
//            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
//            window.setAttributes(lp);

        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(TrackOrderActivity.this);
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<Favourite> call = apiService.getFavOrder(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<Favourite>() {
                @Override
                public void onResponse(Call<Favourite> call, Response<Favourite> response) {
                    Log.d("TAG", "onResponse: " + response);
                    if (response.isSuccessful()) {
                        Favourite orderItems = response.body();
                        try {
                            if (orderItems.getStatus()) {
                                String message = orderItems.getMessage();

                                String networkStatus = NetworkUtil.getConnectivityStatusString(TrackOrderActivity.this);
                                if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                                    new TrackOrderApi().execute();
                                } else {
                                    if (language.equalsIgnoreCase("En")) {
                                        Toast.makeText(TrackOrderActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                                    } else {
                                        Toast.makeText(TrackOrderActivity.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                                    }
                                }


                            } else {
                                //                          status false case
                                if (language.equalsIgnoreCase("En")) {
                                    String failureResponse = orderItems.getMessage();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name),
                                            getResources().getString(R.string.ok), TrackOrderActivity.this);
                                } else {
                                    String failureResponse = orderItems.getMessageAr();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name_ar),
                                            getResources().getString(R.string.ok_ar), TrackOrderActivity.this);
                                }
                            }
                            Log.i("TAG", "onResponse: " + data.size());
                        } catch (Exception e) {
                            e.printStackTrace();
                            if (language.equalsIgnoreCase("En")) {
                                Toast.makeText(TrackOrderActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(TrackOrderActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                            }
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(TrackOrderActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(TrackOrderActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }

                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<Favourite> call, Throwable t) {
                    Log.d("TAG", "onFailure: " + t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(TrackOrderActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(TrackOrderActivity.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(TrackOrderActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(TrackOrderActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }

                    Constants.closeLoadingDialog();
                }
            });
            return null;
        }

        private String prepareVerifyMobileJson() {
            JSONObject parentObj = new JSONObject();
            try {

                parentObj.put("UserId", userid);
                parentObj.put("OrderId", orderid);
                parentObj.put("CommandType", 2);
                parentObj.put("IsFavourite", isfav);

            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.d("TAG", "prepareVerifyMobileJson: " + parentObj);
            return parentObj.toString();
        }
    }

    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }


    private class TrackOrderApi extends AsyncTask<String, String, String> {

        ACProgressFlower dialog;
        String inputStr;
        AlertDialog customDialog = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareVerifyMobileJson();
//            dialog = new ACProgressFlower.Builder(TrackOrderActivity.this)
//                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
//                    .themeColor(Color.WHITE)
//                    .fadeColor(Color.DKGRAY).build();
//            dialog.show();
//            Constants.showLoadingDialog(TrackOrderActivity.this);

            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(TrackOrderActivity.this);
            // ...Irrelevant code for customizing the buttons and title
            LayoutInflater inflater = getLayoutInflater();
            int layout = R.layout.progress_bar_alert;
            View dialogView = inflater.inflate(layout, null);
            dialogBuilder.setView(dialogView);
            dialogBuilder.setCancelable(false);

            customDialog = dialogBuilder.create();
            customDialog.show();
            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            Window window = customDialog.getWindow();
            window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            lp.copyFrom(window.getAttributes());
            //This makes the progressDialog take up the full width
            Display display = getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            int screenWidth = size.x;

            double d = screenWidth * 0.45;
            lp.width = (int) d;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
            window.setAttributes(lp);

//            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(TrackOrderActivity.this);
//            // ...Irrelevant code for customizing the buttons and title
//            LayoutInflater inflater = getLayoutInflater();
//            int layout = R.layout.loder_dialog;
//            View dialogView = inflater.inflate(layout, null);
//            dialogBuilder.setView(dialogView);
//            dialogBuilder.setCancelable(true);
//
//            loaderDialog = dialogBuilder.create();
//            loaderDialog.show();
//            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
//            Window window = loaderDialog.getWindow();
//            window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//            lp.copyFrom(window.getAttributes());
//            //This makes the dialog take up the full width
//            Display display = getWindowManager().getDefaultDisplay();
//            Point size = new Point();
//            display.getSize(size);
//            int screenWidth = size.x;
//
//            double d = screenWidth * 0.85;
//            lp.width = (int) d;
//            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
//            window.setAttributes(lp);
        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(TrackOrderActivity.this);
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<TrackOrder> call = apiService.getTrackOrder(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<TrackOrder>() {
                @Override
                public void onResponse(Call<TrackOrder> call, Response<TrackOrder> response) {
                    Log.d("TAG", "onResponse: " + response);
                    if (response.isSuccessful()) {
                        TrackOrder orderItems = response.body();
                        data = orderItems.getData();
                        try {
                            if (orderItems.getStatus()) {
                                String message = orderItems.getMessage();

                                for (int i = 0; i < data.size(); i++) {

                                    item_total.setText("" + Constants.decimalFormat.format(data.get(i).getOrderMain().getSubTotal()));
                                    if (data.get(i).getOrderMain().getDeliveryCharges() == 0){
                                        servie_charges.setText("+ 0.00");
                                        if (language.equalsIgnoreCase("En")) {
                                            service.setText("Service Charges");
                                        } else {
                                            service.setText("رسوم الخدمات");
                                        }
                                    } else {
                                        servie_charges.setText("+ " + Constants.decimalFormat.format(data.get(i).getOrderMain().getDeliveryCharges()));
                                        if (language.equalsIgnoreCase("En")) {
                                            service.setText("Delivery Charges");
                                        } else {
                                            service.setText("رسوم التوصيل");
                                        }
                                    }

                                    coupon_discount.setText("- " + Constants.decimalFormat.format(data.get(i).getOrderMain().getCouponAmount()));
                                    mtotal_price.setText("" + Constants.decimalFormat.format(data.get(i).getOrderMain().getTotalPrice()));

//                                    tax = myDbHelper.getTotalOrderPrice() * Constants.vatper;
                                    mvat.setText("+ " + Constants.decimalFormat.format(data.get(i).getOrderMain().getVatCharges()));
                                    float subtotal;
                                    subtotal = (float) (data.get(i).getOrderMain().getSubTotal() + data.get(i).getOrderMain().getVatCharges());
                                    sub_total.setText("" + Constants.decimalFormat.format(subtotal));


                                    if (language.equalsIgnoreCase("En")) {
                                        String order_type = "";
                                        if (data.get(i).getOrderMain().getOrderType() == 1) {
                                            order_type = "Dine-In";
                                        } else if (data.get(i).getOrderMain().getOrderType() == 2) {
                                            order_type = "Pick Up";
                                        } else if (data.get(i).getOrderMain().getOrderType() == 3) {
                                            order_type = "Delivery";
                                        }

                                        date.setText("" + order_type + " Date");
                                    } else {
                                        String order_type = "";
                                        if (data.get(i).getOrderMain().getOrderType() == 1) {
                                            order_type = "Dine-In";
                                        } else if (data.get(i).getOrderMain().getOrderType() == 2) {
                                            order_type = "تاريخ الاستلام";
                                        } else if (data.get(i).getOrderMain().getOrderType() == 3) {
                                            order_type = "تاريخ التوصيل";
                                        }

                                        date.setText("" + order_type);
                                    }


                                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
                                    SimpleDateFormat dateFormat1 = new SimpleDateFormat("EEE dd", Locale.US);
                                    SimpleDateFormat dateFormat3 = new SimpleDateFormat("hh:mm aaa", Locale.US);
                                    SimpleDateFormat df3 = new SimpleDateFormat("MMM dd,yyyy hh:mm aaa", Locale.US);


                                    Date date = null;

                                    try {

                                        date = dateFormat.parse(data.get(i).getOrderMain().getExpectedTime());

                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }

                                    String date1;

                                    date1 = df3.format(date);

                                    date_delivery.setText("" + date1);
                                    if (language.equalsIgnoreCase("En")) {

                                        branch_name.setText("" + data.get(i).getOrderMain().getBranchName_En());

                                        String ordertype1 = null;
                                        if (data.get(i).getOrderMain().getOrderType() == 1) {
                                            ordertype1 = "Dine-In";
                                        } else if (data.get(i).getOrderMain().getOrderType() == 2) {
                                            ordertype1 = "Pick Up";
                                        } else if (data.get(i).getOrderMain().getOrderType() == 3) {
                                            ordertype1 = "Delivery";
                                        }
                                        order_type_address.setText("" + ordertype1 + " Address");


                                        order_type = data.get(i).getOrderMain().getOrderType();
                                        if (data.get(i).getOrderMain().getOrderType() == 3) {
                                            summary_address_store.setText("" + data.get(i).getOrderMain().getUserAddress());
                                            delivery_title.setText("Delivered to");
                                            address_txt.setText("" + data.get(i).getOrderMain().getUserAddress());
                                            userlat = Double.parseDouble(data.get(i).getOrderMain().getUserLatitude());
                                            userlongi = Double.parseDouble(data.get(i).getOrderMain().getUserLongitude());
                                        } else {
                                            summary_address_store.setText("" + data.get(i).getOrderMain().getBranchAddress());
                                            delivery_title.setText("Served to");
                                            address_txt.setText("" + data.get(i).getOrderMain().getBranchAddress());
                                            storelat = data.get(i).getOrderMain().getBranchLatitude();
                                            storelongi = data.get(i).getOrderMain().getBranchLongitude();
                                        }
                                    } else {
                                        branch_name.setText("" + data.get(i).getOrderMain().getBranchName_Ar());

                                        String ordertype1 = null;
                                        if (data.get(i).getOrderMain().getOrderType() == 1) {
                                            ordertype1 = "Dine-In";
                                        } else if (data.get(i).getOrderMain().getOrderType() == 2) {
                                            ordertype1 = "عنوان الاستلام";
                                        } else if (data.get(i).getOrderMain().getOrderType() == 3) {
                                            ordertype1 = "عنوان التوصيل";
                                        }
                                        order_type_address.setText("" + ordertype1);


                                        if (data.get(i).getOrderMain().getOrderType() == 3) {
                                            summary_address_store.setText("" + data.get(i).getOrderMain().getUserAddress());
                                            delivery_title.setText("تم توصيله الى");
                                            address_txt.setText("" + data.get(i).getOrderMain().getUserAddress());
                                            userlat = Double.parseDouble(data.get(i).getOrderMain().getUserLatitude());
                                            userlongi = Double.parseDouble(data.get(i).getOrderMain().getUserLongitude());
                                        } else {
                                            summary_address_store.setText("" + data.get(i).getOrderMain().getBranchAddress());
                                            delivery_title.setText("يقدم الى");
                                            address_txt.setText("" + data.get(i).getOrderMain().getBranchAddress());
                                            storelat = data.get(i).getOrderMain().getBranchLatitude();
                                            storelongi = data.get(i).getOrderMain().getBranchLongitude();
                                        }
                                    }

                                    mapFragment = (SupportMapFragment) getSupportFragmentManager()
                                            .findFragmentById(R.id.map);
                                    mapFragment1 = (SupportMapFragment) getSupportFragmentManager()
                                            .findFragmentById(R.id.map1);
                                    mapFragment.getMapAsync(TrackOrderActivity.this);
                                    mapFragment1.getMapAsync(TrackOrderActivity.this);


//                                    brand_address.setText("" + data.get(i).getOrderMain().getBranchAddress());
//                                    Glide.with(TrackOrderActivity.this).load(Constants.IMAGE_URL + data.get(i).getOrderMain().getBrandLogo()).into(mbrand_logo);

//                                    brand_name.setText("" + data.get(i).getOrderMain().getBrandName_En());

                                    final int finalI1 = i;
//                                    map.setOnClickListener(new View.OnClickListener() {
//                                        @Override
//                                        public void onClick(View view) {
//                                            String geoUri = "http://maps.google.com/maps?q=loc:" + data.get(finalI1).getOrderMain().getBranchLatitude() + "," + data.get(finalI1).getOrderMain().getBranchLongitude() + " (" + data.get(finalI1).getOrderMain().getBranchName_En() + ")";
//                                            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(geoUri));
//                                            startActivity(intent);
//                                        }
//                                    });


//                                    if (data.get(i).getOrderMain().getIsFavourite() == true) {
//
//                                        fav_order.setText("REMOVE AS FAVOURITE");
//
//
//                                    } else {
//
//                                        fav_order.setText("MARK AS FAVOURITE");
//
//                                    }
//
//                                    final int finalI = i;
//                                    fav_order.setOnClickListener(new View.OnClickListener() {
//                                        @Override
//                                        public void onClick(View v) {
//
//                                            if (data.get(finalI).getOrderMain().getIsFavourite() == true) {
//
//                                                isfav = false;
//                                                String networkStatus = NetworkUtil.getConnectivityStatusString(TrackOrderActivity.this);
//                                                if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
//                                                    new FavOrderApi().execute();
//                                                } else {
//                                                    Toast.makeText(TrackOrderActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
//                                                }
//
//                                            } else {
//
//                                                isfav = true;
//                                                String networkStatus = NetworkUtil.getConnectivityStatusString(TrackOrderActivity.this);
//                                                if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
//                                                    new FavOrderApi().execute();
//                                                } else {
//                                                    Toast.makeText(TrackOrderActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
//                                                }
//
//                                            }
//
//                                        }
//                                    });

//                                    order_type.setText("" + data.get(i).getOrderMain().getOrderMode_En());
                                    bill_no.setText("#" + data.get(i).getOrderMain().getInvoiceNo());

//                                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.US);
//                                    SimpleDateFormat dateFormat1 = new SimpleDateFormat("EEE dd", Locale.US);
//                                    SimpleDateFormat dateFormat3 = new SimpleDateFormat("hh:mm aaa", Locale.US);
//
//                                    Date date = null;
//
//                                    try {
//
//                                        date = dateFormat.parse(data.get(i).getOrderMain().getOrderDate());
//
//                                    } catch (ParseException e) {
//                                        e.printStackTrace();
//                                    }
//
//                                    String date1;

//                                    date1 = dateFormat3.format(date);

//                                    order_time.setText("" + date1);

//                                    item_count.setText("" + data.get(i).getOrderItems().size() + " Items");

//                                    total_price.setText("" + Constants.decimalFormat.format(data.get(i).getOrderMain().getTotalPrice()));

//                                    Glide.with(TrackOrderActivity.this)
//                                            .load(Constants.IMAGE_URL + data.get(i).getOrderMain().getBrandLogo())
//                                            .into(store_img);
//
//                                    store_name.setText("" + data.get(i).getOrderMain().getBrandName_En());
//                                    store_address.setText("" + data.get(i).getOrderMain().getBranchAddress());

//                                    phone_no = data.get(i).getOrderMain().getBranchMobileNo();
//
//                                    mcall.setOnClickListener(new View.OnClickListener() {
//                                        @Override
//                                        public void onClick(View view) {
//
//                                            int currentapiVersion = Build.VERSION.SDK_INT;
//                                            if (currentapiVersion >= Build.VERSION_CODES.M) {
//                                                if (!canAccessPhonecalls()) {
//                                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//                                                        requestPermissions(PHONE_PERMS, PHONE_REQUEST);
//                                                    }
//                                                } else {
//                                                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel: +" + phone_no));
//                                                    if (ActivityCompat.checkSelfPermission(TrackOrderActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
//                                                        // TODO: Consider calling
//                                                        //    ActivityCompat#requestPermissions
//                                                        // here to request the missing permissions, and then overriding
//                                                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
//                                                        //                                          int[] grantResults)
//                                                        // to handle the case where the user grants the permission. See the documentation
//                                                        // for ActivityCompat#requestPermissions for more details.
//                                                        return;
//                                                    }
//                                                    startActivity(intent);
//                                                }
//                                            } else {
//                                                Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel: +" + phone_no));
//                                                startActivity(intent);
//                                            }
//
//                                        }
//                                    });

//                                    items_total_price.setText("" + Constants.decimalFormat.format(data.get(i).getOrderMain().getSubTotal()));
//                                    vat_per.setText("(" + data.get(i).getOrderMain().getVatPercentage() + ")");
//                                    vat.setText("+ " + Constants.decimalFormat.format(data.get(i).getOrderMain().getVatCharges()));
//                                    if (data.get(i).getOrderMain().getBranchDiscountAmt() == 0) {
//                                    coupon_discount.setText("_");
//                                    } else {
//                                        coupon_discount.setText("" + data.get(i).getOrderMain().getBranchDiscountAmt());
//                                    }

//                                    delivery_chargers.setText("" + data.get(i).getOrderMain().getDeliveryCharges());
//                                    final_price.setText("SAR " + Constants.decimalFormat.format(data.get(i).getOrderMain().getTotalPrice()));

                                    mAdapter = new TrackOrderItemsAdapter(TrackOrderActivity.this, data.get(i).getOrderItems(), language);
                                    list_view.setAdapter(mAdapter);
                                }

                                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
                                SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
                                SimpleDateFormat dateFormat3 = new SimpleDateFormat("hh:mm aaa", Locale.US);


                                for (int k = 0; k < data.size(); k++) {
                                    for (int j = 0; j < data.get(k).getTrackingOrder().size(); j++) {

                                        if (!status.contains(data.get(k).getTrackingOrder().get(j).getTrOrderStatus())) {

                                            status.add(data.get(k).getTrackingOrder().get(j).getTrOrderStatus());

                                        }
                                    }
                                }


//                                        for (int k=0; k<=status.size(); k++) {
                                Log.e("TAG", "onResponse: " + status);


                                for (int k = 0; k < data.size(); k++) {
                                    for (int j = 0; j < data.get(k).getTrackingOrder().size(); j++) {

//                                        status1.setVisibility(View.GONE);

                                        String orderstate = "Received";

                                        if (orderstate.equals(data.get(k).getTrackingOrder().get(j).getTrOrderStatus())) {

                                            Date statusdate1 = null;

                                            order = true;

                                            Log.i("TAG", "trackorder_status1: " + data.get(k).getTrackingOrder().get(j).getTrackingtime());

                                            try {

                                                statusdate1 = dateFormat.parse(data.get(k).getTrackingOrder().get(j).getTrackingtime());

                                            } catch (ParseException e) {
                                                e.printStackTrace();
                                            }

                                            String statustime1;
                                            String statusdate;
                                            statustime1 = dateFormat3.format(statusdate1);
                                            statusdate = dateFormat1.format(statusdate1);

                                            if (language.equalsIgnoreCase("En")) {
                                                String name = getColoredSpanned("Your order has been send to", "#888888");
                                                String surName = getColoredSpanned(data.get(k).getOrderMain().getBrandName_En(), "#000000");


                                                submit_txt.setText(Html.fromHtml(name + " " + surName));
                                            } else {
                                                String name = getColoredSpanned("تم ارسال طلبك الى", "#888888");
                                                String surName = getColoredSpanned(data.get(k).getOrderMain().getBrandName_Ar(), "#000000");


                                                submit_txt.setText(Html.fromHtml(surName + " " + name));
                                            }

//                                            submit_date.setVisibility(View.VISIBLE);
//                                            submit_time.setVisibility(View.VISIBLE);
                                            submit_layout.setVisibility(View.VISIBLE);
//                                            accept_date.setVisibility(View.INVISIBLE);
//                                            accept_time.setVisibility(View.INVISIBLE);
                                            accept_layout.setVisibility(View.INVISIBLE);
//                                            ready_date.setVisibility(View.INVISIBLE);
//                                            ready_time.setVisibility(View.INVISIBLE);
                                            ready_layout.setVisibility(View.INVISIBLE);
//                                            delivery_date.setVisibility(View.INVISIBLE);
//                                            delivery_time.setVisibility(View.INVISIBLE);
                                            delivery_layout.setVisibility(View.INVISIBLE);

                                            submit_time.setText("" + statustime1);
                                            submit_date.setText("" + statusdate);
                                            Log.i("TAG", "time: " + statustime1);

//                                            Glide.with(TrackOrderActivity.this)
//                                                    .load(R.drawable.track_order_black)
//                                                    .into(mtrack_order);
//
//                                            Glide.with(TrackOrderActivity.this)
//                                                    .load(R.drawable.track_order_black)
//                                                    .into(cup_img);

                                            ratingBar1.setVisibility(View.GONE);
                                            view.setVisibility(View.GONE);
//                                            fav_order.setVisibility(View.VISIBLE);
                                            order_cancel.setVisibility(View.VISIBLE);

                                        }

                                    }
                                }

                                for (int k = 0; k < data.size(); k++) {
                                    for (int j = 0; j < data.get(k).getTrackingOrder().size(); j++) {

                                        String orderstate = "Cancel";

//                                        status2.setVisibility(View.GONE);
                                        if (orderstate.equals(data.get(k).getTrackingOrder().get(j).getTrOrderStatus())) {
                                            if (language.equalsIgnoreCase("En")) {
                                                accept_title.setText("Cancel");
                                            } else {
                                                accept_title.setText("إلغاء");
                                            }


                                            Date statusdate1 = null;

                                            cancel_order = true;

                                            Log.i("TAG", "trackorder_status: " + data.get(k).getTrackingOrder().get(j).getTrackingtime());

                                            try {

                                                statusdate1 = dateFormat.parse(data.get(k).getTrackingOrder().get(j).getTrackingtime());

                                            } catch (ParseException e) {
                                                e.printStackTrace();
                                            }

                                            String statustime1;
                                            String statusdate;
                                            statustime1 = dateFormat3.format(statusdate1);
                                            statusdate = dateFormat1.format(statusdate1);
                                            if (language.equalsIgnoreCase("En")) {
                                                accept_txt.setText("Order Cancel on " + statusdate + " " + statustime1);
                                            } else {
                                                accept_txt.setText("" + statustime1 + " " + statusdate + "الطلب ملغي في ");
                                            }
//                                            order_update_time.setText(statustime1 + " by " + data.get(k).getTrackingOrder().get(j).getAcceptedBy());

//                                            submit_date.setVisibility(View.VISIBLE);
//                                            submit_time.setVisibility(View.VISIBLE);
                                            submit_layout.setVisibility(View.VISIBLE);
//                                            accept_date.setVisibility(View.VISIBLE);
//                                            accept_time.setVisibility(View.VISIBLE);
                                            accept_layout.setVisibility(View.VISIBLE);
//                                            ready_date.setVisibility(View.INVISIBLE);
//                                            ready_time.setVisibility(View.INVISIBLE);
                                            ready_layout.setVisibility(View.INVISIBLE);
//                                            delivery_date.setVisibility(View.INVISIBLE);
//                                            delivery_time.setVisibility(View.INVISIBLE);
                                            delivery_layout.setVisibility(View.INVISIBLE);

                                            accept_time.setText("" + statustime1);
                                            accept_date.setText("" + statusdate);

                                            Glide.with(TrackOrderActivity.this)
                                                    .load(R.drawable.accept)
                                                    .into(accept_img);

//                                            Glide.with(TrackOrderActivity.this)
//                                                    .load(R.drawable.track_confirmed_black)
//                                                    .into(cup_img);

                                            ratingBar1.setVisibility(View.GONE);
                                            view.setVisibility(View.GONE);
//                                            fav_order.setVisibility(View.GONE);
                                            order_cancel.setVisibility(View.GONE);

//                                            order_cancel.setText("Re-order");

//                                            order_cancel.setOnClickListener(new View.OnClickListener() {
//                                                @Override
//                                                public void onClick(View v) {
//
//                                                    Intent a = new Intent(TrackOrderActivity.this, MainActivity.class);
//                                                    a.putExtra("class", "splashscreen");
//                                                    startActivity(a);
//
//                                                }
//                                            });

//                                                        layout_confirm.setVisibility(View.GONE);
                                            layout_ready.setVisibility(View.GONE);
                                            layout_delivery.setVisibility(View.GONE);

                                        }

                                    }
                                }


//                                if (status.contains("Accepted")) {
//                                    status2.setText("Done");

                                for (int k = 0; k < data.size(); k++) {
                                    for (int j = 0; j < data.get(k).getTrackingOrder().size(); j++) {
                                        String orderstate = "Accepted";
//                                        status2.setVisibility(View.GONE);

                                        if (orderstate.equals(data.get(k).getTrackingOrder().get(j).getTrOrderStatus())) {

                                            if (language.equalsIgnoreCase("En")) {
                                                accept_title.setText("Accepted");
                                            } else {
                                                accept_title.setText("تم قبوله");
                                            }

                                            Date statusdate2 = null;

                                            confirm_order = true;

                                            try {

                                                statusdate2 = dateFormat.parse(data.get(k).getTrackingOrder().get(j).getTrackingtime());

                                            } catch (ParseException e) {
                                                e.printStackTrace();
                                            }

                                            String statustime2, statusdate;

                                            statustime2 = dateFormat3.format(statusdate2);
                                            statusdate = dateFormat1.format(statusdate2);
                                            if (language.equalsIgnoreCase("En")) {
                                                accept_txt.setText("Your Order is been processed");
                                            } else {
                                                accept_txt.setText("طلبك قيد المعالجة");
                                            }

//                                            submit_date.setVisibility(View.VISIBLE);
//                                            submit_time.setVisibility(View.VISIBLE);
                                            submit_layout.setVisibility(View.VISIBLE);
//                                            accept_date.setVisibility(View.VISIBLE);
//                                            accept_time.setVisibility(View.VISIBLE);
                                            accept_layout.setVisibility(View.VISIBLE);
//                                            ready_date.setVisibility(View.INVISIBLE);
//                                            ready_time.setVisibility(View.INVISIBLE);
                                            ready_layout.setVisibility(View.INVISIBLE);
//                                            delivery_date.setVisibility(View.INVISIBLE);
//                                            delivery_time.setVisibility(View.INVISIBLE);
                                            delivery_layout.setVisibility(View.INVISIBLE);

                                            accept_time.setText("" + statustime2);
                                            accept_date.setText("" + statusdate);

                                            Glide.with(TrackOrderActivity.this)
                                                    .load(R.drawable.accept)
                                                    .into(accept_img);

//                                            Glide.with(TrackOrderActivity.this)
//                                                    .load(R.drawable.track_confirmed_black)
//                                                    .into(cup_img);

                                            ratingBar1.setVisibility(View.GONE);
                                            view.setVisibility(View.GONE);
//                                            fav_order.setVisibility(View.VISIBLE);
                                            order_cancel.setVisibility(View.VISIBLE);

                                        }

                                    }
                                }


                                for (int k = 0; k < data.size(); k++) {
                                    for (int j = 0; j < data.get(k).getTrackingOrder().size(); j++) {

                                        String orderstate = "Cancel";
//                                        status3.setVisibility(View.GONE);

                                        if (orderstate.equals(data.get(k).getTrackingOrder().get(j).getTrOrderStatus())) {

                                            if (language.equalsIgnoreCase("En")) {
                                                ready_title.setText("Cancel");
                                            } else {
                                                ready_title.setText("إلغاء");
                                            }


                                            Date statusdate1 = null;

                                            cancel_order1 = true;

                                            Log.i("TAG", "trackorder_status: " + data.get(k).getTrackingOrder().get(j).getTrackingtime());

                                            try {

                                                statusdate1 = dateFormat.parse(data.get(k).getTrackingOrder().get(j).getTrackingtime());

                                            } catch (ParseException e) {
                                                e.printStackTrace();
                                            }

                                            String statustime1;
                                            String statusdate;
                                            statustime1 = dateFormat3.format(statusdate1);
                                            statusdate = dateFormat1.format(statusdate1);
                                            if (language.equalsIgnoreCase("En")) {
                                                ready_txt.setText("Order Cancel on " + statusdate + " " + statustime1);
                                            } else {
                                                ready_txt.setText("" + statustime1 + " " + statusdate + "الطلب ملغي في ");

                                            }

//                                            submit_date.setVisibility(View.VISIBLE);
//                                            submit_time.setVisibility(View.VISIBLE);
                                            submit_layout.setVisibility(View.VISIBLE);
//                                            accept_date.setVisibility(View.VISIBLE);
//                                            accept_time.setVisibility(View.VISIBLE);
                                            accept_layout.setVisibility(View.VISIBLE);
//                                            ready_date.setVisibility(View.VISIBLE);
//                                            ready_time.setVisibility(View.VISIBLE);
                                            ready_layout.setVisibility(View.VISIBLE);
//                                            delivery_date.setVisibility(View.INVISIBLE);
//                                            delivery_time.setVisibility(View.INVISIBLE);
                                            delivery_layout.setVisibility(View.INVISIBLE);

                                            ready_time.setText("" + statustime1);
                                            ready_date.setText("" + statusdate);

                                            Glide.with(TrackOrderActivity.this)
                                                    .load(R.drawable.ready)
                                                    .into(ready_img);

//                                            Glide.with(TrackOrderActivity.this)
//                                                    .load(R.drawable.track_order_black)
//                                                    .into(cup_img);

                                            ratingBar1.setVisibility(View.GONE);
                                            view.setVisibility(View.GONE);
//                                            fav_order.setVisibility(View.GONE);
                                            order_cancel.setVisibility(View.GONE);

//                                            order_cancel.setText("Re-order");

//                                            order_cancel.setOnClickListener(new View.OnClickListener() {
//                                                @Override
//                                                public void onClick(View v) {
//
//                                                    Intent a = new Intent(TrackOrderActivity.this, MainActivity.class);
//                                                    a.putExtra("class", "splashscreen");
//                                                    startActivity(a);
//
//                                                }
//                                            });

//                                                    layout_confirm.setVisibility(View.GONE);
//                                                    layout_ready.setVisibility(View.GONE);
                                            layout_delivery.setVisibility(View.GONE);

                                        }

                                    }
                                }
//                                    }

//                                if (status.contains("Ready")) {
//                                    status3.setText("Done");

                                for (int k = 0; k < data.size(); k++) {
                                    for (int j = 0; j < data.get(k).getTrackingOrder().size(); j++) {
//                                        status3.setVisibility(View.GONE);

                                        String orderstate = "Ready";

                                        if (orderstate.equals(data.get(k).getTrackingOrder().get(j).getTrOrderStatus())) {
                                            Date statusdate3 = null;

                                            if (language.equalsIgnoreCase("En")) {
                                                ready_title.setText("Ready");
                                            } else {
                                                ready_title.setText("جاهز");
                                            }

                                            ready_order = true;

                                            try {

                                                statusdate3 = dateFormat.parse(data.get(k).getTrackingOrder().get(j).getTrackingtime());

                                            } catch (ParseException e) {
                                                e.printStackTrace();
                                            }

                                            String statustime3, statusdate;

                                            statustime3 = dateFormat3.format(statusdate3);
                                            statusdate = dateFormat1.format(statusdate3);

                                            if (language.equalsIgnoreCase("En")) {
                                                if (data.get(k).getOrderMain().getOrderType() == 1 || data.get(k).getOrderMain().getOrderType() == 2) {
                                                    ready_txt.setText("Your order is ready to be served");
                                                } else if (data.get(k).getOrderMain().getOrderType() == 3) {
                                                    ready_txt.setText("Your order is ready for delivery");
                                                }
                                            } else {
                                                if (data.get(k).getOrderMain().getOrderType() == 1 || data.get(k).getOrderMain().getOrderType() == 2) {
                                                    ready_txt.setText("طلبك جاهز");
                                                } else if (data.get(k).getOrderMain().getOrderType() == 3) {
                                                    ready_txt.setText("طلبك جاهز للتوصيل");
                                                }
                                            }

//                                            submit_date.setVisibility(View.VISIBLE);
//                                            submit_time.setVisibility(View.VISIBLE);
                                            submit_layout.setVisibility(View.VISIBLE);
//                                            accept_date.setVisibility(View.VISIBLE);
//                                            accept_time.setVisibility(View.VISIBLE);
                                            accept_layout.setVisibility(View.VISIBLE);
//                                            ready_date.setVisibility(View.VISIBLE);
//                                            ready_time.setVisibility(View.VISIBLE);
                                            ready_layout.setVisibility(View.VISIBLE);
//                                            delivery_date.setVisibility(View.INVISIBLE);
//                                            delivery_time.setVisibility(View.INVISIBLE);
                                            delivery_layout.setVisibility(View.INVISIBLE);

                                            ready_time.setText("" + statustime3);
                                            ready_date.setText("" + statusdate);

                                            Glide.with(TrackOrderActivity.this)
                                                    .load(R.drawable.ready)
                                                    .into(ready_img);

//                                            Glide.with(TrackOrderActivity.this)
//                                                    .load(R.drawable.track_ready_black)
//                                                    .into(cup_img);

                                            ratingBar1.setVisibility(View.GONE);
                                            view.setVisibility(View.GONE);
//                                            fav_order.setVisibility(View.VISIBLE);
                                            order_cancel.setVisibility(View.GONE);

//                                            order_cancel.setText("Re-order");

//                                            order_cancel.setOnClickListener(new View.OnClickListener() {
//                                                @Override
//                                                public void onClick(View v) {
//
//                                                    Intent a = new Intent(TrackOrderActivity.this, MainActivity.class);
//                                                    a.putExtra("class", "splashscreen");
//                                                    startActivity(a);
//
//                                                }
//                                            });


//                                            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
//                                            params.setMargins(10, 0, 10, 0);
//                                            fav_order.setLayoutParams(params);

                                        }
                                    }
                                }


//                                if (status.contains("Delivered") || status.contains("Served")) {
//                                    status4.setText("Done");


                                for (int k = 0; k < data.size(); k++) {
                                    for (int j = 0; j < data.get(k).getTrackingOrder().size(); j++) {
//                                        status4.setVisibility(View.GONE);

                                        String orderstate = "Close";

                                        if (orderstate.equals(data.get(k).getTrackingOrder().get(j).getTrOrderStatus())) {

                                            Date statusdate4 = null;
                                            delivered_order = true;

                                            try {

                                                statusdate4 = dateFormat.parse(data.get(k).getTrackingOrder().get(j).getTrackingtime());

                                            } catch (ParseException e) {
                                                e.printStackTrace();
                                            }

                                            String statustime4, statusdate;

                                            statustime4 = dateFormat3.format(statusdate4);
                                            statusdate = dateFormat1.format(statusdate4);
//                                            if (data.get(k).getTrackingOrder().get(j).getTrOrderStatus().contains("Delivered")) {
//                                                address_txt.setText("" + data.get(k).getOrderMain().getBranchAddress()); // here we have to show user addreess change it later after parmeter is add.
//                                            } else if (data.get(k).getTrackingOrder().get(j).getTrOrderStatus().contains("Served")) {
//                                                delivery_title.setText("Served to");
//                                                address_txt.setText("" + data.get(k).getOrderMain().getBranchAddress());
//                                            }

//                                            order_update_time.setText(statustime4 + " by " + data.get(k).getTrackingOrder().get(j).getAcceptedBy());

//                                            submit_date.setVisibility(View.VISIBLE);
//                                            submit_time.setVisibility(View.VISIBLE);
                                            submit_layout.setVisibility(View.VISIBLE);
//                                            accept_date.setVisibility(View.VISIBLE);
//                                            accept_time.setVisibility(View.VISIBLE);
                                            accept_layout.setVisibility(View.VISIBLE);
//                                            ready_date.setVisibility(View.VISIBLE);
//                                            ready_time.setVisibility(View.VISIBLE);
                                            ready_layout.setVisibility(View.VISIBLE);
//                                            delivery_date.setVisibility(View.VISIBLE);
//                                            delivery_time.setVisibility(View.VISIBLE);
                                            delivery_layout.setVisibility(View.VISIBLE);

                                            delivery_time.setText("" + statustime4);
                                            delivery_date.setText("" + statusdate);

//                                            Glide.with(TrackOrderActivity.this)
//                                                    .load(R.drawable.track_delivered_black)
//                                                    .into(mtrack_delivered);
//
//                                            Glide.with(TrackOrderActivity.this)
//                                                    .load(R.drawable.track_delivered_black)
//                                                    .into(cup_img);

                                            ratingBar1.setVisibility(View.VISIBLE);
                                            view.setVisibility(View.GONE);
//                                            fav_order.setVisibility(View.GONE);
                                            order_cancel.setVisibility(View.GONE);

                                            final int finalI12 = k;
                                            if (data.get(k).getOrderMain().getRating() == 0) {

                                                ratingBar1.setRating(data.get(k).getOrderMain().getRating());
                                                ratingBar1.setOnRatingChangeListener(new MaterialRatingBar.OnRatingChangeListener() {
                                                    @Override
                                                    public void onRatingChanged(MaterialRatingBar ratingBar, float rating) {
                                                        Log.i("TAG", "onCreate: " + ratingBar1.getNumStars());

                                                        rating = ratingBar1.getRating();

//                                                        Bundle args = new Bundle();
////                    Log.d("TAG", "storeArrayList "+storesArrayList.size());
////                                            Log.d("TAG", "pos " + getAdapterPosition());
//                                                        args.putInt("UserId", userid);
//                                                        args.putInt("OrderId", orderid);
//                                                        args.putFloat("rating", rating);
//                                                        String comment = data.get(finalI12).getOrderMain().getRatingComment();
//                                                        if (comment == null) {
//
//                                                            comment = "";
//                                                        }
//                                                        args.putString("comment", comment);
//                                                        args.putString("brand_name_ar", data.get(finalI12).getOrderMain().getBrandName_Ar());
//                                                        args.putString("brand_name", data.get(finalI12).getOrderMain().getBrandName_En());
//
//                                                        Log.i("TAG", "onRatingChanged: " + rating);
//
//                                                        final TrackListDialog newFragment = TrackListDialog.newInstance();
//                                                        newFragment.setCancelable(true);
//                                                        newFragment.setArguments(args);
//                                                        newFragment.show((TrackOrderActivity.this).getSupportFragmentManager(), "track_order ");

                                                        Intent a = new Intent(TrackOrderActivity.this, RatingActivity.class);
                                                        a.putExtra("UserId", userid);
                                                        a.putExtra("OrderId", orderid);
                                                        a.putExtra("rating", rating);
                                                        String comment = data.get(finalI12).getOrderMain().getRatingComment();
                                                        if (comment == null) {

                                                            comment = "";
                                                        }
                                                        a.putExtra("comment", comment);
                                                        a.putExtra("brand_name_ar", data.get(finalI12).getOrderMain().getBrandName_Ar());
                                                        a.putExtra("brand_name", data.get(finalI12).getOrderMain().getBrandName_En());
                                                        startActivity(a);
                                                    }
                                                });
                                            } else {

                                                ratingBar1.setVisibility(View.GONE);
                                                ratingBar1.setOnRatingChangeListener(new MaterialRatingBar.OnRatingChangeListener() {
                                                    @Override
                                                    public void onRatingChanged(MaterialRatingBar ratingBar, float rating) {
                                                        Log.i("TAG", "onCreate: " + ratingBar1.getNumStars());

                                                        rating = ratingBar1.getRating();

//                                                        Bundle args = new Bundle();
////                    Log.d("TAG", "storeArrayList "+storesArrayList.size());
////                                            Log.d("TAG", "pos " + getAdapterPosition());
//                                                        args.putInt("UserId", userid);
//                                                        args.putInt("OrderId", orderid);
//                                                        args.putFloat("rating", rating);
//                                                        String comment = data.get(finalI12).getOrderMain().getRatingComment();
//                                                        if (comment == null) {
//
//                                                            comment = "";
//                                                        }
//                                                        args.putString("comment", comment);
//                                                        args.putString("brand_name_ar", data.get(finalI12).getOrderMain().getBrandName_Ar());
//                                                        args.putString("brand_name", data.get(finalI12).getOrderMain().getBrandName_En());
//
//                                                        final TrackListDialog newFragment = TrackListDialog.newInstance();
//                                                        newFragment.setCancelable(true);
//                                                        newFragment.setArguments(args);
//                                                        newFragment.show((TrackOrderActivity.this).getSupportFragmentManager(), "track_order ");

                                                        Intent a = new Intent(TrackOrderActivity.this, RatingActivity.class);
                                                        a.putExtra("UserId", userid);
                                                        a.putExtra("OrderId", orderid);
                                                        a.putExtra("rating", rating);
                                                        String comment = data.get(finalI12).getOrderMain().getRatingComment();
                                                        if (comment == null) {

                                                            comment = "";
                                                        }
                                                        a.putExtra("comment", comment);
                                                        a.putExtra("brand_name_ar", data.get(finalI12).getOrderMain().getBrandName_Ar());
                                                        a.putExtra("brand_name", data.get(finalI12).getOrderMain().getBrandName_En());
                                                        startActivity(a);
                                                    }
                                                });

                                            }

                                            Log.i("TAG", "onCreate: " + ratingBar1.getRating());

//                                            order_cancel.setText("Re-order");
//
//                                            order_cancel.setOnClickListener(new View.OnClickListener() {
//                                                @Override
//                                                public void onClick(View v) {
//
//                                                    Intent a = new Intent(TrackOrderActivity.this, MainActivity.class);
//                                                    a.putExtra("class", "splashscreen");
//                                                    startActivity(a);
//
//                                                }
//                                            });

                                        }

                                    }
                                }


                                if (order) {

                                } else {
//                                    submit_time.setVisibility(View.INVISIBLE);
////                                    status1.setText("Next");
//                                    submit_date.setVisibility(View.INVISIBLE);
                                    submit_layout.setVisibility(View.INVISIBLE);
                                }

                                if (confirm_order || cancel_order) {

                                } else {
//                                    accept_date.setVisibility(View.INVISIBLE);
////                                    status1.setText("Next");
//                                    accept_time.setVisibility(View.INVISIBLE);
                                    accept_layout.setVisibility(View.INVISIBLE);
                                }

                                if (ready_order || cancel_order1) {

                                } else {
//                                    ready_date.setVisibility(View.INVISIBLE);
////                                    status1.setText("Next");
//                                    ready_time.setVisibility(View.INVISIBLE);
                                    ready_layout.setVisibility(View.INVISIBLE);
                                }

                                if (delivered_order) {

                                } else {
//                                    delivery_date.setVisibility(View.INVISIBLE);
////                                    status4.setText("Next");
//                                    delivery_time.setVisibility(View.INVISIBLE);
                                    delivery_layout.setVisibility(View.INVISIBLE);
                                }


//                                mscrollview.smoothScrollTo(0, 0);
//                                bids_count.setText(""+data.size());

                            } else {
                                //                          status false case
                                if (language.equalsIgnoreCase("En")) {
                                    String failureResponse = orderItems.getMessage();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name),
                                            getResources().getString(R.string.ok), TrackOrderActivity.this);
                                } else {
                                    String failureResponse = orderItems.getMessageAr();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name_ar),
                                            getResources().getString(R.string.ok_ar), TrackOrderActivity.this);
                                }
                            }
                            Log.i("TAG", "onResponse: " + data.size());
                        } catch (Exception e) {
                            e.printStackTrace();
                            Log.i("TAG", "onResponse: " + data.size());
                            if (language.equalsIgnoreCase("En")) {
                                Toast.makeText(TrackOrderActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(TrackOrderActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                            }
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(TrackOrderActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(TrackOrderActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }

                    if (customDialog != null){
                        customDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<TrackOrder> call, Throwable t) {
                    Log.d("TAG", "onFailure: " + t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(TrackOrderActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(TrackOrderActivity.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(TrackOrderActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(TrackOrderActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }

                    if (customDialog != null){
                        customDialog.dismiss();
                    }

                }
            });
            return null;
        }

        private String prepareVerifyMobileJson() {
            JSONObject parentObj = new JSONObject();
            try {

                parentObj.put("OrderId", orderid);

            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.d("TAG", "prepareVerifyMobileJson: " + parentObj);
            return parentObj.toString();
        }

    }

    private boolean canAccessPhonecalls() {
        return (hasPermission(android.Manifest.permission.CALL_PHONE));
    }

    private boolean hasPermission(String perm) {
        return (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(TrackOrderActivity.this, perm));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {


            case PHONE_REQUEST:
                if (canAccessPhonecalls()) {
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + phone_no));
                    if (ActivityCompat.checkSelfPermission(TrackOrderActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return;
                    }
                    startActivity(intent);
                } else {
                    if (language.equalsIgnoreCase("En")) {
                        Toast.makeText(TrackOrderActivity.this, "Call phone permission denied, unable to make call", Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(TrackOrderActivity.this, "Call phone permission denied, unable to make call", Toast.LENGTH_LONG).show();
                    }
                }
                break;
        }
    }

}
