package com.cs.chef.Activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.chef.Adapter.MenuGridAdapter;
import com.cs.chef.Adapter.MenuHeaderAdapter;
import com.cs.chef.Constants;
import com.cs.chef.DataBaseHelper;
import com.cs.chef.Models.CatNames;
import com.cs.chef.Models.Items;
import com.cs.chef.Models.StoreMenuItems;
import com.cs.chef.Models.StoresList;
import com.cs.chef.R;
import com.cs.chef.Rest.APIInterface;
import com.cs.chef.Rest.ApiClient;
import com.cs.chef.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MenuActivity extends AppCompatActivity {

    RelativeLayout bag_layout;
    TextView bag_count, brand_name, header_title;
    Button checkout;
    RecyclerView horizontal_view, grid_view;
    ImageView backbtn;
    int branchid = 0;
    ArrayList<StoreMenuItems.MenuItems> data = new ArrayList<>();
    ArrayList<CatNames> catNames = new ArrayList<>();
    ArrayList<Integer> catname_id = new ArrayList<>();
    ArrayList<Integer> itemname_id = new ArrayList<>();
    ArrayList<Items> itemsArrayList = new ArrayList<>();
    public static String selectedcat = "all";
    public static String selectedHotel1;
    MenuHeaderAdapter mAdapter;
    MenuGridAdapter menuGridAdapter;
    public static ArrayList<StoresList.StoresDetails> storeData = new ArrayList<>();
    public static ArrayList<StoresList.StoreDetails> storeData1 = new ArrayList<>();
    public static int pos;
    public static boolean banner = false, filter = false;
    DataBaseHelper myDbHelper;

    String language;
    SharedPreferences languagePrefs;
    boolean header_pos = true;

//    TextView total_qty, total_price, add_card;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.menu_fragment);
        } else {
            setContentView(R.layout.menu_fragment_arabic);
        }

        if (getIntent().getStringExtra("banner").equals("banner")) {
            banner = true;
            storeData1 = (ArrayList<StoresList.StoreDetails>) getIntent().getSerializableExtra("array");
            pos = getIntent().getIntExtra("pos", 0);
            branchid = storeData1.get(pos).getBranchId();
        } else {
            banner = false;
            storeData = (ArrayList<StoresList.StoresDetails>) getIntent().getSerializableExtra("array");
            pos = getIntent().getIntExtra("pos", 0);
            branchid = storeData.get(pos).getBranchId();
        }
        catNames.clear();
        itemsArrayList.clear();
        myDbHelper = new DataBaseHelper(MenuActivity.this);
        backbtn = findViewById(R.id.back_btn);
        horizontal_view = findViewById(R.id.horizontal_recycler_view);
        grid_view = findViewById(R.id.grid_view);
        header_title = findViewById(R.id.header_title);
//        total_qty = findViewById(R.id.qty);
//        total_price = findViewById(R.id.total_price);
        checkout = findViewById(R.id.add_card);
        bag_count = findViewById(R.id.bag_count);
        brand_name = findViewById(R.id.brand_name);
        bag_layout = findViewById(R.id.bag_layout);

        if (language.equalsIgnoreCase("En")) {
            if (banner) {
                header_title.setText("" + storeData1.get(pos).getBranchName_En());
            } else {
                header_title.setText("" + storeData.get(pos).getBranchName_En());
            }
        } else {
            if (banner) {
                header_title.setText("" + storeData1.get(pos).getBranchName_Ar());
            } else {
                header_title.setText("" + storeData.get(pos).getBranchName_Ar());
            }
        }

        bag_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent a = new Intent(MenuActivity.this, MainActivity.class);
                a.putExtra("class", "checkout");
                startActivity(a);

            }
        });


        backbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();

            }
        });

        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);

        horizontal_view.setLayoutManager(layoutManager);
        final LinearLayoutManager layoutManager1 = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        grid_view.setLayoutManager(layoutManager1);
//        grid_view.setLayoutManager(new GridLayoutManager(getApplicationContext(), 2));
//        int spacingInPixels = 20;
//        grid_view.addItemDecoration(new SpacesItemDecoration(spacingInPixels));

        String networkStatus1 = NetworkUtil.getConnectivityStatusString(MenuActivity.this);
        if (!networkStatus1.equalsIgnoreCase("Not connected to Internet")) {
            new StoreMenuApi().execute();
        } else {
            if (language.equalsIgnoreCase("En")) {
                Toast.makeText(MenuActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(MenuActivity.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
            }
        }

        LocalBroadcastManager.getInstance(MenuActivity.this).registerReceiver(
                mheader_name, new IntentFilter("HeaderUpdate"));

        grid_view.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                int scrollPosition = layoutManager1.findFirstVisibleItemPosition();
                Log.i("TAG", "onScrolled: " + scrollPosition);
                Log.i("TAG", "onSelect: " + selectedcat);
                selectedcat = "all";
                mAdapter.notifyDataSetChanged();
                if (!header_pos) {
                    selectedcat = catNames.get(scrollPosition).getCatname_en();
                    mAdapter.notifyDataSetChanged();
                    if (scrollPosition == 1) {
                        horizontal_view.scrollToPosition(scrollPosition - 1);
                    } else {
                        horizontal_view.scrollToPosition(scrollPosition);
                    }
                }

            }
        });

    }

    private BroadcastReceiver mheader_name = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String b = intent.getStringExtra("cat_name");
            int c = intent.getIntExtra("pos", 0);

            try {
                if (b.equalsIgnoreCase("All")) {
                    grid_view.scrollToPosition(0);
                    header_pos = true;
                } else if (b.equalsIgnoreCase(catNames.get(1).getCatname_en())) {
                    grid_view.scrollToPosition(0);
                    header_pos = false;
                } else {
                    grid_view.scrollToPosition(c);
                    header_pos = false;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            Log.i("TAG", "onReceive: " + header_pos);

        }
    };

    private class StoreMenuApi extends AsyncTask<String, String, String> {

        //        ACProgressFlower dialog = null;
        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareVerifyMobileJson();
//            dialog = new ACProgressFlower.Builder(MenuActivity.this)
//                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
//                    .themeColor(Color.WHITE)
//                    .fadeColor(Color.DKGRAY).build();
//            dialog.show();
            Constants.showLoadingDialog(MenuActivity.this);
//            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(MenuActivity.this);
//            // ...Irrelevant code for customizing the buttons and title
//            LayoutInflater inflater = getLayoutInflater();
//            int layout = R.layout.loder_dialog;
//            View dialogView = inflater.inflate(layout, null);
//            dialogBuilder.setView(dialogView);
//            dialogBuilder.setCancelable(true);
//
//            dialog = dialogBuilder.create();
//            dialog.show();
//            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
//            Window window = dialog.getWindow();
//            window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//            lp.copyFrom(window.getAttributes());
//            //This makes the dialog take up the full width
//            Display display = MenuActivity.this.getWindowManager().getDefaultDisplay();
//            Point size = new Point();
//            display.getSize(size);
//            int screenWidth = size.x;
//
//            double d = screenWidth*0.85;
//            lp.width = (int) d;
//            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
//            window.setAttributes(lp);
        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(MenuActivity.this);
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<StoreMenuItems> call = apiService.getStoreMenu(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<StoreMenuItems>() {
                @Override
                public void onResponse(Call<StoreMenuItems> call, Response<StoreMenuItems> response) {
                    Log.d("TAG", "onResponse: " + response);
                    if (response.isSuccessful()) {
                        StoreMenuItems storeMenuItems = response.body();
                        data = storeMenuItems.getData().getMenuitems();
                        try {
                            if (storeMenuItems.getStatus()) {
                                String message = storeMenuItems.getMessage();


//                                for (int i = 0; i < data.size(); i++) {
//                                    ArrayList<StoreMenuextendsubitems> storeMenuextendsubitems = new ArrayList<>();
//                                    for (int j = 0; j < data.get(i).getMenuitems().size(); j++) {
//                                        StoreMenuextendsubitems storeMenuextendsubitems1 = new StoreMenuextendsubitems();
//                                        Log.i("TAG", "itemid: " + data.get(i).getMenuitems().get(j).getItemid());
//                                        storeMenuextendsubitems1.setItemid(String.valueOf(data.get(i).getMenuitems().get(j).getItemid()));
//                                        storeMenuextendsubitems1.setItemnameEn(data.get(i).getMenuitems().get(j).getItemnameEn());
//                                        storeMenuextendsubitems1.setItemnameAr(data.get(i).getMenuitems().get(j).getItemnameAr());
//                                        storeMenuextendsubitems1.setItemdescEn(data.get(i).getMenuitems().get(j).getItemdescEn());
//                                        storeMenuextendsubitems1.setItemdescAr(data.get(i).getMenuitems().get(j).getItemdescAr());
//                                        storeMenuextendsubitems1.setItemimage(data.get(i).getMenuitems().get(j).getItemimage());
//                                        storeMenuextendsubitems1.setPrice(String.valueOf(data.get(i).getMenuitems().get(j).getPrice()));
//
//                                        storeMenuextendsubitems.add(storeMenuextendsubitems1);
//
//                                    }
//                                    StoreMenuextenditems storeMenuextenditems = new StoreMenuextenditems(data.get(i).getCategorynameEn(), storeMenuextendsubitems);
//                                    storeMenuextenditems.setCategorynameEn(data.get(i).getCategorynameEn());
//                                    storeMenuextenditems.setCategorynameAr(data.get(i).getCategorynameAr());
//                                    storeMenuextenditems.setCatdescEn(data.get(i).getCatdescEn());
//                                    storeMenuextenditems.setCatdescAr(data.get(i).getCatdescAr());
//                                    storeMenuextenditems.setCategoryid(String.valueOf(data.get(i).getCategoryid()));
//                                    storeMenuextenditems.setCatimage(data.get(i).getCatimage());
//
//
////                                    storeMenuextenditems.setMenuitem(storeMenuextendsubitems);
//                                    storeMenuextended.add(storeMenuextenditems);
//                                }
//
//                                for (int i = 0; i < data.size(); i++) {
//                                    ArrayList<SubitemsSearchFilter> storeMenuextendsubitems = new ArrayList<>();
//                                    for (int j = 0; j < data.get(i).getMenuitems().size(); j++) {
//                                        SubitemsSearchFilter storeMenuextendsubitems1 = new SubitemsSearchFilter();
//                                        Log.i("TAG", "itemid: " + data.get(i).getMenuitems().get(j).getItemid());
//                                        storeMenuextendsubitems1.setItemid(String.valueOf(data.get(i).getMenuitems().get(j).getItemid()));
//                                        storeMenuextendsubitems1.setItemnameEn(data.get(i).getMenuitems().get(j).getItemnameEn());
//                                        storeMenuextendsubitems1.setItemnameAr(data.get(i).getMenuitems().get(j).getItemnameAr());
//                                        storeMenuextendsubitems1.setItemdescEn(data.get(i).getMenuitems().get(j).getItemdescEn());
//                                        storeMenuextendsubitems1.setItemdescAr(data.get(i).getMenuitems().get(j).getItemdescAr());
//                                        storeMenuextendsubitems1.setItemimage(data.get(i).getMenuitems().get(j).getItemimage());
//                                        storeMenuextendsubitems1.setPrice(String.valueOf(data.get(i).getMenuitems().get(j).getPrice()));
//
//                                        storeMenuextendsubitems.add(storeMenuextendsubitems1);
//
//                                    }
//                                    SearchFilter storeMenuextenditems = new SearchFilter(data.get(i).getCategorynameEn(), storeMenuextendsubitems);
//                                    storeMenuextenditems.setCategorynameEn(data.get(i).getCategorynameEn());
//                                    storeMenuextenditems.setCategorynameAr(data.get(i).getCategorynameAr());
//                                    storeMenuextenditems.setCatdescEn(data.get(i).getCatdescEn());
//                                    storeMenuextenditems.setCatdescAr(data.get(i).getCatdescAr());
//                                    storeMenuextenditems.setCategoryid(String.valueOf(data.get(i).getCategoryid()));
//                                    storeMenuextenditems.setCatimage(data.get(i).getCatimage());
//
//
////                                    storeMenuextenditems.setMenuitem(storeMenuextendsubitems);
//                                    searchFilters.add(storeMenuextenditems);
//                                }

                                for (int i = 0; i < data.size(); i++) {

                                    String all = null;
                                    String all_ar = null;
                                    all = "All";
                                    all_ar = "الكل";
                                    int all_id = -1;
                                    if (!catname_id.contains(all_id)) {
                                        catname_id.add(all_id);
                                        CatNames catname = new CatNames();
                                        catname.setCatname_en(all);
                                        catname.setCatname_ar(all_ar);

                                        ArrayList<StoreMenuItems.MenuItems> menuItemsArrayList = new ArrayList<>();
                                        ArrayList<Items> itemnames = new ArrayList<>();
                                        itemname_id.clear();
                                        menuItemsArrayList.addAll(data);
                                        for (int j = 0; j < menuItemsArrayList.size(); j++) {
                                            if (data.get(i).getCategorynameEn().equalsIgnoreCase(menuItemsArrayList.get(j).getCategorynameEn())) {
                                                for (int k = 0; k < menuItemsArrayList.get(j).getMenuitems().size(); k++) {
//                                                    if (!itemname_id.contains(menuItemsArrayList.get(j).getMenuitems().get(k).getItemid())) {
                                                    Items itemname = new Items();
//                                                        itemname_id.add(menuItemsArrayList.get(j).getMenuitems().get(k).getItemid());
                                                    itemname.setItemid(menuItemsArrayList.get(j).getMenuitems().get(k).getItemid());
                                                    itemname.setItem_name_en(menuItemsArrayList.get(j).getMenuitems().get(k).getItemnameEn());
                                                    itemname.setItem_name_ar(menuItemsArrayList.get(j).getMenuitems().get(k).getItemnameAr());
                                                    itemname.setItem_desc_en(menuItemsArrayList.get(j).getMenuitems().get(k).getItemdescEn());
                                                    itemname.setItem_desc_ar(menuItemsArrayList.get(j).getMenuitems().get(k).getItemdescAr());
                                                    itemname.setItem_price(menuItemsArrayList.get(j).getMenuitems().get(k).getPrice());
                                                    itemname.setItem_image(menuItemsArrayList.get(j).getMenuitems().get(k).getItemimage());

                                                    Log.i("TAG", "catall: " + catname.getCatname_en());
                                                    Log.i("TAG", "itemall: " + itemname.getItem_name_en());

                                                    itemnames.add(itemname);
//                                                    }
                                                }
                                            }
                                        }
                                        catname.setItemsArrayList(itemnames);

                                        catNames.add(catname);
                                    }

                                    if (!catname_id.contains(data.get(i).getCategoryid())) {
                                        CatNames catname = new CatNames();
                                        catname_id.add(data.get(i).getCategoryid());
                                        catname.setCatname_en(data.get(i).getCategorynameEn());
                                        catname.setCatname_ar(data.get(i).getCategorynameAr());

                                        ArrayList<StoreMenuItems.MenuItems> menuItemsArrayList = new ArrayList<>();
                                        ArrayList<Items> itemnames = new ArrayList<>();
                                        itemname_id.clear();
                                        menuItemsArrayList.addAll(data);
                                        for (int j = 0; j < menuItemsArrayList.size(); j++) {
                                            if (data.get(i).getCategorynameEn().equalsIgnoreCase(menuItemsArrayList.get(j).getCategorynameEn())) {
                                                for (int k = 0; k < menuItemsArrayList.get(j).getMenuitems().size(); k++) {
                                                    if (!itemname_id.contains(menuItemsArrayList.get(j).getMenuitems().get(k).getItemid())) {
                                                        Items itemname = new Items();
                                                        itemname_id.add(menuItemsArrayList.get(j).getMenuitems().get(k).getItemid());
                                                        itemname.setItemid(menuItemsArrayList.get(j).getMenuitems().get(k).getItemid());
                                                        itemname.setItem_name_en(menuItemsArrayList.get(j).getMenuitems().get(k).getItemnameEn());
                                                        itemname.setItem_name_ar(menuItemsArrayList.get(j).getMenuitems().get(k).getItemnameAr());
                                                        itemname.setItem_desc_en(menuItemsArrayList.get(j).getMenuitems().get(k).getItemdescEn());
                                                        itemname.setItem_desc_ar(menuItemsArrayList.get(j).getMenuitems().get(k).getItemdescAr());
                                                        itemname.setItem_price(menuItemsArrayList.get(j).getMenuitems().get(k).getPrice());
                                                        itemname.setItem_image(menuItemsArrayList.get(j).getMenuitems().get(k).getItemimage());

                                                        Log.i("TAG", "cat: " + catname.getCatname_en());
                                                        Log.i("TAG", "item: " + itemname.getItem_name_en());

                                                        itemnames.add(itemname);
                                                    }
                                                }
                                            }
                                        }
                                        catname.setItemsArrayList(itemnames);
                                        catNames.add(catname);
                                    }
                                }

                                for (int j = 0; j < catNames.size(); j++) {

                                    Log.i("TAG", "cat1: " + catNames.get(j).getCatname_en());
                                    for (int k = 0; k < catNames.get(j).getItemsArrayList().size(); k++) {

                                        Log.i("TAG", "item1: " + catNames.get(j).getItemsArrayList().get(k).getItem_name_en());
                                    }
                                }

                                menuGridAdapter = new MenuGridAdapter(MenuActivity.this, catNames, itemsArrayList, language);
                                grid_view.setAdapter(menuGridAdapter);

                                mAdapter = new MenuHeaderAdapter(MenuActivity.this, catNames, language);
                                horizontal_view.setAdapter(mAdapter);

//                                if (selectedcat.equalsIgnoreCase("all")) {
//                                    for (int i = 1; i < catNames.size(); i++) {
//                                        for (int j = 0; j < catNames.get(i).getItemsArrayList().size(); j++) {
//                                            Items itemname = new Items();
//                                            itemname.setItemid(catNames.get(i).getItemsArrayList().get(j).getItemid());
//                                            itemname.setItem_name_en(catNames.get(i).getItemsArrayList().get(j).getItem_name_en());
//                                            itemname.setItem_name_ar(catNames.get(i).getItemsArrayList().get(j).getItem_name_ar());
//                                            itemname.setItem_desc_en(catNames.get(i).getItemsArrayList().get(j).getItem_desc_en());
//                                            itemname.setItem_desc_ar(catNames.get(i).getItemsArrayList().get(j).getItem_desc_ar());
//                                            itemname.setItem_price(catNames.get(i).getItemsArrayList().get(j).getItem_price());
//                                            itemname.setItem_image(catNames.get(i).getItemsArrayList().get(j).getItem_image());
////                                            Log.i("TAG", "catall: " + catname.getCatname_en());
//                                            Log.i("TAG", "itemall1: " + itemname.getItem_name_en());
//                                            itemsArrayList.add(itemname);
//                                        }
//                                    }
//                                }

//                                mAdapter = new ExtandableRecyclerAdapter(MenuActivity.this, storeMenuextended);
//                                menu_list.setAdapter(mAdapter);

//                                menu_list.expandGroup(0, true);
//
//                                menu_list.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
//
//                                    @Override
//                                    public boolean onGroupClick(ExpandableListView listview, View view,
//                                                                int group_pos, long id) {
//
//                                        Toast.makeText(MenuActivity.this,
//                                                "You clicked : " + mAdapter.getGroup(group_pos),
//                                                Toast.LENGTH_SHORT).show();
//                                        return false;
//                                    }
//                                });
//
//                                // This listener will expand one group at one time
//                                // You can remove this listener for expanding all groups
//                                menu_list
//                                        .setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
//
//                                            // Default position
//                                            int previousGroup = -1;
//
//                                            @Override
//                                            public void onGroupExpand(int groupPosition) {
//                                                if (groupPosition != previousGroup)
//
//                                                    // Collapse the expanded group
//                                                    menu_list.collapseGroup(previousGroup);
//                                                previousGroup = groupPosition;
//                                            }
//
//                                        });


//                                mscrollview.smoothScrollTo(0, 0);
//                                bids_count.setText(""+data.size());

                            } else {
                                //                          status false case
                                if (language.equalsIgnoreCase("En")) {
                                    String failureResponse = storeMenuItems.getMessage();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name),
                                            getResources().getString(R.string.ok), MenuActivity.this);
                                } else {
                                    String failureResponse = storeMenuItems.getMessageAr();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name_ar),
                                            getResources().getString(R.string.ok_ar), MenuActivity.this);
                                }
                            }
                            Log.i("TAG", "onResponse: " + data.size());
                        } catch (Exception e) {
                            e.printStackTrace();
                            if (language.equalsIgnoreCase("En")) {
                                Toast.makeText(MenuActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(MenuActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                            }
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(MenuActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(MenuActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }

                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<StoreMenuItems> call, Throwable t) {
                    Log.d("TAG", "onFailure: " + t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(MenuActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(MenuActivity.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(MenuActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(MenuActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }

                    Constants.closeLoadingDialog();
                }
            });
            return null;
        }

        private String prepareVerifyMobileJson() {
            JSONObject parentObj = new JSONObject();
            try {
                parentObj.put("BranchId", branchid);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.d("TAG", "prepareVerifyMobileJson: " + parentObj);
            return parentObj.toString();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (myDbHelper.getTotalOrderQty() == 0) {

            bag_count.setVisibility(View.GONE);

        } else {

            bag_count.setVisibility(View.VISIBLE);
            bag_count.setText("" + myDbHelper.getTotalOrderQty());

        }

//        if (myDbHelper.getTotalOrderQty() == 0){
//
//            mbottom_bar.setVisibility(View.GONE);
//
//        }else {

//            total_qty.setText("" + myDbHelper.getTotalOrderQty() + " Item");
//            total_price.setText("SAR " + Constants.decimalFormat.format(myDbHelper.getTotalOrderPrice()));
        checkout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent a = new Intent(MenuActivity.this, MainActivity.class);
                a.putExtra("class", "checkout");
                startActivity(a);

            }
        });

//        }
    }

}
