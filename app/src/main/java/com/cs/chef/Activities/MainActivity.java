package com.cs.chef.Activities;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.design.internal.BottomNavigationItemView;
import android.support.design.internal.BottomNavigationMenuView;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.cs.chef.DataBaseHelper;
import com.cs.chef.Fragments.AccountFragment;
import com.cs.chef.Fragments.HomeScreenFragment;
import com.cs.chef.Models.Order;
import com.cs.chef.R;

import java.lang.reflect.Field;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    FragmentManager fragmentManager = getSupportFragmentManager();
    private static int ACCOUNT_INTENT = 1;
    String userId;
    SharedPreferences userPrefs;
    ArrayList<Order> orders = new ArrayList<>();
    private DataBaseHelper myDbHelper;
    public static BottomNavigationView navigation;
    String format;
    String language;
    SharedPreferences languagePrefs;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @SuppressLint("ResourceType")
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:

                    Fragment mainFragment = new HomeScreenFragment();
                    fragmentManager.beginTransaction().replace(R.id.fragment_layout, mainFragment).commit();

                    item.setIcon(R.drawable.home_icon);
                    Menu menu1 = navigation.getMenu();

                    if (myDbHelper.getTotalOrderQty() == 0) {

                        MenuItem menuItem = menu1.getItem(1);
                        removeBadge(navigation, menuItem.getItemId());

                    } else {
                        MenuItem menuItem = menu1.getItem(1);
                        showBadge(MainActivity.this, navigation, menuItem.getItemId(), String.valueOf(myDbHelper.getTotalOrderQty()));
                    }
                    return true;
//                case R.id.navigation_search:
//
//                    Fragment searchFragment = new SearchFragment();
//                    fragmentManager.beginTransaction().replace(R.id.fragment_layout, searchFragment).commit();
//                    Menu menu2 = navigation.getMenu();
//
//
//                    if (myDbHelper.getTotalOrderQty() == 0) {
//
//                    } else {
//                        MenuItem menuItem =  menu2.getItem(2);
//                        showBadge(MainActivity.this, navigation, menuItem.getItemId(), String.valueOf(myDbHelper.getTotalOrderQty()));
//                    }
//                    return true;

                case R.id.navigation_cart:

                    item.setIcon(R.drawable.check_out);
                    Fragment checkoutFragment = new CheckOutActivity();
                    fragmentManager.beginTransaction().replace(R.id.fragment_layout, checkoutFragment).commit();

                    Menu menu3 = navigation.getMenu();


                    if (myDbHelper.getTotalOrderQty() == 0) {

                        MenuItem menuItem = menu3.getItem(1);
                        removeBadge(navigation, menuItem.getItemId());

                    } else {
                        MenuItem menuItem = menu3.getItem(1);
                        showBadge(MainActivity.this, navigation, menuItem.getItemId(), String.valueOf(myDbHelper.getTotalOrderQty()));
                    }

                    Log.i("TAG", "onNavigationItemSelected: " + item.getItemId());

                    return true;
                case R.id.navigation_more:

                    item.setIcon(R.drawable.account);
                    userId = userPrefs.getString("userId", "0");
                    if (userId.equals("0")) {
                        Fragment accountFragment = new AccountSignInActivity();
                        fragmentManager.beginTransaction().replace(R.id.fragment_layout, accountFragment).commit();
                    } else {
                        Fragment accountFragment = new AccountFragment();
                        fragmentManager.beginTransaction().replace(R.id.fragment_layout, accountFragment).commit();
                    }
                    Menu menu4 = navigation.getMenu();


                    if (myDbHelper.getTotalOrderQty() == 0) {

                        MenuItem menuItem = menu4.getItem(1);
                        removeBadge(navigation, menuItem.getItemId());

                    } else {
                        MenuItem menuItem = menu4.getItem(1);
                        showBadge(MainActivity.this, navigation, menuItem.getItemId(), String.valueOf(myDbHelper.getTotalOrderQty()));
                    }
                    return true;
            }
            return false;
        }
    };

    @SuppressLint("ResourceType")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.activity_main);
        } else {
            setContentView(R.layout.activity_main_arabic);
        }

        myDbHelper = new DataBaseHelper(MainActivity.this);

        orders = myDbHelper.getOrderInfo();


        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userId = userPrefs.getString("userId", "0");

        navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
//        MenuItem item = findViewById(R.id.navigation_cart);
//        item.getItemId();

//        disableShiftMode(navigation);

        Menu menu = navigation.getMenu();

        if (myDbHelper.getTotalOrderQty() == 0) {

            MenuItem menuItem = menu.getItem(1);
            removeBadge(navigation, menuItem.getItemId());

        } else {
            MenuItem menuItem = menu.getItem(1);
            showBadge(MainActivity.this, navigation, menuItem.getItemId(), String.valueOf(myDbHelper.getTotalOrderQty()));
        }

        if (language.equalsIgnoreCase("En")) {

            if (getIntent().getStringExtra("class").equals("checkout")) {
                MenuItem menuItem = menu.getItem(1);
                navigation.setSelectedItemId(menuItem.getItemId());

                Fragment mainFragment = new CheckOutActivity();
                fragmentManager.beginTransaction().replace(R.id.fragment_layout, mainFragment).commit();

            } else {

                Fragment mainFragment = new HomeScreenFragment();
                fragmentManager.beginTransaction().replace(R.id.fragment_layout, mainFragment).commit();

            }
        } else {

            if (getIntent().getStringExtra("class").equals("checkout")) {
                MenuItem menuItem = menu.getItem(1);
                navigation.setSelectedItemId(menuItem.getItemId());

                Fragment mainFragment = new CheckOutActivity();
                fragmentManager.beginTransaction().replace(R.id.fragment_layout, mainFragment).commit();

            } else {
                MenuItem menuItem = menu.getItem(2);
                navigation.setSelectedItemId(menuItem.getItemId());
                Fragment mainFragment = new HomeScreenFragment();
                fragmentManager.beginTransaction().replace(R.id.fragment_layout, mainFragment).commit();

            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ACCOUNT_INTENT && resultCode == RESULT_OK) {
//            Fragment accountFragment = new AccountFragment();
//            FragmentTransaction ft = fragmentManager.beginTransaction().replace(R.id.fragment_layout, accountFragment);
//            ft.commit();
        } else if (resultCode == RESULT_CANCELED) {

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    // Method for disabling ShiftMode of BottomNavigationView
    private void disableShiftMode(BottomNavigationView view) {
        BottomNavigationMenuView menuView = (BottomNavigationMenuView) view.getChildAt(0);
        try {
            Field shiftingMode = menuView.getClass().getDeclaredField("mShiftingMode");
            shiftingMode.setAccessible(true);
            shiftingMode.setBoolean(menuView, false);
            shiftingMode.setAccessible(false);
            for (int i = 0; i < menuView.getChildCount(); i++) {
                BottomNavigationItemView item = (BottomNavigationItemView) menuView.getChildAt(i);
//                item.setShiftingMode(false);
                item.setPadding(0, 15, 0, 0);
                // set once again checked value, so view will be updated
                item.setChecked(item.getItemData().isChecked());
            }
        } catch (NoSuchFieldException e) {
            Log.e("BNVHelper", "Unable to get shift mode field", e);
        } catch (IllegalAccessException e) {
            Log.e("BNVHelper", "Unable to change value of shift mode", e);
        }
    }

    public static void showBadge(Context context, BottomNavigationView
            bottomNavigationView, @IdRes int itemId, String value) {
        removeBadge(bottomNavigationView, itemId);
        BottomNavigationItemView itemView = bottomNavigationView.findViewById(itemId);
        View badge = LayoutInflater.from(context).inflate(R.layout.count_layout, bottomNavigationView, false);

        TextView text = badge.findViewById(R.id.badge_text_view);
        text.setText(value);
        itemView.addView(badge);
    }

    public static void removeBadge(BottomNavigationView bottomNavigationView, @IdRes int itemId) {
        BottomNavigationItemView itemView = bottomNavigationView.findViewById(itemId);
        if (itemView.getChildCount() == 2) {
            itemView.removeViewAt(1);
        }
    }

    @SuppressLint("ResourceType")
    @Override
    protected void onResume() {
        super.onResume();

        Menu menu = navigation.getMenu();

        if (myDbHelper.getTotalOrderQty() == 0) {

            MenuItem menuItem = menu.getItem(1);
            removeBadge(navigation, menuItem.getItemId());

        } else {
            MenuItem menuItem = menu.getItem(1);
            showBadge(MainActivity.this, navigation, menuItem.getItemId(), String.valueOf(myDbHelper.getTotalOrderQty()));
        }

    }

}
