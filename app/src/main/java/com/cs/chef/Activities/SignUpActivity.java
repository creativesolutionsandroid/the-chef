package com.cs.chef.Activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputFilter;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.chef.Constants;
import com.cs.chef.Dialogs.VerifyOtpDialog;
import com.cs.chef.Models.VerifyMobileResponse;
import com.cs.chef.R;
import com.cs.chef.Rest.APIInterface;
import com.cs.chef.Rest.ApiClient;
import com.cs.chef.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignUpActivity extends AppCompatActivity implements View.OnClickListener {

    TextInputLayout inputLayoutName, inputLayoutEmail, inputLayoutMobile, inputLayoutPassword, inputLayoutConfirmPassword;
    EditText inputName, inputEmail, inputMobile, inputPassword, inputConfirmPassword;
    String strName, strEmail, strMobile, strPassword, strConfirmPassword;
//    CheckBox checkBoxTerms;
    Button buttonSignUp;
    LinearLayout textLogin;
    ImageView back_btn;
    Toolbar toolbar;
    Context context;
    AlertDialog customDialog;
    private String serverOtp;
    AlertDialog loaderDialog = null;

    private static final String TAG = "TAG";
    public static boolean isOTPVerified = false;

    String language;
    SharedPreferences languagePrefs;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.activity_signup);
        } else {
            setContentView(R.layout.activity_signup_arabic);
        }
        context = this;
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

//        toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
//        getSupportActionBar().setDisplayShowTitleEnabled(false);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        inputName = (EditText) findViewById(R.id.singup_input_name);
        inputEmail = (EditText) findViewById(R.id.singup_input_email);
        inputMobile = (EditText) findViewById(R.id.singup_input_mobile);
        inputPassword= (EditText) findViewById(R.id.singup_input_password);
        inputConfirmPassword = (EditText) findViewById(R.id.singup_input_confirm_password);

//        inputName.setText("Sudheer");
//        inputEmail.setText("sudheer@gmail.com");
//        inputMobile.setText("987654326");
//        inputPassword.setText("bg");
        inputMobile.setText(Constants.Country_Code);
        inputMobile.setCursorVisible(false);

        inputLayoutName = (TextInputLayout) findViewById(R.id.input_layout_name);
        inputLayoutEmail = (TextInputLayout) findViewById(R.id.input_layout_email);
        inputLayoutMobile = (TextInputLayout) findViewById(R.id.input_layout_mobile);
        inputLayoutPassword= (TextInputLayout) findViewById(R.id.layout_password);
        inputLayoutConfirmPassword= (TextInputLayout) findViewById(R.id.layout_confirm_password);

        buttonSignUp = (Button) findViewById(R.id.signup_button_create);
        textLogin = (LinearLayout) findViewById(R.id.login_layout);
        back_btn=(ImageView)findViewById(R.id.back_btn);
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

//        imagePasswordEye = (ImageView) findViewById(R.id.singup_image_password);

        setFilters();
        setTypeface();

        buttonSignUp.setOnClickListener(this);
        textLogin.setOnClickListener(this);

        inputName.addTextChangedListener(new TextWatcher(inputName));
        inputMobile.addTextChangedListener(new TextWatcher(inputMobile));
        inputEmail.addTextChangedListener(new TextWatcher(inputEmail));
        inputPassword.addTextChangedListener(new TextWatcher(inputPassword));
        inputConfirmPassword.addTextChangedListener(new TextWatcher(inputConfirmPassword));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.signup_button_create:

                if(validations()){
                    showtwoButtonsAlertDialog();
                }

                break;

            case R.id.login_layout:

                Intent a = new Intent(SignUpActivity.this, SignInActivity.class);
                startActivity(a);

                break;
        }
    }

    private void setTypeface(){
        inputName.setTypeface(Constants.getTypeFace(context));
        inputEmail.setTypeface(Constants.getTypeFace(context));
        inputMobile.setTypeface(Constants.getTypeFace(context));
        inputPassword.setTypeface(Constants.getTypeFace(context));
        inputConfirmPassword.setTypeface(Constants.getTypeFace(context));
        buttonSignUp.setTypeface(Constants.getTypeFace(context));
    }

    private boolean validations(){
        strName = inputName.getText().toString().trim();
        strEmail = inputEmail.getText().toString().trim();
        strMobile = inputMobile.getText().toString();
        strPassword = inputPassword.getText().toString();
        strConfirmPassword = inputConfirmPassword.getText().toString();
        strMobile = strMobile.replace("+966 ","");

        if(strName.length() == 0){
            if (language.equalsIgnoreCase("En")) {
                inputLayoutName.setError(getResources().getString(R.string.signup_msg_invalid_name));
            } else {
                inputLayoutName.setError(getResources().getString(R.string.signup_msg_invalid_name_ar));
            }
            Constants.requestEditTextFocus(inputName, SignUpActivity.this);
            return false;
        }
        else if (strMobile.length() == 0){
            if (language.equalsIgnoreCase("En")) {
                inputLayoutMobile.setError(getResources().getString(R.string.signup_msg_enter_mobile));
            } else {
                inputLayoutMobile.setError(getResources().getString(R.string.signup_msg_enter_mobile_ar));
            }
            Constants.requestEditTextFocus(inputMobile, SignUpActivity.this);
            return false;
        }
        else if (strMobile.length() != 9){
            if (language.equalsIgnoreCase("En")) {
                inputLayoutMobile.setError(getResources().getString(R.string.signup_msg_invalid_mobile));
            } else {
                inputLayoutMobile.setError(getResources().getString(R.string.signup_msg_invalid_mobile_ar));
            }
            Constants.requestEditTextFocus(inputMobile, SignUpActivity.this);
            return false;
        }
        else if (strEmail.length() == 0){
            if (language.equalsIgnoreCase("En")) {
                inputLayoutEmail.setError(getResources().getString(R.string.signup_msg_enter_email));
            } else {
                inputLayoutEmail.setError(getResources().getString(R.string.signup_msg_enter_email_ar));
            }
            Constants.requestEditTextFocus(inputEmail, SignUpActivity.this);
            return false;
        }
        else if (!Constants.isValidEmail(strEmail)){
            if (language.equalsIgnoreCase("En")) {
                inputLayoutEmail.setError(getResources().getString(R.string.signup_msg_invalid_email));
            } else {
                inputLayoutEmail.setError(getResources().getString(R.string.signup_msg_invalid_email_ar));
            }
            Constants.requestEditTextFocus(inputEmail, SignUpActivity.this);
            return false;
        }
        else if (strPassword.length() == 0){
            if (language.equalsIgnoreCase("En")) {
                inputLayoutPassword.setError(getResources().getString(R.string.signup_msg_enter_password));
            } else {
                inputLayoutPassword.setError(getResources().getString(R.string.signup_msg_enter_password_ar));
            }
            Constants.requestEditTextFocus(inputPassword, SignUpActivity.this);
            return false;
        }
        else if (strPassword.length() < 4 || strPassword.length() > 8){
            if (language.equalsIgnoreCase("En")) {
                inputLayoutPassword.setError(getResources().getString(R.string.signup_msg_invalid_password));
            } else {
                inputLayoutPassword.setError(getResources().getString(R.string.signup_msg_invalid_password_ar));
            }
            Constants.requestEditTextFocus(inputPassword, SignUpActivity.this);
            return false;
        }
        else if (strConfirmPassword.length() == 0){
            if (language.equalsIgnoreCase("En")) {
                inputLayoutPassword.setError(getResources().getString(R.string.signup_msg_enter_password));
            } else {
                inputLayoutPassword.setError(getResources().getString(R.string.signup_msg_enter_password_ar));
            }
            Constants.requestEditTextFocus(inputPassword, SignUpActivity.this);
            return false;
        }
        else if (!strConfirmPassword.equals(strPassword)){
            if (language.equalsIgnoreCase("En")) {
                inputLayoutPassword.setError(getResources().getString(R.string.signup_msg_confirm_password));
            } else {
                inputLayoutPassword.setError(getResources().getString(R.string.signup_msg_confirm_password_ar));
            }
            Constants.requestEditTextFocus(inputPassword, SignUpActivity.this);
            return false;
        }
        return true;
    }

    private void clearErrors(){
        inputLayoutName.setErrorEnabled(false);
        inputLayoutMobile.setErrorEnabled(false);
        inputLayoutEmail.setErrorEnabled(false);
        inputLayoutPassword.setErrorEnabled(false);
        inputLayoutConfirmPassword.setErrorEnabled(false);
    }

    private class TextWatcher implements android.text.TextWatcher {
        private View view;

        private TextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.singup_input_name:
                    if(editable.toString().startsWith(" ")){
                        inputName.setText("");
                    }
                    clearErrors();
                    break;
                case R.id.singup_input_mobile:
                    inputMobile.setCursorVisible(true);
                    String enteredMobile = editable.toString();
                    if(!enteredMobile.contains(Constants.Country_Code)){
                        if(enteredMobile.length() > Constants.Country_Code.length()){
                            enteredMobile = enteredMobile.substring((Constants.Country_Code.length() - 1));
                            inputMobile.setText(Constants.Country_Code + enteredMobile);
                        }
                        else {
                            inputMobile.setText(Constants.Country_Code);
                        }
                        inputMobile.setSelection(inputMobile.length());
                    }
                    clearErrors();
                    break;
                case R.id.singup_input_email:
                    clearErrors();
                    break;
                case R.id.singup_input_password:
                    clearErrors();
                    if(editable.length() > 8){
                        if (language.equalsIgnoreCase("En")) {
                            inputLayoutPassword.setError(getResources().getString(R.string.signup_msg_invalid_password));
                        } else {
                            inputLayoutPassword.setError(getResources().getString(R.string.signup_msg_invalid_password_ar));
                        }
                    }
                    break;
                case R.id.singup_input_confirm_password:
                    clearErrors();
                    if(editable.length() > 8){
                        if (language.equalsIgnoreCase("En")) {
                            inputLayoutPassword.setError(getResources().getString(R.string.signup_msg_invalid_password));
                        } else {
                            inputLayoutPassword.setError(getResources().getString(R.string.signup_msg_invalid_password_ar));
                        }
                    }
                    break;
            }
        }
    }

//    public void showloaderAlertDialog(){
//        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(SignUpActivity.this);
//        // ...Irrelevant code for customizing the buttons and title
//        LayoutInflater inflater = getLayoutInflater();
//        int layout = R.layout.loder_dialog;
//        View dialogView = inflater.inflate(layout, null);
//        dialogBuilder.setView(dialogView);
//        dialogBuilder.setCancelable(true);
//
//        loaderDialog = dialogBuilder.create();
//        loaderDialog.show();
//        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
//        Window window = loaderDialog.getWindow();
//        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        lp.copyFrom(window.getAttributes());
//        //This makes the dialog take up the full width
//        Display display = getWindowManager().getDefaultDisplay();
//        Point size = new Point();
//        display.getSize(size);
//        int screenWidth = size.x;
//
//        double d = screenWidth*0.85;
//        lp.width = (int) d;
//        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
//        window.setAttributes(lp);
//    }

    private class verifyMobileApi extends AsyncTask<String, String, String> {

//        ACProgressFlower dialog;
        String inputStr;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareVerifyMobileJson();
//            dialog = new ACProgressFlower.Builder(SignUpActivity.this)
//                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
//                    .themeColor(Color.WHITE)
//                    .fadeColor(Color.DKGRAY).build();
//            dialog.show();
            Constants.showLoadingDialog(SignUpActivity.this);
//            showloaderAlertDialog();
        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(SignUpActivity.this);
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<VerifyMobileResponse> call = apiService.verfiyMobileNumber(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<VerifyMobileResponse>() {
                @Override
                public void onResponse(Call<VerifyMobileResponse> call, Response<VerifyMobileResponse> response) {
                    if(response.isSuccessful()){
                        VerifyMobileResponse verifyMobileResponse = response.body();
                        try {
                            if(verifyMobileResponse.getStatus()){
                                serverOtp = verifyMobileResponse.getData().getOtp();
                                Log.i(TAG, "onResponse: "+serverOtp);
                                showVerifyDialog();
                            }
                            else {
    //                          status false case
                                if (language.equalsIgnoreCase("En")) {
                                    String failureResponse = verifyMobileResponse.getMessage();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name),
                                            getResources().getString(R.string.ok), SignUpActivity.this);
                                } else {
                                    String failureResponse = verifyMobileResponse.getMessageAr();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name_ar),
                                            getResources().getString(R.string.ok_ar), SignUpActivity.this);
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            if (language.equalsIgnoreCase("En")) {
                                Toast.makeText(SignUpActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(SignUpActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                    else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(SignUpActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(SignUpActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }

                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<VerifyMobileResponse> call, Throwable t) {
                    if(networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(SignUpActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(SignUpActivity.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }
                    }
                    else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(SignUpActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(SignUpActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }

                    Constants.closeLoadingDialog();
                }
            });
            return null;
        }
    }

    private void showVerifyDialog(){
        Bundle args = new Bundle();
        args.putString("name", strName);
        args.putString("email", strEmail);
        args.putString("mobile", strMobile);
        args.putString("password", strPassword);
        args.putString("otp", serverOtp);

        final VerifyOtpDialog newFragment = VerifyOtpDialog.newInstance();
        newFragment.setCancelable(false);
        newFragment.setArguments(args);
        newFragment.show(getSupportFragmentManager(), "register");

        getSupportFragmentManager().executePendingTransactions();
        newFragment.getDialog().setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {

                if(newFragment!=null){
                    newFragment.dismiss();

                    if(!isOTPVerified){
                        Constants.requestEditTextFocus(inputMobile, SignUpActivity.this);
                        inputMobile.setSelection(inputMobile.length());
                    }
                    else{
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(SignUpActivity.this, "Registration successful", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(SignUpActivity.this, "Registration successful", Toast.LENGTH_SHORT).show();
                        }
                        setResult(RESULT_OK);
                        finish();
                    }
                }
            }
        });
    }

    private String prepareVerifyMobileJson(){
        JSONObject parentObj = new JSONObject();
        JSONObject mobileObj = new JSONObject();
        try {
            mobileObj.put("Mobile","966"+strMobile);
            mobileObj.put("Email",strEmail);
            parentObj.put("VerifyMobile",mobileObj);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(TAG, " prepareVerifyMobileJson: "+parentObj);
        return parentObj.toString();
    }


    public void showtwoButtonsAlertDialog(){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getLayoutInflater();
        int layout;
        if (language.equalsIgnoreCase("En")) {
            layout = R.layout.alert_dialog;
        } else {
            layout = R.layout.alert_dialog_arabic;
        }
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);

        TextView title = (TextView) dialogView.findViewById(R.id.title);
        TextView desc = (TextView) dialogView.findViewById(R.id.desc);
        TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
        TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
        if (language.equalsIgnoreCase("En")) {
            title.setText(R.string.opt_msg_verify);
            yes.setText(getResources().getString(R.string.ok));
            no.setText(getResources().getString(R.string.edit));
            desc.setText(getResources().getString(R.string.signup_alert_mobile_verify1) + "+966 " + strMobile + getResources().getString(R.string.signup_alert_mobile_verify2));
        } else {
            title.setText(R.string.opt_msg_verify_ar);
            yes.setText(getResources().getString(R.string.ok_ar));
            no.setText(getResources().getString(R.string.edit_ar));
            desc.setText(getResources().getString(R.string.signup_alert_mobile_verify1_ar) + "+966 " + strMobile + getResources().getString(R.string.signup_alert_mobile_verify2_ar));
        }

        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String networkStatus = NetworkUtil.getConnectivityStatusString(SignUpActivity.this);
                if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                    new verifyMobileApi().execute();
                }
                else{
                    if (language.equalsIgnoreCase("En")) {
                        Toast.makeText(getApplicationContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getApplicationContext(), R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                    }
                }
                customDialog.dismiss();
            }
        });

        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Constants.requestEditTextFocus(inputMobile, SignUpActivity.this);
                inputMobile.setSelection(inputMobile.length());
                customDialog.dismiss();
            }
        });

        customDialog = dialogBuilder.create();
        customDialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = customDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x;

        double d = screenWidth*0.85;
        lp.width = (int) d;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

    public void setFilters() {
        InputFilter filter = new InputFilter() {
            @Override
            public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
                boolean keepOriginal = true;
                StringBuilder sb = new StringBuilder(end - start);
                for (int i = start; i < end; i++) {
                    char c = source.charAt(i);
                    if (isCharAllowed(c)) // put your condition here
                        sb.append(c);
                    else
                        keepOriginal = false;
                }
                if (keepOriginal)
                    return null;
                else {
                    if (source instanceof Spanned) {
                        SpannableString sp = new SpannableString(sb);
                        TextUtils.copySpansFrom((Spanned) source, start, sb.length(), null, sp, 0);
                        return sp;
                    } else {
                        return sb;
                    }
                }
            }

            private boolean isCharAllowed(char c) {
                return Character.isLetterOrDigit(c) || Character.isSpaceChar(c);
            }
        };

        inputName.setFilters(new InputFilter[] { filter });
    }
}
