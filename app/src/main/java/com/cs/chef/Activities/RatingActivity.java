package com.cs.chef.Activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.chef.Constants;
import com.cs.chef.Dialogs.TrackListDialog;
import com.cs.chef.Models.Rating;
import com.cs.chef.R;
import com.cs.chef.Rest.APIInterface;
import com.cs.chef.Rest.ApiClient;
import com.cs.chef.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cc.cloudist.acplibrary.ACProgressFlower;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RatingActivity extends AppCompatActivity {

    final Context context = this;

    TextView mbrand_name, feedback, btn_nothanks, btn_submit;

    RatingBar ratingbar;
    EditText suggestions;
    ArrayList<Rating> data = new ArrayList<>();
    String Language = "En";
    String TAG = "TAG";
    String str;
    float givenRating = 1;

    int userid = 0, orderid = 0;
    float rating;
    String brand_name, brand_name_ar, comment;

    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefEditor;
    String userId;

    SharedPreferences languagePrefs;
    SharedPreferences.Editor languagePrefsEditor;
    String language;

    AlertDialog loaderDialog = null;
    ImageView back_btn;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.activity_rating);
        } else {
            setContentView(R.layout.activity_rating_arabic);
        }
        super.onCreate(savedInstanceState);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        orderid = getIntent().getIntExtra("OrderId",0);
        userid = getIntent().getIntExtra("UserId", 0);

        rating = getIntent().getFloatExtra("rating", 0);
        comment = getIntent().getStringExtra("comment");
        brand_name_ar = getIntent().getStringExtra("brand_name_ar");
        brand_name = getIntent().getStringExtra("brand_name");

        back_btn = findViewById(R.id.back_btn);

        mbrand_name = (TextView) findViewById(R.id.brand_name);
        feedback = (TextView) findViewById(R.id.feedback);
        ratingbar = (RatingBar) findViewById(R.id.ratingbar);
        suggestions = (EditText) findViewById(R.id.ed_command);
        btn_nothanks = (TextView) findViewById(R.id.btn_nothanks);
        btn_submit = (TextView) findViewById(R.id.btn_submit);

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();

            }
        });

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefEditor = userPrefs.edit();
        userId = userPrefs.getString("userId", null);

        suggestions.setImeOptions(EditorInfo.IME_ACTION_DONE);
        suggestions.setRawInputType(InputType.TYPE_CLASS_TEXT);
//        Log.d(TAG, "workshopname"+getIntent().getStringExtra("wNw"));

        if (language.equalsIgnoreCase("En")) {
            mbrand_name.setText("Rate your food - " + brand_name);
        } else {
            mbrand_name.setText("" + brand_name_ar + "قيم طعامنا - ");
        }

        if (comment.equals("null")){

        } else {
            suggestions.setText(comment);
        }

        suggestions.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String text = editable.toString();
//                if (language.equalsIgnoreCase("En")){

//                }
//                else {
//                    textcharactes.setText((300 - (text.length()))+"حرف متبقي");
//                }

            }
        });
        ratingbar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {
                if(b){
                    if(v == 0){
                        ratingBar.setRating(1);
                    }
                    setComments(v);
                }
            }
        });


        btn_nothanks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validations()) {
                    String networkStatus = NetworkUtil.getConnectivityStatusString(RatingActivity.this);
                    if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        new RatingApi().execute();
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(RatingActivity.this.getApplicationContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(RatingActivity.this.getApplicationContext(), R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }
        });



    }

    private void setComments(float rating){
        givenRating = rating;
        if(rating == 1){
            if (language.equalsIgnoreCase("En")){
                feedback.setText("What disappointed you?");
            }
            else {
                feedback.setText("ماذا ازعجك؟");
            }

        }
        else if(rating == 2){
            if (language.equalsIgnoreCase("En")){
                feedback.setText("What disappointed you?");
            }
            else {
                feedback.setText("ماذا ازعجك؟");
            }

        }
        else if(rating == 3){
            if (language.equalsIgnoreCase("En")){
                feedback.setText("What can we improve? ");
            }
            else {
                feedback.setText("كيف نطور خدمتنا؟");
            }

        }
        else if(rating == 4){
            if (language.equalsIgnoreCase("En")){
                feedback.setText("What can we improve? ");
            }
            else {
                feedback.setText("كيف نطور خدمتنا؟");
            }

        }
        else if(rating == 5){
            if (language.equalsIgnoreCase("En")){
                feedback.setText("What did you like the best? ");
            }
            else {
                feedback.setText("ما أكثر شي أعجبك؟");
            }

        }

    }


    private boolean validations() {
        str = suggestions.getText().toString();
//        if (str.length() == 0) {
//            if (language.equalsIgnoreCase("En")){
//                suggestions.setError(getResources().getString(R.string.enter_suggestion));
//            }
//            else {
//                suggestions.setError(getResources().getString(R.string.enter_suggestion_ar));
//            }

//            return false;
//        }
        return true;
    }

//    public void showloaderAlertDialog(){
//        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(RatingActivity.this);
//        // ...Irrelevant code for customizing the buttons and title
//        LayoutInflater inflater = getLayoutInflater();
//        int layout = R.layout.loder_dialog;
//        View dialogView = inflater.inflate(layout, null);
//        dialogBuilder.setView(dialogView);
//        dialogBuilder.setCancelable(true);
//
//        loaderDialog = dialogBuilder.create();
//        loaderDialog.show();
//        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
//        Window window = loaderDialog.getWindow();
//        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        lp.copyFrom(window.getAttributes());
//        //This makes the dialog take up the full width
//        Display display = getWindowManager().getDefaultDisplay();
//        Point size = new Point();
//        display.getSize(size);
//        int screenWidth = size.x;
//
//        double d = screenWidth*0.85;
//        lp.width = (int) d;
//        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
//        window.setAttributes(lp);
//    }

    private class RatingApi extends AsyncTask<String, String, String> {

        //        ACProgressFlower dialog;
        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareVerifyMobileJson();
//            dialog = new ACProgressFlower.Builder(RatingActivity.this)
//                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
//                    .themeColor(Color.WHITE)
//                    .fadeColor(Color.DKGRAY).build();
//            dialog.show();
            Constants.showLoadingDialog(RatingActivity.this);
//            showloaderAlertDialog();
        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(RatingActivity.this);
            final APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<Rating> call = apiService.getRating(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<Rating>() {
                @Override
                public void onResponse(Call<Rating> call, Response<Rating> response) {
                    Log.d("TAG", "onResponse: " + response);
                    if (response.isSuccessful()) {
                        Rating orderItems = response.body();
                        try {
                            if (orderItems.getStatus()) {
                                String message = orderItems.getMessage();

                                finish();

                            } else {
                                //                          status false case
                                if (language.equalsIgnoreCase("En")) {
                                    String failureResponse = orderItems.getMessage();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name),
                                            getResources().getString(R.string.ok), RatingActivity.this);
                                } else {
                                    String failureResponse = orderItems.getMessageAr();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name_ar),
                                            getResources().getString(R.string.ok_ar), RatingActivity.this);
                                }
                            }
                            Log.i("TAG", "onResponse: " + data.size());
                        } catch (Exception e) {
                            e.printStackTrace();
                            if (language.equalsIgnoreCase("En")) {
                                Toast.makeText(RatingActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(RatingActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                            }
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(RatingActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(RatingActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }

                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<Rating> call, Throwable t) {
                    Log.d("TAG", "onFailure: " + t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(RatingActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(RatingActivity.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(RatingActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(RatingActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }

                    Constants.closeLoadingDialog();
                }
            });
            return null;
        }

        private String prepareVerifyMobileJson() {
            JSONObject parentObj = new JSONObject();
            try {

                parentObj.put("UserId", userId);
                parentObj.put("OrderId", orderid);
                parentObj.put("CommandType", 3);
                parentObj.put("Rating", givenRating);
                parentObj.put("RatingCommand", suggestions.getText().toString());

            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.d("TAG", "prepareVerifyMobileJson: " + parentObj);
            return parentObj.toString();
        }
    }

//    @Override
//    public void onActivityCreated(Bundle arg0) {
//        super.onActivityCreated(arg0);
////        getDialog().getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
//    }

    private void setTypeface(){
        Typeface typeface = Typeface.createFromAsset(RatingActivity.this.getAssets(),
                "helvetica.ttf");

//        storename.setTypeface(typeface);
//        summarytext.setTypeface(typeface);

    }

}
