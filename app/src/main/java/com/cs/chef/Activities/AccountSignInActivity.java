package com.cs.chef.Activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.chef.R;

import java.util.List;

public class AccountSignInActivity extends Fragment {

    Button login;
    RelativeLayout offers, rateus, contactus, more;
    TextView app_version, web_site;

    SharedPreferences languagePrefs;
    String language;

    View rootView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        languagePrefs = getActivity().getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            rootView = inflater.inflate(R.layout.account_sign_in, container, false);
        } else {
            rootView = inflater.inflate(R.layout.account_sign_in_arabic, container, false);
        }

        login = (Button) rootView.findViewById(R.id.login);

        offers = (RelativeLayout) rootView.findViewById(R.id.offer_layout);
        rateus = (RelativeLayout) rootView.findViewById(R.id.rate_us_layout);
        contactus = (RelativeLayout) rootView.findViewById(R.id.contact_us_layout);
        more = (RelativeLayout) rootView.findViewById(R.id.more_layout);

        app_version = (TextView) rootView.findViewById(R.id.appversion);
        web_site = (TextView) rootView.findViewById(R.id.website);

        try {
            PackageInfo pInfo = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0);
            String version12 = pInfo.versionName;
            app_version.setText("App version v"+version12+"("+pInfo.versionCode+")");
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        web_site.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent fbIntent = new Intent(getActivity(), WebViewActivity.class);
                fbIntent.putExtra("title", "Creative Solutions");
                fbIntent.putExtra("url", "http://www.creative-sols.com/");
                startActivity(fbIntent);
            }
        });

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent a = new Intent(getActivity(), SignInActivity.class);
                a.putExtra("account", true);
                startActivity(a);

            }
        });

        offers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent a = new Intent(getActivity(), OfferActivity.class);
                startActivity(a);

            }
        });

        rateus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.cs.chef")));

            }
        });

        contactus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("plain/text");
                i.putExtra(Intent.EXTRA_EMAIL  , new String[]{"info@arab-space.com"});
                i.putExtra(Intent.EXTRA_SUBJECT, "About The Chef App");
                i.putExtra(Intent.EXTRA_TITLE  , "About The Chef App");
                final PackageManager pm = getActivity().getPackageManager();
                final List<ResolveInfo> matches = pm.queryIntentActivities(i, 0);
                String className = null;
                for (final ResolveInfo info : matches) {
                    if (info.activityInfo.packageName.equals("com.google.android.gm")) {
                        className = info.activityInfo.name;

                        if(className != null && !className.isEmpty()){
                            break;
                        }
                    }
                }
                i.setClassName("com.google.android.gm", className);
                try {
                    getActivity().startActivity(Intent.createChooser(i, "Send mail..."));
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(getActivity(), "There are no email apps installed.", Toast.LENGTH_SHORT).show();
                }

            }
        });

        more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent a = new Intent(getContext(), MoreActivity.class);
                startActivity(a);

            }
        });

        return rootView;
    }

}
