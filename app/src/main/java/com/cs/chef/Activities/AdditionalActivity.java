package com.cs.chef.Activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.cs.chef.Adapter.SubAdditionalAdapter;
import com.cs.chef.Adapter.SubAdditionalAdapter2;
import com.cs.chef.Constants;
import com.cs.chef.DataBaseHelper;
import com.cs.chef.Models.Order;
import com.cs.chef.Models.SubAdditionals;
import com.cs.chef.R;
import com.cs.chef.Rest.APIInterface;
import com.cs.chef.Rest.ApiClient;
import com.cs.chef.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import cc.cloudist.acplibrary.ACProgressFlower;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.cs.chef.Adapter.SubAdditionalAdapter2.mainsuborderPrice;
import static com.cs.chef.Adapter.SubAdditionalAdapter2.suborderPrice;


public class AdditionalActivity extends AppCompatActivity {

    Toolbar toolbar;
    TextView item_name, item_desc, header_title, bag_count;
    public static int max_add_to_be_selected;
    public static TextView sub_additionals_name, mmax_add_to_be_selected;
    ListView additional_items;
    public static ListView sub_additional_items;
    public static RelativeLayout sub_additional;
    ImageView mins, plus;
    Button add_to_card;
    public static TextView qty, price, total_price;
    public static float OrderPrice = 0, MainOrderPrice = 0, dis_item_add_price, item_add_price, AdditionalPrice, SubAdditionalPrice;
    public static int Qty;
    AlertDialog customDialog = null;
    AlertDialog loaderDialog = null;
    SubAdditionalAdapter madapter;
    public static SubAdditionalAdapter2 msubadapter;
    ArrayList<SubAdditionals.PricesAndAdditionals> data = new ArrayList<>();
    ArrayList<SubAdditionals.ItemSizePrice> itemdata = new ArrayList<>();
    public static ArrayList<SubAdditionals.Additems> subitemdata = new ArrayList<>();
    ArrayList<String> subitems = new ArrayList<>();
    int branchid = 0;
    public static int itemid = 0;
    String itemdesc, itemdecs_ar, itemimage;
    DataBaseHelper myDbHelper;
    ArrayList<Order> orderList = new ArrayList<>();
    int i, j, k;
    EditText mcomment;
    public static ArrayList<Integer> sub_add_items = new ArrayList<>();
    public static ArrayList<String> itemQty = new ArrayList<>();
    public static ArrayList<String> itemfinalQty = new ArrayList<>();
    public static ArrayList<String> itemfinalprice = new ArrayList<>();
    public static ArrayList<String> boxpos = new ArrayList<>();
    public static ArrayList<String> max_add_selection = new ArrayList<>();
    ImageView mback_btn, item_image;
    //    TextView mitem_name;
    RelativeLayout mchoose_of_size, bag_layout;

    SharedPreferences languagePrefs;
    String language;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.additional_screen);
        } else if (language.equalsIgnoreCase("Ar")) {
            setContentView(R.layout.additional_screen_arabic);
        }


        myDbHelper = new DataBaseHelper(AdditionalActivity.this);

        branchid = getIntent().getIntExtra("branchid", 0);
        itemid = getIntent().getIntExtra("itemid", 0);
        itemdesc = getIntent().getStringExtra("itemdecs");
        itemdecs_ar = getIntent().getStringExtra("itemdecs_ar");
        itemimage = getIntent().getStringExtra("itemimage");

        myDbHelper = new DataBaseHelper(AdditionalActivity.this);

        orderList = myDbHelper.getOrderInfo();

        header_title = findViewById(R.id.header_title);
        bag_count = findViewById(R.id.bag_count);
        item_name = findViewById(R.id.item_name);
        item_desc = findViewById(R.id.item_desc);
        sub_additionals_name = findViewById(R.id.sub_additionals_name);
        mmax_add_to_be_selected = findViewById(R.id.max_add_to_be_selected);
        additional_items = findViewById(R.id.choose_item_size);
        sub_additional_items = findViewById(R.id.sub_additional_items);
        sub_additional = findViewById(R.id.sub_additionals);
        mins = findViewById(R.id.mins);
        plus = findViewById(R.id.plus);
        qty = findViewById(R.id.qty);
        price = findViewById(R.id.price);
        total_price = findViewById(R.id.total_price);
        add_to_card = findViewById(R.id.add_to_card);
        mcomment = findViewById(R.id.comment);
        mback_btn = findViewById(R.id.back_btn);
        item_image = findViewById(R.id.item_image);
        mchoose_of_size = findViewById(R.id.choose_of_size);
        bag_layout = findViewById(R.id.bag_layout);

        if (language.equalsIgnoreCase("En")) {
            if (MenuActivity.banner) {
                header_title.setText("" + MenuActivity.storeData1.get(MenuActivity.pos).getBrandName_En());
            } else {
                header_title.setText("" + MenuActivity.storeData.get(MenuActivity.pos).getBrandName_En());
            }
        } else {
            if (MenuActivity.banner) {
                header_title.setText("" + MenuActivity.storeData1.get(MenuActivity.pos).getBrandName_Ar());
            } else {
                header_title.setText("" + MenuActivity.storeData.get(MenuActivity.pos).getBrandName_Ar());
            }
        }

        if (myDbHelper.getTotalOrderQty() == 0) {

            bag_count.setVisibility(View.GONE);

        } else {

            bag_count.setVisibility(View.VISIBLE);
            bag_count.setText("" + myDbHelper.getTotalOrderQty());

        }

        bag_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent a = new Intent(AdditionalActivity.this, MainActivity.class);
                a.putExtra("class", "checkout");
                startActivity(a);

            }
        });


        mback_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();

            }
        });

        boxpos.clear();
        sub_add_items.clear();
        itemQty.clear();
        itemfinalQty.clear();
        itemfinalprice.clear();
        max_add_selection.clear();
        OrderPrice = 0;
        item_add_price = 0;
        dis_item_add_price = 0;
        MainOrderPrice = 0;
        Qty = 0;


        String networkStatus = NetworkUtil.getConnectivityStatusString(AdditionalActivity.this);
        if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
            new SubAdditionalApi().execute();
        } else {
            if (language.equalsIgnoreCase("En")) {
                Toast.makeText(AdditionalActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(AdditionalActivity.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
            }
        }

//        add_to_card.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                Intent a = new Intent(AdditionalActivity.this, CheckOutActivity.class);
//                startActivity(a);
//
//            }
//        });

        ListUtils.setDynamicHeight(additional_items);
        ListUtils.setDynamicHeight(sub_additional_items);

        Glide.with(AdditionalActivity.this)
                .load(Constants.ITEM_IMAGE_URL + itemimage)
                .into(item_image);

    }

//    public void showloaderAlertDialog(){
//        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(AdditionalActivity.this);
//        // ...Irrelevant code for customizing the buttons and title
//        LayoutInflater inflater = getLayoutInflater();
//        int layout = R.layout.loder_dialog;
//        View dialogView = inflater.inflate(layout, null);
//        dialogBuilder.setView(dialogView);
//        dialogBuilder.setCancelable(true);
//
//        loaderDialog = dialogBuilder.create();
//        loaderDialog.show();
//        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
//        Window window = loaderDialog.getWindow();
//        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        lp.copyFrom(window.getAttributes());
//        //This makes the dialog take up the full width
//        Display display = getWindowManager().getDefaultDisplay();
//        Point size = new Point();
//        display.getSize(size);
//        int screenWidth = size.x;
//
//        double d = screenWidth*0.85;
//        lp.width = (int) d;
//        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
//        window.setAttributes(lp);
//    }

    private class SubAdditionalApi extends AsyncTask<String, String, String> {

        //        ACProgressFlower dialog;
        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareVerifyMobileJson();
//            dialog = new ACProgressFlower.Builder(AdditionalActivity.this)
//                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
//                    .themeColor(Color.WHITE)
//                    .fadeColor(Color.DKGRAY).build();
//            dialog.show();
            Constants.showLoadingDialog(AdditionalActivity.this);

        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(AdditionalActivity.this);
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<SubAdditionals> call = apiService.getSubAdditionals(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<SubAdditionals>() {
                @Override
                public void onResponse(Call<SubAdditionals> call, Response<SubAdditionals> response) {
                    Log.d("TAG", "onResponse: " + response);
                    if (response.isSuccessful()) {
                        SubAdditionals subAdditionals = response.body();
                        data = subAdditionals.getData().getPricesAndAdditionals();
                        try {
                            if (subAdditionals.getStatus()) {
                                String message = subAdditionals.getMessage();


                                for (i = 0; i < data.size(); i++) {
                                    final String itemname;
                                    if (language.equalsIgnoreCase("En")) {
                                        itemname = data.get(i).getItemName_En();
                                        item_desc.setText("" + itemdesc);
                                    } else {
                                        itemname = data.get(i).getItemName_Ar();
                                        item_desc.setText("" + itemdecs_ar);
                                    }


                                    if (data.get(i).getItemSizePrice().size() == 1) {

                                        mchoose_of_size.setVisibility(View.GONE);

                                    } else {

                                        mchoose_of_size.setVisibility(View.VISIBLE);

                                    }


                                    Log.i("TAG", "itemname: " + itemname);

                                    Glide.with(AdditionalActivity.this)
                                            .load(Constants.ITEM_IMAGE_URL + itemimage)
                                            .into(item_image);


                                    item_name.setText("" + itemname);

                                    itemdata = subAdditionals.getData().getPricesAndAdditionals().get(i).getItemSizePrice();
                                    madapter = new SubAdditionalAdapter(AdditionalActivity.this, itemdata, language);
                                    additional_items.setAdapter(madapter);
                                    ListUtils.setDynamicHeight(additional_items);

                                    for (j = 0; j < data.get(i).getItemSizePrice().size(); j++) {

                                        itemdata = subAdditionals.getData().getPricesAndAdditionals().get(i).getItemSizePrice();
                                        madapter = new SubAdditionalAdapter(AdditionalActivity.this, itemdata, language);
                                        additional_items.setAdapter(madapter);
                                        ListUtils.setDynamicHeight(additional_items);


                                        Log.i("TAG", "v: " + subAdditionals.getData().getPricesAndAdditionals().get(i).getItemSizePrice().size());
                                        plus.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {

                                                if (OrderPrice == 0) {
                                                    if (language.equalsIgnoreCase("En")) {
                                                        Constants.showOneButtonAlertDialog("Please select atleast any size", getResources().getString(R.string.app_name), "Ok", AdditionalActivity.this);
                                                    } else {
                                                        Constants.showOneButtonAlertDialog("Please select atleast any size", getResources().getString(R.string.app_name_ar), getResources().getString(R.string.ok_ar), AdditionalActivity.this);
                                                    }
                                                } else {
                                                    Qty = Qty + 1;

                                                    for (int n = 0; n < data.size(); n++) {
//                                                            for (int o = 0; o < data.get(i).getItemSizePrice().size(); o++) {


                                                        if (data.get(n).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getAddgrp().size() != 0) {

                                                            Log.i("TAG", "add suborder " + suborderPrice);
                                                            Log.i("TAG", "add suborder " + OrderPrice);
                                                            Log.i("TAG", "mainorderprice " + MainOrderPrice);
                                                            Log.i("TAG", "add suborder " + data.get(n).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getItemprice());

                                                            if (suborderPrice == 0) {

                                                                if (MenuActivity.banner) {
                                                                    if (MenuActivity.storeData1.get(MenuActivity.pos).getDiscountAmt() == 0) {

                                                                        OrderPrice = OrderPrice + data.get(n).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getItemprice();
                                                                        MainOrderPrice = MainOrderPrice + data.get(n).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getItemprice();

                                                                    } else {

                                                                        float discount_value = data.get(n).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getItemprice() * MenuActivity.storeData1.get(MenuActivity.pos).getDiscountAmt() / 100;
                                                                        float discount_amount = data.get(n).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getItemprice() - discount_value;

                                                                        OrderPrice = OrderPrice + discount_amount;
                                                                        MainOrderPrice = MainOrderPrice + data.get(n).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getItemprice();

                                                                    }
                                                                } else {
                                                                    if (MenuActivity.storeData.get(MenuActivity.pos).getDiscountAmt() == 0) {

                                                                        OrderPrice = OrderPrice + data.get(n).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getItemprice();
                                                                        MainOrderPrice = MainOrderPrice + data.get(n).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getItemprice();

                                                                    } else {

                                                                        float discount_value = data.get(n).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getItemprice() * MenuActivity.storeData.get(MenuActivity.pos).getDiscountAmt() / 100;
                                                                        float discount_amount = data.get(n).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getItemprice() - discount_value;

                                                                        OrderPrice = OrderPrice + discount_amount;
                                                                        MainOrderPrice = MainOrderPrice + data.get(n).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getItemprice();
                                                                    }
                                                                }

                                                            } else {

                                                                OrderPrice = suborderPrice * Qty;

                                                                MainOrderPrice = MainOrderPrice + mainsuborderPrice;

                                                                Log.i("TAG", "suborderpirce: " + suborderPrice);
                                                                Log.i("TAG", "orderrpirce: " + OrderPrice);

                                                            }

                                                        } else {

                                                            if (MenuActivity.banner) {
                                                                if (MenuActivity.storeData1.get(MenuActivity.pos).getDiscountAmt() == 0) {

                                                                    OrderPrice = OrderPrice + data.get(n).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getItemprice();
                                                                    MainOrderPrice = MainOrderPrice + data.get(n).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getItemprice();

                                                                } else {

                                                                    float discount_value = data.get(n).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getItemprice() * MenuActivity.storeData1.get(MenuActivity.pos).getDiscountAmt() / 100;
                                                                    float discount_amount = data.get(n).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getItemprice() - discount_value;

                                                                    OrderPrice = OrderPrice + discount_amount;
                                                                    MainOrderPrice = MainOrderPrice + data.get(n).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getItemprice();

                                                                }
                                                            } else {
                                                                if (MenuActivity.storeData.get(MenuActivity.pos).getDiscountAmt() == 0) {

                                                                    OrderPrice = OrderPrice + data.get(n).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getItemprice();
                                                                    MainOrderPrice = MainOrderPrice + data.get(n).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getItemprice();

                                                                } else {

                                                                    float discount_value = data.get(n).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getItemprice() * MenuActivity.storeData.get(MenuActivity.pos).getDiscountAmt() / 100;
                                                                    float discount_amount = data.get(n).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getItemprice() - discount_value;

                                                                    OrderPrice = OrderPrice + discount_amount;
                                                                    MainOrderPrice = MainOrderPrice + data.get(n).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getItemprice();
                                                                }
                                                            }
                                                        }
//                                                            }
                                                    }
                                                }

                                                try {
//                        DecimalFormat decimalFormat = new DecimalFormat("0.00");

                                                    qty.setText("" + Qty);
//                                                    if (language.equalsIgnoreCase("En")) {
                                                        AdditionalActivity.total_price.setText("" + Constants.decimalFormat.format(OrderPrice));
//                                                    } else {
//                                                        AdditionalActivity.total_price.setText("" + Constants.decimalFormat.format(OrderPrice) + " " + getResources().getString(R.string.price_format_ar));
//                                                    }
//                                    AdditionalActivity.qty.setText("" + Qty);


                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                }

                                            }
                                        });

                                        mins.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {

                                                if (Integer.parseInt(qty.getText().toString()) > 1) {
                                                    Qty = Qty - 1;

                                                    for (int n = 0; n < data.size(); n++) {
//                                                            for (int o = 0; o < data.get(i).getItemSizePrice().size(); o++) {


                                                        if (data.get(n).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getAddgrp().size() != 0) {

                                                            Log.i("TAG", "add suborder " + suborderPrice);
                                                            Log.i("TAG", "add suborder " + OrderPrice);
                                                            Log.i("TAG", "add suborder " + data.get(n).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getItemprice());

                                                            if (suborderPrice == 0) {

                                                                if (MenuActivity.banner) {
                                                                    if (MenuActivity.storeData1.get(MenuActivity.pos).getDiscountAmt() == 0) {

                                                                        OrderPrice = OrderPrice - data.get(n).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getItemprice();
                                                                        MainOrderPrice = MainOrderPrice - data.get(n).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getItemprice();

                                                                    } else {

                                                                        float discount_value = data.get(n).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getItemprice() * MenuActivity.storeData1.get(MenuActivity.pos).getDiscountAmt() / 100;
                                                                        float discount_amount = data.get(n).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getItemprice() - discount_value;

                                                                        OrderPrice = OrderPrice - discount_amount;

                                                                        MainOrderPrice = MainOrderPrice - data.get(n).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getItemprice();
                                                                    }
                                                                } else {
                                                                    if (MenuActivity.storeData.get(MenuActivity.pos).getDiscountAmt() == 0) {

                                                                        OrderPrice = OrderPrice - data.get(n).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getItemprice();
                                                                        MainOrderPrice = MainOrderPrice - data.get(n).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getItemprice();

                                                                    } else {

                                                                        float discount_value = data.get(n).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getItemprice() * MenuActivity.storeData.get(MenuActivity.pos).getDiscountAmt() / 100;
                                                                        float discount_amount = data.get(n).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getItemprice() - discount_value;

                                                                        OrderPrice = OrderPrice - discount_amount;
                                                                        MainOrderPrice = MainOrderPrice - data.get(n).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getItemprice();
                                                                    }
                                                                }


                                                            } else {

                                                                OrderPrice = suborderPrice * Qty;
                                                                MainOrderPrice = MainOrderPrice - mainsuborderPrice;

                                                            }

                                                            Log.i("TAG", "onClick: " + mcomment.getText().toString());

                                                        } else {

                                                            if (MenuActivity.banner) {
                                                                if (MenuActivity.storeData1.get(MenuActivity.pos).getDiscountAmt() == 0) {

                                                                    OrderPrice = OrderPrice - data.get(n).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getItemprice();
                                                                    MainOrderPrice = MainOrderPrice - data.get(n).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getItemprice();

                                                                } else {

                                                                    float discount_value = data.get(n).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getItemprice() * MenuActivity.storeData1.get(MenuActivity.pos).getDiscountAmt() / 100;
                                                                    float discount_amount = data.get(n).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getItemprice() - discount_value;

                                                                    OrderPrice = OrderPrice - discount_amount;
                                                                    MainOrderPrice = MainOrderPrice - data.get(n).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getItemprice();

                                                                }
                                                            } else {
                                                                if (MenuActivity.storeData.get(MenuActivity.pos).getDiscountAmt() == 0) {

                                                                    OrderPrice = OrderPrice - data.get(n).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getItemprice();
                                                                    MainOrderPrice = MainOrderPrice - data.get(n).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getItemprice();

                                                                } else {

                                                                    float discount_value = data.get(n).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getItemprice() * MenuActivity.storeData.get(MenuActivity.pos).getDiscountAmt() / 100;
                                                                    float discount_amount = data.get(n).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getItemprice() - discount_value;

                                                                    OrderPrice = OrderPrice - discount_amount;
                                                                    MainOrderPrice = MainOrderPrice - data.get(n).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getItemprice();
                                                                }
                                                            }

                                                        }
//                                                            }
                                                    }
                                                }

                                                try {
//                        DecimalFormat decimalFormat = new DecimalFormat("0.00");

                                                    qty.setText("" + Qty);
//                                                    if (language.equalsIgnoreCase("En")) {
                                                        AdditionalActivity.total_price.setText("" + Constants.decimalFormat.format(OrderPrice));
//                                                    } else {
//                                                        AdditionalActivity.total_price.setText("" + Constants.decimalFormat.format(OrderPrice) + " " + getResources().getString(R.string.price_format_ar));
//                                                    }
//                                    AdditionalActivity.qty.setText("" + Qty);


                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                }

                                            }
                                        });

                                        Log.i("TAG", "123: " + data.get(i).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getAddgrp().size());
                                        Log.i("TAG", "onResponse: " + OrderPrice);

                                        add_to_card.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {

                                                if (OrderPrice == 0) {
                                                    if (language.equalsIgnoreCase("En")) {
                                                        Constants.showOneButtonAlertDialog("Please select atleast any size", getResources().getString(R.string.app_name), "Ok", AdditionalActivity.this);
                                                    } else {
                                                        Constants.showOneButtonAlertDialog("Please select atleast any size", getResources().getString(R.string.app_name_ar), getResources().getString(R.string.ok_ar), AdditionalActivity.this);
                                                    }
                                                } else {

                                                    if (orderList.size() == 0) {

                                                        for (int o = 0; o < data.size(); o++) {
                                                            if (data.get(o).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getAddgrp().size() == 0) {

                                                                HashMap<String, String> values = new HashMap<>();
                                                                values.put("itemId", String.valueOf(data.get(o).getItemId()));
                                                                values.put("itemTypeId", String.valueOf(data.get(o).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getPriceId()));
                                                                if (MenuActivity.banner) {
                                                                    values.put("subCategoryId", String.valueOf(MenuActivity.storeData1.get(MenuActivity.pos).getBranchId()));
                                                                } else {
                                                                    values.put("subCategoryId", String.valueOf(MenuActivity.storeData.get(MenuActivity.pos).getBranchId()));
                                                                }
                                                                values.put("additionals_name_En", "");
                                                                values.put("additionals_name_Ar", "");
                                                                values.put("qty", String.valueOf(Qty));
                                                                values.put("price", String.valueOf(data.get(o).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getItemprice()));
                                                                Log.i("TAG", "price1: " + data.get(o).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getItemprice());
                                                                values.put("additionalsPrice", "");
                                                                values.put("additionalsTypeId", "");
                                                                values.put("totalAmount", total_price.getText().toString());
                                                                if (mcomment.getText().toString().equalsIgnoreCase(""))
                                                                    values.put("comment", "");
                                                                else
                                                                    values.put("comment", mcomment.getText().toString());
                                                                values.put("status", "1");
                                                                values.put("creationDate", "19/02/2019");
                                                                values.put("modifiedDate", "19/02/2019");
                                                                if (MenuActivity.banner) {
                                                                    values.put("categoryId", String.valueOf(MenuActivity.storeData1.get(MenuActivity.pos).getMobileNo()));
                                                                } else {
                                                                    values.put("categoryId", String.valueOf(MenuActivity.storeData.get(MenuActivity.pos).getMobileNo()));
                                                                }
                                                                values.put("itemName", data.get(o).getItemName_En());
                                                                values.put("itemNameAr", data.get(o).getItemName_Ar());
                                                                if (MenuActivity.banner) {
                                                                    values.put("image", String.valueOf(MenuActivity.storeData1.get(MenuActivity.pos).getStoreLogo_En()));
                                                                } else {
                                                                    values.put("image", String.valueOf(MenuActivity.storeData.get(MenuActivity.pos).getStoreLogo_En()));
                                                                }
                                                                values.put("item_desc", "");
                                                                values.put("item_desc_Ar", "");
                                                                values.put("sub_itemName", "");
                                                                values.put("sub_itemName_Ar", "");
                                                                values.put("sub_itemImage", "");
                                                                values.put("ItemType", String.valueOf(data.get(o).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getPriceId()));
                                                                values.put("sizename_en", data.get(o).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getItemSizeName_En());
                                                                values.put("sizename_ar", data.get(o).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getItemSizeName_Ar());
                                                                if (MenuActivity.banner) {
                                                                    values.put("discount", String.valueOf(MenuActivity.storeData1.get(MenuActivity.pos).getDiscountAmt()));
                                                                } else {
                                                                    values.put("discount", String.valueOf(MenuActivity.storeData.get(MenuActivity.pos).getDiscountAmt()));
                                                                }
                                                                values.put("additionalitemQTY", "");
                                                                if (MenuActivity.banner) {
                                                                    values.put("branchName", String.valueOf(MenuActivity.storeData1.get(MenuActivity.pos).getBranchName_En()));
                                                                    Log.i("TAG", "branchname: " + String.valueOf(MenuActivity.storeData1.get(MenuActivity.pos).getBranchName_En()));
                                                                    values.put("address", String.valueOf(MenuActivity.storeData1.get(MenuActivity.pos).getAddress()));
                                                                    values.put("brandName", String.valueOf(MenuActivity.storeData1.get(MenuActivity.pos).getBrandName_En()));
                                                                    values.put("brandName_Ar", String.valueOf(MenuActivity.storeData1.get(MenuActivity.pos).getBrandName_Ar()));
                                                                    values.put("orderType", String.valueOf(MenuActivity.storeData1.get(MenuActivity.pos).getOrderTypes()));
                                                                    values.put("BrandId", String.valueOf(MenuActivity.storeData1.get(MenuActivity.pos).getBrandId()));
                                                                    values.put("latitute", String.valueOf(MenuActivity.storeData1.get(MenuActivity.pos).getLatitude()));
                                                                    values.put("longitute", String.valueOf(MenuActivity.storeData1.get(MenuActivity.pos).getLongitude()));
                                                                    values.put("branchName_Ar", String.valueOf(MenuActivity.storeData1.get(MenuActivity.pos).getBranchName_Ar()));
                                                                } else {
                                                                    values.put("branchName", String.valueOf(MenuActivity.storeData.get(MenuActivity.pos).getBranchName_En()));
                                                                    values.put("address", String.valueOf(MenuActivity.storeData.get(MenuActivity.pos).getAddress()));
                                                                    values.put("brandName", String.valueOf(MenuActivity.storeData.get(MenuActivity.pos).getBrandName_En()));
                                                                    values.put("brandName_Ar", String.valueOf(MenuActivity.storeData.get(MenuActivity.pos).getBrandName_Ar()));
                                                                    values.put("orderType", String.valueOf(MenuActivity.storeData.get(MenuActivity.pos).getOrderTypes()));
                                                                    values.put("BrandId", String.valueOf(MenuActivity.storeData.get(MenuActivity.pos).getBrandId()));
                                                                    values.put("latitute", String.valueOf(MenuActivity.storeData.get(MenuActivity.pos).getLatitude()));
                                                                    values.put("longitute", String.valueOf(MenuActivity.storeData.get(MenuActivity.pos).getLongitude()));
                                                                    values.put("branchName_Ar", String.valueOf(MenuActivity.storeData.get(MenuActivity.pos).getBranchName_Ar()));
                                                                }
                                                                values.put("item_image", itemimage);
                                                                values.put("mainTotalAmount", String.valueOf(MainOrderPrice));
                                                                values.put("dis_item_add_price", String.valueOf(dis_item_add_price));
                                                                values.put("main_item_add_price", String.valueOf(item_add_price));
                                                                if (MenuActivity.banner) {
                                                                    values.put("min_amount", String.valueOf(MenuActivity.storeData1.get(MenuActivity.pos).getMinOrderCharges()));
                                                                    values.put("max_days", String.valueOf(MenuActivity.storeData1.get(MenuActivity.pos).getFutureOrderDay()));
                                                                    values.put("delivery_charges", String.valueOf(MenuActivity.storeData1.get(MenuActivity.pos).getDeliveryCharges()));
                                                                } else {
                                                                    values.put("min_amount", String.valueOf(MenuActivity.storeData.get(MenuActivity.pos).getMinOrderCharges()));
                                                                    values.put("max_days", String.valueOf(MenuActivity.storeData.get(MenuActivity.pos).getFutureOrderDay()));
                                                                    values.put("delivery_charges", String.valueOf(MenuActivity.storeData.get(MenuActivity.pos).getDeliveryCharges()));
                                                                }
                                                                values.put("price_with_disc", price.getText().toString());


                                                                Log.i("TAG", "onClick: ");

                                                                myDbHelper.insertOrder(values);
                                                            } else {
                                                                Log.i("TAG", "onClick1: ");

                                                                for (int p = 0; p < data.get(o).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getAddgrp().size(); p++) {

                                                                    String addl_ids = null, addl_name = null, addl_name_ar = null, addl_image = null, addl_price = null, addl_qty = null;
//                                                                    if (boxpos != null && boxpos.size() > 0) {
                                                                    for (int n = 0; n < data.get(o).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getAddgrp().get(p).getAdditems().size(); n++) {
                                                                        if (data.get(o).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getAddgrp().get(p).getAdditems().get(n).getAdd_qty() > 0) {

                                                                            if (addl_ids != null && addl_ids.length() > 0) {

                                                                                addl_ids = addl_ids + "," + String.valueOf(data.get(o).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getAddgrp().get(p).getAdditems().get(n).getAdditionalId());
                                                                                addl_name = addl_name + "," + data.get(o).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getAddgrp().get(p).getAdditems().get(n).getAddtionalName_en();
                                                                                addl_name_ar = addl_name_ar + "," + data.get(o).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getAddgrp().get(p).getAdditems().get(n).getAddtionalName_Ar();
//                                                                                addl_image = addl_image + "," + sections.get(position).getChildItems().get(value1).getSubItems().get(n).getSubItemimage();
                                                                                for (int m = 0; m < data.get(o).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getAddgrp().get(p).getAdditems().get(n).getAddprice().size(); m++) {
                                                                                    addl_price = addl_price + "," + String.valueOf(data.get(o).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getAddgrp().get(p).getAdditems().get(n).getAddprice().get(m).getAddprice());
                                                                                }
//                                                                                int count = 0;
//                                                                                for (int j = 0; j < itemQty.size(); j++) {
//                                                                                    if (itemQty.get(j).equals(data.get(o).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getAddgrp().get(p).getAdditems().get(n).getAddtionalName_en())) {
//                                                                                        count = count + 1;
//                                                                                    }
//                                                                                }
                                                                                addl_qty = addl_qty + "," + data.get(o).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getAddgrp().get(p).getAdditems().get(n).getAdd_qty();
                                                                                Log.i("TAG", "addl_qty: " + addl_qty);

                                                                            } else {
                                                                                addl_ids = String.valueOf(data.get(o).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getAddgrp().get(p).getAdditems().get(n).getAdditionalId());
                                                                                addl_name = data.get(o).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getAddgrp().get(p).getAdditems().get(n).getAddtionalName_en();
                                                                                addl_name_ar = data.get(o).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getAddgrp().get(p).getAdditems().get(n).getAddtionalName_Ar();
//                                                                                addl_image = sections.get(position).getChildItems().get(value1).getSubItems().get(Integer.parseInt(boxpos.get(o))).getSubItemimage();
                                                                                for (int m = 0; m < data.get(o).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getAddgrp().get(p).getAdditems().get(n).getAddprice().size(); m++) {
                                                                                    addl_price = String.valueOf(data.get(o).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getAddgrp().get(p).getAdditems().get(n).getAddprice().get(m).getAddprice());
                                                                                }
//                                                                                int count = 0;
//                                                                                for (int j = 0; j < itemQty.size(); j++) {
//                                                                                    if (itemQty.get(j).equals(data.get(o).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getAddgrp().get(p).getAdditems().get(n).getAddtionalName_en())) {
//                                                                                        count = count + 1;
//                                                                                    }
//                                                                                }
                                                                                addl_qty = String.valueOf(data.get(o).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getAddgrp().get(p).getAdditems().get(n).getAdd_qty());
                                                                                Log.i("TAG", "addl_qty: " + addl_qty);
                                                                            }
                                                                        }
                                                                    }
//                                                                    }

                                                                    HashMap<String, String> values1 = new HashMap<>();
                                                                    values1.put("itemId", String.valueOf(data.get(o).getItemId()));
                                                                    values1.put("itemTypeId", String.valueOf(data.get(o).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getPriceId()));
                                                                    if (MenuActivity.banner) {
                                                                        values1.put("subCategoryId", String.valueOf(MenuActivity.storeData1.get(MenuActivity.pos).getBranchId()));
                                                                    } else {
                                                                        values1.put("subCategoryId", String.valueOf(MenuActivity.storeData.get(MenuActivity.pos).getBranchId()));
                                                                    }
                                                                    values1.put("additionals_name_En", addl_name);
                                                                    values1.put("additionals_name_Ar", addl_name_ar);
                                                                    values1.put("qty", String.valueOf(Qty));
                                                                    values1.put("price", String.valueOf(data.get(o).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getItemprice()));
                                                                    Log.i("TAG", "price2: " + data.get(o).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getItemprice());
                                                                    values1.put("additionalsPrice", addl_price);
                                                                    values1.put("additionalsTypeId", addl_ids);
                                                                    values1.put("totalAmount", total_price.getText().toString());
                                                                    if (mcomment.getText().toString().equalsIgnoreCase(""))
                                                                        values1.put("comment", "");
                                                                    else
                                                                        values1.put("comment", mcomment.getText().toString());
                                                                    values1.put("status", "1");
                                                                    values1.put("creationDate", "19/02/2019");
                                                                    values1.put("modifiedDate", "19/02/2019");
                                                                    if (MenuActivity.banner) {
                                                                        values1.put("categoryId", String.valueOf(MenuActivity.storeData1.get(MenuActivity.pos).getMobileNo()));
                                                                    } else {
                                                                        values1.put("categoryId", String.valueOf(MenuActivity.storeData.get(MenuActivity.pos).getMobileNo()));
                                                                    }
                                                                    values1.put("itemName", data.get(o).getItemName_En());
                                                                    values1.put("itemNameAr", data.get(o).getItemName_Ar());
                                                                    if (MenuActivity.banner) {
                                                                        values1.put("image", String.valueOf(MenuActivity.storeData1.get(MenuActivity.pos).getStoreLogo_En()));
                                                                    } else {
                                                                        values1.put("image", String.valueOf(MenuActivity.storeData.get(MenuActivity.pos).getStoreLogo_En()));
                                                                    }
                                                                    values1.put("item_desc", "");
                                                                    values1.put("item_desc_Ar", "");
                                                                    values1.put("sub_itemName", data.get(o).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getAddgrp().get(p).getAddGrpName_En());
                                                                    values1.put("sub_itemName_Ar", data.get(o).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getAddgrp().get(p).getAddGrpName_Ar());
                                                                    values1.put("sub_itemImage", "");
                                                                    values1.put("ItemType", String.valueOf(data.get(o).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getPriceId()));
                                                                    values1.put("sizename_en", data.get(o).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getItemSizeName_En());
                                                                    values1.put("sizename_ar", data.get(o).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getItemSizeName_Ar());
                                                                    if (MenuActivity.banner) {
                                                                        values1.put("discount", String.valueOf(MenuActivity.storeData1.get(MenuActivity.pos).getDiscountAmt()));
                                                                    } else {
                                                                        values1.put("discount", String.valueOf(MenuActivity.storeData.get(MenuActivity.pos).getDiscountAmt()));
                                                                    }
                                                                    values1.put("additionalitemQTY", addl_qty);
                                                                    if (MenuActivity.banner) {
                                                                        values1.put("branchName", String.valueOf(MenuActivity.storeData1.get(MenuActivity.pos).getBranchName_En()));
                                                                        Log.i("TAG", "branchname: " + String.valueOf(MenuActivity.storeData1.get(MenuActivity.pos).getBranchName_En()));
                                                                        values1.put("address", String.valueOf(MenuActivity.storeData1.get(MenuActivity.pos).getAddress()));
                                                                        values1.put("brandName", String.valueOf(MenuActivity.storeData1.get(MenuActivity.pos).getBrandName_En()));
                                                                        values1.put("brandName_Ar", String.valueOf(MenuActivity.storeData1.get(MenuActivity.pos).getBrandName_Ar()));
                                                                        values1.put("orderType", String.valueOf(MenuActivity.storeData1.get(MenuActivity.pos).getOrderTypes()));
                                                                        values1.put("BrandId", String.valueOf(MenuActivity.storeData1.get(MenuActivity.pos).getBrandId()));
                                                                        values1.put("latitute", String.valueOf(MenuActivity.storeData1.get(MenuActivity.pos).getLatitude()));
                                                                        values1.put("longitute", String.valueOf(MenuActivity.storeData1.get(MenuActivity.pos).getLongitude()));
                                                                        values1.put("branchName_Ar", String.valueOf(MenuActivity.storeData1.get(MenuActivity.pos).getBranchName_Ar()));
                                                                    } else {
                                                                        values1.put("branchName", String.valueOf(MenuActivity.storeData.get(MenuActivity.pos).getBranchName_En()));
                                                                        values1.put("address", String.valueOf(MenuActivity.storeData.get(MenuActivity.pos).getAddress()));
                                                                        values1.put("brandName", String.valueOf(MenuActivity.storeData.get(MenuActivity.pos).getBrandName_En()));
                                                                        values1.put("brandName_Ar", String.valueOf(MenuActivity.storeData.get(MenuActivity.pos).getBrandName_Ar()));
                                                                        values1.put("orderType", String.valueOf(MenuActivity.storeData.get(MenuActivity.pos).getOrderTypes()));
                                                                        values1.put("BrandId", String.valueOf(MenuActivity.storeData.get(MenuActivity.pos).getBrandId()));
                                                                        values1.put("latitute", String.valueOf(MenuActivity.storeData.get(MenuActivity.pos).getLatitude()));
                                                                        values1.put("longitute", String.valueOf(MenuActivity.storeData.get(MenuActivity.pos).getLongitude()));
                                                                        values1.put("branchName_Ar", String.valueOf(MenuActivity.storeData.get(MenuActivity.pos).getBranchName_Ar()));
                                                                    }
                                                                    values1.put("item_image", itemimage);
                                                                    values1.put("mainTotalAmount", String.valueOf(MainOrderPrice));
                                                                    values1.put("dis_item_add_price", String.valueOf(dis_item_add_price));
                                                                    values1.put("main_item_add_price", String.valueOf(item_add_price));
                                                                    if (MenuActivity.banner) {
                                                                        values1.put("min_amount", String.valueOf(MenuActivity.storeData1.get(MenuActivity.pos).getMinOrderCharges()));
                                                                        values1.put("max_days", String.valueOf(MenuActivity.storeData1.get(MenuActivity.pos).getFutureOrderDay()));
                                                                        values1.put("delivery_charges", String.valueOf(MenuActivity.storeData1.get(MenuActivity.pos).getDeliveryCharges()));
                                                                    } else {
                                                                        values1.put("min_amount", String.valueOf(MenuActivity.storeData.get(MenuActivity.pos).getMinOrderCharges()));
                                                                        values1.put("max_days", String.valueOf(MenuActivity.storeData.get(MenuActivity.pos).getFutureOrderDay()));
                                                                        values1.put("delivery_charges", String.valueOf(MenuActivity.storeData.get(MenuActivity.pos).getDeliveryCharges()));
                                                                    }
                                                                    values1.put("price_with_disc", price.getText().toString());

                                                                    myDbHelper.insertOrder(values1);

                                                                }
                                                            }
                                                        }

                                                        onBackPressed();
                                                        finish();
//                                                        Intent a = new Intent(AdditionalActivity.this, MenuActivity.class);
//                                                        if (MenuActivity.banner) {
//                                                            a.putExtra("array", MenuActivity.storeData);
//                                                            a.putExtra("pos", MenuActivity.pos);
//                                                            a.putExtra("banner", MenuActivity.banner_store);
//                                                        } else {
//                                                            a.putExtra("array", MenuActivity.storeData);
//                                                            a.putExtra("pos", MenuActivity.pos);
//                                                            a.putExtra("banner", MenuActivity.banner_store);
//                                                        }
//                                                        startActivity(a);
//                                                        AlertDialog customDialog = null;
//                                                        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(AdditionalActivity.this);
//                                                        // ...Irrelevant code for customizing the buttons and title
//                                                        LayoutInflater inflater = getLayoutInflater();
//                                                        int layout;
//                                                        if (language.equalsIgnoreCase("En")) {
//                                                            layout = R.layout.alert_dialog;
//                                                        } else {
//                                                            layout = R.layout.alert_dialog_arabic;
//                                                        }
//                                                        View dialogView = inflater.inflate(layout, null);
//                                                        dialogBuilder.setView(dialogView);
//                                                        dialogBuilder.setCancelable(false);
//
//                                                        TextView title = (TextView) dialogView.findViewById(R.id.title);
//                                                        TextView desc = (TextView) dialogView.findViewById(R.id.desc);
//                                                        TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
//                                                        TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
//                                                        View vert = (View) dialogView.findViewById(R.id.vert_line);
//
//                                                        no.setVisibility(View.GONE);
//                                                        vert.setVisibility(View.GONE);
//
//                                                        if (language.equalsIgnoreCase("En")) {
//                                                            title.setText(R.string.app_name);
//                                                            yes.setText(R.string.ok);
//                                                            desc.setText("Successfully Added to cart");
//                                                        } else {
//                                                            title.setText(R.string.app_name_ar);
//                                                            yes.setText(R.string.ok_ar);
//                                                            desc.setText("تمت إضافته للسله بنجاح");
//                                                        }
//
//                                                        customDialog = dialogBuilder.create();
//                                                        customDialog.show();
//
//                                                        final AlertDialog finalCustomDialog = customDialog;
//                                                        yes.setOnClickListener(new View.OnClickListener() {
//                                                            @Override
//                                                            public void onClick(View view) {
//                                                                onBackPressed();
//                                                                finish();
//                                                                finalCustomDialog.dismiss();
//                                                            }
//                                                        });
//
//                                                        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
//                                                        Window window = customDialog.getWindow();
//                                                        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//                                                        lp.copyFrom(window.getAttributes());
//                                                        //This makes the dialog take up the full width
//                                                        Display display = getWindowManager().getDefaultDisplay();
//                                                        Point size = new Point();
//                                                        display.getSize(size);
//                                                        int screenWidth = size.x;
//
//                                                        double d = screenWidth * 0.85;
//                                                        lp.width = (int) d;
//                                                        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
//                                                        window.setAttributes(lp);

                                                    } else {

                                                        String branchid;

                                                        if (MenuActivity.banner) {
                                                            branchid = String.valueOf(MenuActivity.storeData1.get(MenuActivity.pos).getBranchId());
                                                        } else {
                                                            branchid = String.valueOf(MenuActivity.storeData.get(MenuActivity.pos).getBranchId());
                                                        }


                                                        String DBbranchid = null, DBbranchname = null;
                                                        for (Order order : orderList) {

                                                            DBbranchid = order.getSubCatId();
                                                            DBbranchname = order.getBranchName();
                                                            Log.i("TAG", "Bname1: " + order.getBranchName());

                                                        }

                                                        Log.i("TAG", "DBbranchid: " + DBbranchid);
                                                        Log.i("TAG", "Bname: " + DBbranchname);
                                                        if (!DBbranchid.equals(branchid)) {


                                                            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(AdditionalActivity.this);
                                                            // ...Irrelevant code for customizing the buttons and title
                                                            LayoutInflater inflater = getLayoutInflater();
                                                            int layout;
                                                            if (language.equalsIgnoreCase("En")) {
                                                                layout = R.layout.alert_dialog;
                                                            } else {
                                                                layout = R.layout.alert_dialog_arabic;
                                                            }
                                                            View dialogView = inflater.inflate(layout, null);
                                                            dialogBuilder.setView(dialogView);
                                                            dialogBuilder.setCancelable(false);

                                                            TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                                                            TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                                                            TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                                                            View vert = (View) dialogView.findViewById(R.id.vert_line);

                                                            if (language.equalsIgnoreCase("En")) {
                                                                yes.setText("Yes");
                                                                no.setText("No");
                                                                desc.setText("You already have items for " + DBbranchname + " in your cart. Do you want to replace");
                                                            } else {
                                                                yes.setText("نعم");
                                                                no.setText("لا");
                                                                desc.setText("You already have items for " + DBbranchname + " in your cart. Do you want to replace");
                                                            }

                                                            yes.setOnClickListener(new View.OnClickListener() {
                                                                @Override
                                                                public void onClick(View view) {

                                                                    myDbHelper.deleteOrderTable();

                                                                    for (int o = 0; o < data.size(); o++) {
                                                                        if (data.get(o).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getAddgrp().size() == 0) {

                                                                            HashMap<String, String> values = new HashMap<>();
                                                                            values.put("itemId", String.valueOf(data.get(o).getItemId()));
                                                                            values.put("itemTypeId", String.valueOf(data.get(o).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getPriceId()));
                                                                            if (MenuActivity.banner) {
                                                                                values.put("subCategoryId", String.valueOf(MenuActivity.storeData1.get(MenuActivity.pos).getBranchId()));
                                                                            } else {
                                                                                values.put("subCategoryId", String.valueOf(MenuActivity.storeData.get(MenuActivity.pos).getBranchId()));
                                                                            }
                                                                            values.put("additionals_name_En", "");
                                                                            values.put("additionals_name_Ar", "");
                                                                            values.put("qty", String.valueOf(Qty));
                                                                            values.put("price", String.valueOf(data.get(o).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getItemprice()));
                                                                            Log.i("TAG", "price3: " + data.get(o).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getItemprice());
                                                                            values.put("additionalsPrice", "");
                                                                            values.put("additionalsTypeId", "");
                                                                            values.put("totalAmount", total_price.getText().toString());
                                                                            if (mcomment.getText().toString().equalsIgnoreCase(""))
                                                                                values.put("comment", "");
                                                                            else
                                                                                values.put("comment", mcomment.getText().toString());
                                                                            values.put("status", "1");
                                                                            values.put("creationDate", "19/02/2019");
                                                                            values.put("modifiedDate", "19/02/2019");
                                                                            if (MenuActivity.banner) {
                                                                                values.put("categoryId", String.valueOf(MenuActivity.storeData1.get(MenuActivity.pos).getMobileNo()));
                                                                            } else {
                                                                                values.put("categoryId", String.valueOf(MenuActivity.storeData.get(MenuActivity.pos).getMobileNo()));
                                                                            }
                                                                            values.put("itemName", data.get(o).getItemName_En());
                                                                            values.put("itemNameAr", data.get(o).getItemName_Ar());
                                                                            if (MenuActivity.banner) {
                                                                                values.put("image", String.valueOf(MenuActivity.storeData1.get(MenuActivity.pos).getStoreLogo_En()));
                                                                            } else {
                                                                                values.put("image", String.valueOf(MenuActivity.storeData.get(MenuActivity.pos).getStoreLogo_En()));
                                                                            }
                                                                            values.put("item_desc", "");
                                                                            values.put("item_desc_Ar", "");
                                                                            values.put("sub_itemName", "");
                                                                            values.put("sub_itemName_Ar", "");
                                                                            values.put("sub_itemImage", "");
                                                                            values.put("ItemType", String.valueOf(data.get(o).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getPriceId()));
                                                                            values.put("sizename_en", data.get(o).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getItemSizeName_En());
                                                                            values.put("sizename_ar", data.get(o).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getItemSizeName_Ar());
                                                                            if (MenuActivity.banner) {
                                                                                values.put("discount", String.valueOf(MenuActivity.storeData1.get(MenuActivity.pos).getDiscountAmt()));
                                                                            } else {
                                                                                values.put("discount", String.valueOf(MenuActivity.storeData.get(MenuActivity.pos).getDiscountAmt()));
                                                                            }
                                                                            values.put("additionalitemQTY", "");
                                                                            if (MenuActivity.banner) {
                                                                                values.put("branchName", String.valueOf(MenuActivity.storeData1.get(MenuActivity.pos).getBranchName_En()));
                                                                                values.put("address", String.valueOf(MenuActivity.storeData1.get(MenuActivity.pos).getAddress()));
                                                                                values.put("brandName", String.valueOf(MenuActivity.storeData1.get(MenuActivity.pos).getBrandName_En()));
                                                                                values.put("brandName_Ar", String.valueOf(MenuActivity.storeData1.get(MenuActivity.pos).getBrandName_Ar()));
                                                                                values.put("orderType", String.valueOf(MenuActivity.storeData1.get(MenuActivity.pos).getOrderTypes()));
                                                                                values.put("BrandId", String.valueOf(MenuActivity.storeData1.get(MenuActivity.pos).getBrandId()));
                                                                                values.put("latitute", String.valueOf(MenuActivity.storeData1.get(MenuActivity.pos).getLatitude()));
                                                                                values.put("longitute", String.valueOf(MenuActivity.storeData1.get(MenuActivity.pos).getLongitude()));
                                                                                values.put("branchName_Ar", String.valueOf(MenuActivity.storeData1.get(MenuActivity.pos).getBranchName_Ar()));
                                                                            } else {
                                                                                values.put("branchName", String.valueOf(MenuActivity.storeData.get(MenuActivity.pos).getBranchName_En()));
                                                                                values.put("address", String.valueOf(MenuActivity.storeData.get(MenuActivity.pos).getAddress()));
                                                                                values.put("brandName", String.valueOf(MenuActivity.storeData.get(MenuActivity.pos).getBrandName_En()));
                                                                                values.put("brandName_Ar", String.valueOf(MenuActivity.storeData.get(MenuActivity.pos).getBrandName_Ar()));
                                                                                values.put("orderType", String.valueOf(MenuActivity.storeData.get(MenuActivity.pos).getOrderTypes()));
                                                                                values.put("BrandId", String.valueOf(MenuActivity.storeData.get(MenuActivity.pos).getBrandId()));
                                                                                values.put("latitute", String.valueOf(MenuActivity.storeData.get(MenuActivity.pos).getLatitude()));
                                                                                values.put("longitute", String.valueOf(MenuActivity.storeData.get(MenuActivity.pos).getLongitude()));
                                                                                values.put("branchName_Ar", String.valueOf(MenuActivity.storeData.get(MenuActivity.pos).getBranchName_Ar()));
                                                                            }
                                                                            values.put("item_image", itemimage);
                                                                            values.put("mainTotalAmount", String.valueOf(MainOrderPrice));
                                                                            values.put("dis_item_add_price", String.valueOf(dis_item_add_price));
                                                                            values.put("main_item_add_price", String.valueOf(item_add_price));
                                                                            if (MenuActivity.banner) {
                                                                                values.put("min_amount", String.valueOf(MenuActivity.storeData1.get(MenuActivity.pos).getMinOrderCharges()));
                                                                                values.put("max_days", String.valueOf(MenuActivity.storeData1.get(MenuActivity.pos).getFutureOrderDay()));
                                                                                values.put("delivery_charges", String.valueOf(MenuActivity.storeData1.get(MenuActivity.pos).getDeliveryCharges()));
                                                                            } else {
                                                                                values.put("min_amount", String.valueOf(MenuActivity.storeData.get(MenuActivity.pos).getMinOrderCharges()));
                                                                                values.put("max_days", String.valueOf(MenuActivity.storeData.get(MenuActivity.pos).getFutureOrderDay()));
                                                                                values.put("delivery_charges", String.valueOf(MenuActivity.storeData.get(MenuActivity.pos).getDeliveryCharges()));
                                                                            }
                                                                            values.put("price_with_disc", price.getText().toString());
                                                                            Log.i("TAG", "onClick: ");

                                                                            myDbHelper.insertOrder(values);
                                                                        } else {
                                                                            Log.i("TAG", "onClick1: ");

                                                                            for (int p = 0; p < data.get(o).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getAddgrp().size(); p++) {


                                                                                String addl_ids = null, addl_name = null, addl_name_ar = null, addl_image = null, addl_price = null, addl_qty = null;
//                                                                                if (boxpos != null && boxpos.size() > 0) {
                                                                                for (int n = 0; n < data.get(o).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getAddgrp().get(p).getAdditems().size(); n++) {
                                                                                    if (data.get(o).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getAddgrp().get(p).getAdditems().get(n).getAdd_qty() > 0) {
                                                                                        if (addl_ids != null && addl_ids.length() > 0) {

                                                                                            addl_ids = addl_ids + "," + String.valueOf(data.get(o).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getAddgrp().get(p).getAdditems().get(n).getAdditionalId());
                                                                                            addl_name = addl_name + "," + data.get(o).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getAddgrp().get(p).getAdditems().get(n).getAddtionalName_en();
                                                                                            addl_name_ar = addl_name_ar + "," + data.get(o).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getAddgrp().get(p).getAdditems().get(n).getAddtionalName_Ar();
//                                                                                addl_image = addl_image + "," + sections.get(position).getChildItems().get(value1).getSubItems().get(n).getSubItemimage();
                                                                                            for (int m = 0; m < data.get(o).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getAddgrp().get(p).getAdditems().get(n).getAddprice().size(); m++) {
                                                                                                addl_price = addl_price + "," + String.valueOf(data.get(o).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getAddgrp().get(p).getAdditems().get(n).getAddprice().get(m).getAddprice());
                                                                                            }
//                                                                                            int count = 0;
//                                                                                            for (int j = 0; j < itemQty.size(); j++) {
//                                                                                                if (itemQty.get(j).equals(data.get(o).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getAddgrp().get(p).getAdditems().get(n).getAddtionalName_en())) {
//                                                                                                    count = count + 1;
//                                                                                                }
//                                                                                            }
                                                                                            addl_qty = addl_qty + "," + data.get(o).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getAddgrp().get(p).getAdditems().get(n).getAdd_qty();

                                                                                        } else {
                                                                                            addl_ids = String.valueOf(data.get(o).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getAddgrp().get(p).getAdditems().get(n).getAdditionalId());
                                                                                            addl_name = data.get(o).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getAddgrp().get(p).getAdditems().get(n).getAddtionalName_en();
                                                                                            addl_name_ar = data.get(o).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getAddgrp().get(p).getAdditems().get(n).getAddtionalName_Ar();
//                                                                                addl_image = sections.get(position).getChildItems().get(value1).getSubItems().get(Integer.parseInt(boxpos.get(o))).getSubItemimage();
                                                                                            for (int m = 0; m < data.get(o).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getAddgrp().get(p).getAdditems().get(n).getAddprice().size(); m++) {
                                                                                                addl_price = String.valueOf(data.get(o).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getAddgrp().get(p).getAdditems().get(n).getAddprice().get(m).getAddprice());
                                                                                            }
//                                                                                            int count = 0;
//                                                                                            for (int j = 0; j < itemQty.size(); j++) {
//                                                                                                if (itemQty.get(j).equals(data.get(o).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getAddgrp().get(p).getAdditems().get(n).getAddtionalName_en())) {
//                                                                                                    count = count + 1;
//                                                                                                }
//                                                                                            }
                                                                                            addl_qty = String.valueOf(data.get(o).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getAddgrp().get(p).getAdditems().get(n).getAdd_qty());
                                                                                        }
                                                                                    }
                                                                                }
//                                                                                }

                                                                                HashMap<String, String> values1 = new HashMap<>();
                                                                                values1.put("itemId", String.valueOf(data.get(o).getItemId()));
                                                                                values1.put("itemTypeId", String.valueOf(data.get(o).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getPriceId()));
                                                                                if (MenuActivity.banner) {
                                                                                    values1.put("subCategoryId", String.valueOf(MenuActivity.storeData1.get(MenuActivity.pos).getBranchId()));
                                                                                } else {
                                                                                    values1.put("subCategoryId", String.valueOf(MenuActivity.storeData.get(MenuActivity.pos).getBranchId()));
                                                                                }
                                                                                values1.put("additionals_name_En", addl_name);
                                                                                values1.put("additionals_name_Ar", addl_name_ar);
                                                                                values1.put("qty", String.valueOf(Qty));
                                                                                values1.put("price", String.valueOf(data.get(o).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getItemprice()));
                                                                                Log.i("TAG", "price4: " + data.get(o).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getItemprice());
                                                                                values1.put("additionalsPrice", addl_price);
                                                                                values1.put("additionalsTypeId", addl_ids);
                                                                                values1.put("totalAmount", total_price.getText().toString());
                                                                                if (mcomment.getText().toString().equalsIgnoreCase(""))
                                                                                    values1.put("comment", "");
                                                                                else
                                                                                    values1.put("comment", mcomment.getText().toString());
                                                                                values1.put("status", "1");
                                                                                values1.put("creationDate", "19/02/2019");
                                                                                values1.put("modifiedDate", "19/02/2019");
                                                                                if (MenuActivity.banner) {
                                                                                    values1.put("categoryId", String.valueOf(MenuActivity.storeData1.get(MenuActivity.pos).getMobileNo()));
                                                                                } else {
                                                                                    values1.put("categoryId", String.valueOf(MenuActivity.storeData.get(MenuActivity.pos).getMobileNo()));
                                                                                }
                                                                                values1.put("itemName", data.get(o).getItemName_En());
                                                                                values1.put("itemNameAr", data.get(o).getItemName_Ar());
                                                                                if (MenuActivity.banner) {
                                                                                    values1.put("image", String.valueOf(MenuActivity.storeData1.get(MenuActivity.pos).getStoreLogo_En()));
                                                                                } else {
                                                                                    values1.put("image", String.valueOf(MenuActivity.storeData.get(MenuActivity.pos).getStoreLogo_En()));
                                                                                }
                                                                                values1.put("item_desc", "");
                                                                                values1.put("item_desc_Ar", "");
                                                                                values1.put("sub_itemName", data.get(o).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getAddgrp().get(p).getAddGrpName_En());
                                                                                values1.put("sub_itemName_Ar", data.get(o).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getAddgrp().get(p).getAddGrpName_Ar());
                                                                                values1.put("sub_itemImage", "");
                                                                                values1.put("ItemType", String.valueOf(data.get(o).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getPriceId()));
                                                                                values1.put("sizename_en", data.get(o).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getItemSizeName_En());
                                                                                values1.put("sizename_ar", data.get(o).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getItemSizeName_Ar());
                                                                                if (MenuActivity.banner) {
                                                                                    values1.put("discount", String.valueOf(MenuActivity.storeData1.get(MenuActivity.pos).getDiscountAmt()));
                                                                                } else {
                                                                                    values1.put("discount", String.valueOf(MenuActivity.storeData.get(MenuActivity.pos).getDiscountAmt()));
                                                                                }
                                                                                values1.put("additionalitemQTY", addl_qty);
                                                                                if (MenuActivity.banner) {
                                                                                    values1.put("branchName", String.valueOf(MenuActivity.storeData1.get(MenuActivity.pos).getBranchName_En()));
                                                                                    values1.put("address", String.valueOf(MenuActivity.storeData1.get(MenuActivity.pos).getAddress()));
                                                                                    values1.put("brandName", String.valueOf(MenuActivity.storeData1.get(MenuActivity.pos).getBrandName_En()));
                                                                                    values1.put("brandName_Ar", String.valueOf(MenuActivity.storeData1.get(MenuActivity.pos).getBrandName_Ar()));
                                                                                    values1.put("orderType", String.valueOf(MenuActivity.storeData1.get(MenuActivity.pos).getOrderTypes()));
                                                                                    values1.put("BrandId", String.valueOf(MenuActivity.storeData1.get(MenuActivity.pos).getBrandId()));
                                                                                    values1.put("latitute", String.valueOf(MenuActivity.storeData1.get(MenuActivity.pos).getLatitude()));
                                                                                    values1.put("longitute", String.valueOf(MenuActivity.storeData1.get(MenuActivity.pos).getLongitude()));
                                                                                    values1.put("branchName_Ar", String.valueOf(MenuActivity.storeData1.get(MenuActivity.pos).getBranchName_Ar()));
                                                                                } else {
                                                                                    values1.put("branchName", String.valueOf(MenuActivity.storeData.get(MenuActivity.pos).getBranchName_En()));
                                                                                    values1.put("address", String.valueOf(MenuActivity.storeData.get(MenuActivity.pos).getAddress()));
                                                                                    values1.put("brandName", String.valueOf(MenuActivity.storeData.get(MenuActivity.pos).getBrandName_En()));
                                                                                    values1.put("brandName_Ar", String.valueOf(MenuActivity.storeData.get(MenuActivity.pos).getBrandName_Ar()));
                                                                                    values1.put("orderType", String.valueOf(MenuActivity.storeData.get(MenuActivity.pos).getOrderTypes()));
                                                                                    values1.put("BrandId", String.valueOf(MenuActivity.storeData.get(MenuActivity.pos).getBrandId()));
                                                                                    values1.put("latitute", String.valueOf(MenuActivity.storeData.get(MenuActivity.pos).getLatitude()));
                                                                                    values1.put("longitute", String.valueOf(MenuActivity.storeData.get(MenuActivity.pos).getLongitude()));
                                                                                    values1.put("branchName_Ar", String.valueOf(MenuActivity.storeData.get(MenuActivity.pos).getBranchName_Ar()));
                                                                                }
                                                                                values1.put("item_image", itemimage);
                                                                                values1.put("mainTotalAmount", String.valueOf(MainOrderPrice));
                                                                                values1.put("dis_item_add_price", String.valueOf(dis_item_add_price));
                                                                                values1.put("main_item_add_price", String.valueOf(item_add_price));
                                                                                if (MenuActivity.banner) {
                                                                                    values1.put("min_amount", String.valueOf(MenuActivity.storeData1.get(MenuActivity.pos).getMinOrderCharges()));
                                                                                    values1.put("max_days", String.valueOf(MenuActivity.storeData1.get(MenuActivity.pos).getFutureOrderDay()));
                                                                                    values1.put("delivery_charges", String.valueOf(MenuActivity.storeData1.get(MenuActivity.pos).getDeliveryCharges()));
                                                                                } else {
                                                                                    values1.put("min_amount", String.valueOf(MenuActivity.storeData.get(MenuActivity.pos).getMinOrderCharges()));
                                                                                    values1.put("max_days", String.valueOf(MenuActivity.storeData.get(MenuActivity.pos).getFutureOrderDay()));
                                                                                    values1.put("delivery_charges", String.valueOf(MenuActivity.storeData.get(MenuActivity.pos).getDeliveryCharges()));
                                                                                }
                                                                                values1.put("price_with_disc", price.getText().toString());

                                                                                myDbHelper.insertOrder(values1);

                                                                            }
                                                                        }
                                                                    }


                                                                    customDialog.dismiss();
                                                                    onBackPressed();
                                                                    finish();

//                                                                    AlertDialog customDialog = null;
//                                                                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(AdditionalActivity.this);
//                                                                    // ...Irrelevant code for customizing the buttons and title
//                                                                    LayoutInflater inflater = getLayoutInflater();
//                                                                    int layout;
//                                                                    if (language.equalsIgnoreCase("En")) {
//                                                                        layout = R.layout.alert_dialog;
//                                                                    } else {
//                                                                        layout = R.layout.alert_dialog_arabic;
//                                                                    }
//                                                                    View dialogView = inflater.inflate(layout, null);
//                                                                    dialogBuilder.setView(dialogView);
//                                                                    dialogBuilder.setCancelable(false);
//
//                                                                    TextView title = (TextView) dialogView.findViewById(R.id.title);
//                                                                    TextView desc = (TextView) dialogView.findViewById(R.id.desc);
//                                                                    TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
//                                                                    TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
//                                                                    View vert = (View) dialogView.findViewById(R.id.vert_line);
//
//                                                                    no.setVisibility(View.GONE);
//                                                                    vert.setVisibility(View.GONE);
//
//                                                                    if (language.equalsIgnoreCase("En")) {
//                                                                        title.setText(R.string.app_name);
//                                                                        yes.setText(R.string.ok);
//                                                                        desc.setText("Successfully Added to cart");
//                                                                    } else {
//                                                                        title.setText(getResources().getString(R.string.app_name_ar));
//                                                                        yes.setText(getResources().getString(R.string.ok_ar));
//                                                                        desc.setText("تمت إضافته للسله بنجاح");
//                                                                    }
//
//                                                                    customDialog = dialogBuilder.create();
//                                                                    customDialog.show();
//
//                                                                    final AlertDialog finalCustomDialog = customDialog;
//                                                                    yes.setOnClickListener(new View.OnClickListener() {
//                                                                        @Override
//                                                                        public void onClick(View view) {
//                                                                            onBackPressed();
//                                                                            finish();
//                                                                            finalCustomDialog.dismiss();
//                                                                        }
//                                                                    });
//
//                                                                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
//                                                                    Window window = customDialog.getWindow();
//                                                                    window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//                                                                    lp.copyFrom(window.getAttributes());
//                                                                    //This makes the dialog take up the full width
//                                                                    Display display = getWindowManager().getDefaultDisplay();
//                                                                    Point size = new Point();
//                                                                    display.getSize(size);
//                                                                    int screenWidth = size.x;
//
//                                                                    double d = screenWidth * 0.85;
//                                                                    lp.width = (int) d;
//                                                                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
//                                                                    window.setAttributes(lp);
                                                                }
                                                            });


                                                            no.setOnClickListener(new View.OnClickListener() {
                                                                @Override
                                                                public void onClick(View view) {
                                                                    customDialog.dismiss();
                                                                }
                                                            });

                                                            customDialog = dialogBuilder.create();
                                                            customDialog.show();
                                                            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                                                            Window window = customDialog.getWindow();
                                                            lp.copyFrom(window.getAttributes());
                                                            //This makes the dialog take up the full width
                                                            Display display = getWindowManager().getDefaultDisplay();
                                                            Point size = new Point();
                                                            display.getSize(size);
                                                            int screenWidth = size.x;

                                                            double d = screenWidth * 0.85;
                                                            lp.width = (int) d;
                                                            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                                                            window.setAttributes(lp);

                                                        } else {

                                                            for (int o = 0; o < data.size(); o++) {
                                                                if (data.get(o).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getAddgrp().size() == 0) {

                                                                    HashMap<String, String> values = new HashMap<>();
                                                                    values.put("itemId", String.valueOf(data.get(o).getItemId()));
                                                                    values.put("itemTypeId", String.valueOf(data.get(o).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getPriceId()));
                                                                    if (MenuActivity.banner) {
                                                                        values.put("subCategoryId", String.valueOf(MenuActivity.storeData1.get(MenuActivity.pos).getBranchId()));
                                                                    } else {
                                                                        values.put("subCategoryId", String.valueOf(MenuActivity.storeData.get(MenuActivity.pos).getBranchId()));
                                                                    }
                                                                    values.put("additionals_name_En", "");
                                                                    values.put("additionals_name_Ar", "");
                                                                    values.put("qty", String.valueOf(Qty));
                                                                    values.put("price", String.valueOf(data.get(o).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getItemprice()));
                                                                    Log.i("TAG", "price5: " + data.get(o).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getItemprice());
                                                                    values.put("additionalsPrice", "");
                                                                    values.put("additionalsTypeId", "");
                                                                    values.put("totalAmount", total_price.getText().toString());
                                                                    if (mcomment.getText().toString().equalsIgnoreCase(""))
                                                                        values.put("comment", "");
                                                                    else
                                                                        values.put("comment", mcomment.getText().toString());
                                                                    values.put("status", "1");
                                                                    values.put("creationDate", "19/02/2019");
                                                                    values.put("modifiedDate", "19/02/2019");
                                                                    if (MenuActivity.banner) {
                                                                        values.put("categoryId", String.valueOf(MenuActivity.storeData1.get(MenuActivity.pos).getMobileNo()));
                                                                    } else {
                                                                        values.put("categoryId", String.valueOf(MenuActivity.storeData.get(MenuActivity.pos).getMobileNo()));
                                                                    }
                                                                    values.put("itemName", data.get(o).getItemName_En());
                                                                    values.put("itemNameAr", data.get(o).getItemName_Ar());
                                                                    if (MenuActivity.banner) {
                                                                        values.put("image", String.valueOf(MenuActivity.storeData1.get(MenuActivity.pos).getStoreLogo_En()));
                                                                    } else {
                                                                        values.put("image", String.valueOf(MenuActivity.storeData.get(MenuActivity.pos).getStoreLogo_En()));
                                                                    }
                                                                    values.put("item_desc", "");
                                                                    values.put("item_desc_Ar", "");
                                                                    values.put("sub_itemName", "");
                                                                    values.put("sub_itemName_Ar", "");
                                                                    values.put("sub_itemImage", "");
                                                                    values.put("ItemType", String.valueOf(data.get(o).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getPriceId()));
                                                                    values.put("sizename_en", data.get(o).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getItemSizeName_En());
                                                                    values.put("sizename_ar", data.get(o).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getItemSizeName_Ar());
                                                                    if (MenuActivity.banner) {
                                                                        values.put("discount", String.valueOf(MenuActivity.storeData1.get(MenuActivity.pos).getDiscountAmt()));
                                                                    } else {
                                                                        values.put("discount", String.valueOf(MenuActivity.storeData.get(MenuActivity.pos).getDiscountAmt()));
                                                                    }
                                                                    values.put("additionalitemQTY", "");
                                                                    if (MenuActivity.banner) {
                                                                        values.put("branchName", String.valueOf(MenuActivity.storeData1.get(MenuActivity.pos).getBranchName_En()));
                                                                        values.put("address", String.valueOf(MenuActivity.storeData1.get(MenuActivity.pos).getAddress()));
                                                                        values.put("brandName", String.valueOf(MenuActivity.storeData1.get(MenuActivity.pos).getBrandName_En()));
                                                                        values.put("brandName_Ar", String.valueOf(MenuActivity.storeData1.get(MenuActivity.pos).getBrandName_Ar()));
                                                                        values.put("orderType", String.valueOf(MenuActivity.storeData1.get(MenuActivity.pos).getOrderTypes()));
                                                                        values.put("BrandId", String.valueOf(MenuActivity.storeData1.get(MenuActivity.pos).getBrandId()));
                                                                        values.put("latitute", String.valueOf(MenuActivity.storeData1.get(MenuActivity.pos).getLatitude()));
                                                                        values.put("longitute", String.valueOf(MenuActivity.storeData1.get(MenuActivity.pos).getLongitude()));
                                                                        values.put("branchName_Ar", String.valueOf(MenuActivity.storeData1.get(MenuActivity.pos).getBranchName_Ar()));
                                                                    } else {
                                                                        values.put("branchName", String.valueOf(MenuActivity.storeData.get(MenuActivity.pos).getBranchName_En()));
                                                                        values.put("address", String.valueOf(MenuActivity.storeData.get(MenuActivity.pos).getAddress()));
                                                                        values.put("brandName", String.valueOf(MenuActivity.storeData.get(MenuActivity.pos).getBrandName_En()));
                                                                        values.put("brandName_Ar", String.valueOf(MenuActivity.storeData.get(MenuActivity.pos).getBrandName_Ar()));
                                                                        values.put("orderType", String.valueOf(MenuActivity.storeData.get(MenuActivity.pos).getOrderTypes()));
                                                                        values.put("BrandId", String.valueOf(MenuActivity.storeData.get(MenuActivity.pos).getBrandId()));
                                                                        values.put("latitute", String.valueOf(MenuActivity.storeData.get(MenuActivity.pos).getLatitude()));
                                                                        values.put("longitute", String.valueOf(MenuActivity.storeData.get(MenuActivity.pos).getLongitude()));
                                                                        values.put("branchName_Ar", String.valueOf(MenuActivity.storeData.get(MenuActivity.pos).getBranchName_Ar()));
                                                                    }
                                                                    values.put("item_image", itemimage);
                                                                    values.put("mainTotalAmount", String.valueOf(MainOrderPrice));
                                                                    values.put("dis_item_add_price", String.valueOf(dis_item_add_price));
                                                                    values.put("main_item_add_price", String.valueOf(item_add_price));
                                                                    if (MenuActivity.banner) {
                                                                        values.put("min_amount", String.valueOf(MenuActivity.storeData1.get(MenuActivity.pos).getMinOrderCharges()));
                                                                        values.put("max_days", String.valueOf(MenuActivity.storeData1.get(MenuActivity.pos).getFutureOrderDay()));
                                                                        values.put("delivery_charges", String.valueOf(MenuActivity.storeData1.get(MenuActivity.pos).getDeliveryCharges()));
                                                                    } else {
                                                                        values.put("min_amount", String.valueOf(MenuActivity.storeData.get(MenuActivity.pos).getMinOrderCharges()));
                                                                        values.put("max_days", String.valueOf(MenuActivity.storeData.get(MenuActivity.pos).getFutureOrderDay()));
                                                                        values.put("delivery_charges", String.valueOf(MenuActivity.storeData.get(MenuActivity.pos).getDeliveryCharges()));
                                                                    }
                                                                    values.put("price_with_disc", price.getText().toString());

                                                                    Log.i("TAG", "onClick: ");

                                                                    myDbHelper.insertOrder(values);
                                                                } else {
                                                                    Log.i("TAG", "onClick1: ");

                                                                    for (int p = 0; p < data.get(o).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getAddgrp().size(); p++) {

                                                                        String addl_ids = null, addl_name = null, addl_name_ar = null, addl_image = null, addl_price = null, addl_qty = null;
//                                                                        if (boxpos != null && boxpos.size() > 0) {
                                                                        for (int n = 0; n < data.get(o).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getAddgrp().get(p).getAdditems().size(); n++) {
                                                                            if (data.get(o).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getAddgrp().get(p).getAdditems().get(n).getAdd_qty() > 0) {
                                                                                if (addl_ids != null && addl_ids.length() > 0) {

                                                                                    addl_ids = addl_ids + "," + String.valueOf(data.get(o).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getAddgrp().get(p).getAdditems().get(n).getAdditionalId());
                                                                                    addl_name = addl_name + "," + data.get(o).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getAddgrp().get(p).getAdditems().get(n).getAddtionalName_en();
                                                                                    addl_name_ar = addl_name_ar + "," + data.get(o).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getAddgrp().get(p).getAdditems().get(n).getAddtionalName_Ar();
//                                                                                addl_image = addl_image + "," + sections.get(position).getChildItems().get(value1).getSubItems().get(n).getSubItemimage();
                                                                                    for (int m = 0; m < data.get(o).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getAddgrp().get(p).getAdditems().get(n).getAddprice().size(); m++) {
                                                                                        addl_price = addl_price + "," + String.valueOf(data.get(o).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getAddgrp().get(p).getAdditems().get(n).getAddprice().get(m).getAddprice());
                                                                                    }
//                                                                                    int count = 0;
//                                                                                    for (int j = 0; j < itemQty.size(); j++) {
//                                                                                        if (itemQty.get(j).equals(data.get(o).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getAddgrp().get(p).getAdditems().get(n).getAddtionalName_en())) {
//                                                                                            count = count + 1;
//                                                                                        }
//                                                                                    }
                                                                                    addl_qty = addl_qty + "," + data.get(o).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getAddgrp().get(p).getAdditems().get(n).getAdd_qty();

                                                                                } else {
                                                                                    addl_ids = String.valueOf(data.get(o).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getAddgrp().get(p).getAdditems().get(n).getAdditionalId());
                                                                                    addl_name = data.get(o).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getAddgrp().get(p).getAdditems().get(n).getAddtionalName_en();
                                                                                    addl_name_ar = data.get(o).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getAddgrp().get(p).getAdditems().get(n).getAddtionalName_Ar();
//                                                                                addl_image = sections.get(position).getChildItems().get(value1).getSubItems().get(Integer.parseInt(boxpos.get(o))).getSubItemimage();
                                                                                    for (int m = 0; m < data.get(o).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getAddgrp().get(p).getAdditems().get(n).getAddprice().size(); m++) {
                                                                                        addl_price = String.valueOf(data.get(o).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getAddgrp().get(p).getAdditems().get(n).getAddprice().get(m).getAddprice());
                                                                                    }
//                                                                                    int count = 0;
//                                                                                    for (int j = 0; j < itemQty.size(); j++) {
//                                                                                        if (itemQty.get(j).equals(data.get(o).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getAddgrp().get(p).getAdditems().get(n).getAddtionalName_en())) {
//                                                                                            count = count + 1;
//                                                                                        }
//                                                                                    }
                                                                                    addl_qty = String.valueOf(data.get(o).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getAddgrp().get(p).getAdditems().get(n).getAdd_qty());
                                                                                }
                                                                            }
                                                                        }
//                                                                        }

                                                                        HashMap<String, String> values1 = new HashMap<>();
                                                                        values1.put("itemId", String.valueOf(data.get(o).getItemId()));
                                                                        values1.put("itemTypeId", String.valueOf(data.get(o).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getPriceId()));
                                                                        if (MenuActivity.banner) {
                                                                            values1.put("subCategoryId", String.valueOf(MenuActivity.storeData1.get(MenuActivity.pos).getBranchId()));
                                                                        } else {
                                                                            values1.put("subCategoryId", String.valueOf(MenuActivity.storeData.get(MenuActivity.pos).getBranchId()));
                                                                        }
                                                                        values1.put("additionals_name_En", addl_name);
                                                                        values1.put("additionals_name_Ar", addl_name_ar);
                                                                        values1.put("qty", String.valueOf(Qty));
                                                                        values1.put("price", String.valueOf(data.get(o).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getItemprice()));
                                                                        Log.i("TAG", "price6: " + data.get(o).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getItemprice());
                                                                        values1.put("additionalsPrice", addl_price);
                                                                        values1.put("additionalsTypeId", addl_ids);
                                                                        values1.put("totalAmount", total_price.getText().toString());
                                                                        if (mcomment.getText().toString().equalsIgnoreCase(""))
                                                                            values1.put("comment", "");
                                                                        else
                                                                            values1.put("comment", mcomment.getText().toString());
                                                                        values1.put("status", "1");
                                                                        values1.put("creationDate", "19/02/2019");
                                                                        values1.put("modifiedDate", "19/02/2019");
                                                                        if (MenuActivity.banner) {
                                                                            values1.put("categoryId", String.valueOf(MenuActivity.storeData1.get(MenuActivity.pos).getMobileNo()));
                                                                        } else {
                                                                            values1.put("categoryId", String.valueOf(MenuActivity.storeData.get(MenuActivity.pos).getMobileNo()));
                                                                        }
                                                                        values1.put("itemName", data.get(o).getItemName_En());
                                                                        values1.put("itemNameAr", data.get(o).getItemName_Ar());
                                                                        if (MenuActivity.banner) {
                                                                            values1.put("image", String.valueOf(MenuActivity.storeData1.get(MenuActivity.pos).getStoreLogo_En()));
                                                                        } else {
                                                                            values1.put("image", String.valueOf(MenuActivity.storeData.get(MenuActivity.pos).getStoreLogo_En()));
                                                                        }
                                                                        values1.put("item_desc", "");
                                                                        values1.put("item_desc_Ar", "");
                                                                        values1.put("sub_itemName", data.get(o).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getAddgrp().get(p).getAddGrpName_En());
                                                                        values1.put("sub_itemName_Ar", data.get(o).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getAddgrp().get(p).getAddGrpName_Ar());
                                                                        values1.put("sub_itemImage", "");
                                                                        values1.put("ItemType", String.valueOf(data.get(o).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getPriceId()));
                                                                        values1.put("sizename_en", data.get(o).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getItemSizeName_En());
                                                                        values1.put("sizename_ar", data.get(o).getItemSizePrice().get(SubAdditionalAdapter.sub_item_pos).getItemSizeName_Ar());
                                                                        if (MenuActivity.banner) {
                                                                            values1.put("discount", String.valueOf(MenuActivity.storeData1.get(MenuActivity.pos).getDiscountAmt()));
                                                                        } else {
                                                                            values1.put("discount", String.valueOf(MenuActivity.storeData.get(MenuActivity.pos).getDiscountAmt()));
                                                                        }
                                                                        values1.put("additionalitemQTY", addl_qty);
                                                                        if (MenuActivity.banner) {
                                                                            values1.put("branchName", String.valueOf(MenuActivity.storeData1.get(MenuActivity.pos).getBranchName_En()));
                                                                            values1.put("address", String.valueOf(MenuActivity.storeData1.get(MenuActivity.pos).getAddress()));
                                                                            values1.put("brandName", String.valueOf(MenuActivity.storeData1.get(MenuActivity.pos).getBrandName_En()));
                                                                            values1.put("brandName_Ar", String.valueOf(MenuActivity.storeData1.get(MenuActivity.pos).getBrandName_Ar()));
                                                                            values1.put("orderType", String.valueOf(MenuActivity.storeData1.get(MenuActivity.pos).getOrderTypes()));
                                                                            values1.put("BrandId", String.valueOf(MenuActivity.storeData1.get(MenuActivity.pos).getBrandId()));
                                                                            values1.put("latitute", String.valueOf(MenuActivity.storeData1.get(MenuActivity.pos).getLatitude()));
                                                                            values1.put("longitute", String.valueOf(MenuActivity.storeData1.get(MenuActivity.pos).getLongitude()));
                                                                            values1.put("branchName_Ar", String.valueOf(MenuActivity.storeData1.get(MenuActivity.pos).getBranchName_Ar()));
                                                                        } else {
                                                                            values1.put("branchName", String.valueOf(MenuActivity.storeData.get(MenuActivity.pos).getBranchName_En()));
                                                                            values1.put("address", String.valueOf(MenuActivity.storeData.get(MenuActivity.pos).getAddress()));
                                                                            values1.put("brandName", String.valueOf(MenuActivity.storeData.get(MenuActivity.pos).getBrandName_En()));
                                                                            values1.put("brandName_Ar", String.valueOf(MenuActivity.storeData.get(MenuActivity.pos).getBrandName_Ar()));
                                                                            values1.put("orderType", String.valueOf(MenuActivity.storeData.get(MenuActivity.pos).getOrderTypes()));
                                                                            values1.put("BrandId", String.valueOf(MenuActivity.storeData.get(MenuActivity.pos).getBrandId()));
                                                                            values1.put("latitute", String.valueOf(MenuActivity.storeData.get(MenuActivity.pos).getLatitude()));
                                                                            values1.put("longitute", String.valueOf(MenuActivity.storeData.get(MenuActivity.pos).getLongitude()));
                                                                            values1.put("branchName_Ar", String.valueOf(MenuActivity.storeData.get(MenuActivity.pos).getBranchName_Ar()));
                                                                        }
                                                                        values1.put("item_image", itemimage);
                                                                        values1.put("mainTotalAmount", String.valueOf(MainOrderPrice));
                                                                        values1.put("dis_item_add_price", String.valueOf(dis_item_add_price));
                                                                        values1.put("main_item_add_price", String.valueOf(item_add_price));
                                                                        if (MenuActivity.banner) {
                                                                            values1.put("min_amount", String.valueOf(MenuActivity.storeData1.get(MenuActivity.pos).getMinOrderCharges()));
                                                                            values1.put("max_days", String.valueOf(MenuActivity.storeData1.get(MenuActivity.pos).getFutureOrderDay()));
                                                                            values1.put("delivery_charges", String.valueOf(MenuActivity.storeData1.get(MenuActivity.pos).getDeliveryCharges()));
                                                                        } else {
                                                                            values1.put("min_amount", String.valueOf(MenuActivity.storeData.get(MenuActivity.pos).getMinOrderCharges()));
                                                                            values1.put("max_days", String.valueOf(MenuActivity.storeData.get(MenuActivity.pos).getFutureOrderDay()));
                                                                            values1.put("delivery_charges", String.valueOf(MenuActivity.storeData.get(MenuActivity.pos).getDeliveryCharges()));
                                                                        }
                                                                        values1.put("price_with_disc", price.getText().toString());

                                                                        myDbHelper.insertOrder(values1);

                                                                    }
                                                                }

                                                                onBackPressed();
                                                                finish();


//                                                                AlertDialog customDialog = null;
//                                                                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(AdditionalActivity.this);
//                                                                // ...Irrelevant code for customizing the buttons and title
//                                                                LayoutInflater inflater = getLayoutInflater();
//                                                                int layout;
//                                                                if (language.equalsIgnoreCase("En")) {
//                                                                    layout = R.layout.alert_dialog;
//                                                                } else {
//                                                                    layout = R.layout.alert_dialog_arabic;
//                                                                }
//                                                                View dialogView = inflater.inflate(layout, null);
//                                                                dialogBuilder.setView(dialogView);
//                                                                dialogBuilder.setCancelable(false);
//
//                                                                TextView title = (TextView) dialogView.findViewById(R.id.title);
//                                                                TextView desc = (TextView) dialogView.findViewById(R.id.desc);
//                                                                TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
//                                                                TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
//                                                                View vert = (View) dialogView.findViewById(R.id.vert_line);
//
//                                                                no.setVisibility(View.GONE);
//                                                                vert.setVisibility(View.GONE);
//
//                                                                if (language.equalsIgnoreCase("En")) {
//                                                                    title.setText(R.string.app_name);
//                                                                    yes.setText(R.string.ok);
//                                                                    desc.setText("Successfully Added to cart");
//                                                                } else {
//                                                                    title.setText(getResources().getString(R.string.app_name_ar));
//                                                                    yes.setText(getResources().getString(R.string.ok_ar));
//                                                                    desc.setText("تمت إضافته للسله بنجاح");
//                                                                }
//
//                                                                customDialog = dialogBuilder.create();
//                                                                customDialog.show();
//
//                                                                final AlertDialog finalCustomDialog = customDialog;
//                                                                yes.setOnClickListener(new View.OnClickListener() {
//                                                                    @Override
//                                                                    public void onClick(View view) {
//                                                                        onBackPressed();
//                                                                        finish();
//                                                                        finalCustomDialog.dismiss();
//                                                                    }
//                                                                });
//
//                                                                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
//                                                                Window window = customDialog.getWindow();
//                                                                window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//                                                                lp.copyFrom(window.getAttributes());
//                                                                //This makes the dialog take up the full width
//                                                                Display display = getWindowManager().getDefaultDisplay();
//                                                                Point size = new Point();
//                                                                display.getSize(size);
//                                                                int screenWidth = size.x;
//
//                                                                double d = screenWidth * 0.85;
//                                                                lp.width = (int) d;
//                                                                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
//                                                                window.setAttributes(lp);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        });

                                        Log.i("TAG", "itemsize: " + data.get(i).getItemSizePrice().get(j).getAddgrp().size());

                                        if (data.get(i).getItemSizePrice().get(j).getAddgrp().size() == 0) {

                                            Log.i("TAG", "Gone");

                                            sub_additional.setVisibility(View.GONE);

                                        } else {

                                            for (k = 0; k < data.get(i).getItemSizePrice().get(j).getAddgrp().size(); k++) {

                                                Log.i("TAG", "Visible ");

                                                sub_additional.setVisibility(View.VISIBLE);

                                                if (language.equalsIgnoreCase("En")) {
                                                    sub_additionals_name.setText("" + data.get(i).getItemSizePrice().get(j).getAddgrp().get(k).getAddGrpName_En());
                                                    mmax_add_to_be_selected.setText("Maximum " + data.get(i).getItemSizePrice().get(j).getAddgrp().get(k).getMaxSelection());
                                                } else {
                                                    sub_additionals_name.setText("" + data.get(i).getItemSizePrice().get(j).getAddgrp().get(k).getAddGrpName_Ar());
                                                    mmax_add_to_be_selected.setText(" الحد الأقصى " + data.get(i).getItemSizePrice().get(j).getAddgrp().get(k).getMaxSelection());
                                                }
                                                max_add_to_be_selected = data.get(i).getItemSizePrice().get(j).getAddgrp().get(k).getMaxSelection();
//                                                for (int l = 0; l <= data.get(i).getItemSizePrice().get(j).getAddgrp().get(k).getAdditems().size(); l++) {

                                                for (int m = 0; m < data.get(i).getItemSizePrice().get(j).getAddgrp().get(k).getAdditems().size(); m++) {
                                                    AdditionalActivity.sub_add_items.add(0);
                                                    Log.i("TAG", "onResponse: " + itemQty);
                                                }

                                                subitemdata = data.get(i).getItemSizePrice().get(j).getAddgrp().get(k).getAdditems();
                                                msubadapter = new SubAdditionalAdapter2(AdditionalActivity.this, subitemdata, language);
                                                sub_additional_items.setAdapter(msubadapter);
                                                ListUtils.setDynamicHeight(sub_additional_items);
//                                                    for (m = 0; m <= data.get(i).getItemSizePrice().get(j).getAddgrp().get(k).getAdditems().get(l).getAddprice().size(); m++) {
//
//                                                        HashMap<String, String> values1 = new HashMap<>();
//                                                        values1.put("itemId", String.valueOf(data.get(i).getItemId()));
//                                                        values1.put("itemTypeId", String.valueOf(data.get(i).getItemSizePrice().get(j).getPriceId()));
//                                                        values1.put("subCategoryId", "");
//                                                        values1.put("additionals_name_En", data.get(i).getItemSizePrice().get(j).getAddgrp().get(k).getAdditems().get(l).getAddtionalName_en());
//                                                        values1.put("additionals_name_Ar", data.get(i).getItemSizePrice().get(j).getAddgrp().get(k).getAdditems().get(l).getAddtionalName_Ar());
////                                                        values1.put("qty", String.valueOf(count));
//                                                        values1.put("price", String.valueOf(data.get(i).getItemSizePrice().get(j).getItemprice()));
//                                                        values1.put("additionalsPrice", String.valueOf(data.get(i).getItemSizePrice().get(j).getAddgrp().get(k).getAdditems().get(l).getAddprice().get(m).getAddprice()));
//                                                        values1.put("additionalsTypeId", String.valueOf(data.get(i).getItemSizePrice().get(j).getAddgrp().get(k).getAdditems().get(l).getAdditionalId()));
////                                                        values1.put("totalAmount", String.valueOf(SectionRecyclerViewAdapter.orderPrice));
//                                                        values1.put("comment", "");
//                                                        values1.put("status", "1");
//                                                        values1.put("creationDate", "19/02/2019");
//                                                        values1.put("modifiedDate", "19/02/2019");
//                                                        values1.put("categoryId", "");
//                                                        values1.put("itemName", data.get(i).getItemName_En());
//                                                        values1.put("itemNameAr", data.get(i).getItemName_Ar());
//                                                        values1.put("image", "");
//                                                        values1.put("item_desc", "");
//                                                        values1.put("item_desc_Ar", "");
//                                                        values1.put("sub_itemName", data.get(i).getItemSizePrice().get(j).getAddgrp().get(k).getAddGrpName_En());
//                                                        values1.put("sub_itemName_Ar", data.get(i).getItemSizePrice().get(j).getAddgrp().get(k).getAddGrpName_Ar());
//                                                        values1.put("sub_itemImage", "");
//                                                        values1.put("ItemType", "");
//                                                        values1.put("sizename_en", data.get(i).getItemSizePrice().get(j).getItemSizeName_En());
//                                                        values1.put("sizename_ar", data.get(i).getItemSizePrice().get(j).getItemSizeName_Ar());
//
//                                                        myDbHelper.insertOrder(values1);
//                                                    }
//                                                }
                                            }
                                        }
                                    }
                                }

//                                mscrollview.smoothScrollTo(0, 0);
//                                bids_count.setText(""+data.size());

                            } else {
                                //                          status false case
                                if (language.equalsIgnoreCase("En")) {
                                    String failureResponse = subAdditionals.getMessage();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name),
                                            getResources().getString(R.string.ok), AdditionalActivity.this);
                                } else {
                                    String failureResponse = subAdditionals.getMessageAr();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name_ar),
                                            getResources().getString(R.string.ok_ar), AdditionalActivity.this);
                                }
                            }
                            Log.i("TAG", "onResponse: " + data.size());
                        } catch (Exception e) {
                            e.printStackTrace();
                            if (language.equalsIgnoreCase("En")) {
                                Toast.makeText(AdditionalActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(AdditionalActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                            }
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(AdditionalActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(AdditionalActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }

                    Constants.closeLoadingDialog();

                }

                @Override
                public void onFailure(Call<SubAdditionals> call, Throwable t) {
                    Log.d("TAG", "onFailure: " + t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(AdditionalActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(AdditionalActivity.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(AdditionalActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(AdditionalActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }

                    Constants.closeLoadingDialog();

                }
            });
            return null;
        }

        private String prepareVerifyMobileJson() {
            JSONObject parentObj = new JSONObject();
            try {
                parentObj.put("ItemId", itemid);
                parentObj.put("BranchId", branchid);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.d("TAG", "prepareVerifyMobileJson: " + parentObj);
            return parentObj.toString();
        }
    }

    public static class ListUtils {
        public static void setDynamicHeight(ListView mListView) {
            ListAdapter mListAdapter = mListView.getAdapter();
            if (mListAdapter == null) {
                // when adapter is null
                return;
            }
            int height = 0;
            int desiredWidth = View.MeasureSpec.makeMeasureSpec(mListView.getWidth(), View.MeasureSpec.UNSPECIFIED);
            for (int i = 0; i < mListAdapter.getCount(); i++) {
                View listItem = mListAdapter.getView(i, null, mListView);
                listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
                height += listItem.getMeasuredHeight();
            }
            ViewGroup.LayoutParams params = mListView.getLayoutParams();
            params.height = height + (mListView.getDividerHeight() * (mListAdapter.getCount() - 1));
            mListView.setLayoutParams(params);
            mListView.requestLayout();
        }
    }
}

//    public static class Utility {
//
//        public static void setListViewHeightBasedOnChildren(ListView listView) {
//            ListAdapter listAdapter = listView.getAdapter();
//            if (listAdapter == null) {
//                // pre-condition
//                return;
//            }
//
//            int totalHeight = 0;
//            int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.AT_MOST);
//            for (int i = 0; i < listAdapter.getCount(); i++) {
//                View listItem = listAdapter.getView(i, null, listView);
//                listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
//                totalHeight += listItem.getMeasuredHeight();
//            }
//
//            ViewGroup.LayoutParams params = listView.getLayoutParams();
//            params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
//            listView.setLayoutParams(params);
//            listView.requestLayout();
//        }
//    }

