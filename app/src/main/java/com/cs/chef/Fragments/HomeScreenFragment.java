package com.cs.chef.Fragments;

import android.Manifest;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.chef.Activities.ChangeAddressActivity;
import com.cs.chef.Activities.FilterActivity;
import com.cs.chef.Activities.MainActivity;
import com.cs.chef.Activities.SignInActivity;
import com.cs.chef.Activities.SplashScreenActivity;
import com.cs.chef.Adapter.BannersAdapter;
import com.cs.chef.Adapter.FilterCatAdapter;
import com.cs.chef.Adapter.StoresAdapter;
import com.cs.chef.Constants;
import com.cs.chef.Models.ActiveOrders;
import com.cs.chef.Models.Banners;
import com.cs.chef.Models.Brands;
import com.cs.chef.Models.ChangeLanguageList;
import com.cs.chef.Models.Filter;
import com.cs.chef.Models.SearchFilter;
import com.cs.chef.Models.StoreMenuextenditems;
import com.cs.chef.Models.StoresList;
import com.cs.chef.R;
import com.cs.chef.Rest.APIInterface;
import com.cs.chef.Rest.ApiClient;
import com.cs.chef.Utils.GPSTracker;
import com.cs.chef.Utils.NetworkUtil;
import com.daasuu.bl.ArrowDirection;
import com.daasuu.bl.BubbleLayout;
import com.daasuu.bl.BubblePopupHelper;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.warkiz.widget.IndicatorSeekBar;
import com.warkiz.widget.OnSeekChangeListener;
import com.warkiz.widget.SeekParams;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import cc.cloudist.acplibrary.ACProgressFlower;
import cn.trinea.android.view.autoscrollviewpager.AutoScrollViewPager;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static com.cs.chef.Constants.currentLatitude;
import static com.cs.chef.Constants.currentLongitude;
import static com.cs.chef.Constants.isCurrentLocationSelected;

//import com.cs.chef.Dialogs.SearchListDialog;

public class HomeScreenFragment extends Fragment implements View.OnClickListener, OnMapReadyCallback {

    private TextView localityTitle;
    LinearLayout store_layout;
    ImageView locationArrow;
    //    TextView mhome_promoted_text;
    public static String locality;
    private RecyclerView storesListView, bannerListView;

    private AutoScrollViewPager bannersViewpage;
    private StoresAdapter mStoreAdapter;
    private BannersAdapter mBanners;
    //    private LinearLayout locationLayout;
//    LinearLayout promotedlayout, populerlayout, itsnewlayout, aroundlayout, bannerlayout , new_stores_layout;
    LinearLayout bannerlayout;
    //    RelativeLayout lookigsomthinglayout;
//    private RelativeLayout typesLayout;
    //    CircleIndicator defaultIndicator;
    ACProgressFlower dialog;
    SharedPreferences userPrefs;
    public static ArrayList<StoresList.BannerResult> bannerList = new ArrayList<>();
    public static ArrayList<StoresList.StoresDetails> storesList = new ArrayList<>();
    public static ArrayList<StoresList.FilterCategory> filtercatList = new ArrayList<>();
    //    public static ArrayList<StoresList.StoresDetails> itsnewList = new ArrayList<>();
//    public static ArrayList<StoresList.StoresDetails> promotedList = new ArrayList<>();
//    public static ArrayList<StoresList.StoresDetails> threekmList = new ArrayList<>();
    public static ArrayList<StoresList.StoreCategories> somthingList = new ArrayList<>();
    ArrayList<ActiveOrders.Data> data = new ArrayList<>();
    public static boolean filter = false;
    public static float filterdistance, storedistance;

    public static ArrayList<Filter> filterslist = new ArrayList<>();
    public static ArrayList<StoresList.StoresDetails> filter_main_list = new ArrayList<>();
    public static ArrayList<Brands> brandslist = new ArrayList<>();
    public static ArrayList<Banners> bannerslist = new ArrayList<>();

    ArrayList<Integer> seletedbrands = new ArrayList<>();
    ArrayList<String> seletedbanners = new ArrayList<>();
    ArrayList<Integer> filterseletedbrands = new ArrayList<>();
    GPSTracker gps;
    String TAG = "TAGF";
    private static final String[] LOCATION_PERMS = {
            android.Manifest.permission.ACCESS_FINE_LOCATION
    };
    private static final int LOCATION_REQUEST = 1;
    private static final int ADDRESS_REQUEST = 2;
    private Context context;

    //    private TextView popularText, nearbyText, newStoresText, lookingsomthingTex;
//    private TextView lookingText, offertext1, offertext2, shareText1, shareText2;
//    SupportMapFragment mapFragment;
    private GoogleMap mMap;
    //    private LinearLayout mapLayout;
    private NestedScrollView listLayout;
    //    private ImageView mapIcon;
//    public static Double latitude, longitude;
    ImageView mhome_filter;
    String userId;
    AlertDialog customDialog;
    boolean filter_back_btn = false;

//    LinearLayout order_tracking;
//    ImageView order_status;

    private Timer timer = new Timer();

    public static ArrayList<Integer> filterid = new ArrayList<>();

    ArrayList<StoreMenuextenditems> storeMenuextended = new ArrayList<>();
    ArrayList<SearchFilter> searchFilters = new ArrayList<>();
    public static FilterCatAdapter mAdapter;

    private FusedLocationProviderClient mFusedLocationClient;
    View rootView;

    SharedPreferences languagePrefs;
    SharedPreferences.Editor languagePrefsEditor;
    SharedPreferences LocationPrefs;
    SharedPreferences.Editor LocationPrefsEditor;
    String location_status;
    String locality_name, latitute, longitude;

    String language;
    TextView mlanguage;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        languagePrefs = getActivity().getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        languagePrefsEditor = languagePrefs.edit();
        language = languagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            rootView = inflater.inflate(R.layout.home_fragment, container, false);
        } else if (language.equalsIgnoreCase("Ar")) {
            rootView = inflater.inflate(R.layout.home_fragment_arabic, container, false);
        }

        LocationPrefs = getActivity().getSharedPreferences("LOCATION_STATUS", Context.MODE_PRIVATE);
        LocationPrefsEditor = LocationPrefs.edit();
        location_status = LocationPrefs.getString("Location_status", null);
        locality_name = LocationPrefs.getString("LocationName", "");
        latitute = LocationPrefs.getString("Latitute", "");
        longitude = LocationPrefs.getString("Longitute", "");


        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

//        mapFragment = (SupportMapFragment) getChildFragmentManager()
//                .findFragmentById(R.id.map);
//        mapFragment.getMapAsync(this);

        filterslist.clear();
        brandslist.clear();
        bannerslist.clear();
        seletedbanners.clear();

        localityTitle = (TextView) rootView.findViewById(R.id.home_location);
        store_layout = rootView.findViewById(R.id.store_layout);
        locationArrow = rootView.findViewById(R.id.location_arrow);
//        popularText = (TextView) rootView.findViewById(R.id.home_popular_text);
//        nearbyText = (TextView) rootView.findViewById(R.id.home_nearby_text);
//        newStoresText = (TextView) rootView.findViewById(R.id.home_new_text);
//        lookingText = (TextView) rootView.findViewById(R.id.home_looking_text);
//        promotedviewall = (TextView) rootView.findViewById(R.id.promoted_viewall);
//        lookingviewall = (TextView) rootView.findViewById(R.id.looking_viewall);
//        aroundviewall = (TextView) rootView.findViewById(R.id.around__viewall);
//        popularviewall = (TextView) rootView.findViewById(R.id.popular_viewall);
//        itsnewviewall = (TextView) rootView.findViewById(R.id.itsnew__viewall);
//        mhome_promoted_text = (TextView) rootView.findViewById(R.id.home_promoted_text);
//        defaultIndicator = (CircleIndicator) rootView.findViewById(R.id.indicator);

//        offertext1 = (TextView) rootView.findViewById(R.id.offer_text1);
//        offertext2 = (TextView) rootView.findViewById(R.id.offer_text2);
//        shareText1 = (TextView) rootView.findViewById(R.id.share_text1);
//        shareText2 = (TextView) rootView.findViewById(R.id.share_text2);
//        mapIcon = (ImageView) rootView.findViewById(R.id.home_map_icon);
//        order_status = (ImageView) rootView.findViewById(R.id.order_status);
        mhome_filter = rootView.findViewById(R.id.home_filter);

        listLayout = (NestedScrollView) rootView.findViewById(R.id.list_layout);
//        mapLayout = (LinearLayout) rootView.findViewById(R.id.map_layout);
//        locationLayout = (LinearLayout) rootView.findViewById(R.id.location_layout);
//        order_tracking = rootView.findViewById(R.id.order_tracking);
//        typesLayout = (RelativeLayout) rootView.findViewById(R.id.looking_layout);

//        promotedViewPager = (ViewPager) rootView.findViewById(R.id.home_promoted_list);
//        popuplarViewPager = (ViewPager) rootView.findViewById(R.id.home_popular_list);
//        aroundViewPager = (ViewPager) rootView.findViewById(R.id.home_nearby_list);
//        itsnewStoresViewPager = (ViewPager) rootView.findViewById(R.id.home_itsnew_list);
        bannersViewpage = (AutoScrollViewPager) rootView.findViewById(R.id.home_banner_list);
//        lookingStoresViewPager = (ViewPager) rootView.findViewById(R.id.home_looking_list);

//        shimmerPromotedLayout = (ShimmerLayout) rootView.findViewById(R.id.shimmer_promoted_layout);
//        shimmerPopularLayout = (ShimmerLayout) rootView.findViewById(R.id.shimmer_popular_layout);
//        shimmerNearbyLayout = (ShimmerLayout) rootView.findViewById(R.id.shimmer_nearby_layout);
//        shimmerNewStoresLayout = (ShimmerLayout) rootView.findViewById(R.id.shimmer_new_stores_layout);
        storesListView = (RecyclerView) rootView.findViewById(R.id.home_stores_list);

//        shimmerNearbyLayout.startShimmerAnimation();
//        shimmerNewStoresLayout.startShimmerAnimation();
//        shimmerPopularLayout.startShimmerAnimation();
//        shimmerPromotedLayout.startShimmerAnimation();
//        storesListView.showShimmerAdapter();

//        promotedlayout = (LinearLayout) rootView.findViewById(R.id.promoted_layout);
//        populerlayout = (LinearLayout) rootView.findViewById(R.id.popular_layout);
//        itsnewlayout = (LinearLayout) rootView.findViewById(R.id.itsnew_layout);
//        aroundlayout = (LinearLayout) rootView.findViewById(R.id.nearby_layout);
        bannerlayout = (LinearLayout) rootView.findViewById(R.id.banner_layout);
//        new_stores_layout = (LinearLayout) rootView.findViewById(R.id.new_stores_layout);
//        lookigsomthinglayout = (RelativeLayout) rootView.findViewById(R.id.looking_layout);

        userPrefs = getActivity().getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userId = userPrefs.getString("userId", "0");

//        new_stores_layout.setVisibility(View.GONE);

        mlanguage = rootView.findViewById(R.id.language);

        mlanguage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (language.equalsIgnoreCase("En")) {
                    languagePrefsEditor.putString("language", "Ar");
                    languagePrefsEditor.commit();
                    new ChangeLanguageApi().execute();
                    if (storesList.size() > 0) {

                        try {
                            mBanners = new BannersAdapter(getContext(), bannerslist, language);
                            bannersViewpage.setAdapter(mBanners);
                            mBanners.notifyDataSetChanged();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
//                    mBanners.notifyDataSetChanged();
                    Intent intent = new Intent(getActivity(), MainActivity.class);
                    intent.putExtra("class", "splash");
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    getActivity().finish();
                } else {
                    languagePrefsEditor.putString("language", "En");
                    languagePrefsEditor.commit();
                    new ChangeLanguageApi().execute();
                    if (storesList.size() > 0) {
                        try {
                            mBanners = new BannersAdapter(getContext(), bannerslist, language);
                            bannersViewpage.setAdapter(mBanners);
                            mBanners.notifyDataSetChanged();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
//                    mBanners.notifyDataSetChanged();
                    Intent intent = new Intent(getActivity(), MainActivity.class);
                    intent.putExtra("class", "splash");
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    getActivity().finish();
                }

            }
        });

        bannersViewpage.setInterval(3000);
        bannersViewpage.setCycle(true);
        bannersViewpage.startAutoScroll();


        if (location_status != null){

            currentLatitude = Double.valueOf(latitute);
            currentLongitude = Double.valueOf(longitude);
            localityTitle.setText("" + locality_name);

            if (filter) {
                new GetFilterApi().execute();
            } else {
                new GetStoresApi().execute();
            }

            Log.d(TAG, "canAccessLocation(): "+canAccessLocation());
            if (canAccessLocation()) {
                try {
                    gps = new GPSTracker(getActivity());
                    getGPSCoordinates();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

//        promotedviewall.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(getActivity(), PromotedViewallScreenActivity.class);
//                intent.putExtra("viewaall", "Promoted");
//                startActivity(intent);
//            }
//        });
//        aroundviewall.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(getActivity(), PromotedViewallScreenActivity.class);
//                intent.putExtra("viewaall", "Around");
//                startActivity(intent);
//            }
//        });
//        popularviewall.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(getActivity(), PromotedViewallScreenActivity.class);
//                intent.putExtra("viewaall", "Popular");
//                startActivity(intent);
//            }
//        });
//        itsnewviewall.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(getActivity(), PromotedViewallScreenActivity.class);
//                intent.putExtra("viewaall", "Itsnew");
//                startActivity(intent);
//            }
//        });
//
//        lookingviewall.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                FragmentManager fragmentManager = getFragmentManager();
//                Fragment SearchFragment = new SearchFragment();
//                fragmentManager.beginTransaction().replace(R.id.fragment_layout, SearchFragment).commit();
//            }
//        });

        LocalBroadcastManager.getInstance(context).registerReceiver(
                mFilter, new IntentFilter("FilterUpdate"));


        if (storesList.size() > 0) {
            for (int i = 0; i < filterslist.size(); i++) {
                mStoreAdapter = new StoresAdapter(getContext(), filterslist.get(i).getBrands(), getActivity(), language);
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
                storesListView.setLayoutManager(mLayoutManager);
                storesListView.setAdapter(mStoreAdapter);
            }
//            mPromotedAdapter = new PromotedStoresAdapter(getContext(), storesList);
//            mPopularAdapter = new PopularStoresAdapter(getContext(), storesList);
//            mAround = new Around3kmStoresAdapter(getContext(), storesList);
//            mItsnewAdapter = new ItsNewStoresAdapter(getContext(), storesList);

//            mLookingAdapter = new HomeLookingForStoresAdapter(getContext(), somthingList);
//            promotedViewPager.setAdapter(mPromotedAdapter);
//            popuplarViewPager.setAdapter(mPopularAdapter);
//            aroundViewPager.setAdapter(mAround);
//            itsnewStoresViewPager.setAdapter(mItsnewAdapter);
            try {
                mBanners = new BannersAdapter(getContext(), bannerslist, language);
                bannersViewpage.setAdapter(mBanners);
                mBanners.notifyDataSetChanged();
            } catch (Exception e) {
                e.printStackTrace();
            }
//            lookingStoresViewPager.setAdapter(mLookingAdapter);

//            shimmerPromotedLayout.stopShimmerAnimation();
//            shimmerPopularLayout.stopShimmerAnimation();
//            shimmerNearbyLayout.stopShimmerAnimation();
//            shimmerNewStoresLayout.stopShimmerAnimation();
//            shimmerSomthingLayout.stopShimmerAnimation();

//            shimmerPromotedLayout.setVisibility(View.GONE);
//            shimmerPopularLayout.setVisibility(View.GONE);
//            shimmerNearbyLayout.setVisibility(View.GONE);
//            shimmerNewStoresLayout.setVisibility(View.GONE);

//            promotedViewPager.setVisibility(View.VISIBLE);
//            popuplarViewPager.setVisibility(View.VISIBLE);
//            aroundViewPager.setVisibility(View.VISIBLE);
//            itsnewStoresViewPager.setVisibility(View.VISIBLE);
            bannersViewpage.setVisibility(View.VISIBLE);
//            typesLayout.setVisibility(View.VISIBLE);
//            lookingStoresViewPager.setVisibility(View.VISIBLE);
        }
//        if (brandslist.size() == 0) {
//            Constants.showOneButtonAlertDialog("Stores Not Availabe", getResources().getString(R.string.appname),
//                    getResources().getString(R.string.Done), getActivity());
//        }


//        if (userId.equals("0")) {
//            order_tracking.setVisibility(View.GONE);
//        } else {
//            String networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
//            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
//                new ActiveOrderApi().execute();
//            } else {
//                Toast.makeText(getActivity(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
//            }
//        }


//        lookigsomthinglayout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Bundle args = new Bundle();
////                    Log.d("TAG", "storeArrayList "+storesArrayList.size());
////                    Log.d("TAG", "pos1"+getAdapterPosition());
//                args.putSerializable("array2", storesList);
////                args.putInt("pos2", getArguments());
//
//                final SearchListDialog newFragment = SearchListDialog.newInstance();
//                newFragment.setCancelable(true);
//                newFragment.setArguments(args);
//                newFragment.show(((AppCompatActivity) context).getSupportFragmentManager(), "store1");
////                    Pair<View, String> p1 = Pair.create((View)store_image, "store_image");
////                    Pair<View, String> p2 = Pair.create((View)store_name, "store_name");
////                    Pair<View, String> p3= Pair.create((View)store_type, "store_type");
////                    Intent intent = new Intent(context, StoreMenuActivity.class);
////                    intent.putExtra("array", storesArrayList.get(getPosition()));
////                    ActivityOptionsCompat options = ActivityOptionsCompat.
////                            makeSceneTransitionAnimation(activity, p1, p2, p3);
////                    context.startActivity(intent, options.toBundle());
//            }
//        });
//
        store_layout.setOnClickListener(this);
//        mapIcon.setOnClickListener(this);

//        LocalBroadcastManager.getInstance(context).registerReceiver(
//                mMessageReceiver, new IntentFilter("GPSLocationUpdates"));

//        ProgressDialog progress = new ProgressDialog(getContext());
//        progress.setTitle("Loading");
//        progress.setMessage("Wait while loading...");
//        progress.setCancelable(false); // disable dismiss by tapping outside of the dialog
//        progress.show();
////      To dismiss the dialog
//        progress.dismiss();
        return rootView;
    }

    private String prepareChangeLangJson() {
        JSONObject parentObj = new JSONObject();

        try {
            parentObj.put("Flag", 1);
            parentObj.put("UserId", userId);
            parentObj.put("Language", language);
            parentObj.put("deviceToken", SplashScreenActivity.regId);
            parentObj.put("DeviceType","android");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return parentObj.toString();
    }

    private class ChangeLanguageApi extends AsyncTask<String, Integer, String> {

        //        ACProgressFlower dialog;
        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareChangeLangJson();
//            dialog = new ACProgressFlower.Builder(getActivity())
//                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
//                    .themeColor(Color.WHITE)
//                    .fadeColor(Color.DKGRAY).build();
//            dialog.show();
//            Constants.showLoadingDialog(getActivity());

        }

        @Override
        protected String doInBackground(String... strings) {
            APIInterface apiService = ApiClient.getClient().create(APIInterface.class);

            Call<ChangeLanguageList> call = apiService.getchangelang(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<ChangeLanguageList>() {
                @Override
                public void onResponse(Call<ChangeLanguageList> call, Response<ChangeLanguageList> response) {
                    if (response.isSuccessful()) {
                        ChangeLanguageList Response = response.body();


                        Log.i("TAG", "onResponse: " + Response.getMessage());

                    }
                }

                @Override
                public void onFailure(Call<ChangeLanguageList> call, Throwable t) {

                }
            });
            return null;
        }
    }


    private class MyTimerTask extends TimerTask {

        @Override
        public void run() {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    String networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
                    if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        new GetStoresApi().execute();

                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(getActivity(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getActivity(), R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            });
        }
    }


    public void showloaderAlertDialog() {

        mhome_filter.setVisibility(View.GONE);

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getLayoutInflater();
        int layout;
        if (language.equalsIgnoreCase("En")) {
            layout = R.layout.filter_screen;
        } else {
            layout = R.layout.filter_screen_arabic;
        }
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);

        final TextView one = dialogView.findViewById(R.id.one);
        final TextView ten = dialogView.findViewById(R.id.ten);
        TextView done = dialogView.findViewById(R.id.done);
        TextView clear = dialogView.findViewById(R.id.clear);
        final RecyclerView menu_list = dialogView.findViewById(R.id.recycler_view);
        final IndicatorSeekBar seekBar = dialogView.findViewById(R.id.seek_bar);
        ImageView filterback = dialogView.findViewById(R.id.filter_back);


        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        menu_list.setLayoutManager(mLayoutManager);
        mAdapter = new FilterCatAdapter(getActivity(), filtercatList, language);
        menu_list.setAdapter(mAdapter);

        seekBar.setOnSeekChangeListener(new OnSeekChangeListener() {
            @Override
            public void onSeeking(SeekParams seekParams) {
                Log.i(TAG, "showloaderAlertDialog1: " + seekBar.getProgress());

                filterdistance = seekBar.getProgress();

                if (seekBar.getProgress() == 1) {
                    one.setVisibility(View.GONE);
                } else {
                    one.setVisibility(View.VISIBLE);
                }
                if (seekBar.getProgress() == 10) {
                    ten.setVisibility(View.GONE);
                } else {
                    ten.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void onStartTrackingTouch(IndicatorSeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(IndicatorSeekBar seekBar) {

            }
        });

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (filterid != null || filterid.size() > 0) {
                    HomeScreenFragment.filter = true;
                } else {
                    HomeScreenFragment.filter = false;
                }
                mhome_filter.setVisibility(View.VISIBLE);
                customDialog.dismiss();

            }
        });

        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                HomeScreenFragment.filter = false;
                filterid.clear();
                for (int i = 0; i < filtercatList.size(); i++) {
                    for (int j = 0; j < filtercatList.get(i).getFilters().size(); j++) {
                        filterid.add(filtercatList.get(i).getFilters().get(j).getFilterId());
                    }
                }
                mAdapter = new FilterCatAdapter(getActivity(), filtercatList, language);
                menu_list.setAdapter(mAdapter);
//                customDialog.dismiss();

            }
        });

        filterback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mhome_filter.setVisibility(View.VISIBLE);
                customDialog.dismiss();

            }
        });

        customDialog = dialogBuilder.create();
        customDialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = customDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        Display display = getActivity().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x;

        double d = screenWidth * 0.95;
        lp.width = (int) d;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);


    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

//    private void flipCard() {
//        FlipAnimation flipAnimation = new FlipAnimation(listLayout, mapLayout);
//
//        if (listLayout.getVisibility() == View.GONE) {
//            flipAnimation.reverse();
//            mapIcon.setImageDrawable(getResources().getDrawable(R.drawable.home_screen_map_icon));
//        } else {
//            mapIcon.setImageDrawable(getResources().getDrawable(R.drawable.tablelist));
//        }
//        mapLayout.startAnimation(flipAnimation);
//    }

    private void setTypeface() {
//        localityTitle.setTypeface(Constants.getTypeFace(context));
//        popularText.setTypeface(Constants.getTypeFace(context));
//        nearbyText.setTypeface(Constants.getTypeFace(context));
//        newStoresText.setTypeface(Constants.getTypeFace(context));
//        lookingText.setTypeface(Constants.getTypeFace(context));
//        offertext1.setTypeface(Constants.getTypeFace(context));
//        offertext2.setTypeface(Constants.getTypeFace(context));
//        shareText1.setTypeface(Constants.getTypeFace(context));
//        shareText2.setTypeface(Constants.getTypeFace(context));
    }

    private boolean canAccessLocation() {
        return (hasPermission(android.Manifest.permission.ACCESS_FINE_LOCATION));
    }

    private boolean hasPermission(String perm) {
        return (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(context, perm));
    }

    public void getGPSCoordinates() throws IOException {

        if (gps != null) {
            if (gps.canGetLocation()) {

//                currentLatitude = gps.getLatitude();
//                currentLongitude = gps.getLongitude();
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //Do something after 100ms
                        showLocationChangedPopup(gps.getLatitude(), gps.getLongitude());
                    }
                }, 2000);

                Log.i("TAG", "fused lat " + currentLatitude);
                Log.i("TAG", "fused long " + currentLongitude);

                try {

                    if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }



//                if (filter) {
//                    new GetFilterApi().execute();
//                } else {
//                    new GetStoresApi().execute();
//                }
            } else {
                // can't get location
                // GPS or Network is not enabled
                // Ask user to enable GPS/network in settings
//                gps.showSettingsAlert();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {

            case LOCATION_REQUEST:
                if (canAccessLocation()) {
                    gps = new GPSTracker(getActivity());
                    try {
                        getGPSCoordinates();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    if (language.equalsIgnoreCase("En")) {
                        Toast.makeText(getActivity(), "Location permission denied, Unable to show nearby stores", Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getActivity(), "Location permission denied, Unable to show nearby stores", Toast.LENGTH_LONG).show();
                    }
                }
                break;
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (gps != null) {
            gps.stopUsingGPS();
        }
        Constants.closeLoadingDialog();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (gps != null) {
            gps.stopUsingGPS();
        }
        timer.cancel();
        Constants.closeLoadingDialog();
    }

//    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
//        @Override
//        public void onReceive(Context context, Intent intent) {
//            Log.d(TAG, "onBroadcastReceive: ");
//            // Get extra data included in the Intent
//            Bundle b = intent.getBundleExtra("Location");
//            Location location = (Location) b.getParcelable("Location");
//            if (location != null) {
//                if (isCurrentLocationSelected) {
//                    currentLatitude = location.getLatitude();
//                    currentLongitude = location.getLongitude();
//                    if (mMap != null) {
//                        mMap.clear();
//                        LatLng latLng = new LatLng(currentLatitude, currentLongitude);
//                        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
//                        mMap.moveCamera(CameraUpdateFactory.zoomTo(15.0f));
//                        MarkerOptions markerOptions = new MarkerOptions();
//                        markerOptions.position(latLng);
//                        mMap.addMarker(markerOptions);
//                    }
//                    getLocality();
//                }
//
//            }
//        }
//    };

    private BroadcastReceiver mFilter = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG, "onBroadcastReceive: Filter");
            // Get extra data included in the Intent
            filterslist.clear();
            filterseletedbrands.clear();
            bannerslist.clear();
            seletedbanners.clear();

//            for (int j = 0; j < brandslist.size(); j++) {
//
//                for (int k = 0; k < brandslist.get(j).getBrands().size(); k++) {
//
////                                    if (storesList.get(i).getDiscountamt() > 0) {
//                    Filter filter = new Filter();
//                    filter.setBrandId(storesList.get(i).getBrandId());
//                    filter.setBradNameEn(storesList.get(i).getBrandName_En());
//                    filter.setBranddescAr(storesList.get(i).getBrandName_Ar());
//                    filter.setBrandimageEn(storesList.get(i).getStoreImage_En());
//                    filter.setBranddescAr(storesList.get(i).getBranchDescription_Ar());
//                    filter.setBranddescEn(storesList.get(i).getBranchDescription_En());
//
//                    ArrayList<StoresList.StoresDetails> fillterstore = new ArrayList<>();
//                    for (int l = 0; l < brandslist.size(); l++) {
//                        for (int m = 0; m < brandslist.get(l).getBrands().size(); m++) {
//                            if (storesList.get(i).getBrandId() == brandslist.get(l).getBrands().get(m).getBrandId()) {
//                                fillterstore.add(brandslist.get(l).getBrands().get(m));
//                            }
//                        }
//                    }
//                    filter.setBrands(fillterstore);
//                    filterslist.add(filter);
//
////                                    }
//                }
//            }
//
//            for (int l = 0; l < filterslist.size(); l++) {
//                Collections.sort(filterslist.get(l).getBrands(), Brands.distanceSort);
//            }
//
//            Log.i(TAG, "brands_size: " + brandslist.size());
//            Log.i(TAG, "filter_size " + filterslist.size());
//
//            Log.i(TAG, "filter " + filter);


            if (filter) {
                if (FilterActivity.Relavance.isChecked()) {

                    seletedbanners.clear();
                    for (int i = 0; i < bannerList.size(); i++) {

                        if (!seletedbanners.contains(bannerList.get(i).getBannerName_En())) {
                            seletedbanners.add(bannerList.get(i).getBannerName_En());
                            Banners banners = new Banners();
                            banners.setBannerNameEn(bannerList.get(i).getBannerName_En());
                            banners.setBrandimageEn(bannerList.get(i).getBannerImage());
                            banners.setBannerNameAr(bannerList.get(i).getBannerName_Ar());

                            ArrayList<StoresList.BannerResult> fillterstore = new ArrayList<>();
                            for (int j = 0; j < bannerList.size(); j++) {
                                if (bannerList.get(i).getBannerName_En() == bannerList.get(j).getBannerName_En()) {
                                    fillterstore.add(bannerList.get(j));
                                    for (int k = 0; k < bannerList.get(i).getStoreDetails().size(); k++) {
                                        ArrayList<StoresList.StoreDetails> bannerlist = new ArrayList<>();
                                        for (int m = 0; m < bannerList.get(j).getStoreDetails().size(); m++) {

//                                            if (bannerList.get(i).getStoreDetails().get(k).getBrandId() == bannerList.get(j).getStoreDetails().get(m).getBrandId()) {
                                            bannerlist.add(bannerList.get(j).getStoreDetails().get(m));
//                                            }

                                        }

                                        banners.setBannerList(bannerlist);

                                    }
                                }
                            }


                            banners.setBanners(fillterstore);
                            bannerslist.add(banners);

                        }

                        Log.d(TAG, "" + brandslist.size());

                    }

                    for (int l = 0; l < bannerslist.size(); l++) {
                        Collections.sort(bannerslist.get(l).getBannerList(), Banners.distanceSort);
                    }

                    filterseletedbrands.clear();
                    for (int i = 0; i < storesList.size(); i++) {

//                                            if (storesList.get(i).getStorestatus().equals("Open")) {
                        Filter filter = new Filter();

                        filterseletedbrands.add(storesList.get(i).getBrandId());
                        filter.setBrandId(storesList.get(i).getBrandId());
                        filter.setBradNameEn(storesList.get(i).getBrandName_En());
                        filter.setBranddescAr(storesList.get(i).getBrandName_Ar());
                        filter.setBrandimageEn(storesList.get(i).getStoreImage_En());
                        filter.setBranddescAr(storesList.get(i).getBranchDescription_Ar());
                        filter.setBranddescEn(storesList.get(i).getBranchDescription_En());

                        ArrayList<StoresList.StoresDetails> fillterstore = new ArrayList<>();
                        for (int j = 0; j < storesList.size(); j++) {
//                            if (storesList.get(i).getBrandId() == storesList.get(j).getBrandId()) {
                            fillterstore.add(storesList.get(j));
//                            }

                        }
                        filter.setBrands(fillterstore);
                        filter_main_list.addAll(fillterstore);
                        filterslist.add(filter);
//                                            }

                    }

                    for (int l = 0; l < filterslist.size(); l++) {
                        Collections.sort(filterslist.get(l).getBrands(), Filter.distanceSort);
                    }
                    Log.i(TAG, "Relavence ");


                }
            }

            Log.i(TAG, "brandslistsort " + brandslist);

            for (int i = 0; i < filterslist.size(); i++) {
                mStoreAdapter = new StoresAdapter(getContext(), filterslist.get(i).getBrands(), getActivity(), language);
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
                storesListView.setLayoutManager(mLayoutManager);
                storesListView.setAdapter(mStoreAdapter);
            }

//            promotedList.clear();
//            threekmList.clear();
//            itsnewList.clear();
//            popularList.clear();


//            for (int j = 0; j < filterslist.size(); j++)

//            {
//                for (int k = 0; k < filterslist.get(j).getBrands().size(); k++) {

//                    if (filterslist.get(j).getBrands().get(k).getPopular()) {
//                        popularList.add(filterslist.get(j).getBrands().get(k));
//                    }
//                    if (filterslist.get(j).getBrands().get(k).getIsNew()) {
//                        itsnewList.add(filterslist.get(j).getBrands().get(k));
//                    }
//                    if (filterslist.get(j).getBrands().get(k).getIsPromoted()) {
//                        promotedList.add(filterslist.get(j).getBrands().get(k));
//                    }
//                    if (filterslist.get(j).getBrands().get(k).getDistance() <= 3) {
//                        threekmList.add(filterslist.get(j).getBrands().get(k));
//                    }

//                    if (promotedList.size() == 0) {
//                        promotedlayout.setVisibility(View.GONE);
//                    } else {
//                        promotedlayout.setVisibility(View.VISIBLE);
//                        mPromotedAdapter = new PromotedStoresAdapter(getContext(), promotedList);
//                        promotedViewPager.setAdapter(mPromotedAdapter);
//                    }
//
//                    if (popularList.size() == 0) {
//                        populerlayout.setVisibility(View.GONE);
//                    } else {
//                        populerlayout.setVisibility(View.VISIBLE);
//                        mPopularAdapter = new PopularStoresAdapter(getContext(), popularList);
//                        popuplarViewPager.setAdapter(mPopularAdapter);
//                    }
//
//                    if (threekmList.size() == 0) {
//                        aroundlayout.setVisibility(View.GONE);
//                    } else {
//                        aroundlayout.setVisibility(View.VISIBLE);
//                        mAround = new Around3kmStoresAdapter(getContext(), threekmList);
//                        aroundViewPager.setAdapter(mAround);
//                    }
//
//                    if (itsnewList.size() == 0) {
//                        itsnewlayout.setVisibility(View.GONE);
//                    } else {
//                        itsnewlayout.setVisibility(View.VISIBLE);
//                        mItsnewAdapter = new ItsNewStoresAdapter(getContext(), itsnewList);
//                        itsnewStoresViewPager.setAdapter(mItsnewAdapter);
//                    }

//            if (bannerList.size() == 0) {
//                bannerlayout.setVisibility(View.GONE);
//            } else {
            bannerlayout.setVisibility(View.VISIBLE);
            try {
                mBanners = new BannersAdapter(getContext(), bannerslist, language);
//                                    defaultIndicator.setViewPager(bannersViewpage);
                bannersViewpage.setAdapter(mBanners);
                mBanners.notifyDataSetChanged();
            } catch (Exception e) {
                e.printStackTrace();
            }
//            }
//                    if (somthingList.size() == 0) {
//                        lookigsomthinglayout.setVisibility(View.GONE);
//                    } else {
//                        lookigsomthinglayout.setVisibility(View.VISIBLE);
//                        mLookingAdapter = new HomeLookingForStoresAdapter(getContext(), somthingList);
//                        lookingStoresViewPager.setAdapter(mLookingAdapter);
//                    }
//
//                    shimmerPromotedLayout.stopShimmerAnimation();
//                    shimmerPopularLayout.stopShimmerAnimation();
//                    shimmerNearbyLayout.stopShimmerAnimation();
//                    shimmerNewStoresLayout.stopShimmerAnimation();
//
//                    shimmerPromotedLayout.setVisibility(View.GONE);
//                    shimmerPopularLayout.setVisibility(View.GONE);
//                    shimmerNearbyLayout.setVisibility(View.GONE);
//                    shimmerNewStoresLayout.setVisibility(View.GONE);
//
//                    promotedViewPager.setVisibility(View.VISIBLE);
//                    popuplarViewPager.setVisibility(View.VISIBLE);
//                    aroundViewPager.setVisibility(View.VISIBLE);
//                    itsnewStoresViewPager.setVisibility(View.VISIBLE);
            bannersViewpage.setVisibility(View.VISIBLE);
//                    typesLayout.setVisibility(View.VISIBLE);
//                    lookingStoresViewPager.setVisibility(View.VISIBLE);

//                }
        }


//        }
    };

    public void getLocality() {
        Location mLocation = new Location("");
        mLocation.setLatitude(currentLatitude);
        mLocation.setLongitude(currentLongitude);

        Geocoder geocoder = null;
        try {
            geocoder = new Geocoder(context, Locale.getDefault());
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Address found using the Geocoder.
        List<Address> addresses = null;
        String errorMessage = "";

        try {
            addresses = geocoder.getFromLocation(
                    mLocation.getLatitude(),
                    mLocation.getLongitude(),
                    // In this sample, we get just a single address.
                    1);
        } catch (IOException ioException) {
            // Catch network or other I/O problems.
            errorMessage = context.getString(R.string.service_not_available);
            Log.e(TAG, errorMessage, ioException);
        } catch (IllegalArgumentException illegalArgumentException) {
            // Catch invalid latitude or longitude values.
            errorMessage = context.getString(R.string.invalid_lat_long_used);
            Log.e(TAG, errorMessage + ". " +
                    "Latitude = " + mLocation.getLatitude() +
                    ", Longitude = " + mLocation.getLongitude(), illegalArgumentException);
        }

        if (addresses == null || addresses.size() == 0) {
            if (errorMessage.isEmpty()) {
                errorMessage = getString(R.string.no_address_found);
                Log.e(TAG, errorMessage);
            }
        } else {
            Address address = addresses.get(0);
            Log.d(TAG, "getLocality: " + address);
            if (address.getSubLocality() != null) {
                localityTitle.setText(address.getSubLocality());
                LocationPrefsEditor.putString("LocationName", address.getSubLocality());
            }
            else {
                localityTitle.setText(address.getLocality());
                LocationPrefsEditor.putString("LocationName", address.getLocality());
            }

            Log.d(TAG, "getLocality: "+currentLatitude);
            LocationPrefsEditor.putString("Location_status", "locationON");
            LocationPrefsEditor.putString("Latitute", String.valueOf(currentLatitude));
            LocationPrefsEditor.putString("Longitute", String.valueOf(currentLongitude));
            LocationPrefsEditor.commit();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.store_layout:
                Constants.addressboolean = false;
                Constants.homeboolean = true;
                userId = userPrefs.getString("userId", "0");
                if (userId.equals("0")) {
                    Intent a = new Intent(getActivity(), SignInActivity.class);
                    startActivity(a);
                } else {
                    Intent intent = new Intent(getContext(), ChangeAddressActivity.class);
                    startActivityForResult(intent, ADDRESS_REQUEST);
                }
                break;

//            case R.id.home_map_icon:
//                flipCard();
//                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == ADDRESS_REQUEST && resultCode == RESULT_OK) {
            currentLatitude = data.getDoubleExtra("lat", 0);
            currentLongitude = data.getDoubleExtra("longi", 0);
            locality = data.getStringExtra("loc");
            Log.i("TAG", "currentlat: " + currentLatitude);
            if (currentLatitude == 0) {

                Log.i("TAG", "currentlat1: " + currentLatitude);

//                isCurrentLocationSelected = true;

                try {
                    gps = new GPSTracker(getActivity());
                    ChangeLocationCoordinates();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            } else {

                isCurrentLocationSelected = false;

                if (mMap != null) {
                    mMap.clear();
                    LatLng latLng = new LatLng(currentLatitude, currentLongitude);
                    mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                    mMap.moveCamera(CameraUpdateFactory.zoomTo(15.0f));
                    MarkerOptions markerOptions = new MarkerOptions();
                    markerOptions.position(latLng);
                    mMap.addMarker(markerOptions);
                }

                if (filter) {
                    new GetFilterApi().execute();
                } else {
                    new GetStoresApi().execute();
                }

            }

            if (locality == null || locality.equals("")) {
                getLocality();
            } else {
                localityTitle.setText(locality);
            }

        }
        super.onActivityResult(requestCode, resultCode, data);

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        if (canAccessLocation()) {
            mMap = googleMap;
            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            mMap.setMyLocationEnabled(true);
            mMap.getUiSettings().setMyLocationButtonEnabled(false);
            mMap.clear();
        }
//        LatLng latLng = new LatLng(latitude, longitude);
//        MarkerOptions markerOptions = new MarkerOptions();
//        markerOptions.position(latLng);
//        mMap.addMarker(markerOptions);
    }


    private class ActiveOrderApi extends AsyncTask<String, String, String> {

        //        ACProgressFlower dialog;
        String inputStr;
        AlertDialog loaderDialog = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareVerifyMobileJson();
//            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
//            // ...Irrelevant code for customizing the buttons and title
//            LayoutInflater inflater = getLayoutInflater();
//            int layout = R.layout.loder_dialog;
//            View dialogView = inflater.inflate(layout, null);
//            dialogBuilder.setView(dialogView);
//            dialogBuilder.setCancelable(true);
//
//            loaderDialog = dialogBuilder.create();
//            loaderDialog.show();
//            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
//            Window window = loaderDialog.getWindow();
//            window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//            lp.copyFrom(window.getAttributes());
//            //This makes the dialog take up the full width
//            Display display = getActivity().getWindowManager().getDefaultDisplay();
//            Point size = new Point();
//            display.getSize(size);
//            int screenWidth = size.x;
//
//            double d = screenWidth*0.85;
//            lp.width = (int) d;
//            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
//            window.setAttributes(lp);
//            dialog = new ACProgressFlower.Builder(getActivity())
//                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
//                    .themeColor(Color.WHITE)
//                    .fadeColor(Color.DKGRAY).build();
//            dialog.show();
            Constants.showLoadingDialog(getActivity());
        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<ActiveOrders> call = apiService.getActiveOrders(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<ActiveOrders>() {
                @Override
                public void onResponse(Call<ActiveOrders> call, Response<ActiveOrders> response) {
                    Log.d("TAG", "onResponse: " + response);
                    if (response.isSuccessful()) {
                        ActiveOrders orderItems = response.body();
                        data = orderItems.getData();
                        try {
                            if (orderItems.getStatus()) {
                                String message = orderItems.getMessage();

                                for (int i = 0; i < data.size(); i++) {
                                    if (data.get(i).getOrderStatus().equals("Close")) {

//                                        order_tracking.setVisibility(View.GONE);

                                    } else {

//                                        order_tracking.setVisibility(View.VISIBLE);

                                        if (data.get(i).getOrderStatus().equals("New")) {

//                                            Glide.with(getActivity())
//                                                    .load(R.drawable.track_order_black)
//                                                    .into(order_status);

                                        } else if (data.get(i).getOrderStatus().equals("Accepted")) {

//                                            Glide.with(getActivity())
//                                                    .load(R.drawable.track_confirmed_black)
//                                                    .into(order_status);

                                        } else if (data.get(i).getOrderStatus().equals("Ready")) {

//                                            Glide.with(getActivity())
//                                                    .load(R.drawable.track_ready_black)
//                                                    .into(order_status);

                                        } else if (data.get(i).getOrderStatus().equals("Served")) {

//                                            Glide.with(getActivity())
//                                                    .load(R.drawable.track_delivered_black)
//                                                    .into(order_status);

                                        } else if (data.get(i).getOrderStatus().equals("Delivered")) {

//                                            Glide.with(getActivity())
//                                                    .load(R.drawable.track_delivered_black)
//                                                    .into(order_status);

                                        }
                                        final int finalI = i;
                                        final int finalI1 = i;
//                                        order_tracking.setOnClickListener(new View.OnClickListener() {
//                                            @Override
//                                            public void onClick(View v) {
//                                                Intent a = new Intent(getActivity(), TrackOrderActivity.class);
//                                                a.putExtra("userid", data.get(finalI).getUserId());
//                                                a.putExtra("orderid", data.get(finalI).getOrderId());
//                                                a.putExtra("track_order", "home_screen");
//                                                startActivity(a);

                                        Log.i(TAG, "userid: " + data.get(finalI1).getUserId());
                                        Log.i(TAG, "orderid: " + data.get(finalI).getOrderId());
//                                            }
//                                        });
                                    }
                                }


                            } else {
                                //                          status false case
                                if (language.equalsIgnoreCase("En")) {
                                    String failureResponse = orderItems.getMessage();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name),
                                            getResources().getString(R.string.ok), getActivity());
                                } else {

                                    String failureResponse = orderItems.getMessageAr();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name),
                                            getResources().getString(R.string.ok), getActivity());

                                }
                            }
                            Log.i("TAG", "onResponse: " + data.size());
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(getActivity(), getResources().getString(R.string.cannot_reach_server), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(getActivity(), getResources().getString(R.string.cannot_reach_server), Toast.LENGTH_SHORT).show();
                    }

                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<ActiveOrders> call, Throwable t) {
                    Log.d("TAG", "onFailure: " + t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(getActivity(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getActivity(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }

                    Constants.closeLoadingDialog();
                }
            });
            return null;
        }

        private String prepareVerifyMobileJson() {
            JSONObject parentObj = new JSONObject();
            try {

                parentObj.put("UserId", userId);

            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.d("TAG", "prepareVerifyMobileJson: " + parentObj);
            return parentObj.toString();
        }
    }

    private class GetStoresApi extends AsyncTask<String, Integer, String> {

        String inputStr;
//        AlertDialog loaderDialog = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareGetStoresJSON();
//            dialog = new ACProgressFlower.Builder(getContext())
//                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
//                    .themeColor(Color.WHITE)
//                    .fadeColor(Color.DKGRAY).build();
//            dialog.show();
            Constants.showLoadingDialog(getActivity());
//            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
//            // ...Irrelevant code for customizing the buttons and title
//            LayoutInflater inflater = getLayoutInflater();
//            int layout = R.layout.loder_dialog;
//            View dialogView = inflater.inflate(layout, null);
//            dialogBuilder.setView(dialogView);
//            dialogBuilder.setCancelable(true);
//
//            loaderDialog = dialogBuilder.create();
//            loaderDialog.show();
//            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
//            Window window = loaderDialog.getWindow();
//            window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//            lp.copyFrom(window.getAttributes());
//            //This makes the dialog take up the full width
//            Display display = getActivity().getWindowManager().getDefaultDisplay();
//            Point size = new Point();
//            display.getSize(size);
//            int screenWidth = size.x;
//
//            double d = screenWidth*0.85;
//            lp.width = (int) d;
//            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
//            window.setAttributes(lp);
        }

        @Override
        protected String doInBackground(final String... strings) {
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<StoresList> call = apiService.getStoresList(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<StoresList>() {
                @Override
                public void onResponse(Call<StoresList> call, Response<StoresList> response) {
                    Log.i("TAG", "store servies responce " + response);
                    if (response.isSuccessful()) {

                        StoresList stores = response.body();
                        if (stores.getStatus()) {
                            storesList = stores.getData().getStoresDetails();
                            bannerList = stores.getData().getBannerResult();
                            somthingList = stores.getData().getStoreCategories();
                            filtercatList = stores.getData().getFilterCategory();

                            mhome_filter.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                    showloaderAlertDialog();

                                }
                            });


                            brandslist.clear();
                            filterslist.clear();
                            bannerslist.clear();

                            for (int i = 0; i < storesList.size(); i++) {
                                if (!seletedbrands.contains(storesList.get(i).getBrandId())) {
                                    seletedbrands.add(storesList.get(i).getBrandId());
                                    Brands brands = new Brands();
                                    brands.setBrandId(storesList.get(i).getBrandId());
                                    brands.setBradNameEn(storesList.get(i).getBrandName_En());
                                    brands.setBranddescAr(storesList.get(i).getBrandName_Ar());
                                    brands.setBrandimageEn(storesList.get(i).getStoreImage_En());
                                    brands.setBranddescAr(storesList.get(i).getBranchDescription_Ar());
                                    brands.setBranddescEn(storesList.get(i).getBranchDescription_En());

                                    ArrayList<StoresList.StoresDetails> fillterstore = new ArrayList<>();
                                    for (int j = 0; j < storesList.size(); j++) {
                                        if (storesList.get(i).getBrandId() == storesList.get(j).getBrandId()) {
                                            fillterstore.add(storesList.get(j));
                                        }
                                    }
                                    brands.setBrands(fillterstore);
                                    brandslist.add(brands);

                                }

                                Log.d(TAG, "" + brandslist.size());
                            }

                            seletedbanners.clear();
                            for (int i = 0; i < bannerList.size(); i++) {
                                if (!seletedbanners.contains(bannerList.get(i).getBannerName_En())) {
                                    seletedbanners.add(bannerList.get(i).getBannerName_En());
                                    Banners banners = new Banners();
                                    banners.setBannerNameEn(bannerList.get(i).getBannerName_En());
                                    banners.setBrandimageEn(bannerList.get(i).getBannerImage());
                                    banners.setBannerNameAr(bannerList.get(i).getBannerName_Ar());

                                    ArrayList<StoresList.BannerResult> fillterstore = new ArrayList<>();
                                    for (int j = 0; j < bannerList.size(); j++) {
                                        if (bannerList.get(i).getBannerName_En() == bannerList.get(j).getBannerName_En()) {
                                            fillterstore.add(bannerList.get(j));
                                            for (int k = 0; k < bannerList.get(i).getStoreDetails().size(); k++) {
                                                ArrayList<StoresList.StoreDetails> bannerlist = new ArrayList<>();
                                                for (int m = 0; m < bannerList.get(j).getStoreDetails().size(); m++) {

//                                                    if (bannerList.get(i).getStoreDetails().get(k).getBrandId() == bannerList.get(j).getStoreDetails().get(m).getBrandId()) {
                                                    bannerlist.add(bannerList.get(j).getStoreDetails().get(m));
//                                                    }

                                                }

                                                banners.setBannerList(bannerlist);

                                            }
                                        }
                                    }


                                    banners.setBanners(fillterstore);
                                    bannerslist.add(banners);

                                }

                                Log.d(TAG, "" + brandslist.size());
                            }

                            for (int l = 0; l < bannerslist.size(); l++) {
                                if (bannerslist.get(l).getBannerList() == null || bannerslist.get(l).getBannerList().size() == 0) {

                                } else {

                                    Collections.sort(bannerslist.get(l).getBannerList(), Banners.distanceSort);

                                }
                            }

                            Log.d(TAG, "onResponse" + bannerList);
                            Log.d(TAG, "onResponse " + stores.getData().getBannerResult());

                            filterseletedbrands.clear();
                            for (int i = 0; i < storesList.size(); i++) {

//                                    if (storesList.get(i).getDiscountamt() > 0) {


                                filterseletedbrands.add(storesList.get(i).getBrandId());
                                Filter filter = new Filter();

                                filter.setBrandId(storesList.get(i).getBrandId());
                                filter.setBradNameEn(storesList.get(i).getBrandName_En());
                                filter.setBranddescAr(storesList.get(i).getBrandName_Ar());
                                filter.setBrandimageEn(storesList.get(i).getStoreImage_En());
                                filter.setBranddescAr(storesList.get(i).getBranchDescription_Ar());
                                filter.setBranddescEn(storesList.get(i).getBranchDescription_En());

                                ArrayList<StoresList.StoresDetails> fillterstore = new ArrayList<>();
                                for (int j = 0; j < storesList.size(); j++) {
//                                    if (storesList.get(i).getBrandId() == storesList.get(j).getBrandId()) {
                                    fillterstore.add(storesList.get(j));

//                                    }
                                }
                                filter.setBrands(fillterstore);
                                filterslist.add(filter);


                            }

                            for (int l = 0; l < filterslist.size(); l++) {
                                Collections.sort(filterslist.get(l).getBrands(), Filter.distanceSort);
                            }

                            Log.i(TAG, "brands_size: " + brandslist.size());
                            Log.i(TAG, "filter_size " + filterslist.size());
//
//                            Log.i(TAG, "filter " + filter);
//                            if (filter) {
//                                if (FilterActivity.Discount.isChecked() && FilterActivity.Open_stores.isChecked() && FilterActivity.Relavance.isChecked()) {
//
//                                    for (int j = 0; j < brandslist.size(); j++) {
//
//                                        for (int k = 0; k < brandslist.get(j).getBrands().size(); k++) {
//
//                                            if (storesList.get(i).getDiscountAmt() > 0 && storesList.get(i).getStoreStatus().equals("Open")) {
//                                                Filter filter = new Filter();
//                                                filter.setBrandId(storesList.get(i).getBrandId());
//                                                filter.setBradNameEn(storesList.get(i).getBrandName_En());
//                                                filter.setBranddescAr(storesList.get(i).getBrandName_Ar());
//                                                filter.setBrandimageEn(storesList.get(i).getStoreImage_En());
//                                                filter.setBranddescAr(storesList.get(i).getBranchDescription_Ar());
//                                                filter.setBranddescEn(storesList.get(i).getBranchDescription_En());
//
//                                                ArrayList<StoresList.StoresDetails> fillterstore = new ArrayList<>();
//                                                for (int l = 0; l < brandslist.size(); l++) {
//                                                    for (int m = 0; m < brandslist.get(l).getBrands().size(); m++) {
//                                                        if (storesList.get(i).getBrandId() == brandslist.get(l).getBrands().get(m).getBrandId()) {
//                                                            fillterstore.add(brandslist.get(l).getBrands().get(m));
//                                                        }
//                                                    }
//                                                }
//                                                filter.setBrands(fillterstore);
//                                                filterslist.add(filter);
//
//                                            }
//                                        }
//                                    }
//
//                                    for (int l = 0; l < filterslist.size(); l++) {
//                                        Collections.sort(filterslist.get(l).getBrands(), Brands.distanceSort);
//                                    }
//
//                                    Log.i(TAG, "dicount && open store && R ");
//
//                                } else if (FilterActivity.Discount.isChecked() && FilterActivity.Open_stores.isChecked() && FilterActivity.Rating.isChecked()) {
//
//                                    for (int j = 0; j < brandslist.size(); j++) {
//
//                                        for (int k = 0; k < brandslist.get(j).getBrands().size(); k++) {
//
//                                            if (storesList.get(i).getDiscountAmt() > 0 && storesList.get(i).getStoreStatus().equals("Open")) {
//                                                Filter filter = new Filter();
//                                                filter.setBrandId(storesList.get(i).getBrandId());
//                                                filter.setBradNameEn(storesList.get(i).getBrandName_En());
//                                                filter.setBranddescAr(storesList.get(i).getBrandName_Ar());
//                                                filter.setBrandimageEn(storesList.get(i).getStoreImage_En());
//                                                filter.setBranddescAr(storesList.get(i).getBranchDescription_Ar());
//                                                filter.setBranddescEn(storesList.get(i).getBranchDescription_En());
//
//                                                ArrayList<StoresList.StoresDetails> fillterstore = new ArrayList<>();
//                                                for (int l = 0; l < brandslist.size(); l++) {
//                                                    for (int m = 0; m < brandslist.get(l).getBrands().size(); m++) {
//                                                        if (storesList.get(i).getBrandId() == brandslist.get(l).getBrands().get(m).getBrandId()) {
//                                                            fillterstore.add(brandslist.get(l).getBrands().get(m));
//                                                        }
//                                                    }
//                                                }
//                                                filter.setBrands(fillterstore);
//                                                filterslist.add(filter);
//
//                                            }
//                                        }
//                                    }
//
//                                    for (int l = 0; l < filterslist.size(); l++) {
//                                        Collections.sort(filterslist.get(l).getBrands(), Brands.Rating);
//                                    }
//
//                                    Log.i(TAG, "dicount && open store && Rating ");
//
//                                } else if (FilterActivity.Discount.isChecked() && FilterActivity.Open_stores.isChecked() && FilterActivity.Price.isChecked()) {
//
//                                    for (int j = 0; j < brandslist.size(); j++) {
//
//                                        for (int k = 0; k < brandslist.get(j).getBrands().size(); k++) {
//
//                                            if (storesList.get(i).getDiscountAmt() > 0 && storesList.get(i).getStoreStatus().equals("Open")) {
//                                                Filter filter = new Filter();
//                                                filter.setBrandId(storesList.get(i).getBrandId());
//                                                filter.setBradNameEn(storesList.get(i).getBrandName_En());
//                                                filter.setBranddescAr(storesList.get(i).getBrandName_Ar());
//                                                filter.setBrandimageEn(storesList.get(i).getStoreImage_En());
//                                                filter.setBranddescAr(storesList.get(i).getBranchDescription_Ar());
//                                                filter.setBranddescEn(storesList.get(i).getBranchDescription_En());
//
//                                                ArrayList<StoresList.StoresDetails> fillterstore = new ArrayList<>();
//                                                for (int l = 0; l < brandslist.size(); l++) {
//                                                    for (int m = 0; m < brandslist.get(l).getBrands().size(); m++) {
//                                                        if (storesList.get(i).getBrandId() == brandslist.get(l).getBrands().get(m).getBrandId()) {
//                                                            fillterstore.add(brandslist.get(l).getBrands().get(m));
//                                                        }
//                                                    }
//                                                }
//                                                filter.setBrands(fillterstore);
//                                                filterslist.add(filter);
//
//                                            }
//                                        }
//                                    }
//
//                                    for (int l = 0; l < filterslist.size(); l++) {
//                                        Collections.sort(filterslist.get(l).getBrands(), Brands.Price);
//                                    }
//
//                                    Log.i(TAG, "dicount && open store && price ");
//
//
//                                } else if (FilterActivity.Discount.isChecked() && FilterActivity.Relavance.isChecked()) {
//
//
//                                    for (int j = 0; j < brandslist.size(); j++) {
//
//                                        for (int k = 0; k < brandslist.get(j).getBrands().size(); k++) {
//
//                                            if (storesList.get(i).getDiscountAmt() > 0) {
//                                                Filter filter = new Filter();
//                                                filter.setBrandId(storesList.get(i).getBrandId());
//                                                filter.setBradNameEn(storesList.get(i).getBrandName_En());
//                                                filter.setBranddescAr(storesList.get(i).getBrandName_Ar());
//                                                filter.setBrandimageEn(storesList.get(i).getStoreImage_En());
//                                                filter.setBranddescAr(storesList.get(i).getBranchDescription_Ar());
//                                                filter.setBranddescEn(storesList.get(i).getBranchDescription_En());
//
//                                                ArrayList<StoresList.StoresDetails> fillterstore = new ArrayList<>();
//                                                for (int l = 0; l < brandslist.size(); l++) {
//                                                    for (int m = 0; m < brandslist.get(l).getBrands().size(); m++) {
//                                                        if (storesList.get(i).getBrandId() == brandslist.get(l).getBrands().get(m).getBrandId()) {
//                                                            fillterstore.add(brandslist.get(l).getBrands().get(m));
//                                                        }
//                                                    }
//                                                }
//                                                filter.setBrands(fillterstore);
//                                                filterslist.add(filter);
//
//                                            }
//                                        }
//                                    }
//
//                                    for (int l = 0; l < filterslist.size(); l++) {
//                                        Collections.sort(filterslist.get(l).getBrands(), Brands.distanceSort);
//                                    }
//
//                                    Log.i(TAG, "dicount && R ");
//
//
//                                } else if (FilterActivity.Discount.isChecked() && FilterActivity.Rating.isChecked()) {
//
//
//                                    for (int j = 0; j < brandslist.size(); j++) {
//
//                                        for (int k = 0; k < brandslist.get(j).getBrands().size(); k++) {
//
//                                            if (storesList.get(i).getDiscountAmt() > 0) {
//                                                Filter filter = new Filter();
//                                                filter.setBrandId(storesList.get(i).getBrandId());
//                                                filter.setBradNameEn(storesList.get(i).getBrandName_En());
//                                                filter.setBranddescAr(storesList.get(i).getBrandName_Ar());
//                                                filter.setBrandimageEn(storesList.get(i).getStoreImage_En());
//                                                filter.setBranddescAr(storesList.get(i).getBranchDescription_Ar());
//                                                filter.setBranddescEn(storesList.get(i).getBranchDescription_En());
//
//                                                ArrayList<StoresList.StoresDetails> fillterstore = new ArrayList<>();
//                                                for (int l = 0; l < brandslist.size(); l++) {
//                                                    for (int m = 0; m < brandslist.get(l).getBrands().size(); m++) {
//                                                        if (storesList.get(i).getBrandId() == brandslist.get(l).getBrands().get(m).getBrandId()) {
//                                                            fillterstore.add(brandslist.get(l).getBrands().get(m));
//                                                        }
//                                                    }
//                                                }
//                                                filter.setBrands(fillterstore);
//                                                filterslist.add(filter);
//
//                                            }
//                                        }
//                                    }
//
//                                    for (int l = 0; l < filterslist.size(); l++) {
//                                        Collections.sort(filterslist.get(l).getBrands(), Brands.Rating);
//                                    }
//
//                                    Log.i(TAG, "dicount && Rating ");
//
//
//                                } else if (FilterActivity.Discount.isChecked() && FilterActivity.Price.isChecked()) {
//
//
//                                    for (int j = 0; j < brandslist.size(); j++) {
//
//                                        for (int k = 0; k < brandslist.get(j).getBrands().size(); k++) {
//
//                                            if (storesList.get(i).getDiscountAmt() > 0) {
//                                                Filter filter = new Filter();
//                                                filter.setBrandId(storesList.get(i).getBrandId());
//                                                filter.setBradNameEn(storesList.get(i).getBrandName_En());
//                                                filter.setBranddescAr(storesList.get(i).getBrandName_Ar());
//                                                filter.setBrandimageEn(storesList.get(i).getStoreImage_En());
//                                                filter.setBranddescAr(storesList.get(i).getBranchDescription_Ar());
//                                                filter.setBranddescEn(storesList.get(i).getBranchDescription_En());
//
//                                                ArrayList<StoresList.StoresDetails> fillterstore = new ArrayList<>();
//                                                for (int l = 0; l < brandslist.size(); l++) {
//                                                    for (int m = 0; m < brandslist.get(l).getBrands().size(); m++) {
//                                                        if (storesList.get(i).getBrandId() == brandslist.get(l).getBrands().get(m).getBrandId()) {
//                                                            fillterstore.add(brandslist.get(l).getBrands().get(m));
//                                                        }
//                                                    }
//                                                }
//                                                filter.setBrands(fillterstore);
//                                                filterslist.add(filter);
//
//                                            }
//                                        }
//                                    }
//
//                                    for (int l = 0; l < filterslist.size(); l++) {
//                                        Collections.sort(filterslist.get(l).getBrands(), Brands.Price);
//                                    }
//
//                                    Log.i(TAG, "dicount && price ");
//
//                                } else if (FilterActivity.Open_stores.isChecked() && FilterActivity.Relavance.isChecked()) {
//
//                                    for (int j = 0; j < brandslist.size(); j++) {
//
//                                        for (int k = 0; k < brandslist.get(j).getBrands().size(); k++) {
//
//                                            if (storesList.get(i).getStoreStatus().equals("Open")) {
//                                                Filter filter = new Filter();
//                                                filter.setBrandId(storesList.get(i).getBrandId());
//                                                filter.setBradNameEn(storesList.get(i).getBrandName_En());
//                                                filter.setBranddescAr(storesList.get(i).getBrandName_Ar());
//                                                filter.setBrandimageEn(storesList.get(i).getStoreImage_En());
//                                                filter.setBranddescAr(storesList.get(i).getBranchDescription_Ar());
//                                                filter.setBranddescEn(storesList.get(i).getBranchDescription_En());
//
//                                                ArrayList<StoresList.StoresDetails> fillterstore = new ArrayList<>();
//                                                for (int l = 0; l < brandslist.size(); l++) {
//                                                    for (int m = 0; m < brandslist.get(l).getBrands().size(); m++) {
//                                                        if (storesList.get(i).getBrandId() == brandslist.get(l).getBrands().get(m).getBrandId()) {
//                                                            fillterstore.add(brandslist.get(l).getBrands().get(m));
//                                                        }
//                                                    }
//                                                }
//                                                filter.setBrands(fillterstore);
//                                                filterslist.add(filter);
//
//                                            }
//                                        }
//                                    }
//
//                                    for (int l = 0; l < filterslist.size(); l++) {
//                                        Collections.sort(filterslist.get(l).getBrands(), Brands.distanceSort);
//                                    }
//
//                                    Log.i(TAG, "open store && relavence ");
//
//                                } else if (FilterActivity.Open_stores.isChecked() && FilterActivity.Rating.isChecked()) {
//
//                                    for (int j = 0; j < brandslist.size(); j++) {
//
//                                        for (int k = 0; k < brandslist.get(j).getBrands().size(); k++) {
//
//                                            if (storesList.get(i).getStoreStatus().equals("Open")) {
//                                                Filter filter = new Filter();
//                                                filter.setBrandId(storesList.get(i).getBrandId());
//                                                filter.setBradNameEn(storesList.get(i).getBrandName_En());
//                                                filter.setBranddescAr(storesList.get(i).getBrandName_Ar());
//                                                filter.setBrandimageEn(storesList.get(i).getStoreImage_En());
//                                                filter.setBranddescAr(storesList.get(i).getBranchDescription_Ar());
//                                                filter.setBranddescEn(storesList.get(i).getBranchDescription_En());
//
//                                                ArrayList<StoresList.StoresDetails> fillterstore = new ArrayList<>();
//                                                for (int l = 0; l < brandslist.size(); l++) {
//                                                    for (int m = 0; m < brandslist.get(l).getBrands().size(); m++) {
//                                                        if (storesList.get(i).getBrandId() == brandslist.get(l).getBrands().get(m).getBrandId()) {
//                                                            fillterstore.add(brandslist.get(l).getBrands().get(m));
//                                                        }
//                                                    }
//                                                }
//                                                filter.setBrands(fillterstore);
//                                                filterslist.add(filter);
//
//                                            }
//                                        }
//                                    }
//
//                                    for (int l = 0; l < filterslist.size(); l++) {
//                                        Collections.sort(filterslist.get(l).getBrands(), Brands.Rating);
//                                    }
//
//                                    Log.i(TAG, "open store && rating ");
//
//                                } else if (FilterActivity.Open_stores.isChecked() && FilterActivity.Price.isChecked()) {
//
//                                    for (int j = 0; j < brandslist.size(); j++) {
//
//                                        for (int k = 0; k < brandslist.get(j).getBrands().size(); k++) {
//
//                                            if (storesList.get(i).getStoreStatus().equals("Open")) {
//                                                Filter filter = new Filter();
//                                                filter.setBrandId(storesList.get(i).getBrandId());
//                                                filter.setBradNameEn(storesList.get(i).getBrandName_En());
//                                                filter.setBranddescAr(storesList.get(i).getBrandName_Ar());
//                                                filter.setBrandimageEn(storesList.get(i).getStoreImage_En());
//                                                filter.setBranddescAr(storesList.get(i).getBranchDescription_Ar());
//                                                filter.setBranddescEn(storesList.get(i).getBranchDescription_En());
//
//                                                ArrayList<StoresList.StoresDetails> fillterstore = new ArrayList<>();
//                                                for (int l = 0; l < brandslist.size(); l++) {
//                                                    for (int m = 0; m < brandslist.get(l).getBrands().size(); m++) {
//                                                        if (storesList.get(i).getBrandId() == brandslist.get(l).getBrands().get(m).getBrandId()) {
//                                                            fillterstore.add(brandslist.get(l).getBrands().get(m));
//                                                        }
//                                                    }
//                                                }
//                                                filter.setBrands(fillterstore);
//                                                filterslist.add(filter);
//
//                                            }
//                                        }
//                                    }
//
//                                    for (int l = 0; l < filterslist.size(); l++) {
//                                        Collections.sort(filterslist.get(l).getBrands(), Brands.Price);
//                                    }
//
//                                    Log.i(TAG, "open store && price ");
//
//                                } else if (FilterActivity.Relavance.isChecked()) {
//
//                                    for (int j = 0; j < brandslist.size(); j++) {
//
//                                        for (int k = 0; k < brandslist.get(j).getBrands().size(); k++) {
//
////                                            if (storesList.get(i).getStorestatus().equals("Open")) {
//                                            Filter filter = new Filter();
//                                            filter.setBrandId(storesList.get(i).getBrandId());
//                                            filter.setBradNameEn(storesList.get(i).getBrandName_En());
//                                            filter.setBranddescAr(storesList.get(i).getBrandName_Ar());
//                                            filter.setBrandimageEn(storesList.get(i).getStoreImage_En());
//                                            filter.setBranddescAr(storesList.get(i).getBranchDescription_Ar());
//                                            filter.setBranddescEn(storesList.get(i).getBranchDescription_En());
//
//                                            ArrayList<StoresList.StoresDetails> fillterstore = new ArrayList<>();
//                                            for (int l = 0; l < brandslist.size(); l++) {
//                                                for (int m = 0; m < brandslist.get(l).getBrands().size(); m++) {
//                                                    if (storesList.get(i).getBrandId() == brandslist.get(l).getBrands().get(m).getBrandId()) {
//                                                        fillterstore.add(brandslist.get(l).getBrands().get(m));
//                                                    }
//                                                }
//                                            }
//                                            filter.setBrands(fillterstore);
//                                            filterslist.add(filter);
//
////                                            }
//                                        }
//                                    }
//
//                                    for (int l = 0; l < filterslist.size(); l++) {
//                                        Collections.sort(filterslist.get(l).getBrands(), Brands.distanceSort);
//                                    }
//                                    Log.i(TAG, "Relavence ");
//
//                                } else if (FilterActivity.Rating.isChecked()) {
//
//                                    for (int j = 0; j < brandslist.size(); j++) {
//
//                                        for (int k = 0; k < brandslist.get(j).getBrands().size(); k++) {
//
////                                            if (storesList.get(i).getStorestatus().equals("Open")) {
//                                            Filter filter = new Filter();
//                                            filter.setBrandId(storesList.get(i).getBrandId());
//                                            filter.setBradNameEn(storesList.get(i).getBrandName_En());
//                                            filter.setBranddescAr(storesList.get(i).getBrandName_Ar());
//                                            filter.setBrandimageEn(storesList.get(i).getStoreImage_En());
//                                            filter.setBranddescAr(storesList.get(i).getBranchDescription_Ar());
//                                            filter.setBranddescEn(storesList.get(i).getBranchDescription_En());
//
//                                            ArrayList<StoresList.StoresDetails> fillterstore = new ArrayList<>();
//                                            for (int l = 0; l < brandslist.size(); l++) {
//                                                for (int m = 0; m < brandslist.get(l).getBrands().size(); m++) {
//                                                    if (storesList.get(i).getBrandId() == brandslist.get(l).getBrands().get(m).getBrandId()) {
//                                                        fillterstore.add(brandslist.get(l).getBrands().get(m));
//                                                    }
//                                                }
//                                            }
//                                            filter.setBrands(fillterstore);
//                                            filterslist.add(filter);
//
////                                            }
//                                        }
//                                    }
//
//                                    for (int l = 0; l < filterslist.size(); l++) {
//                                        Collections.sort(filterslist.get(l).getBrands(), Brands.Rating);
//                                    }
//
//                                    Log.i(TAG, "Rating ");
//
//                                } else if (FilterActivity.Price.isChecked()) {
//
//                                    for (int j = 0; j < brandslist.size(); j++) {
//
//                                        for (int k = 0; k < brandslist.get(j).getBrands().size(); k++) {
//
////                                            if (storesList.get(i).getStorestatus().equals("Open")) {
//                                            Filter filter = new Filter();
//                                            filter.setBrandId(storesList.get(i).getBrandId());
//                                            filter.setBradNameEn(storesList.get(i).getBrandName_En());
//                                            filter.setBranddescAr(storesList.get(i).getBrandName_Ar());
//                                            filter.setBrandimageEn(storesList.get(i).getStoreImage_En());
//                                            filter.setBranddescAr(storesList.get(i).getBranchDescription_Ar());
//                                            filter.setBranddescEn(storesList.get(i).getBranchDescription_En());
//
//                                            ArrayList<StoresList.StoresDetails> fillterstore = new ArrayList<>();
//                                            for (int l = 0; l < brandslist.size(); l++) {
//                                                for (int m = 0; m < brandslist.get(l).getBrands().size(); m++) {
//                                                    if (storesList.get(i).getBrandId() == brandslist.get(l).getBrands().get(m).getBrandId()) {
//                                                        fillterstore.add(brandslist.get(l).getBrands().get(m));
//                                                    }
//                                                }
//                                            }
//                                            filter.setBrands(fillterstore);
//                                            filterslist.add(filter);
//
////                                            }
//                                        }
//                                    }
//
//                                    for (int l = 0; l < filterslist.size(); l++) {
//                                        Collections.sort(filterslist.get(l).getBrands(), Brands.Price);
//                                    }
//
//                                    Log.i(TAG, "Price ");
//
//                                }
//                            }
//
//                            Log.i(TAG, "brandslistsort " + brandslist);
//
                            if (getActivity() != null) {
                                for (int i = 0; i < filterslist.size(); i++) {
                                    mStoreAdapter = new StoresAdapter(getContext(), filterslist.get(i).getBrands(), getActivity(), language);
                                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
                                    storesListView.setLayoutManager(mLayoutManager);
                                    storesListView.setAdapter(mStoreAdapter);
                                }
                                bannersViewpage.setAdapter(mBanners);
                            }
//                            lookingStoresViewPager.setAdapter(mLookingAdapter);


//                            promotedList.clear();
//                            threekmList.clear();
//                            itsnewList.clear();
//                            popularList.clear();


//                            for (int j = 0; j < filterslist.size(); j++) {
//                                for (int k = 0; k < filterslist.get(j).getBrands().size(); k++) {
//
//                                    if (filterslist.get(j).getBrands().get(k).getPopular()) {
//                                        popularList.add(filterslist.get(j).getBrands().get(k));
//                                    }
//                                    if (filterslist.get(j).getBrands().get(k).getIsNew()) {
//                                        itsnewList.add(filterslist.get(j).getBrands().get(k));
//                                    }
//                                    if (filterslist.get(j).getBrands().get(k).getIsPromoted()) {
//                                        promotedList.add(filterslist.get(j).getBrands().get(k));
//                                    }
//                                    if (filterslist.get(j).getBrands().get(k).getDistance() <= 3) {
//
//                                        threekmList.add(filterslist.get(j).getBrands().get(k));
//                                    }
//                                    if (getActivity() != null) {
//                                        if (promotedList.size() == 0) {
//                                            promotedlayout.setVisibility(View.GONE);
//                                        } else {
//                                            promotedlayout.setVisibility(View.VISIBLE);
//                                            mPromotedAdapter = new PromotedStoresAdapter(getContext(), promotedList);
//                                            promotedViewPager.setAdapter(mPromotedAdapter);
//                                        }
//                                    }
//
//                                    if (getActivity() != null) {
//                                        if (popularList.size() == 0) {
//                                            populerlayout.setVisibility(View.GONE);
//                                        } else {
//                                            populerlayout.setVisibility(View.VISIBLE);
//                                            mPopularAdapter = new PopularStoresAdapter(getContext(), popularList);
//                                            popuplarViewPager.setAdapter(mPopularAdapter);
//                                        }
//                                    }
//
//                                    if (getActivity() != null) {
//                                        if (threekmList.size() == 0) {
//                                            aroundlayout.setVisibility(View.GONE);
//                                        } else {
//                                            aroundlayout.setVisibility(View.VISIBLE);
//                                            mAround = new Around3kmStoresAdapter(getContext(), threekmList);
//                                            aroundViewPager.setAdapter(mAround);
//                                        }
//                                    }
//                                    if (getActivity() != null) {
//                                        if (itsnewList.size() == 0) {
//                                            itsnewlayout.setVisibility(View.GONE);
//                                        } else {
//
//                                            itsnewlayout.setVisibility(View.VISIBLE);
//                                            mItsnewAdapter = new ItsNewStoresAdapter(getContext(), itsnewList);
//                                            itsnewStoresViewPager.setAdapter(mItsnewAdapter);
//                                        }
//                                    }

                            if (getActivity() != null) {
//                                if (bannerslist.size() == 0) {
//                                    bannerlayout.setVisibility(View.GONE);
//                                } else {
                                bannerlayout.setVisibility(View.VISIBLE);
                                try {
                                    mBanners = new BannersAdapter(getContext(), bannerslist, language);
//                                    defaultIndicator.setViewPager(bannersViewpage);
                                    bannersViewpage.setAdapter(mBanners);
                                    mBanners.notifyDataSetChanged();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
//                                }
                            }
//                                    if (getActivity() != null) {
//                                        if (somthingList.size() == 0) {
//                                            lookigsomthinglayout.setVisibility(View.GONE);
//                                        } else {
//                                            lookigsomthinglayout.setVisibility(View.VISIBLE);
//                                            mLookingAdapter = new HomeLookingForStoresAdapter(getContext(), somthingList);
//                                            lookingStoresViewPager.setAdapter(mLookingAdapter);
//                                        }
//                                    }
//
//                                    shimmerPromotedLayout.stopShimmerAnimation();
//                                    shimmerPopularLayout.stopShimmerAnimation();
//                                    shimmerNearbyLayout.stopShimmerAnimation();
//                                    shimmerNewStoresLayout.stopShimmerAnimation();
//
//                                    shimmerPromotedLayout.setVisibility(View.GONE);
//                                    shimmerPopularLayout.setVisibility(View.GONE);
//                                    shimmerNearbyLayout.setVisibility(View.GONE);
//                                    shimmerNewStoresLayout.setVisibility(View.GONE);
//
//                                    promotedViewPager.setVisibility(View.VISIBLE);
//                                    popuplarViewPager.setVisibility(View.VISIBLE);
//                                    aroundViewPager.setVisibility(View.VISIBLE);
//                                    itsnewStoresViewPager.setVisibility(View.VISIBLE);
                            bannersViewpage.setVisibility(View.VISIBLE);
//                                    typesLayout.setVisibility(View.VISIBLE);
//                                    lookingStoresViewPager.setVisibility(View.VISIBLE);

//                                }
//                            }
                            for (int i = 0; i < storesList.size(); i++) {

                            }
                        } else {
                            if (getActivity() != null) {
                                if (language.equalsIgnoreCase("En")) {
                                    String failureResponse = stores.getMessage();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name),
                                            getResources().getString(R.string.ok), getActivity());
                                } else {
                                    String failureResponse = stores.getMessageAr();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name_ar),
                                            getResources().getString(R.string.ok_ar), getActivity());
                                }
                            }
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(getActivity(), response.message(), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getActivity(), response.message(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<StoresList> call, Throwable t) {
                    Log.i(TAG, "onFailure: " + t);
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(getContext());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(getContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getContext(), R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(getContext(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getContext(), R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }
                    Log.i("TAG", "onFailure: " + t);

                    Constants.closeLoadingDialog();
                }
            });
            return null;
        }


    }

    private String prepareGetStoresJSON() {
        JSONObject parentObj = new JSONObject();

        try {
            parentObj.put("Latitude", currentLatitude);
            parentObj.put("Longitude", currentLongitude);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.d(TAG, "prepareVerifyMobileJson: " + parentObj);
        return parentObj.toString();
    }


    private class GetFilterApi extends AsyncTask<String, Integer, String> {

        String inputStr;
//        AlertDialog loaderDialog = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareGetStoresJSON1();
//            dialog = new ACProgressFlower.Builder(getContext())
//                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
//                    .themeColor(Color.WHITE)
//                    .fadeColor(Color.DKGRAY).build();
//            dialog.show();
            Constants.showLoadingDialog(getActivity());
//            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
//            // ...Irrelevant code for customizing the buttons and title
//            LayoutInflater inflater = getLayoutInflater();
//            int layout = R.layout.loder_dialog;
//            View dialogView = inflater.inflate(layout, null);
//            dialogBuilder.setView(dialogView);
//            dialogBuilder.setCancelable(true);
//
//            loaderDialog = dialogBuilder.create();
//            loaderDialog.show();
//            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
//            Window window = loaderDialog.getWindow();
//            window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//            lp.copyFrom(window.getAttributes());
//            //This makes the dialog take up the full width
//            Display display = getActivity().getWindowManager().getDefaultDisplay();
//            Point size = new Point();
//            display.getSize(size);
//            int screenWidth = size.x;
//
//            double d = screenWidth*0.85;
//            lp.width = (int) d;
//            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
//            window.setAttributes(lp);
        }

        @Override
        protected String doInBackground(final String... strings) {
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<StoresList> call = apiService.getStoresList(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<StoresList>() {
                @Override
                public void onResponse(Call<StoresList> call, Response<StoresList> response) {
                    Log.i("TAG", "store servies responce " + response);
                    if (response.isSuccessful()) {

                        StoresList stores = response.body();
                        if (stores.getStatus()) {
                            storesList = stores.getData().getStoresDetails();
                            bannerList = stores.getData().getBannerResult();
                            somthingList = stores.getData().getStoreCategories();
                            filtercatList = stores.getData().getFilterCategory();

                            mhome_filter.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                    showloaderAlertDialog();

                                }
                            });


                            brandslist.clear();
                            filterslist.clear();
                            bannerslist.clear();

                            for (int i = 0; i < storesList.size(); i++) {
                                if (!seletedbrands.contains(storesList.get(i).getBrandId())) {
                                    seletedbrands.add(storesList.get(i).getBrandId());
                                    Brands brands = new Brands();
                                    brands.setBrandId(storesList.get(i).getBrandId());
                                    brands.setBradNameEn(storesList.get(i).getBrandName_En());
                                    brands.setBranddescAr(storesList.get(i).getBrandName_Ar());
                                    brands.setBrandimageEn(storesList.get(i).getStoreImage_En());
                                    brands.setBranddescAr(storesList.get(i).getBranchDescription_Ar());
                                    brands.setBranddescEn(storesList.get(i).getBranchDescription_En());

                                    ArrayList<StoresList.StoresDetails> fillterstore = new ArrayList<>();
                                    for (int j = 0; j < storesList.size(); j++) {
                                        if (storesList.get(i).getBrandId() == storesList.get(j).getBrandId()) {
//                                            if (filterdistance >= storesList.get(i).getDistance()) {
                                            fillterstore.add(storesList.get(j));
//                                            }
                                        }
                                    }
                                    brands.setBrands(fillterstore);
                                    brandslist.add(brands);

                                }

                                Log.d(TAG, "" + brandslist.size());
                            }

                            seletedbanners.clear();
                            for (int i = 0; i < bannerList.size(); i++) {
                                if (!seletedbanners.contains(bannerList.get(i).getBannerName_En())) {
                                    seletedbanners.add(bannerList.get(i).getBannerName_En());
                                    Banners banners = new Banners();
                                    banners.setBannerNameEn(bannerList.get(i).getBannerName_En());
                                    banners.setBrandimageEn(bannerList.get(i).getBannerImage());
                                    banners.setBannerNameAr(bannerList.get(i).getBannerName_Ar());

                                    ArrayList<StoresList.BannerResult> fillterstore = new ArrayList<>();
                                    for (int j = 0; j < bannerList.size(); j++) {
                                        if (bannerList.get(i).getBannerName_En() == bannerList.get(j).getBannerName_En()) {
                                            fillterstore.add(bannerList.get(j));
                                            for (int k = 0; k < bannerList.get(i).getStoreDetails().size(); k++) {
                                                ArrayList<StoresList.StoreDetails> bannerlist = new ArrayList<>();
                                                for (int m = 0; m < bannerList.get(j).getStoreDetails().size(); m++) {

//                                                    if (bannerList.get(i).getStoreDetails().get(k).getBrandId() == bannerList.get(j).getStoreDetails().get(m).getBrandId()) {
                                                    bannerlist.add(bannerList.get(j).getStoreDetails().get(m));
//                                                    }

                                                }

                                                banners.setBannerList(bannerlist);

                                            }
                                        }
                                    }


                                    banners.setBanners(fillterstore);
                                    bannerslist.add(banners);

                                }

                                Log.d(TAG, "" + brandslist.size());
                            }

                            for (int l = 0; l < bannerslist.size(); l++) {
                                if (bannerslist.get(l).getBannerList() == null || bannerslist.get(l).getBannerList().size() == 0) {

                                } else {

                                    Collections.sort(bannerslist.get(l).getBannerList(), Banners.distanceSort);

                                }
                            }

                            Log.d(TAG, "onResponse" + bannerList);
                            Log.d(TAG, "onResponse " + stores.getData().getBannerResult());

                            filterseletedbrands.clear();
                            for (int i = 0; i < storesList.size(); i++) {

//                                    if (storesList.get(i).getDiscountamt() > 0) {


                                filterseletedbrands.add(storesList.get(i).getBrandId());
                                Filter filter = new Filter();

                                filter.setBrandId(storesList.get(i).getBrandId());
                                filter.setBradNameEn(storesList.get(i).getBrandName_En());
                                filter.setBranddescAr(storesList.get(i).getBrandName_Ar());
                                filter.setBrandimageEn(storesList.get(i).getStoreImage_En());
                                filter.setBranddescAr(storesList.get(i).getBranchDescription_Ar());
                                filter.setBranddescEn(storesList.get(i).getBranchDescription_En());

                                ArrayList<StoresList.StoresDetails> fillterstore = new ArrayList<>();
                                for (int j = 0; j < storesList.size(); j++) {
//                                    if (storesList.get(i).getBrandId() == storesList.get(j).getBrandId()) {
                                    fillterstore.add(storesList.get(j));

//                                    }
                                }
                                filter.setBrands(fillterstore);
                                filterslist.add(filter);


                            }

                            for (int l = 0; l < filterslist.size(); l++) {
                                Collections.sort(filterslist.get(l).getBrands(), Filter.distanceSort);
                            }

                            Log.i(TAG, "brands_size: " + brandslist.size());
                            Log.i(TAG, "filter_size " + filterslist.size());

                            if (getActivity() != null) {
                                for (int i = 0; i < filterslist.size(); i++) {
                                    mStoreAdapter = new StoresAdapter(getContext(), filterslist.get(i).getBrands(), getActivity(), language);
                                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
                                    storesListView.setLayoutManager(mLayoutManager);
                                    storesListView.setAdapter(mStoreAdapter);
                                }
                                bannersViewpage.setAdapter(mBanners);
                                mBanners.notifyDataSetChanged();
                            }


                            if (getActivity() != null) {
//                                if (bannerslist.size() == 0) {
//                                    bannerlayout.setVisibility(View.GONE);
//                                } else {
                                bannerlayout.setVisibility(View.VISIBLE);
                                try {
                                    mBanners = new BannersAdapter(getContext(), bannerslist, language);
//                                    defaultIndicator.setViewPager(bannersViewpage);
                                    bannersViewpage.setAdapter(mBanners);
                                    mBanners.notifyDataSetChanged();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
//                                }
                            }

                            bannersViewpage.setVisibility(View.VISIBLE);
                            for (int i = 0; i < storesList.size(); i++) {

                            }
                        } else {
                            if (language.equalsIgnoreCase("En")) {
                                String failureResponse = stores.getMessage();
                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name),
                                        getResources().getString(R.string.ok), getActivity());
                            } else {
                                String failureResponse = stores.getMessageAr();
                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name_ar),
                                        getResources().getString(R.string.ok_ar), getActivity());
                            }
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(getActivity(), response.message(), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getActivity(), response.message(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<StoresList> call, Throwable t) {
                    Log.i(TAG, "onFailure: " + t);
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(getContext());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(getContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getContext(), R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(getContext(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getContext(), R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }

                    Constants.closeLoadingDialog();
                }
            });
            return null;
        }


    }

    private String prepareGetStoresJSON1() {
        JSONObject parentObj = new JSONObject();

        String filter = null;

        for (int i = 0; i < filterid.size(); i++) {

            if (filter != null && filter.length() > 0) {

                filter = filter + "," + filterid.get(i);

            } else {

                filter = String.valueOf(filterid.get(i));

            }
        }

        try {
            parentObj.put("Latitude", currentLatitude);
            parentObj.put("Longitude", currentLongitude);
            parentObj.put("filterIds", filter);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.d(TAG, "prepareVerifyMobileJson: " + parentObj);
        return parentObj.toString();
    }

    @Override
    public void onResume() {
        super.onResume();

        Log.i(TAG, "onResume: " + HomeScreenFragment.filter);

        if (getActivity() != null) {
            if (bannerList.size() > 0) {
                try {
                    mBanners = new BannersAdapter(getContext(), bannerslist, language);
                    bannersViewpage.setAdapter(mBanners);
                    mBanners.notifyDataSetChanged();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        if (filter) {

            String networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                new GetFilterApi().execute();

            } else {
                if (language.equalsIgnoreCase("En")) {
                    Toast.makeText(getActivity(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getActivity(), R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    private void showLocationChangedPopup(double updatedLatitude, double updatedLongitude) {

        Location loc1 = new Location("");
        loc1.setLatitude(currentLatitude);
        loc1.setLongitude(currentLongitude);

        Location loc2 = new Location("");
        loc2.setLatitude(updatedLatitude);
        loc2.setLongitude(updatedLongitude);

        float distanceInMeters = loc1.distanceTo(loc2);

        Log.d(TAG, "distanceInMeters: "+distanceInMeters);

        if (distanceInMeters > 300) {
            try {
                final BubbleLayout bubbleLayout = (BubbleLayout) LayoutInflater.from(getActivity()).inflate(R.layout.bubble_location_popup, null);
                final PopupWindow popupWindow = BubblePopupHelper.create(getActivity(), bubbleLayout);

                Button closeBtn = (Button) bubbleLayout.findViewById(R.id.close_button);
                Button changeAddressBtn = (Button) bubbleLayout.findViewById(R.id.change_location_button);

                closeBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        popupWindow.dismiss();
                    }
                });

                changeAddressBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        try {
                            gps = new GPSTracker(getActivity());
                            ChangeLocationCoordinates();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        popupWindow.dismiss();
                    }
                });

                int[] location = new int[2];
                locationArrow.getLocationOnScreen(location);
                bubbleLayout.setArrowDirection(ArrowDirection.TOP_CENTER);
                popupWindow.showAtLocation(locationArrow, Gravity.NO_GRAVITY, location[0], locationArrow.getHeight() + location[1]);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void ChangeLocationCoordinates() throws IOException {

        if (gps != null) {
            if (gps.canGetLocation()) {

                currentLatitude = gps.getLatitude();
                currentLongitude = gps.getLongitude();

                getLocality();
                if (filter) {
                    new GetFilterApi().execute();
                } else {
                    new GetStoresApi().execute();
                }
            }
        }
    }
}
