package com.cs.chef.Fragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.chef.Activities.ChangePasswordActivity;
import com.cs.chef.Activities.EditProfileActivity;
import com.cs.chef.Activities.MainActivity;
import com.cs.chef.Activities.ManageCards;
import com.cs.chef.Activities.MoreActivity;
import com.cs.chef.Activities.MyAddressActivity;
import com.cs.chef.Activities.OfferActivity;
import com.cs.chef.Activities.OrderHistory;
import com.cs.chef.Activities.WebViewActivity;
import com.cs.chef.Constants;
import com.cs.chef.R;

import java.util.List;

import static android.app.Activity.RESULT_OK;

public class AccountFragment extends Fragment implements View.OnClickListener{

    TextView tvManageAddress, tvChangePassword,  tvPayments,rateus,concatus, tvOffers, tvLogout, tvMore,tvManagecards, tvMyOrder,appversion, website;
    TextView tvName, tvMobile, tvEmail, tvEditProfile;
    String strName, strMobile, strEmail;
    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefsEditor;
    String userId;
    Toolbar toolbar;

    private static String TAG = Constants.TAG;
    private static int EDIT_REQUEST = 1;

    SharedPreferences languagePrefs;
    String language;

    View rootView;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        languagePrefs = getActivity().getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            rootView = inflater.inflate(R.layout.fragment_account, container, false);
        } else {
            rootView = inflater.inflate(R.layout.fragment_account_arabic, container, false);
        }
        userPrefs = getContext().getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userId = userPrefs.getString("userId","0");
        userPrefsEditor = userPrefs.edit();
        tvName = (TextView) rootView.findViewById(R.id.account_name);
        tvEmail = (TextView) rootView.findViewById(R.id.account_email);
        tvMobile = (TextView) rootView.findViewById(R.id.account_mobile);
        tvEditProfile = (TextView) rootView.findViewById(R.id.account_edit);
        tvMyOrder = (TextView) rootView.findViewById(R.id.account_my_orders);
        appversion = (TextView) rootView.findViewById(R.id.appversion);
        website = (TextView) rootView.findViewById(R.id.website);

        tvOffers = (TextView) rootView.findViewById(R.id.account_offers);
        tvManagecards = (TextView) rootView.findViewById(R.id.account_managecards);
        tvMore = (TextView) rootView.findViewById(R.id.account_more);
        rateus = (TextView) rootView.findViewById(R.id.account_rate);
        concatus = (TextView) rootView.findViewById(R.id.account_concat);
        tvLogout = (TextView) rootView.findViewById(R.id.account_logout);
        tvPayments = (TextView) rootView.findViewById(R.id.account_payments);
        tvManageAddress = (TextView) rootView.findViewById(R.id.account_manage_address);
        tvChangePassword = (TextView) rootView.findViewById(R.id.account_change_password);
        setTypeface();

        tvName.setText(userPrefs.getString("name","-"));
        tvEmail.setText("+"+(userPrefs.getString("mobile","")+" - ")+userPrefs.getString("email",""));


        try {
            PackageInfo pInfo = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0);
            String version12 = pInfo.versionName;
            appversion.setText("App version v"+version12+"("+pInfo.versionCode+")");
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        website.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent fbIntent = new Intent(getActivity(), WebViewActivity.class);
                fbIntent.putExtra("title", "Creative Solutions");
                fbIntent.putExtra("url", "http://www.creative-sols.com/");
                startActivity(fbIntent);
            }
        });
        concatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Intent i = new Intent(Intent.ACTION_SEND);
                    i.setType("plain/text");
                    i.putExtra(Intent.EXTRA_EMAIL  , new String[]{"info@arab-space.com"});
                    i.putExtra(Intent.EXTRA_SUBJECT, "About The Chef App");
                    i.putExtra(Intent.EXTRA_TITLE  , "About The Chef App");
                    final PackageManager pm = getActivity().getPackageManager();
                    final List<ResolveInfo> matches = pm.queryIntentActivities(i, 0);
                    String className = null;
                    for (final ResolveInfo info : matches) {
                        if (info.activityInfo.packageName.equals("com.google.android.gm")) {
                            className = info.activityInfo.name;

                            if(className != null && !className.isEmpty()){
                                break;
                            }
                        }
                    }
                    i.setClassName("com.google.android.gm", className);
                    try {
                        getActivity().startActivity(Intent.createChooser(i, "Send mail..."));
                    } catch (android.content.ActivityNotFoundException ex) {
                        Toast.makeText(getActivity(), "There are no email apps installed.", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity(), "There are no email apps installed.", Toast.LENGTH_SHORT).show();
                }
            }
        });


        rateus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.cs.chef")));
            }
        });



        tvEditProfile.setOnClickListener(this);
        tvManageAddress.setOnClickListener(this);
        tvChangePassword.setOnClickListener(this);
        tvManagecards.setOnClickListener(this);
        tvOffers.setOnClickListener(this);
        tvMore.setOnClickListener(this);
        tvLogout.setOnClickListener(this);
        tvMyOrder.setOnClickListener(this);
        return rootView;
    }

    private void setTypeface(){
        tvName.setTypeface(Constants.getTypeFace(getContext()));
        tvMobile.setTypeface(Constants.getTypeFace(getContext()));
        tvEmail.setTypeface(Constants.getTypeFace(getContext()));
        tvEditProfile.setTypeface(Constants.getTypeFace(getContext()));
        tvManageAddress.setTypeface(Constants.getTypeFace(getContext()));
        tvChangePassword.setTypeface(Constants.getTypeFace(getContext()));
        rateus.setTypeface(Constants.getTypeFace(getContext()));
        concatus.setTypeface(Constants.getTypeFace(getContext()));
        tvOffers.setTypeface(Constants.getTypeFace(getContext()));
        tvMyOrder.setTypeface(Constants.getTypeFace(getContext()));
        tvLogout.setTypeface(Constants.getTypeFace(getContext()));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.account_edit:
                Intent intent = new Intent(getContext(), EditProfileActivity.class);
                intent.putExtra("name", userPrefs.getString("name","-"));
                intent.putExtra("email", userPrefs.getString("email","-"));
                intent.putExtra("mobile", userPrefs.getString("mobile","-"));
                startActivityForResult(intent, EDIT_REQUEST);
                break;
            case R.id.account_change_password:
                startActivity(new Intent(getContext(), ChangePasswordActivity.class));
                break;
            case R.id.account_my_orders:
                startActivity(new Intent(getContext(), OrderHistory.class));
                break;
            case R.id.account_manage_address:
                startActivity(new Intent(getContext(), MyAddressActivity.class));
                break;
            case R.id.account_managecards:
                startActivity(new Intent(getContext(), ManageCards.class));
                break;
            case R.id.account_more:
                startActivity(new Intent(getContext(), MoreActivity.class));
                break;
            case R.id.account_offers:
                startActivity(new Intent(getContext(), OfferActivity.class));
                break;

            case R.id.account_logout:
                AlertDialog customDialog = null;
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
                // ...Irrelevant code for customizing the buttons and title
                LayoutInflater inflater = getLayoutInflater();
                int layout;
                if (language.equalsIgnoreCase("En")) {
                    layout = R.layout.alert_dialog;
                } else {
                    layout = R.layout.alert_dialog_arabic;
                }
                View dialogView = inflater.inflate(layout, null);
                dialogBuilder.setView(dialogView);
                dialogBuilder.setCancelable(false);
                TextView title = (TextView) dialogView.findViewById(R.id.title);
                TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                View vert = (View) dialogView.findViewById(R.id.vert_line);
                if (language.equalsIgnoreCase("En")) {
                    title.setText(getResources().getString(R.string.app_name));
                    desc.setText("Are You Sure to Logout ?");
                } else {
                    title.setText(getResources().getString(R.string.app_name_ar));
                    desc.setText("هل انت متأكد من تسجيل الخروج؟");
                }
                customDialog = dialogBuilder.create();
                customDialog.show();
                final AlertDialog finalCustomDialog = customDialog;
                yes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        userPrefsEditor.clear();
                        userPrefsEditor.commit();
                        Intent intent1 = new Intent(getContext(), MainActivity.class);
                        intent1.putExtra("class","splash");
                        startActivity(intent1);
                        getActivity().finish();
                        finalCustomDialog.dismiss();
                    }
                });
                final AlertDialog finalCustomDialog1 = customDialog;
                no.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        finalCustomDialog1.dismiss();
                    }
                });
                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                Window window = customDialog.getWindow();
                window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                lp.copyFrom(window.getAttributes());
                //This makes the dialog take up the full width
                Display display = getActivity().getWindowManager().getDefaultDisplay();
                Point size = new Point();
                display.getSize(size);
                int screenWidth = size.x;
                double d = screenWidth*0.85;
                lp.width = (int) d;
                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                window.setAttributes(lp);
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == EDIT_REQUEST && resultCode == RESULT_OK){
//            new DisplayProfileApi().execute();
            tvName.setText(userPrefs.getString("name","-"));
            tvEmail.setText(userPrefs.getString("email","-"));
            tvMobile.setText("+"+userPrefs.getString("mobile","-")+" - ");
        }
    }
}
