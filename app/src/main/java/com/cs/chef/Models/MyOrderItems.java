package com.cs.chef.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class MyOrderItems implements Serializable {


    @Expose
    @SerializedName("Data")
    private ArrayList<Data> Data;
    @Expose
    @SerializedName("MessageAr")
    private String MessageAr;
    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("Status")
    private boolean Status;

    public ArrayList<Data> getData() {
        return Data;
    }

    public void setData(ArrayList<Data> Data) {
        this.Data = Data;
    }

    public String getMessageAr() {
        return MessageAr;
    }

    public void setMessageAr(String MessageAr) {
        this.MessageAr = MessageAr;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public boolean getStatus() {
        return Status;
    }

    public void setStatus(boolean Status) {
        this.Status = Status;
    }

    public static class Data {
        @Expose
        @SerializedName("Rows")
        private int Rows;
        @Expose
        @SerializedName("OrderItems")
        private ArrayList<OrderItems> OrderItems;
        @Expose
        @SerializedName("OrderMain")
        private OrderMain OrderMain;

        public int getRows() {
            return Rows;
        }

        public void setRows(int Rows) {
            this.Rows = Rows;
        }

        public ArrayList<OrderItems> getOrderItems() {
            return OrderItems;
        }

        public void setOrderItems(ArrayList<OrderItems> OrderItems) {
            this.OrderItems = OrderItems;
        }

        public OrderMain getOrderMain() {
            return OrderMain;
        }

        public void setOrderMain(OrderMain OrderMain) {
            this.OrderMain = OrderMain;
        }
    }

    public static class OrderItems {
        @Expose
        @SerializedName("AdditionalItems")
        private ArrayList<AdditionalItems> AdditionalItems;
        @Expose
        @SerializedName("ItemSizeName_Ar")
        private String ItemSizeName_Ar;
        @Expose
        @SerializedName("ItemSizeName_En")
        private String ItemSizeName_En;
        @Expose
        @SerializedName("SizeId")
        private int SizeId;
        @Expose
        @SerializedName("Quantity")
        private int Quantity;
        @Expose
        @SerializedName("ItemPrice")
        private double ItemPrice;
        @Expose
        @SerializedName("ItemImage")
        private String ItemImage;
        @Expose
        @SerializedName("ItemName_En")
        private String ItemName_En;
        @Expose
        @SerializedName("ItemName_Ar")
        private String ItemName_Ar;
        @Expose
        @SerializedName("ItemId")
        private int ItemId;
        @Expose
        @SerializedName("OrderId")
        private int OrderId;
        @Expose
        @SerializedName("OItemId")
        private int OItemId;

        public ArrayList<AdditionalItems> getAdditionalItems() {
            return AdditionalItems;
        }

        public void setAdditionalItems(ArrayList<AdditionalItems> AdditionalItems) {
            this.AdditionalItems = AdditionalItems;
        }

        public String getItemSizeName_Ar() {
            return ItemSizeName_Ar;
        }

        public void setItemSizeName_Ar(String ItemSizeName_Ar) {
            this.ItemSizeName_Ar = ItemSizeName_Ar;
        }

        public String getItemSizeName_En() {
            return ItemSizeName_En;
        }

        public void setItemSizeName_En(String ItemSizeName_En) {
            this.ItemSizeName_En = ItemSizeName_En;
        }

        public int getSizeId() {
            return SizeId;
        }

        public void setSizeId(int SizeId) {
            this.SizeId = SizeId;
        }

        public int getQuantity() {
            return Quantity;
        }

        public void setQuantity(int Quantity) {
            this.Quantity = Quantity;
        }

        public double getItemPrice() {
            return ItemPrice;
        }

        public void setItemPrice(double ItemPrice) {
            this.ItemPrice = ItemPrice;
        }

        public String getItemImage() {
            return ItemImage;
        }

        public void setItemImage(String ItemImage) {
            this.ItemImage = ItemImage;
        }

        public String getItemName_En() {
            return ItemName_En;
        }

        public void setItemName_En(String ItemName_En) {
            this.ItemName_En = ItemName_En;
        }

        public String getItemName_Ar() {
            return ItemName_Ar;
        }

        public void setItemName_Ar(String ItemName_Ar) {
            this.ItemName_Ar = ItemName_Ar;
        }

        public int getItemId() {
            return ItemId;
        }

        public void setItemId(int ItemId) {
            this.ItemId = ItemId;
        }

        public int getOrderId() {
            return OrderId;
        }

        public void setOrderId(int OrderId) {
            this.OrderId = OrderId;
        }

        public int getOItemId() {
            return OItemId;
        }

        public void setOItemId(int OItemId) {
            this.OItemId = OItemId;
        }
    }

    public static class AdditionalItems {
        @Expose
        @SerializedName("AddQuantity")
        private int AddQuantity;
        @Expose
        @SerializedName("AddItemPrice")
        private double AddItemPrice;
        @Expose
        @SerializedName("AddItem_Ar")
        private String AddItem_Ar;
        @Expose
        @SerializedName("AddItem_En")
        private String AddItem_En;
        @Expose
        @SerializedName("AddItemId")
        private int AddItemId;

        public int getAddQuantity() {
            return AddQuantity;
        }

        public void setAddQuantity(int AddQuantity) {
            this.AddQuantity = AddQuantity;
        }

        public double getAddItemPrice() {
            return AddItemPrice;
        }

        public void setAddItemPrice(double AddItemPrice) {
            this.AddItemPrice = AddItemPrice;
        }

        public String getAddItem_Ar() {
            return AddItem_Ar;
        }

        public void setAddItem_Ar(String AddItem_Ar) {
            this.AddItem_Ar = AddItem_Ar;
        }

        public String getAddItem_En() {
            return AddItem_En;
        }

        public void setAddItem_En(String AddItem_En) {
            this.AddItem_En = AddItem_En;
        }

        public int getAddItemId() {
            return AddItemId;
        }

        public void setAddItemId(int AddItemId) {
            this.AddItemId = AddItemId;
        }
    }

    public static class OrderMain {
        @Expose
        @SerializedName("TotalItemsQty")
        private int TotalItemsQty;
        @Expose
        @SerializedName("TotalRows")
        private int TotalRows;
        @Expose
        @SerializedName("MobileNo")
        private String MobileNo;
        @Expose
        @SerializedName("EmailId")
        private String EmailId;
        @Expose
        @SerializedName("PrayerTime")
        private int PrayerTime;
        @Expose
        @SerializedName("KichenStartTime")
        private String KichenStartTime;
        @Expose
        @SerializedName("DeviceToken")
        private String DeviceToken;
        @Expose
        @SerializedName("OrderMode_En")
        private String OrderMode_En;
        @Expose
        @SerializedName("OrderType")
        private int OrderType;
        @Expose
        @SerializedName("TravelTime")
        private int TravelTime;
        @Expose
        @SerializedName("Version")
        private String Version;
        @Expose
        @SerializedName("RatingComment")
        private String RatingComment;
        @Expose
        @SerializedName("Rating")
        private int Rating;
        @Expose
        @SerializedName("IsFavourite")
        private boolean IsFavourite;
        @Expose
        @SerializedName("DeliveryCharges")
        private int DeliveryCharges;
        @Expose
        @SerializedName("PreparationTime")
        private int PreparationTime;
        @Expose
        @SerializedName("AddressId")
        private int AddressId;
        @Expose
        @SerializedName("ExpectedTime")
        private String ExpectedTime;
        @Expose
        @SerializedName("OrderStatus")
        private String OrderStatus;
        @Expose
        @SerializedName("OrderMode_Ar")
        private String OrderMode_Ar;
        @Expose
        @SerializedName("OrderDate")
        private String OrderDate;
        @Expose
        @SerializedName("InvoiceNo")
        private String InvoiceNo;
        @Expose
        @SerializedName("PaymentType")
        private int PaymentType;
        @Expose
        @SerializedName("PaymentMode")
        private String PaymentMode;
        @Expose
        @SerializedName("TotalPrice")
        private double TotalPrice;
        @Expose
        @SerializedName("VatPercentage")
        private double VatPercentage;
        @Expose
        @SerializedName("VatCharges")
        private double VatCharges;
        @Expose
        @SerializedName("SubTotal")
        private double SubTotal;
        @Expose
        @SerializedName("UserLongitude")
        private double UserLongitude;
        @Expose
        @SerializedName("UserLatitude")
        private double UserLatitude;
        @Expose
        @SerializedName("UserAddress")
        private String UserAddress;
        @Expose
        @SerializedName("UserName")
        private String UserName;
        @Expose
        @SerializedName("UserId")
        private int UserId;
        @Expose
        @SerializedName("BranchDiscountAmt")
        private int BranchDiscountAmt;
        @Expose
        @SerializedName("BrandImage")
        private String BrandImage;
        @Expose
        @SerializedName("BrandLogo")
        private String BrandLogo;
        @Expose
        @SerializedName("BrandName_En")
        private String BrandName_En;
        @Expose
        @SerializedName("BrandName_Ar")
        private String BrandName_Ar;
        @Expose
        @SerializedName("BrandId")
        private int BrandId;
        @Expose
        @SerializedName("BranchMobileNo")
        private String BranchMobileNo;
        @Expose
        @SerializedName("BranchLongitude")
        private double BranchLongitude;
        @Expose
        @SerializedName("BranchLatitude")
        private double BranchLatitude;
        @Expose
        @SerializedName("BranchAddress")
        private String BranchAddress;
        @Expose
        @SerializedName("BranchName_Ar")
        private String BranchName_Ar;
        @Expose
        @SerializedName("BranchName_En")
        private String BranchName_En;
        @Expose
        @SerializedName("BranchId")
        private int BranchId;
        @Expose
        @SerializedName("OrderId")
        private int OrderId;

        public int getTotalItemsQty() {
            return TotalItemsQty;
        }

        public void setTotalItemsQty(int TotalItemsQty) {
            this.TotalItemsQty = TotalItemsQty;
        }

        public int getTotalRows() {
            return TotalRows;
        }

        public void setTotalRows(int TotalRows) {
            this.TotalRows = TotalRows;
        }

        public String getMobileNo() {
            return MobileNo;
        }

        public void setMobileNo(String MobileNo) {
            this.MobileNo = MobileNo;
        }

        public String getEmailId() {
            return EmailId;
        }

        public void setEmailId(String EmailId) {
            this.EmailId = EmailId;
        }

        public int getPrayerTime() {
            return PrayerTime;
        }

        public void setPrayerTime(int PrayerTime) {
            this.PrayerTime = PrayerTime;
        }

        public String getKichenStartTime() {
            return KichenStartTime;
        }

        public void setKichenStartTime(String KichenStartTime) {
            this.KichenStartTime = KichenStartTime;
        }

        public String getDeviceToken() {
            return DeviceToken;
        }

        public void setDeviceToken(String DeviceToken) {
            this.DeviceToken = DeviceToken;
        }

        public String getOrderMode_En() {
            return OrderMode_En;
        }

        public void setOrderMode_En(String OrderMode_En) {
            this.OrderMode_En = OrderMode_En;
        }

        public int getOrderType() {
            return OrderType;
        }

        public void setOrderType(int OrderType) {
            this.OrderType = OrderType;
        }

        public int getTravelTime() {
            return TravelTime;
        }

        public void setTravelTime(int TravelTime) {
            this.TravelTime = TravelTime;
        }

        public String getVersion() {
            return Version;
        }

        public void setVersion(String Version) {
            this.Version = Version;
        }

        public String getRatingComment() {
            return RatingComment;
        }

        public void setRatingComment(String RatingComment) {
            this.RatingComment = RatingComment;
        }

        public int getRating() {
            return Rating;
        }

        public void setRating(int Rating) {
            this.Rating = Rating;
        }

        public boolean getIsFavourite() {
            return IsFavourite;
        }

        public void setIsFavourite(boolean IsFavourite) {
            this.IsFavourite = IsFavourite;
        }

        public int getDeliveryCharges() {
            return DeliveryCharges;
        }

        public void setDeliveryCharges(int DeliveryCharges) {
            this.DeliveryCharges = DeliveryCharges;
        }

        public int getPreparationTime() {
            return PreparationTime;
        }

        public void setPreparationTime(int PreparationTime) {
            this.PreparationTime = PreparationTime;
        }

        public int getAddressId() {
            return AddressId;
        }

        public void setAddressId(int AddressId) {
            this.AddressId = AddressId;
        }

        public String getExpectedTime() {
            return ExpectedTime;
        }

        public void setExpectedTime(String ExpectedTime) {
            this.ExpectedTime = ExpectedTime;
        }

        public String getOrderStatus() {
            return OrderStatus;
        }

        public void setOrderStatus(String OrderStatus) {
            this.OrderStatus = OrderStatus;
        }

        public String getOrderMode_Ar() {
            return OrderMode_Ar;
        }

        public void setOrderMode_Ar(String OrderMode_Ar) {
            this.OrderMode_Ar = OrderMode_Ar;
        }

        public String getOrderDate() {
            return OrderDate;
        }

        public void setOrderDate(String OrderDate) {
            this.OrderDate = OrderDate;
        }

        public String getInvoiceNo() {
            return InvoiceNo;
        }

        public void setInvoiceNo(String InvoiceNo) {
            this.InvoiceNo = InvoiceNo;
        }

        public int getPaymentType() {
            return PaymentType;
        }

        public void setPaymentType(int PaymentType) {
            this.PaymentType = PaymentType;
        }

        public String getPaymentMode() {
            return PaymentMode;
        }

        public void setPaymentMode(String PaymentMode) {
            this.PaymentMode = PaymentMode;
        }

        public double getTotalPrice() {
            return TotalPrice;
        }

        public void setTotalPrice(double TotalPrice) {
            this.TotalPrice = TotalPrice;
        }

        public double getVatPercentage() {
            return VatPercentage;
        }

        public void setVatPercentage(double VatPercentage) {
            this.VatPercentage = VatPercentage;
        }

        public double getVatCharges() {
            return VatCharges;
        }

        public void setVatCharges(double VatCharges) {
            this.VatCharges = VatCharges;
        }

        public double getSubTotal() {
            return SubTotal;
        }

        public void setSubTotal(double SubTotal) {
            this.SubTotal = SubTotal;
        }

        public double getUserLongitude() {
            return UserLongitude;
        }

        public void setUserLongitude(double UserLongitude) {
            this.UserLongitude = UserLongitude;
        }

        public double getUserLatitude() {
            return UserLatitude;
        }

        public void setUserLatitude(double UserLatitude) {
            this.UserLatitude = UserLatitude;
        }

        public String getUserAddress() {
            return UserAddress;
        }

        public void setUserAddress(String UserAddress) {
            this.UserAddress = UserAddress;
        }

        public String getUserName() {
            return UserName;
        }

        public void setUserName(String UserName) {
            this.UserName = UserName;
        }

        public int getUserId() {
            return UserId;
        }

        public void setUserId(int UserId) {
            this.UserId = UserId;
        }

        public int getBranchDiscountAmt() {
            return BranchDiscountAmt;
        }

        public void setBranchDiscountAmt(int BranchDiscountAmt) {
            this.BranchDiscountAmt = BranchDiscountAmt;
        }

        public String getBrandImage() {
            return BrandImage;
        }

        public void setBrandImage(String BrandImage) {
            this.BrandImage = BrandImage;
        }

        public String getBrandLogo() {
            return BrandLogo;
        }

        public void setBrandLogo(String BrandLogo) {
            this.BrandLogo = BrandLogo;
        }

        public String getBrandName_En() {
            return BrandName_En;
        }

        public void setBrandName_En(String BrandName_En) {
            this.BrandName_En = BrandName_En;
        }

        public String getBrandName_Ar() {
            return BrandName_Ar;
        }

        public void setBrandName_Ar(String BrandName_Ar) {
            this.BrandName_Ar = BrandName_Ar;
        }

        public int getBrandId() {
            return BrandId;
        }

        public void setBrandId(int BrandId) {
            this.BrandId = BrandId;
        }

        public String getBranchMobileNo() {
            return BranchMobileNo;
        }

        public void setBranchMobileNo(String BranchMobileNo) {
            this.BranchMobileNo = BranchMobileNo;
        }

        public double getBranchLongitude() {
            return BranchLongitude;
        }

        public void setBranchLongitude(double BranchLongitude) {
            this.BranchLongitude = BranchLongitude;
        }

        public double getBranchLatitude() {
            return BranchLatitude;
        }

        public void setBranchLatitude(double BranchLatitude) {
            this.BranchLatitude = BranchLatitude;
        }

        public String getBranchAddress() {
            return BranchAddress;
        }

        public void setBranchAddress(String BranchAddress) {
            this.BranchAddress = BranchAddress;
        }

        public String getBranchName_Ar() {
            return BranchName_Ar;
        }

        public void setBranchName_Ar(String BranchName_Ar) {
            this.BranchName_Ar = BranchName_Ar;
        }

        public String getBranchName_En() {
            return BranchName_En;
        }

        public void setBranchName_En(String BranchName_En) {
            this.BranchName_En = BranchName_En;
        }

        public int getBranchId() {
            return BranchId;
        }

        public void setBranchId(int BranchId) {
            this.BranchId = BranchId;
        }

        public int getOrderId() {
            return OrderId;
        }

        public void setOrderId(int OrderId) {
            this.OrderId = OrderId;
        }
    }
}
