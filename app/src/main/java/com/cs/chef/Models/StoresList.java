package com.cs.chef.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;


public class StoresList implements Serializable {

    @Expose
    @SerializedName("Data")
    private Data Data;
    @Expose
    @SerializedName("Message")
    private String Message;
    @SerializedName("MessageAr")
    private String MessageAr;
    @Expose
    @SerializedName("Status")
    private boolean Status;

    public Data getData() {
        return Data;
    }

    public void setData(Data Data) {
        this.Data = Data;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public String getMessageAr() {
        return MessageAr;
    }

    public void setMessageAr(String messageAr) {
        MessageAr = messageAr;
    }

    public boolean getStatus() {
        return Status;
    }

    public void setStatus(boolean Status) {
        this.Status = Status;
    }

    public static class Data implements Serializable  {
        @Expose
        @SerializedName("FilterCategory")
        private ArrayList<FilterCategory> FilterCategory;
        @Expose
        @SerializedName("BannerResult")
        private ArrayList<BannerResult> BannerResult;
        @Expose
        @SerializedName("StoreCategories")
        private ArrayList<StoreCategories> StoreCategories;
        @Expose
        @SerializedName("StoresDetails")
        private ArrayList<StoresDetails> StoresDetails;

        public ArrayList<FilterCategory> getFilterCategory() {
            return FilterCategory;
        }

        public void setFilterCategory(ArrayList<FilterCategory> FilterCategory) {
            this.FilterCategory = FilterCategory;
        }

        public ArrayList<BannerResult> getBannerResult() {
            return BannerResult;
        }

        public void setBannerResult(ArrayList<BannerResult> BannerResult) {
            this.BannerResult = BannerResult;
        }

        public ArrayList<StoreCategories> getStoreCategories() {
            return StoreCategories;
        }

        public void setStoreCategories(ArrayList<StoreCategories> StoreCategories) {
            this.StoreCategories = StoreCategories;
        }

        public ArrayList<StoresDetails> getStoresDetails() {
            return StoresDetails;
        }

        public void setStoresDetails(ArrayList<StoresDetails> StoresDetails) {
            this.StoresDetails = StoresDetails;
        }
    }

    public static class FilterCategory implements Serializable {
        @Expose
        @SerializedName("Filters")
        private ArrayList<Filters> Filters;
        @Expose
        @SerializedName("FilterTypeName_Ar")
        private String FilterTypeName_Ar;
        @Expose
        @SerializedName("FilterTypeName_En")
        private String FilterTypeName_En;
        @Expose
        @SerializedName("FilterTypeId")
        private int FilterTypeId;

        public ArrayList<Filters> getFilters() {
            return Filters;
        }

        public void setFilters(ArrayList<Filters> Filters) {
            this.Filters = Filters;
        }

        public String getFilterTypeName_Ar() {
            return FilterTypeName_Ar;
        }

        public void setFilterTypeName_Ar(String FilterTypeName_Ar) {
            this.FilterTypeName_Ar = FilterTypeName_Ar;
        }

        public String getFilterTypeName_En() {
            return FilterTypeName_En;
        }

        public void setFilterTypeName_En(String FilterTypeName_En) {
            this.FilterTypeName_En = FilterTypeName_En;
        }

        public int getFilterTypeId() {
            return FilterTypeId;
        }

        public void setFilterTypeId(int FilterTypeId) {
            this.FilterTypeId = FilterTypeId;
        }
    }

    public static class Filters implements Serializable  {
        @Expose
        @SerializedName("FilterName_Ar")
        private String FilterName_Ar;
        @Expose
        @SerializedName("FilterName_En")
        private String FilterName_En;
        @Expose
        @SerializedName("FilterId")
        private int FilterId;

        public String getFilterName_Ar() {
            return FilterName_Ar;
        }

        public void setFilterName_Ar(String FilterName_Ar) {
            this.FilterName_Ar = FilterName_Ar;
        }

        public String getFilterName_En() {
            return FilterName_En;
        }

        public void setFilterName_En(String FilterName_En) {
            this.FilterName_En = FilterName_En;
        }

        public int getFilterId() {
            return FilterId;
        }

        public void setFilterId(int FilterId) {
            this.FilterId = FilterId;
        }
    }

    public static class BannerResult implements Serializable  {
        @Expose
        @SerializedName("StoreDetails")
        private ArrayList<StoreDetails> StoreDetails;
        @Expose
        @SerializedName("BannerImage")
        private String BannerImage;
        @Expose
        @SerializedName("BannerName_Ar")
        private String BannerName_Ar;
        @Expose
        @SerializedName("BannerName_En")
        private String BannerName_En;

        public ArrayList<StoreDetails> getStoreDetails() {
            return StoreDetails;
        }

        public void setStoreDetails(ArrayList<StoreDetails> StoreDetails) {
            this.StoreDetails = StoreDetails;
        }

        public String getBannerImage() {
            return BannerImage;
        }

        public void setBannerImage(String BannerImage) {
            this.BannerImage = BannerImage;
        }

        public String getBannerName_Ar() {
            return BannerName_Ar;
        }

        public void setBannerName_Ar(String BannerName_Ar) {
            this.BannerName_Ar = BannerName_Ar;
        }

        public String getBannerName_En() {
            return BannerName_En;
        }

        public void setBannerName_En(String BannerName_En) {
            this.BannerName_En = BannerName_En;
        }
    }

    public static class StoreDetails implements Serializable  {
        @Expose
        @SerializedName("PriceCategory")
        private String PriceCategory;
        @Expose
        @SerializedName("DiscountAmt")
        private int DiscountAmt;
        @Expose
        @SerializedName("PaymentType")
        private String PaymentType;
        @Expose
        @SerializedName("StoreType")
        private String StoreType;
        @Expose
        @SerializedName("StoreType_Ar")
        private String StoreType_Ar;
        @Expose
        @SerializedName("OrderTypes")
        private String OrderTypes;
        @Expose
        @SerializedName("RatingCount")
        private int RatingCount;
        @Expose
        @SerializedName("PromotedBranch_Ar")
        private String PromotedBranch_Ar;
        @Expose
        @SerializedName("PromotedBranch_En")
        private String PromotedBranch_En;
        @Expose
        @SerializedName("IsNew")
        private boolean IsNew;
        @Expose
        @SerializedName("Popular")
        private boolean Popular;
        @Expose
        @SerializedName("IsPromoted")
        private boolean IsPromoted;
        @Expose
        @SerializedName("Serving")
        private int Serving;
        @Expose
        @SerializedName("FutureOrderDay")
        private int FutureOrderDay;
        @Expose
        @SerializedName("AvgPreparationTime")
        private int AvgPreparationTime;
        @Expose
        @SerializedName("DeliveryCharges")
        private int DeliveryCharges;
        @Expose
        @SerializedName("MinOrderCharges")
        private int MinOrderCharges;
        @Expose
        @SerializedName("Longitude")
        private double Longitude;
        @Expose
        @SerializedName("Latitude")
        private double Latitude;
        @Expose
        @SerializedName("Rating")
        private String Rating;
        @Expose
        @SerializedName("Distance")
        private double Distance;
        @Expose
        @SerializedName("StoreImage_En")
        private String StoreImage_En;
        @Expose
        @SerializedName("StoreLogo_En")
        private String StoreLogo_En;
        @Expose
        @SerializedName("StoreStatus")
        private String StoreStatus;
        @Expose
        @SerializedName("TakeAwayDistance")
        private int TakeAwayDistance;
        @Expose
        @SerializedName("DeliveryDistance")
        private int DeliveryDistance;
        @Expose
        @SerializedName("EmailId")
        private String EmailId;
        @Expose
        @SerializedName("MobileNo")
        private String MobileNo;
        @Expose
        @SerializedName("TelephoneNo")
        private String TelephoneNo;
        @Expose
        @SerializedName("Address")
        private String Address;
        @Expose
        @SerializedName("BranchCode")
        private String BranchCode;
        @Expose
        @SerializedName("CurrentDateTime")
        private String CurrentDateTime;
        @Expose
        @SerializedName("EndDateTime")
        private String EndDateTime;
        @Expose
        @SerializedName("StarDateTime")
        private String StarDateTime;
        @Expose
        @SerializedName("BranchDescription_Ar")
        private String BranchDescription_Ar;
        @Expose
        @SerializedName("BranchDescription_En")
        private String BranchDescription_En;
        @Expose
        @SerializedName("BrandName_Ar")
        private String BrandName_Ar;
        @Expose
        @SerializedName("BrandName_En")
        private String BrandName_En;
        @Expose
        @SerializedName("BrandId")
        private int BrandId;
        @Expose
        @SerializedName("BranchName_Ar")
        private String BranchName_Ar;
        @Expose
        @SerializedName("BranchName_En")
        private String BranchName_En;
        @Expose
        @SerializedName("BranchId")
        private int BranchId;
        @Expose
        @SerializedName("WeekNo")
        private int WeekNo;
        @Expose
        @SerializedName("Id")
        private int Id;

        public String getPriceCategory() {
            return PriceCategory;
        }

        public void setPriceCategory(String PriceCategory) {
            this.PriceCategory = PriceCategory;
        }

        public int getDiscountAmt() {
            return DiscountAmt;
        }

        public void setDiscountAmt(int DiscountAmt) {
            this.DiscountAmt = DiscountAmt;
        }

        public String getPaymentType() {
            return PaymentType;
        }

        public void setPaymentType(String PaymentType) {
            this.PaymentType = PaymentType;
        }

        public String getStoreType() {
            return StoreType;
        }

        public void setStoreType(String StoreType) {
            this.StoreType = StoreType;
        }


        public String getStoreType_Ar() {
            return StoreType_Ar;
        }

        public void setStoreType_Ar(String storeType_Ar) {
            StoreType_Ar = storeType_Ar;
        }

        public String getOrderTypes() {
            return OrderTypes;
        }

        public void setOrderTypes(String OrderTypes) {
            this.OrderTypes = OrderTypes;
        }

        public int getRatingCount() {
            return RatingCount;
        }

        public void setRatingCount(int RatingCount) {
            this.RatingCount = RatingCount;
        }

        public String getPromotedBranch_Ar() {
            return PromotedBranch_Ar;
        }

        public void setPromotedBranch_Ar(String PromotedBranch_Ar) {
            this.PromotedBranch_Ar = PromotedBranch_Ar;
        }

        public String getPromotedBranch_En() {
            return PromotedBranch_En;
        }

        public void setPromotedBranch_En(String PromotedBranch_En) {
            this.PromotedBranch_En = PromotedBranch_En;
        }

        public boolean getIsNew() {
            return IsNew;
        }

        public void setIsNew(boolean IsNew) {
            this.IsNew = IsNew;
        }

        public boolean getPopular() {
            return Popular;
        }

        public void setPopular(boolean Popular) {
            this.Popular = Popular;
        }

        public boolean getIsPromoted() {
            return IsPromoted;
        }

        public void setIsPromoted(boolean IsPromoted) {
            this.IsPromoted = IsPromoted;
        }

        public int getServing() {
            return Serving;
        }

        public void setServing(int Serving) {
            this.Serving = Serving;
        }

        public int getFutureOrderDay (){
            return FutureOrderDay;
        }

        public void setFutureOrderDay(int futureOrderDay) {
            this.FutureOrderDay = futureOrderDay;
        }

        public int getAvgPreparationTime() {
            return AvgPreparationTime;
        }

        public void setAvgPreparationTime(int AvgPreparationTime) {
            this.AvgPreparationTime = AvgPreparationTime;
        }

        public int getDeliveryCharges() {
            return DeliveryCharges;
        }

        public void setDeliveryCharges(int DeliveryCharges) {
            this.DeliveryCharges = DeliveryCharges;
        }

        public int getMinOrderCharges() {
            return MinOrderCharges;
        }

        public void setMinOrderCharges(int MinOrderCharges) {
            this.MinOrderCharges = MinOrderCharges;
        }

        public double getLongitude() {
            return Longitude;
        }

        public void setLongitude(double Longitude) {
            this.Longitude = Longitude;
        }

        public double getLatitude() {
            return Latitude;
        }

        public void setLatitude(double Latitude) {
            this.Latitude = Latitude;
        }

        public String getRating() {
            return Rating;
        }

        public void setRating(String Rating) {
            this.Rating = Rating;
        }

        public double getDistance() {
            return Distance;
        }

        public void setDistance(double Distance) {
            this.Distance = Distance;
        }

        public String getStoreImage_En() {
            return StoreImage_En;
        }

        public void setStoreImage_En(String StoreImage_En) {
            this.StoreImage_En = StoreImage_En;
        }

        public String getStoreLogo_En() {
            return StoreLogo_En;
        }

        public void setStoreLogo_En(String StoreLogo_En) {
            this.StoreLogo_En = StoreLogo_En;
        }

        public String getStoreStatus() {
            return StoreStatus;
        }

        public void setStoreStatus(String StoreStatus) {
            this.StoreStatus = StoreStatus;
        }

        public int getTakeAwayDistance() {
            return TakeAwayDistance;
        }

        public void setTakeAwayDistance(int TakeAwayDistance) {
            this.TakeAwayDistance = TakeAwayDistance;
        }

        public int getDeliveryDistance() {
            return DeliveryDistance;
        }

        public void setDeliveryDistance(int DeliveryDistance) {
            this.DeliveryDistance = DeliveryDistance;
        }

        public String getEmailId() {
            return EmailId;
        }

        public void setEmailId(String EmailId) {
            this.EmailId = EmailId;
        }

        public String getMobileNo() {
            return MobileNo;
        }

        public void setMobileNo(String MobileNo) {
            this.MobileNo = MobileNo;
        }

        public String getTelephoneNo() {
            return TelephoneNo;
        }

        public void setTelephoneNo(String TelephoneNo) {
            this.TelephoneNo = TelephoneNo;
        }

        public String getAddress() {
            return Address;
        }

        public void setAddress(String Address) {
            this.Address = Address;
        }

        public String getBranchCode() {
            return BranchCode;
        }

        public void setBranchCode(String BranchCode) {
            this.BranchCode = BranchCode;
        }

        public String getCurrentDateTime() {
            return CurrentDateTime;
        }

        public void setCurrentDateTime(String CurrentDateTime) {
            this.CurrentDateTime = CurrentDateTime;
        }

        public String getEndDateTime() {
            return EndDateTime;
        }

        public void setEndDateTime(String EndDateTime) {
            this.EndDateTime = EndDateTime;
        }

        public String getStarDateTime() {
            return StarDateTime;
        }

        public void setStarDateTime(String StarDateTime) {
            this.StarDateTime = StarDateTime;
        }

        public String getBranchDescription_Ar() {
            return BranchDescription_Ar;
        }

        public void setBranchDescription_Ar(String BranchDescription_Ar) {
            this.BranchDescription_Ar = BranchDescription_Ar;
        }

        public String getBranchDescription_En() {
            return BranchDescription_En;
        }

        public void setBranchDescription_En(String BranchDescription_En) {
            this.BranchDescription_En = BranchDescription_En;
        }

        public String getBrandName_Ar() {
            return BrandName_Ar;
        }

        public void setBrandName_Ar(String BrandName_Ar) {
            this.BrandName_Ar = BrandName_Ar;
        }

        public String getBrandName_En() {
            return BrandName_En;
        }

        public void setBrandName_En(String BrandName_En) {
            this.BrandName_En = BrandName_En;
        }

        public int getBrandId() {
            return BrandId;
        }

        public void setBrandId(int BrandId) {
            this.BrandId = BrandId;
        }

        public String getBranchName_Ar() {
            return BranchName_Ar;
        }

        public void setBranchName_Ar(String BranchName_Ar) {
            this.BranchName_Ar = BranchName_Ar;
        }

        public String getBranchName_En() {
            return BranchName_En;
        }

        public void setBranchName_En(String BranchName_En) {
            this.BranchName_En = BranchName_En;
        }

        public int getBranchId() {
            return BranchId;
        }

        public void setBranchId(int BranchId) {
            this.BranchId = BranchId;
        }

        public int getWeekNo() {
            return WeekNo;
        }

        public void setWeekNo(int WeekNo) {
            this.WeekNo = WeekNo;
        }

        public int getId() {
            return Id;
        }

        public void setId(int Id) {
            this.Id = Id;
        }
    }

    public static class StoreCategories implements Serializable  {
        @Expose
        @SerializedName("BranchId")
        private int BranchId;
        @Expose
        @SerializedName("FilterIds")
        private String FilterIds;
        @Expose
        @SerializedName("Longitude")
        private int Longitude;
        @Expose
        @SerializedName("Latitude")
        private int Latitude;
        @Expose
        @SerializedName("Image_Ar")
        private String Image_Ar;
        @Expose
        @SerializedName("Image_En")
        private String Image_En;
        @Expose
        @SerializedName("Description_Ar")
        private String Description_Ar;
        @Expose
        @SerializedName("Description_En")
        private String Description_En;
        @Expose
        @SerializedName("CatetgoryName_Ar")
        private String CatetgoryName_Ar;
        @Expose
        @SerializedName("CatetgoryName_En")
        private String CatetgoryName_En;
        @Expose
        @SerializedName("StoreCatId")
        private int StoreCatId;

        public int getBranchId() {
            return BranchId;
        }

        public void setBranchId(int BranchId) {
            this.BranchId = BranchId;
        }

        public String getFilterIds() {
            return FilterIds;
        }

        public void setFilterIds(String FilterIds) {
            this.FilterIds = FilterIds;
        }

        public int getLongitude() {
            return Longitude;
        }

        public void setLongitude(int Longitude) {
            this.Longitude = Longitude;
        }

        public int getLatitude() {
            return Latitude;
        }

        public void setLatitude(int Latitude) {
            this.Latitude = Latitude;
        }

        public String getImage_Ar() {
            return Image_Ar;
        }

        public void setImage_Ar(String Image_Ar) {
            this.Image_Ar = Image_Ar;
        }

        public String getImage_En() {
            return Image_En;
        }

        public void setImage_En(String Image_En) {
            this.Image_En = Image_En;
        }

        public String getDescription_Ar() {
            return Description_Ar;
        }

        public void setDescription_Ar(String Description_Ar) {
            this.Description_Ar = Description_Ar;
        }

        public String getDescription_En() {
            return Description_En;
        }

        public void setDescription_En(String Description_En) {
            this.Description_En = Description_En;
        }

        public String getCatetgoryName_Ar() {
            return CatetgoryName_Ar;
        }

        public void setCatetgoryName_Ar(String CatetgoryName_Ar) {
            this.CatetgoryName_Ar = CatetgoryName_Ar;
        }

        public String getCatetgoryName_En() {
            return CatetgoryName_En;
        }

        public void setCatetgoryName_En(String CatetgoryName_En) {
            this.CatetgoryName_En = CatetgoryName_En;
        }

        public int getStoreCatId() {
            return StoreCatId;
        }

        public void setStoreCatId(int StoreCatId) {
            this.StoreCatId = StoreCatId;
        }
    }

    public static class StoresDetails implements Serializable  {
        @Expose
        @SerializedName("PriceCategory")
        private String PriceCategory;
        @Expose
        @SerializedName("DiscountAmt")
        private int DiscountAmt;
        @Expose
        @SerializedName("PaymentType")
        private String PaymentType;
        @Expose
        @SerializedName("StoreType")
        private String StoreType;
        @Expose
        @SerializedName("StoreType_Ar")
        private String StoreType_Ar;
        @Expose
        @SerializedName("OrderTypes")
        private String OrderTypes;
        @Expose
        @SerializedName("RatingCount")
        private int RatingCount;
        @Expose
        @SerializedName("PromotedBranch_Ar")
        private String PromotedBranch_Ar;
        @Expose
        @SerializedName("PromotedBranch_En")
        private String PromotedBranch_En;
        @Expose
        @SerializedName("IsNew")
        private boolean IsNew;
        @Expose
        @SerializedName("Popular")
        private boolean Popular;
        @Expose
        @SerializedName("IsPromoted")
        private boolean IsPromoted;
        @Expose
        @SerializedName("Serving")
        private int Serving;
        @Expose
        @SerializedName("FutureOrderDay")
        private int FutureOrderDay;
        @Expose
        @SerializedName("AvgPreparationTime")
        private int AvgPreparationTime;
        @Expose
        @SerializedName("DeliveryCharges")
        private int DeliveryCharges;
        @Expose
        @SerializedName("MinOrderCharges")
        private int MinOrderCharges;
        @Expose
        @SerializedName("Longitude")
        private double Longitude;
        @Expose
        @SerializedName("Latitude")
        private double Latitude;
        @Expose
        @SerializedName("Rating")
        private String Rating;
        @Expose
        @SerializedName("Distance")
        private double Distance;
        @Expose
        @SerializedName("StoreImage_En")
        private String StoreImage_En;
        @Expose
        @SerializedName("StoreLogo_En")
        private String StoreLogo_En;
        @Expose
        @SerializedName("StoreStatus")
        private String StoreStatus;
        @Expose
        @SerializedName("TakeAwayDistance")
        private int TakeAwayDistance;
        @Expose
        @SerializedName("DeliveryDistance")
        private int DeliveryDistance;
        @Expose
        @SerializedName("EmailId")
        private String EmailId;
        @Expose
        @SerializedName("MobileNo")
        private String MobileNo;
        @Expose
        @SerializedName("TelephoneNo")
        private String TelephoneNo;
        @Expose
        @SerializedName("Address")
        private String Address;
        @Expose
        @SerializedName("BranchCode")
        private String BranchCode;
        @Expose
        @SerializedName("CurrentDateTime")
        private String CurrentDateTime;
        @Expose
        @SerializedName("EndDateTime")
        private String EndDateTime;
        @Expose
        @SerializedName("StarDateTime")
        private String StarDateTime;
        @Expose
        @SerializedName("BranchDescription_Ar")
        private String BranchDescription_Ar;
        @Expose
        @SerializedName("BranchDescription_En")
        private String BranchDescription_En;
        @Expose
        @SerializedName("BrandName_Ar")
        private String BrandName_Ar;
        @Expose
        @SerializedName("BrandName_En")
        private String BrandName_En;
        @Expose
        @SerializedName("BrandId")
        private int BrandId;
        @Expose
        @SerializedName("BranchName_Ar")
        private String BranchName_Ar;
        @Expose
        @SerializedName("BranchName_En")
        private String BranchName_En;
        @Expose
        @SerializedName("BranchId")
        private int BranchId;
        @Expose
        @SerializedName("WeekNo")
        private int WeekNo;
        @Expose
        @SerializedName("Id")
        private int Id;

        public String getPriceCategory() {
            return PriceCategory;
        }

        public void setPriceCategory(String PriceCategory) {
            this.PriceCategory = PriceCategory;
        }

        public int getDiscountAmt() {
            return DiscountAmt;
        }

        public void setDiscountAmt(int DiscountAmt) {
            this.DiscountAmt = DiscountAmt;
        }

        public String getPaymentType() {
            return PaymentType;
        }

        public void setPaymentType(String PaymentType) {
            this.PaymentType = PaymentType;
        }

        public String getStoreType() {
            return StoreType;
        }

        public void setStoreType(String StoreType) {
            this.StoreType = StoreType;
        }


        public String getStoreType_Ar() {
            return StoreType_Ar;
        }

        public void setStoreType_Ar(String storeType_Ar) {
            StoreType_Ar = storeType_Ar;
        }

        public String getOrderTypes() {
            return OrderTypes;
        }

        public void setOrderTypes(String OrderTypes) {
            this.OrderTypes = OrderTypes;
        }

        public int getRatingCount() {
            return RatingCount;
        }

        public void setRatingCount(int RatingCount) {
            this.RatingCount = RatingCount;
        }

        public String getPromotedBranch_Ar() {
            return PromotedBranch_Ar;
        }

        public void setPromotedBranch_Ar(String PromotedBranch_Ar) {
            this.PromotedBranch_Ar = PromotedBranch_Ar;
        }

        public String getPromotedBranch_En() {
            return PromotedBranch_En;
        }

        public void setPromotedBranch_En(String PromotedBranch_En) {
            this.PromotedBranch_En = PromotedBranch_En;
        }

        public boolean getIsNew() {
            return IsNew;
        }

        public void setIsNew(boolean IsNew) {
            this.IsNew = IsNew;
        }

        public boolean getPopular() {
            return Popular;
        }

        public void setPopular(boolean Popular) {
            this.Popular = Popular;
        }

        public boolean getIsPromoted() {
            return IsPromoted;
        }

        public void setIsPromoted(boolean IsPromoted) {
            this.IsPromoted = IsPromoted;
        }

        public int getServing() {
            return Serving;
        }

        public void setServing(int Serving) {
            this.Serving = Serving;
        }

        public int getFutureOrderDay (){
            return FutureOrderDay;
        }

        public void setFutureOrderDay(int futureOrderDay) {
            this.FutureOrderDay = futureOrderDay;
        }

        public int getAvgPreparationTime() {
            return AvgPreparationTime;
        }

        public void setAvgPreparationTime(int AvgPreparationTime) {
            this.AvgPreparationTime = AvgPreparationTime;
        }

        public int getDeliveryCharges() {
            return DeliveryCharges;
        }

        public void setDeliveryCharges(int DeliveryCharges) {
            this.DeliveryCharges = DeliveryCharges;
        }

        public int getMinOrderCharges() {
            return MinOrderCharges;
        }

        public void setMinOrderCharges(int MinOrderCharges) {
            this.MinOrderCharges = MinOrderCharges;
        }

        public double getLongitude() {
            return Longitude;
        }

        public void setLongitude(double Longitude) {
            this.Longitude = Longitude;
        }

        public double getLatitude() {
            return Latitude;
        }

        public void setLatitude(double Latitude) {
            this.Latitude = Latitude;
        }

        public String getRating() {
            return Rating;
        }

        public void setRating(String Rating) {
            this.Rating = Rating;
        }

        public double getDistance() {
            return Distance;
        }

        public void setDistance(double Distance) {
            this.Distance = Distance;
        }

        public String getStoreImage_En() {
            return StoreImage_En;
        }

        public void setStoreImage_En(String StoreImage_En) {
            this.StoreImage_En = StoreImage_En;
        }

        public String getStoreLogo_En() {
            return StoreLogo_En;
        }

        public void setStoreLogo_En(String StoreLogo_En) {
            this.StoreLogo_En = StoreLogo_En;
        }

        public String getStoreStatus() {
            return StoreStatus;
        }

        public void setStoreStatus(String StoreStatus) {
            this.StoreStatus = StoreStatus;
        }

        public int getTakeAwayDistance() {
            return TakeAwayDistance;
        }

        public void setTakeAwayDistance(int TakeAwayDistance) {
            this.TakeAwayDistance = TakeAwayDistance;
        }

        public int getDeliveryDistance() {
            return DeliveryDistance;
        }

        public void setDeliveryDistance(int DeliveryDistance) {
            this.DeliveryDistance = DeliveryDistance;
        }

        public String getEmailId() {
            return EmailId;
        }

        public void setEmailId(String EmailId) {
            this.EmailId = EmailId;
        }

        public String getMobileNo() {
            return MobileNo;
        }

        public void setMobileNo(String MobileNo) {
            this.MobileNo = MobileNo;
        }

        public String getTelephoneNo() {
            return TelephoneNo;
        }

        public void setTelephoneNo(String TelephoneNo) {
            this.TelephoneNo = TelephoneNo;
        }

        public String getAddress() {
            return Address;
        }

        public void setAddress(String Address) {
            this.Address = Address;
        }

        public String getBranchCode() {
            return BranchCode;
        }

        public void setBranchCode(String BranchCode) {
            this.BranchCode = BranchCode;
        }

        public String getCurrentDateTime() {
            return CurrentDateTime;
        }

        public void setCurrentDateTime(String CurrentDateTime) {
            this.CurrentDateTime = CurrentDateTime;
        }

        public String getEndDateTime() {
            return EndDateTime;
        }

        public void setEndDateTime(String EndDateTime) {
            this.EndDateTime = EndDateTime;
        }

        public String getStarDateTime() {
            return StarDateTime;
        }

        public void setStarDateTime(String StarDateTime) {
            this.StarDateTime = StarDateTime;
        }

        public String getBranchDescription_Ar() {
            return BranchDescription_Ar;
        }

        public void setBranchDescription_Ar(String BranchDescription_Ar) {
            this.BranchDescription_Ar = BranchDescription_Ar;
        }

        public String getBranchDescription_En() {
            return BranchDescription_En;
        }

        public void setBranchDescription_En(String BranchDescription_En) {
            this.BranchDescription_En = BranchDescription_En;
        }

        public String getBrandName_Ar() {
            return BrandName_Ar;
        }

        public void setBrandName_Ar(String BrandName_Ar) {
            this.BrandName_Ar = BrandName_Ar;
        }

        public String getBrandName_En() {
            return BrandName_En;
        }

        public void setBrandName_En(String BrandName_En) {
            this.BrandName_En = BrandName_En;
        }

        public int getBrandId() {
            return BrandId;
        }

        public void setBrandId(int BrandId) {
            this.BrandId = BrandId;
        }

        public String getBranchName_Ar() {
            return BranchName_Ar;
        }

        public void setBranchName_Ar(String BranchName_Ar) {
            this.BranchName_Ar = BranchName_Ar;
        }

        public String getBranchName_En() {
            return BranchName_En;
        }

        public void setBranchName_En(String BranchName_En) {
            this.BranchName_En = BranchName_En;
        }

        public int getBranchId() {
            return BranchId;
        }

        public void setBranchId(int BranchId) {
            this.BranchId = BranchId;
        }

        public int getWeekNo() {
            return WeekNo;
        }

        public void setWeekNo(int WeekNo) {
            this.WeekNo = WeekNo;
        }

        public int getId() {
            return Id;
        }

        public void setId(int Id) {
            this.Id = Id;
        }
    }
}
