package com.cs.chef.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Promotions {


    @Expose
    @SerializedName("Data")
    private ArrayList<Data> Data;
    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("Status")
    private boolean Status;

    public ArrayList<Data> getData() {
        return Data;
    }

    public void setData(ArrayList<Data> Data) {
        this.Data = Data;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public boolean getStatus() {
        return Status;
    }

    public void setStatus(boolean Status) {
        this.Status = Status;
    }

    public static class Data {
        @Expose
        @SerializedName("CategoryPromotion")
        private ArrayList<CategoryPromotion> CategoryPromotion;
        @Expose
        @SerializedName("StoreInfo")
        private ArrayList<StoreInfo> StoreInfo;
        @Expose
        @SerializedName("PromotionDays")
        private ArrayList<PromotionDays> PromotionDays;
        @Expose
        @SerializedName("DayOftheWeek")
        private String DayOftheWeek;
        @Expose
        @SerializedName("TodayAvailable")
        private boolean TodayAvailable;
        @Expose
        @SerializedName("ReferelId")
        private int ReferelId;
        @Expose
        @SerializedName("UserId")
        private int UserId;
        @Expose
        @SerializedName("ExpireDays")
        private int ExpireDays;
        @Expose
        @SerializedName("Msg_Ar")
        private String Msg_Ar;
        @Expose
        @SerializedName("Msg_En")
        private String Msg_En;
        @Expose
        @SerializedName("MinSpend")
        private int MinSpend;
        @Expose
        @SerializedName("MaxAmount")
        private int MaxAmount;
        @Expose
        @SerializedName("Desc_Ar")
        private String Desc_Ar;
        @Expose
        @SerializedName("Desc_En")
        private String Desc_En;
        @Expose
        @SerializedName("PromoTitle_Ar")
        private String PromoTitle_Ar;
        @Expose
        @SerializedName("PromoTitle_En")
        private String PromoTitle_En;
        @Expose
        @SerializedName("Image")
        private String Image;
        @Expose
        @SerializedName("DiscountAmt")
        private int DiscountAmt;
        @Expose
        @SerializedName("DiscountType")
        private int DiscountType;
        @Expose
        @SerializedName("PromotionCode")
        private String PromotionCode;
        @Expose
        @SerializedName("PromoName")
        private String PromoName;
        @Expose
        @SerializedName("PromoType")
        private int PromoType;
        @Expose
        @SerializedName("PromotionId")
        private int PromotionId;

        public ArrayList<CategoryPromotion> getCategoryPromotion() {
            return CategoryPromotion;
        }

        public void setCategoryPromotion(ArrayList<CategoryPromotion> CategoryPromotion) {
            this.CategoryPromotion = CategoryPromotion;
        }

        public ArrayList<StoreInfo> getStoreInfo() {
            return StoreInfo;
        }

        public void setStoreInfo(ArrayList<StoreInfo> StoreInfo) {
            this.StoreInfo = StoreInfo;
        }

        public ArrayList<PromotionDays> getPromotionDays() {
            return PromotionDays;
        }

        public void setPromotionDays(ArrayList<PromotionDays> PromotionDays) {
            this.PromotionDays = PromotionDays;
        }

        public String getDayOftheWeek() {
            return DayOftheWeek;
        }

        public void setDayOftheWeek(String DayOftheWeek) {
            this.DayOftheWeek = DayOftheWeek;
        }

        public boolean getTodayAvailable() {
            return TodayAvailable;
        }

        public void setTodayAvailable(boolean TodayAvailable) {
            this.TodayAvailable = TodayAvailable;
        }

        public int getReferelId() {
            return ReferelId;
        }

        public void setReferelId(int ReferelId) {
            this.ReferelId = ReferelId;
        }

        public int getUserId() {
            return UserId;
        }

        public void setUserId(int UserId) {
            this.UserId = UserId;
        }

        public int getExpireDays() {
            return ExpireDays;
        }

        public void setExpireDays(int ExpireDays) {
            this.ExpireDays = ExpireDays;
        }

        public String getMsg_Ar() {
            return Msg_Ar;
        }

        public void setMsg_Ar(String Msg_Ar) {
            this.Msg_Ar = Msg_Ar;
        }

        public String getMsg_En() {
            return Msg_En;
        }

        public void setMsg_En(String Msg_En) {
            this.Msg_En = Msg_En;
        }

        public int getMinSpend() {
            return MinSpend;
        }

        public void setMinSpend(int MinSpend) {
            this.MinSpend = MinSpend;
        }

        public int getMaxAmount() {
            return MaxAmount;
        }

        public void setMaxAmount(int MaxAmount) {
            this.MaxAmount = MaxAmount;
        }

        public String getDesc_Ar() {
            return Desc_Ar;
        }

        public void setDesc_Ar(String Desc_Ar) {
            this.Desc_Ar = Desc_Ar;
        }

        public String getDesc_En() {
            return Desc_En;
        }

        public void setDesc_En(String Desc_En) {
            this.Desc_En = Desc_En;
        }

        public String getPromoTitle_Ar() {
            return PromoTitle_Ar;
        }

        public void setPromoTitle_Ar(String PromoTitle_Ar) {
            this.PromoTitle_Ar = PromoTitle_Ar;
        }

        public String getPromoTitle_En() {
            return PromoTitle_En;
        }

        public void setPromoTitle_En(String PromoTitle_En) {
            this.PromoTitle_En = PromoTitle_En;
        }

        public String getImage() {
            return Image;
        }

        public void setImage(String Image) {
            this.Image = Image;
        }

        public int getDiscountAmt() {
            return DiscountAmt;
        }

        public void setDiscountAmt(int DiscountAmt) {
            this.DiscountAmt = DiscountAmt;
        }

        public int getDiscountType() {
            return DiscountType;
        }

        public void setDiscountType(int DiscountType) {
            this.DiscountType = DiscountType;
        }

        public String getPromotionCode() {
            return PromotionCode;
        }

        public void setPromotionCode(String PromotionCode) {
            this.PromotionCode = PromotionCode;
        }

        public String getPromoName() {
            return PromoName;
        }

        public void setPromoName(String PromoName) {
            this.PromoName = PromoName;
        }

        public int getPromoType() {
            return PromoType;
        }

        public void setPromoType(int PromoType) {
            this.PromoType = PromoType;
        }

        public int getPromotionId() {
            return PromotionId;
        }

        public void setPromotionId(int PromotionId) {
            this.PromotionId = PromotionId;
        }
    }

    public static class CategoryPromotion {
        @Expose
        @SerializedName("Items")
        private ArrayList<Items> Items;
        @Expose
        @SerializedName("CategoryImageAr")
        private String CategoryImageAr;
        @Expose
        @SerializedName("CategoryImageEn")
        private String CategoryImageEn;
        @Expose
        @SerializedName("CategoryNameAr")
        private String CategoryNameAr;
        @Expose
        @SerializedName("CategoryNameEn")
        private String CategoryNameEn;
        @Expose
        @SerializedName("Id")
        private int Id;

        public ArrayList<Items> getItems() {
            return Items;
        }

        public void setItems(ArrayList<Items> Items) {
            this.Items = Items;
        }

        public String getCategoryImageAr() {
            return CategoryImageAr;
        }

        public void setCategoryImageAr(String CategoryImageAr) {
            this.CategoryImageAr = CategoryImageAr;
        }

        public String getCategoryImageEn() {
            return CategoryImageEn;
        }

        public void setCategoryImageEn(String CategoryImageEn) {
            this.CategoryImageEn = CategoryImageEn;
        }

        public String getCategoryNameAr() {
            return CategoryNameAr;
        }

        public void setCategoryNameAr(String CategoryNameAr) {
            this.CategoryNameAr = CategoryNameAr;
        }

        public String getCategoryNameEn() {
            return CategoryNameEn;
        }

        public void setCategoryNameEn(String CategoryNameEn) {
            this.CategoryNameEn = CategoryNameEn;
        }

        public int getId() {
            return Id;
        }

        public void setId(int Id) {
            this.Id = Id;
        }
    }

    public static class Items {
        @Expose
        @SerializedName("ItemImage")
        private String ItemImage;
        @Expose
        @SerializedName("ItemNameAr")
        private String ItemNameAr;
        @Expose
        @SerializedName("ItemNameEn")
        private String ItemNameEn;
        @Expose
        @SerializedName("Id")
        private int Id;

        public String getItemImage() {
            return ItemImage;
        }

        public void setItemImage(String ItemImage) {
            this.ItemImage = ItemImage;
        }

        public String getItemNameAr() {
            return ItemNameAr;
        }

        public void setItemNameAr(String ItemNameAr) {
            this.ItemNameAr = ItemNameAr;
        }

        public String getItemNameEn() {
            return ItemNameEn;
        }

        public void setItemNameEn(String ItemNameEn) {
            this.ItemNameEn = ItemNameEn;
        }

        public int getId() {
            return Id;
        }

        public void setId(int Id) {
            this.Id = Id;
        }
    }

    public static class StoreInfo {
        @Expose
        @SerializedName("BranchNameAr")
        private String BranchNameAr;
        @Expose
        @SerializedName("BranchNameEn")
        private String BranchNameEn;
        @Expose
        @SerializedName("BranchId")
        private int BranchId;
        @Expose
        @SerializedName("BrandNameAr")
        private String BrandNameAr;
        @Expose
        @SerializedName("BrandNameEn")
        private String BrandNameEn;
        @Expose
        @SerializedName("BrandId")
        private int BrandId;

        public String getBranchNameAr() {
            return BranchNameAr;
        }

        public void setBranchNameAr(String BranchNameAr) {
            this.BranchNameAr = BranchNameAr;
        }

        public String getBranchNameEn() {
            return BranchNameEn;
        }

        public void setBranchNameEn(String BranchNameEn) {
            this.BranchNameEn = BranchNameEn;
        }

        public int getBranchId() {
            return BranchId;
        }

        public void setBranchId(int BranchId) {
            this.BranchId = BranchId;
        }

        public String getBrandNameAr() {
            return BrandNameAr;
        }

        public void setBrandNameAr(String BrandNameAr) {
            this.BrandNameAr = BrandNameAr;
        }

        public String getBrandNameEn() {
            return BrandNameEn;
        }

        public void setBrandNameEn(String BrandNameEn) {
            this.BrandNameEn = BrandNameEn;
        }

        public int getBrandId() {
            return BrandId;
        }

        public void setBrandId(int BrandId) {
            this.BrandId = BrandId;
        }
    }

    public static class PromotionDays {
        @Expose
        @SerializedName("DayName")
        private String DayName;

        public String getDayName() {
            return DayName;
        }

        public void setDayName(String DayName) {
            this.DayName = DayName;
        }
    }
}
