package com.cs.chef.Models;

/**
 * Created by SKT on 14-02-2016.
 */
public class Order {
    String orderId, itemId, itemTypeId, subCatId, additionals_nameEn, additionals_name_Ar, qty, price, additionalsPrice, additionalsTypeId, totalAmount, comment, status, creationDate, modifiedDate, categoryId, itemName, itemNameAr, image, item_desc, item_desc_Ar, sub_itemName, sub_itemName_Ar, sub_itemImage ,sub_itemId, sub_itemcount, ItemType, sizename_en, sizename_ar, discount, additionalitemQTY, branchName, address, brandName, brandName_Ar, orderType, brandId, latitute, longitute;
    String branchName_Ar, item_image, mainTotalAmount, dis_item_add_price, main_item_add_price, min_amount, max_days, price_with_disc, delivery_charges;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getItemTypeId() {
        return itemTypeId;
    }

    public void setItemTypeId(String itemTypeId) {
        this.itemTypeId = itemTypeId;
    }

    public String getSubCatId() {
        return subCatId;
    }

    public void setSubCatId(String subCatId) {
        this.subCatId = subCatId;
    }

    public String getAdditionals_nameEn() {
        return additionals_nameEn;
    }

    public void setAdditionals_nameEn(String additionals_nameEn) {
        this.additionals_nameEn = additionals_nameEn;
    }

    public String getAdditionals_name_Ar() {
        return additionals_name_Ar;
    }

    public void setAdditionals_name_Ar(String additionals_name_Ar) {
        this.additionals_name_Ar = additionals_name_Ar;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getAdditionalsPrice() {
        return additionalsPrice;
    }

    public void setAdditionalsPrice(String additionalsPrice) {
        this.additionalsPrice = additionalsPrice;
    }

    public String getAdditionalsTypeId() {
        return additionalsTypeId;
    }

    public void setAdditionalsTypeId(String additionalsTypeId) {
        this.additionalsTypeId = additionalsTypeId;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public String getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(String modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemNameAr() {
        return itemNameAr;
    }

    public void setItemNameAr(String itemNameAr) {
        this.itemNameAr = itemNameAr;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getItem_desc() {
        return item_desc;
    }

    public void setItem_desc(String item_desc) {
        this.item_desc = item_desc;
    }

    public String getItem_desc_Ar() {
        return item_desc_Ar;
    }

    public void setItem_desc_Ar(String item_desc_Ar) {
        this.item_desc_Ar = item_desc_Ar;
    }

    public String getSub_itemName() {
        return sub_itemName;
    }

    public void setSub_itemName(String sub_itemName) {
        this.sub_itemName = sub_itemName;
    }

    public String getSub_itemName_Ar() {
        return sub_itemName_Ar;
    }

    public void setSub_itemName_Ar(String sub_itemName_Ar) {
        this.sub_itemName_Ar = sub_itemName_Ar;
    }

    public String getSub_itemImage() {
        return sub_itemImage;
    }

    public void setSub_itemImage(String sub_itemImage) {
        this.sub_itemImage = sub_itemImage;
    }

    public String getSub_itemId() {
        return sub_itemId;
    }

    public void setSub_itemId(String sub_itemId) {
        this.sub_itemId = sub_itemId;
    }

    public String getSub_itemcount() {
        return sub_itemcount;
    }

    public void setSub_itemcount(String sub_itemcount) {
        this.sub_itemcount = sub_itemcount;
    }

    public String getItemType() {
        return ItemType;
    }

    public void setItemType(String itemType) {
        ItemType = itemType;
    }

    public String getSizename_en() {
        return sizename_en;
    }

    public void setSizename_en(String sizename_en) {
        this.sizename_en = sizename_en;
    }

    public String getSizename_ar() {
        return sizename_ar;
    }

    public void setSizename_ar(String sizename_ar) {
        this.sizename_ar = sizename_ar;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getAdditionalitemQTY() {
        return additionalitemQTY;
    }

    public void setAdditionalitemQTY(String additionalitemQTY) {
        this.additionalitemQTY = additionalitemQTY;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getBrandName_Ar() {
        return brandName_Ar;
    }

    public void setBrandName_Ar(String brandName_Ar) {
        this.brandName_Ar = brandName_Ar;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public String getBrandId() {
        return brandId;
    }

    public void setBrandId(String brandId) {
        this.brandId = brandId;
    }

    public String getLatitute() {
        return latitute;
    }

    public void setLatitute(String latitute) {
        this.latitute = latitute;
    }

    public String getLongitute() {
        return longitute;
    }

    public void setLongitute(String longitute) {
        this.longitute = longitute;
    }

    public String getBranchName_Ar() {
        return branchName_Ar;
    }

    public void setBranchName_Ar(String branchName_Ar) {
        this.branchName_Ar = branchName_Ar;
    }

    public String getItem_image() {
        return item_image;
    }

    public void setItem_image(String item_image) {
        this.item_image = item_image;
    }

    public String getMainTotalAmount() {
        return mainTotalAmount;
    }

    public void setMainTotalAmount(String mainTotalAmount) {
        this.mainTotalAmount = mainTotalAmount;
    }

    public String getDis_item_add_price() {
        return dis_item_add_price;
    }

    public void setDis_item_add_price(String dis_item_add_price) {
        this.dis_item_add_price = dis_item_add_price;
    }

    public String getMain_item_add_price() {
        return main_item_add_price;
    }

    public void setMain_item_add_price(String main_item_add_price) {
        this.main_item_add_price = main_item_add_price;
    }

    public String getMin_amount() {
        return min_amount;
    }

    public void setMin_amount(String min_amount) {
        this.min_amount = min_amount;
    }


    public String getMax_days() {
        return max_days;
    }

    public void setMax_days(String max_days) {
        this.max_days = max_days;
    }


    public String getPrice_with_disc() {
        return price_with_disc;
    }

    public void setPrice_with_disc(String price_with_disc) {
        this.price_with_disc = price_with_disc;
    }

    public String getDelivery_charges() {
        return delivery_charges;
    }

    public void setDelivery_charges(String delivery_charges) {
        this.delivery_charges = delivery_charges;
    }
}
