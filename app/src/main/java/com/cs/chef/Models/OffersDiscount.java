package com.cs.chef.Models;

public class OffersDiscount {

    String PromoTitle_En, PromoTitle_Ar, Desc_En, Desc_Ar;
    int DiscountType, min_spending, max_spending;
    float DiscountAmt, Discount_main;
    boolean todaysavailable;

    public String getPromoTitle_En() {
        return PromoTitle_En;
    }

    public void setPromoTitle_En(String promoTitle_En) {
        PromoTitle_En = promoTitle_En;
    }

    public String getPromoTitle_Ar() {
        return PromoTitle_Ar;
    }

    public void setPromoTitle_Ar(String promoTitle_Ar) {
        PromoTitle_Ar = promoTitle_Ar;
    }

    public String getDesc_En() {
        return Desc_En;
    }

    public void setDesc_En(String desc_En) {
        Desc_En = desc_En;
    }

    public String getDesc_Ar() {
        return Desc_Ar;
    }

    public void setDesc_Ar(String desc_Ar) {
        Desc_Ar = desc_Ar;
    }

    public int getDiscountType() {
        return DiscountType;
    }

    public void setDiscountType(int discountType) {
        DiscountType = discountType;
    }

    public float getDiscountAmt() {
        return DiscountAmt;
    }

    public void setDiscountAmt(float discountAmt) {
        DiscountAmt = discountAmt;
    }

    public float getDiscount_main() {
        return Discount_main;
    }

    public void setDiscount_main(float discount_main) {
        Discount_main = discount_main;
    }

    public boolean isTodaysavailable() {
        return todaysavailable;
    }

    public void setTodaysavailable(boolean todaysavailable) {
        this.todaysavailable = todaysavailable;
    }

    public int getMin_spending() {
        return min_spending;
    }

    public void setMin_spending(int min_spending) {
        this.min_spending = min_spending;
    }

    public int getMax_spending() {
        return max_spending;
    }

    public void setMax_spending(int max_spending) {
        this.max_spending = max_spending;
    }
}
