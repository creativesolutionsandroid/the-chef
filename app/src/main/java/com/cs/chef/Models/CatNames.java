package com.cs.chef.Models;

import java.io.Serializable;
import java.util.ArrayList;

public class CatNames implements Serializable {

    String catname_en, catname_ar;
    ArrayList<Items> itemsArrayList;

    public ArrayList<Items> getItemsArrayList() {
        return itemsArrayList;
    }

    public void setItemsArrayList(ArrayList<Items> itemsArrayList) {
        this.itemsArrayList= itemsArrayList;
    }

    public String getCatname_en() {
        return catname_en;
    }

    public void setCatname_en(String catname_en) {
        this.catname_en = catname_en;
    }

    public String getCatname_ar() {
        return catname_ar;
    }

    public void setCatname_ar(String catname_ar) {
        this.catname_ar = catname_ar;
    }
}
